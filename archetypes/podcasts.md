---
title: "{{ printf "Full Circle Weekly News %s" ( delimit ( last 1 ( split .Name "-" ) ) "" ) }}"
date: {{ time.Format "2006-01-02" .Date }}
draft: true
tags:
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "{{ printf "http://dl.fullcirclemagazine.org/podcast/Full%%20Circle%%20Weekly%%20News%%20%s.mp3" ( delimit ( last 1 ( split .Name "-" ) ) "" ) }}"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

**Features of the Podcast**

- [Title of the link](https://example.com)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
