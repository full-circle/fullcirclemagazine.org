---
title: "{{ replace .Name "-" " " | title }}"
date: {{ time.Format "2006-01-02" .Date }}
draft: true
tags:
  - "special"
  - "ubuntu"
cover: "covers/special/{{ .Name }}.webp"
---

**Features of the Special Edition**

Paragraph on the special edition.

- [English PDF](https://dl.fullcirclemagazine.org/{{ replace .Name "-" "" }}\_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/{{ replace .Name "-" "" }}\_en.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/{{ replace .Name "-" "" }}\_it.pdf)
