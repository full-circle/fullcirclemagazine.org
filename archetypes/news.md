---
title: "{{ replace .Name "-" " " | title }}"
date: {{ time.Format "2006-01-02" .Date }}
draft: true
tags:
  - "ubuntu"
cover: "covers/news/{{ .Name }}.webp"
---

**News**
