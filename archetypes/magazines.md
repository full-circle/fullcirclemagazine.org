---
title: "{{ printf "Full Circle Magazine %s" ( delimit ( last 1 ( split .Name "-" ) ) "" ) }}"
date: {{ time.Format "2006-01-02" .Date }}
draft: true
tags:
  - "ubuntu"
cover: "covers/magazines/{{ .Name }}.webp"
pdf: "https://dl.fullcirclemagazine.org/{{ replace .Name "-" "" }}_en.pdf"
epub: "https://dl.fullcirclemagazine.org/{{ replace .Name "-" "" }}_en.epub"
---

**Features of the Magazine**

- List of features
- Line by line

Some paragraph on _Plus_.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/{{ replace .Name "-" "" }}\_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/{{ replace .Name "-" "" }}\_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/{{ replace .Name "-" "" }}\_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/{{ replace .Name "-" "" }}\_ru.pdf)
