module.exports = {
  content: ["./content/**/*.md", "./content/**/*.html", "./layouts/**/*.html"],
  theme: {
    extend: {
      colors: {
        epub: "#85b916",
        "epub-active": "#85ca16",
        "fc-orange": "#df4c18",
        "fc-orange-active": "#ff4c18",
        "fc-brown": "#110000",
        "fc-brown-active": "#11111",
        mp3: "#5a9595",
        "mp3-active": "#5a95a6",
        patreon: "#FF424D",
        paypal: "#003e8d",
        pdf: "#ee1b0e",
        "pdf-active": "#ff1b0e",
        spotify: "#17d860",
        "spotify-active": "#29d860",
      },
    },
  },
  variants: {
    extend: {
      filter: ["dark"],
      invert: ["dark"],
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
