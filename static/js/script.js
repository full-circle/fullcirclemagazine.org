var doc = document.documentElement;
var w = window;

var prevScroll = w.scrollY || doc.scrollTop;
var curScroll;
var direction = 0;
var prevDirection = 0;

var header = document.getElementById("header");
var main = document.getElementsByTagName("main")[0];
var menu = document.getElementById("menu");
var sections = document.getElementById("sections");

menu.style.transform = "rotate(0deg)";

var checkScroll = function () {
  /*
   ** Find the direction of scroll
   ** 0 - initial, 1 - up, 2 - down
   */

  curScroll = w.scrollY || doc.scrollTop;
  if (curScroll > prevScroll) {
    //scrolled up
    direction = 2;
  } else if (curScroll < prevScroll) {
    //scrolled down
    direction = 1;
  }

  if (direction !== prevDirection) {
    toggleHeader(direction, curScroll);
  }

  prevScroll = curScroll;
};

var resetMenu = function () {
  if (menu.style.transform === "rotate(90deg)") {
    slideNavigation();
  }
};

var slideNavigation = function () {
  if (menu.style.transform === "rotate(0deg)") {
    menu.style.transform = "rotate(90deg)";
    sections.style.display = "block";
  } else {
    menu.style.transform = "rotate(0deg)";
    sections.style.display = "none";
  }
};

var toggleHeader = function (direction, curScroll) {
  if (direction === 2 && curScroll > 52) {
    header.style.top = `-${header.offsetHeight}px`;
    prevDirection = direction;
  } else if (direction === 1) {
    header.style.top = 0;
    prevDirection = direction;
  }
};

var resizeChecks = function () {
  if (window.innerWidth >= 1024) {
    sections.style.display = "block";
  } else {
    sections.style.display = "none";
  }
};

main.addEventListener("click", resetMenu);
menu.addEventListener("click", slideNavigation);
window.addEventListener("resize", resizeChecks);
window.addEventListener("scroll", checkScroll);
window.addEventListener("scroll", resetMenu);
