const search = document.getElementById("search");
const resultsDiv = document.getElementById("results");
const tresult = document.getElementById("tresult");

const indexURL = `/index.json`;
const fuseIndexURL = `/fuse-index.json`;

search.placeholder = "Something went wrong...";
const index = await fetch(indexURL).then((res) => res.json());
const fuseIndex = Fuse.parseIndex(
  await fetch(fuseIndexURL).then((res) => res.json())
);


const fuse = new Fuse(
  index,
  { keys: ["title", "summary", "tags"] },
  fuseIndex
);
search.disabled = false;
search.placeholder = "Search for posts...";

function createNode(result) {
  let resultNode = tresult.content.cloneNode(true);
  let nodes = resultNode.children[0];
  nodes.children[0].src = result.cover;
  nodes.children[0].alt = result.title;
  nodes.children[1].children[0].text = result.title;
  nodes.children[1].children[0].href = result.permalink;
  nodes.children[1].children[1].innerText = result.summary;
  return resultNode;
}

function searchForResults(event) {
  let results = fuse.search(search.value);
  resultsDiv.replaceChildren(
    ...results.map((result) => {
      if (!result) return;
      return createNode(result.item);
    })
  );
}

search.addEventListener("input", searchForResults);
