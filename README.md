# Full Circle

The website is written in [Hugo](https://gohugo.io), uses [Tailwind CSS](https://tailwindcss.com) for CSS and [Fuse.js](https://fusejs.io/) for search functionality.

## Build

To build the website for production, use the following lines of command.

```sh
$ npm install
$ npm run build
```

Currently, GitLab CI/CD is used to build and deploy the website. When a commit is pushed to the repository, GitLab builds the site and copies the files to DreamHost server via SFTP.

## Development

For testing the website in your computer, you can use the following commands.

```sh
$ npm run watch:tw
```

This watches for changes in CSS. Now open an other terminal or tab for Hugo.

```sh
$ npm run watch:hugo
```

Open the URL displayed in the screen and the website should be visible. If you make any changes to the files, they will be automatically loaded.

## Usage

Currently the website has magazines, podcasts, special editions and news. For every such kind, an archetype (i.e. template) is available for convenience. The following commands creates a new file in the appropriate folder of `/content`, that will have all the basic details filled. After entering the essential information, **change the value of draft to false** and open the website, the new page should be available.

```sh
# Create a new magazine
$ hugo new magazines/issue-256.md
# Create a new podcast
$ hugo new podcasts/podcast-300.md
# Create a new news article
$ hugo new news/ubuntu-launches-new-theme.md
# Create a new special edition
$ hugo new special/kdenlive.md
```

- You can change the templates by editing the files in `/archetypes` directory.
- Follow the conventional naming scheme of `issue-RELEASE.md` for magazines and `podcast-RELEASE.md` for podcasts (because all other magazines and podcasts of the website follow that rule).
- Use [WebP](https://developers.google.com/speed/webp) as format for pictures (because it has lower size and thus website loads faster). WebP should be available from your OS package manager.
- Don't forget to insert valid links for PDF/ePub/MP3/Spotify in the created `.md` file.
- Always test the new page locally before pushing the changes to production.

## Maintenance

Follow the releases of Hugo, TailwindCSS and Fuse.js to know about critical updates.
