import * as fs from "fs";
import Fuse from "fuse.js";

try {
  const index = JSON.parse(fs.readFileSync("./public/index.json"));
  const fuseIndex = Fuse.createIndex(["title", "summary", "tags"], index);
  fs.writeFileSync(
    "public/fuse-index.json",
    JSON.stringify(fuseIndex.toJSON())
  );
  console.log("Fuse index created successfully.");
} catch (err) {
  console.error(err);
  process.exit(1);
}
