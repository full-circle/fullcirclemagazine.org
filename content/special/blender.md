---
title: "Blender Special Edition"
date: 2014-06-26
draft: false
tags:
  - "blender"
  - "special"
cover: "covers/special/blender.webp"
---

We continue our assembly of articles with this new one, Blender – Volume 1. This is parts 01-10 in one PDF.

- [English PDF](https://dl.fullcirclemagazine.org/issueBL01_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issueBL01_en.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issueBL01_it.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issueBL01_it.epub)
