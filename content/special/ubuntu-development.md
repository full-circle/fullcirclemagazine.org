---
title: "Ubuntu Development Special Edition"
date: 2012-05-13
draft: false
tags:
  - "special"
  - "ubuntu"
cover: "covers/special/ubuntu-development.webp"
---

It’s another single-topic Special Edition; this time Daniel Holbach’s behind-the-scenes run-through of the Ubuntu Development Process.

- [English PDF](https://dl.fullcirclemagazine.org/issueUD01_en.pdf)
- [Italian](https://dl.fullcirclemagazine.org/issueUD01_it.pdf)
