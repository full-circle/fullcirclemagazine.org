---
title: "Python Special Editions"
date: 2017-03-05
draft: false
tags:
  - "python"
  - "special"
cover: "covers/special/python.webp"
---

**Volume 1**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY01_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issuePY01_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issuePY01_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issuePY01_it.pdf)

**Volume 2**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY02_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issuePY02_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issuePY02_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issuePY02_it.pdf)

**Volume 3**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY03_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issuePY03_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issuePY03_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issuePY03_it.pdf)

**Volume 4**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY04_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issuePY04_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issuePY04_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issuePY04_it.pdf)

**Volume 5**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY05_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issuePY05_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issuePY05_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issuePY05_it.pdf)

**Volume 6**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY06_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issuePY06_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issuePY06_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issuePY06_it.pdf)

**Volume 7**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY07_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issuePY07_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issuePY07_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issuePY07_it.pdf)

**Volume 8**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY08_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issuePY08_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issuePY08_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issuePY08_it.pdf)

**Volume 9**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY09_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issuePY09_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issuePY09_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issuePY09_it.pdf)

**Volume 10**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY10_en.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issuePY10_hu.pdf)

**Volume 11**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY11_en.pdf)

**Volume 12**

- [English PDF](https://dl.fullcirclemagazine.org/issuePY12_en.pdf)
