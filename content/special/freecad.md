---
title: "FreeCAD Special Edition"
date: 2018-02-19
draft: false
tags:
  - "freecad"
  - "special"
cover: "covers/special/freecad.webp"
---

_Jonathan Hoskin_ has compiled all ten FreeCAD articles (from _Alan Ward_) into a single Special Edition.

- [English PDF](https://dl.fullcirclemagazine.org/issueFC01_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issueFC01_en.epub)
