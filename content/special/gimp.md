---
title: "GIMP Special Edition"
date: 2015-05-11
draft: false
tags:
  - "gimp"
  - "special"
cover: "covers/special/gimp.webp"
---

**Volume 1**

- [English PDF](https://dl.fullcirclemagazine.org/issueGP01_en.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issueGP01_it.pdf)
