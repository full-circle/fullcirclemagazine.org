---
title: "Virtualisation Special Edition"
date: 2012-03-11
draft: false
tags:
  - "special"
  - "ubuntu"
  - "virtualbox"
  - "virtualisation"
cover: "covers/special/virtualisation.webp"
---

Presenting Lucas Westerman’s Virtualisation Series in one collected volume of parts 1 through 6.

- [English PDF](https://dl.fullcirclemagazine.org/issueVM01_en.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issueVM01_it.pdf)
