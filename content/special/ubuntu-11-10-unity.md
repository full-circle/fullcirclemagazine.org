---
title: "Ubuntu 11.10 and Unity Special Edition"
date: 2012-02-12
draft: false
tags:
  - "11-10"
  - "special"
  - "ubuntu"
  - "unity"
cover: "covers/special/ubuntu-11-10-unity.webp"
---

**Volume 1**

- [English PDF](https://dl.fullcirclemagazine.org/issueUU01_en.pdf)
- [French PDF](https://dl.fullcirclemagazine.org/issueUU01_fr.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issueUU01_it.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issueUU01_it.epub)
