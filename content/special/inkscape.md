---
title: "Inkscape Special Editions"
date: 2018-05-05
draft: false
tags:
  - "inkscape"
  - "special"
cover: covers/special/inkscape.webp
---

**Volume 1**

- [English PDF](https://dl.fullcirclemagazine.org/issueIS01_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issueIS01_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issueIS01_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issueIS01_it.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issueIS01_it.epub)

**Volume 2**

- [English PDF](https://dl.fullcirclemagazine.org/issueIS02_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issueIS02_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issueIS02_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issueIS02_it.pdf)

**Volume 3**

- [English PDF](https://dl.fullcirclemagazine.org/issueIS03_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issueIS03_en.epub)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issueIS03_hu.pdf)

**Volume 4**

- [English PDF](https://dl.fullcirclemagazine.org/issueIS04_en.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issueIS04_hu.pdf)

**Volume 5**

- [English PDF](https://dl.fullcirclemagazine.org/issueIS05_en.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issueIS05_hu.pdf)

**Volume 6**

- [English PDF](https://dl.fullcirclemagazine.org/issueIS06_en.pdf)

**Volume 7**

- [English PDF](https://dl.fullcirclemagazine.org/issueIS07_en.pdf)

**Volume 8**

- [English PDF](https://dl.fullcirclemagazine.org/issueIS08_en.pdf)

**Volume 9**

- [English PDF](https://dl.fullcirclemagazine.org/issueIS09_en.pdf)

**Volume 10**

- [English PDF](https://dl.fullcirclemagazine.org/issueIS10_en.pdf)
