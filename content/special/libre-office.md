---
title: "Libre Office Special Edition"
date: 2015-08-08
draft: false
tags:
  - "libreoffice"
  - "special"
cover: "covers/special/libre-office.webp"
---

This Special Edition is indeed special. Brian (he of the EPUBs) has created a LibreOffice PDF pulling together the first fifty articles into one bumper issue.

It’s a big ‘un though. It’s a 45MB download.

- [English PDF](https://dl.fullcirclemagazine.org/issueLOGOLD01_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issueLOGOLD01_en.epub)
