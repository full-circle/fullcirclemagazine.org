---
title: "Scribus Special Edition"
date: 2012-02-12
draft: true
tags:
  - "scribus"
  - "special"
  - "ubuntu"
cover: "covers/special/scribus.webp"
---

Many thanks to Brian for pulling together my Scribus articles from early FCM and, not only that, but adding some updated screenshots!

- [English PDF](https://dl.fullcirclemagazine.org/issueSC01_en.pdf)
- [English EPUB](https://dl.fullcirclemagazine.org/issueSC01_en.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issueSC01_it.pdf)
