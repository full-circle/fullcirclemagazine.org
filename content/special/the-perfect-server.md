---
title: "The Perfect Server Special Edition"
date: 2011-01-26
draft: false
tags:
  - "perfect"
  - "server"
  - "special"
  - "ubuntu"
cover: "covers/special/the-perfect-server.webp"
---

This is a special edition of Full Circle that is a direct reprint of the Perfect Server articles that were first published in FCM#31-#34.

- [English PDF](https://dl.fullcirclemagazine.org/issueSE01_en.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issueSE01_it.pdf)
