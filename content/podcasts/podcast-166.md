---
title: "Full Circle Weekly News 166"
date: 2020-03-29
draft: false
tags:
  - "arch"
  - "binutils"
  - "cinnamon"
  - "firefox"
  - "fsf"
  - "gnu"
  - "godot"
  - "google"
  - "gpu"
  - "kali"
  - "libreoffice"
  - "lighttpd"
  - "linux"
  - "lmde"
  - "mesa"
  - "mint"
  - "mpv"
  - "mzla"
  - "nd2h"
  - "nvidia"
  - "openmandriva"
  - "opensmtpd"
  - "opnsense"
  - "thunderbird"
  - "vulnerability"
  - "wifi"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20166.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Thunderbird Ownership Transferred to MZLA](https://blog.thunderbird.net/2020/01/thunderbirds-new-home/)

- [ND2H Wi-Fi Adapter Is Now FSF Certified](https://www.fsf.org/news/libiquity-wi-fri-nd2h-wi-fi-card-now-fsf-certified-to-respect-your-freedom)

- [Nvidia Dropping Support for Older Graphics Drivers](https://nvidia.custhelp.com/app/answers/detail/a_id/3142/)

- [Linux Mint Focuses on LMDE 4 and Cinnamon](https://blog.linuxmint.com/?p=3858)

- [Google Paid $6.5M in Vulnerability Rewards in 2019](https://security.googleblog.com/2020/01/vulnerability-reward-program-2019-year.html)

- [Firefox 72.0.2 Fixes Minor Regressions](https://usn.ubuntu.com/4234-2/)

- [Sudo Vulnerability Affecting Some Downstream Ubuntu Distributions](https://www.sudo.ws/alerts/pwfeedback.html)

- [OpenSMTPD Vulnerability](https://www.openwall.com/lists/oss-security/2020/01/28/3)

- [LibreOffice 6.4 Released](https://blog.documentfoundation.org/blog/2020/01/29/libreoffice-6-4/)

- [MPV 0.32 Released](https://github.com/mpv-player/mpv/releases/tag/v0.32.0)

- [OPNsense 20.1 Released](https://forum.opnsense.org/index.php?topic=15664.0)

- [Godot 3.2 Released](https://godotengine.org/article/here-comes-godot-3-2)

- [LightTPD 1.4.55 Released](http://blog.lighttpd.net/articles/2020/01/31/lighttpd-1.4.55-released/)

- [GNU Binutils 2.34 Released](https://www.mail-archive.com/info-gnu@gnu.org/msg02693.html)

- [OpenMandriva Lx 4.1 Released](https://www.openmandriva.org/en/news/article/and-openmandriva-did-better-omlx-4-1-final-release-is-out-now)

- [Kali 2020.1 Released](https://www.kali.org/releases/kali-linux-2020-1-release/)

- [Mesa 19.3.3 Released](https://mesa3d.org/relnotes/19.3.3.html)

- [Arch Linux's February Image Released](https://www.archlinux.org/releng/releases/2020.02.01/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
