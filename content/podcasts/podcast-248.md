---
title: "Full Circle Weekly News 248"
date: 2022-02-13
draft: false
tags:
  - "browser"
  - "composite"
  - "core"
  - "desktop"
  - "falkon"
  - "kde"
  - "libreoffice"
  - "minecraft"
  - "minetest"
  - "nitrux"
  - "rancher"
  - "rqlite"
  - "server"
  - "slackware"
  - "suse"
  - "trisquel"
  - "weston"
  - "wolvic"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20248.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [SUSE releases Rancher Desktop 1.0](https://www.suse.com/c/rancher_blog/rancher-desktop-1-0-0-has-arrived/)

- [rqlite 7.0, a distributed fault-tolerant DBMS](https://www.philipotoole.com/rqlite-7-0-designing-node-discovery-and-automatic-clustering/)

- [Release of Nitrux 2.0](https://nxos.org/changelog/release-announcement-nitrux-2-0-0/)

- [Minetest 5.5.0, an open source clone of MineCraft](https://forum.minetest.net/viewtopic.php?f=18&t=27754)

- [Falkon 3.2.0 browser developed by the KDE project](https://www.falkon.org/)

- [Release of Tiny Core Linux 13](http://forum.tinycorelinux.net/index.php/topic,25531.0.html)

- [Trisquel 10.0 Free Linux Distribution Available](http://trisquel.info/en/)

- [GNU screen 4.9.0 console window manager released](http://savannah.gnu.org/forum/forum.php?forum_id=10107)

- [Weston Composite Server 10.0 Released](https://lists.freedesktop.org/archives/wayland-devel/2022-February/042103.html)

- [LibreOffice 7.3 office suite released](https://blog.documentfoundation.org/blog/2022/02/02/libreoffice-73-community/)

- [Slackware 15.0 distribution released](http://www.slackware.com/releasenotes/15.0.php)

- [Igalia introduced Wolvic, a web browser for virtual reality devices](https://www.igalia.com/2022/02/03/Introducing-Wolvic.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
