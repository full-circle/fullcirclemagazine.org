---
title: "Full Circle Weekly News 374"
date: 2024-07-14
draft: false
tags:
  - "openbsd"
  - "ladybird"
  - "browser"
  - "mysql"
  - "fedora"
  - "apache"
  - "cozystack"
  - "freerdp"
  - "wireguard"
  - "gnupg"
  - "fooyin"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20374.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [OpenBSD removes dhclient utility in favor of a background process called dhcpleased:](https://marc.info/?l%3Dopenbsd-cvs%26m%3D171976865018013%26w%3D2)

- [Ladybird receives $1 million donation from GitHub co-founder:](http://ladybird.org/why-ladybird.html)

- [MySQL 9.0.0 DBMS available:](https://dev.mysql.com/downloads/mysql/)

- [Fedora 42 intends to implement telemetry:](https://www.mail-archive.com/devel-announce@lists.fedoraproject.org/msg03320.html)

- [Release of Apache 2.4.61:](https://www.mail-archive.com/announce@httpd.apache.org/msg00178.html)

- [Release of Cozystack 0.8.0:](https://t.me/aenix_io/110)

- [Release of FreeRDP 3.6:](https://www.freerdp.com/2024/07/04/3_6_2-release)

- [Wireguard Developer Seriously Speeds Up Getrandom() on Linux:](https://lore.kernel.org/lkml/20240703183115.1075219-1-Jason@zx2c4.com/)

- [Release of GnuPG 2.5.0:](https://lists.gnupg.org/pipermail/gnupg-users/2024-July/067193.html)

- [Fooyin 0.5 music player available:](https://github.com/fooyin/fooyin/releases)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
