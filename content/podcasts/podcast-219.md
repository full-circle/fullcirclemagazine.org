---
title: "Full Circle Weekly News 219"
date: 2021-07-20
draft: false
tags:
  - "amazon"
  - "binwalk"
  - "cbl"
  - "deck"
  - "distro"
  - "elasticsearch"
  - "emulator"
  - "eurolinux"
  - "firmware"
  - "kernel"
  - "labs"
  - "mariner"
  - "microsoft"
  - "mondodb"
  - "netfilter"
  - "nintendo"
  - "opensearch"
  - "origin"
  - "ota-18"
  - "ota18"
  - "pine64"
  - "pinetime"
  - "powerdns"
  - "refirm"
  - "RHEL"
  - "server"
  - "snes"
  - "solus"
  - "steam"
  - "touch"
  - "ublock"
  - "ubports"
  - "valve"
  - "watch"
  - "wine"
  - "zsnes"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20219.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Solus 4.3 released](https://getsol.us/2021/07/11/solus-4-3-released/)

- [Microsoft has published CBL-Mariner distribution](https://github.com/microsoft/CBL-Mariner/releases/tag/1.0-stable)

- [RHEL-compatible EuroLinux 8.3 released](https://en.euro-linux.com/eurolinux/eurolinux-8-3-release-notes-overview/)

- [Amazon publishes OpenSearch 1.0, a fork of the Elasticsearch platform](https://aws.amazon.com/blogs/opensource/opensearch-1-0-launches/)

- [PowerDNS Authoritative Server 4.5 Release](https://blog.powerdns.com/2021/07/13/powerdns-authoritative-server-4-5-0/)

- [MongoDB 5.0 available](https://www.mongodb.com/blog/post/launched-today-mongodb-5-0-serverless-atlas-evolution-application-data-platform)

- [Fork of zsnes, Super Nintendo emulator available](https://github.com/xyproto/zsnes/)

- [Eighteenth Ubuntu Touch Firmware Update](https://ubports.com/)

- [Vulnerability in Linux Netfilter Kernel Subsystem](https://google.github.io/security-research/pocs/linux/cve-2021-22555/writeup.html)

- [Wine Launcher 1.5.3 released](https://github.com/hitman249/wine-launcher/releases/tag/v1.5.3)

- [Valve Announces Arch Linux-Based Steam Deck](https://twitter.com/Steam/status/1415718021469925378)

- [Microsoft acquired ReFirm Labs, who developed the Binwalk firmware analysis utility](https://www.microsoft.com/security/blog/2021/06/02/microsoft-acquires-refirm-labs-to-enhance-iot-security/)

- [The Pine64 project has released a waterproof smartwatch PineTime](https://www.pine64.org/2021/07/15/july-update/)

- [Vulnerability in uBlock Origin leading to crash or resource exhaustion](https://github.com/vtriolet/writings/blob/main/posts/2021/ublock_origin_and_umatrix_denial_of_service.adoc)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
