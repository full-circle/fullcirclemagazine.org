---
title: "Full Circle Weekly News 91"
date: 2018-04-30
draft: false
tags:
  - "18-04"
  - "4-15"
  - "alpha"
  - "beaver"
  - "beta"
  - "bionic"
  - "eol"
  - "kde"
  - "kernel"
  - "librem"
  - "linux"
  - "lts"
  - "mobile"
  - "neural-network"
  - "onnx"
  - "plasma"
  - "pureos"
  - "testing"
  - "touch"
  - "ubports"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2091.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Kernel 4.15 Reached End of Life, Users Urged to Move to Linux 4.16 Now](http://news.softpedia.com/news/linux-kernel-4-15-reached-end-of-life-users-urged-to-move-to-linux-4-16-now-520787.shtml)

- [Linux developers: Kernel community will collapse under its own bureaucracy](https://www.heise.de/newsticker/meldung/Linux-Entwickler-Kernel-Community-wird-unter-eigener-Buerokratie-zusammenbrechen-4030460.html)

- [ONNX: the Open Neural Network Exchange Format](https://www.linuxjournal.com/content/onnx-open-neural-network-exchange-format)

- [Ubuntu 18.04 LTS (Bionic Beaver) Is Now Available to Download](https://news.softpedia.com/news/ubuntu-18-04-lts-bionic-beaver-is-now-available-to-download-520855.shtml)

- [Ubuntu Linux Replaces Alpha/Beta Release Model With “Testing Weeks”](https://fossbytes.com/ubuntu-linux-replace-alpha-beta-release-model-test-weeks/)

- [Librem 5 Linux smartphone will support Ubuntu Touch, PureOS, or PureOS with KDE Plasma Mobile](https://liliputing.com/2018/04/librem-5-linux-smartphone-will-support-ubuntu-touch-pureos-or-pureos-with-kde-plasma-mobile.html)
