---
title: "Full Circle Weekly News 47"
date: 2016-12-31
draft: false
tags:
  - "17-04"
  - "airprint"
  - "apple"
  - "browser"
  - "cyanogen"
  - "cyanogenmod"
  - "debian"
  - "desktop"
  - "everywhere"
  - "fedora"
  - "ipp"
  - "malware"
  - "os"
  - "printer"
  - "sanbox"
  - "sandboxed"
  - "tor"
  - "ubuntu"
  - "upgrades"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2047.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 17.04 to Support IPP Everywhere Printers and Apple AirPrint](http://news.softpedia.com/news/ubuntu-17-04-zesty-zapus-to-support-ipp-everywhere-printers-and-apple-airprint-511320.shtml)

- [Debian Is Considering “Automatic Upgrades”](https://fossbytes.com/debian-gnu-linux-considering-automatic-upgrades/)

- [0-days hitting Fedora and Ubuntu open desktops to a world of hurt](http://news.softpedia.com/news/merry-christmas-black-lab-linux-8-now-ready-for-download-based-on-ubuntu-16-04-511013.shtml)

- [New BlackArch Linux ISOs Roll Out with over 1600 Hacking Tools](http://news.softpedia.com/news/new-blackarch-linux-isos-roll-out-with-kernel-4-8-13-over-1600-hacking-tools-511155.shtml)

- [Sandboxed Tor Browser Is Here To Protect Your Anonymity](https://fossbytes.com/alpha-version-sandboxed-tor-browser-0-0-2-finally-protect-anonymity/)

- [New Linux Malware Spotted, Gives Hackers Full Control Over Device](http://www.mobipicker.com/new-linux-malware-spotted-gives-hackers-full-control-device-protect-embedded-device/)

- [Cyanogen failed to kill Android, now it is shuting its services and OS as part of a pivot](https://techcrunch.com/2016/12/24/cyanogen-failed-to-kill-android-now-it-is-shuttering-its-services-and-os-as-part-of-a-pivot/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
