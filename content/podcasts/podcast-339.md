---
title: "Full Circle Weekly News 339"
date: 2023-11-12
draft: false
tags:
  - "ubuntu"
  - "sway"
  - "ghostbsd"
  - "incus"
  - "vlc"
  - "midori"
  - "yggdrasil"
  - "audacity"
  - "awk"
  - "nitrux"
  - "openela"
  - "radix"
  - "exim"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20339.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Ubuntu Sway Remix 23.10:](https://github.com/Ubuntu-Sway/Ubuntu-Sway-Remix)

- [GhostBSD 23.10.1:](https://www.ghostbsd.org/news/GhostBSD_23.10.1_Now_Available)

- [Release of Incus 0.2:](https://stgraber.org/2023/10/30/announcing-incus-0-2/)

- [Release of VLC 3.0.20:](https://code.videolan.org/videolan/vlc/-/tags/3.0.20)

- [Midori 11 web browser based on the Floorp project:](https://astian.org/midori-en/explore-midori-11-0-faster-and-lighter-than-ever/)

- [Release of Pale Moon 32.5:](https://forum.palemoon.org/viewtopic.php?t=30485&p=245150#p245150)

- [Release of Yggdrasil 0.5:](https://yggdrasil-network.github.io/)

- [Audacity 3.4:](https://www.audacityteam.org/blog/audacity-3-4/)

- [New version of the GNU Awk 5.3 interpreter:](http://www.gnu.org/software/gawk)

- [Release of Nitrux 3.1:](https://nxos.org/changelog/release-announcement-nitrux-3-1-0/)

- [OpenELA repositories for RHEL-compatible distributions:](https://openela.org/news/2023.11.02-governance_and_code_availability/)

- [Release of Radix cross 1.9.212:](https://radix.pro/platform/install/)

- [New version of Exim 4.97 mail server:](https://lists.exim.org/lurker/message/20231104.135832.37148bbd.en.html)



**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
