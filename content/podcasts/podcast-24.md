---
title: "Full Circle Weekly News 24"
date: 2016-07-02
draft: false
tags:
  - "18-10"
  - "32-bit"
  - "canonical"
  - "cinnamon"
  - "ddos"
  - "devices"
  - "embedded"
  - "iot"
  - "kernel"
  - "mate"
  - "meizu"
  - "midori"
  - "mint"
  - "patch"
  - "patches"
  - "peppermint"
  - "released"
  - "shuttleworth"
  - "smartphone"
  - "store"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2024.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical Is Considering Dropping Support for 32-bit PCs After Ubuntu 18.10](http://news.softpedia.com/news/canonical-is-considering-dropping-support-for-32-bit-pcs-after-ubuntu-18-10-505761.shtml)

- [Canonical Patches Seven Linux Kernel Vulnerabilities in Ubuntu 16.04](http://news.softpedia.com/news/canonical-patches-seven-linux-kernel-vulnerabilities-in-ubuntu-16-04-update-now-505720.shtml)

- [New Ubuntu smartphone tipped in bug chats](http://www.slashgear.com/midori-ubuntu-smartphone-tipped-in-bug-chats-27445951/)

- [Mark Shuttleworth Doesn't Expect Other OSes to Fetch Snaps from the Ubuntu Store](http://news.softpedia.com/news/shuttleworth-doesn-t-expect-other-oses-to-want-to-fetch-snaps-from-ubuntu-store-505601.shtml)

- [Peppermint OS 7 Officially Released Based on Ubuntu 16.04 LTS](http://news.softpedia.com/news/peppermint-os-7-linux-distribution-officially-released-based-on-ubuntu-16-04-lts-505630.shtml)

- [Over 100 DDoS botnets built using Linux malware for embedded devices](http://www.pcworld.com/article/3090430/over-100-ddos-botnets-built-using-linux-malware-for-embedded-devices.html)

- [Linux Mint 18 Cinnamon and MATE Editions Officially Released](http://news.softpedia.com/news/linux-mint-18-sarah-cinnamon-and-mate-editions-officially-released-505847.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
