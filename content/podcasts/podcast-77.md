---
title: "Full Circle Weekly News 77"
date: 2018-01-20
draft: false
tags:
  - "17-10"
  - "aron"
  - "attack"
  - "barcelona"
  - "boot"
  - "iso"
  - "meltdown"
  - "pycryptominder"
  - "respin"
  - "tails"
  - "wifi"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2077.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [PyCryptoMiner Attacks Linux Machines And Turns Them Into Monero-mining Bots](https://fossbytes.com/pycryptominer-linux-machines-turns-monero-mining-bots/)

- [Linux vs Meltdown](http://www.zdnet.com/article/the-linux-vs-meltdown-and-spectre-battle-continues/)

- [Tails 3.4](http://news.softpedia.com/news/tails-3-4-anonymous-live-system-released-with-meltdown-and-spectre-patches-519302.shtml)

- [Ubuntu Boot Issue](http://www.zdnet.com/article/linux-vs-meltdown-ubuntu-gets-second-update-after-first-one-fails-to-boot/)

- [Ubuntu 17.10 ISO Respins](http://news.softpedia.com/news/canonical-plans-to-release-ubuntu-17-10-respins-for-all-flavors-early-next-week-519258.shtml)

- [City Of Barcelona Chooses Linux And Free Software After Ditching Microsoft](https://fossbytes.com/city-barcelona-linux-open-source/)

- [No Internet? No Problem — “ARON” Is A Futuristic And Free Wi-Fi Alternative That Uses IR](https://fossbytes.com/aron-free-wi-fi-alternative-surefire/)
