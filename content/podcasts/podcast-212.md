---
title: "Full Circle Weekly News 212"
date: 2021-05-31
draft: false
tags:
  - "almalinux"
  - "annotation"
  - "antivirus"
  - "antix"
  - "archinstall"
  - "centos"
  - "debian"
  - "distro"
  - "glimpse"
  - "gnome"
  - "haveibeenpwned"
  - "inkscape"
  - "ksnip"
  - "linux"
  - "material"
  - "music"
  - "nftables"
  - "oracle"
  - "osgeo"
  - "pacman"
  - "player"
  - "qmmp"
  - "screenshot"
  - "shell"
  - "systemd"
  - "virtuozzo"
  - "vzlinux"
  - "widgets"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20212.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [AV Linux 2021.05.22](https://www.bandshed.net/2021/05/22/avl-mxe-2021-05-22-iso-update/)

- [Qmmp Music Player 1.5.0 Released](http://qmmp.ylsoftware.com/index.php)

- [Inkscape 1.1 released](https://inkscape.org/)

- [OSGeo-Live 14.0 is out](https://www.osgeo.org/foundation-news/osgeolive-14-0-malena-released/)

- [Release of nftables 0.9.9](https://www.mail-archive.com/netfilter-announce@lists.netfilter.org/msg00230.html)

- [So, Glimpse died…](https://glimpse-editor.org/posts/a-project-on-hiatus/)

- [Oracle Linux 8.4 Distribution Released](https://blogs.oracle.com/linux/announcing-the-release-of-oracle-linux-8-update-4)

- [AlmaLinux 8.4, continuing development of CentOS 8](https://almalinux.org/blog/almalinux-os-8-4-stable-now-available/)

- [Systemd-free antiX 19.4 Release Brings Latest Debian Packages and Updates](https://www.debugpoint.com/2021/05/antix-19-4-release/)

- [Screenshot And Annotation Tool Ksnip 1.9.0 Adds User-Defined Actions, Dockable Widgets And Other Improvements](https://www.linuxuprising.com/2021/05/screenshot-and-annotation-tool-ksnip.html)

- [Material Shell Gets GNOME Shell 40 Support, New Overview And Gtk4 Settings](https://www.linuxuprising.com/2021/05/material-shell-gets-gnome-shell-40.html)

- [Nitrux 1.4.1 Released with Plasma System Monitor, Heroic Games Launcher, and Pacstall](https://9to5linux.com/nitrux-1-4-1-released-with-plasma-system-monitor-heroic-games-launcher-and-pacstall)

- [Kali Linux team releases Kaboxer, a tool for managing applications in containers](https://www.helpnetsecurity.com/2021/05/27/kali-linux-team-releases-kaboxer/)

- [Virtuozzo publishes VzLinux distribution aimed at replacing CentOS 8](https://www.virtuozzo.com/connect/details/blog/view/virtuozzos-mature-linux-distribution-vzlinux-now-available-to-public.html)

- [The code of the HaveIBeenPwned password checker service is open](https://www.troyhunt.com/pwned-passwords-open-source-in-the-dot-net-foundation-and-working-with-the-fbi/)

- [Pacman 6.0 package manager and Archinstall 2.2.0 installer released](https://git.archlinux.org/pacman.git/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
