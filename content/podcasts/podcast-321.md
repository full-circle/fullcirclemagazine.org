---
title: "Full Circle Weekly News 321"
date: 2023-07-08
draft: false
tags:
  - "kernel"
  - "libreboot"
  - "workstation"
  - "inkbox"
  - "blender"
  - "proxmox"
  - "backup"
  - "oracle"
  - "udisks"
  - "nvme"
  - "rhel"
  - "redhat"
  - "xonotic"
  - "nitrux"
  - "i2p"
  - "darktable"
  - "peppermint"
  - "unsnap"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20321.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux 6.4 kernel released:](https://lkml.org/lkml/2023/6/25/453)

- [Release of Libreboot 20230625:](https://libreboot.org/news/libreboot20230625.html)

- [Fedora Workstation 39 new installer:](https://www.mail-archive.com/devel-announce@lists.fedoraproject.org/msg03077.html)

- [InkBox OS 2.0:](https://www.reddit.com/r/linux/comments/14i4e29/inkbox_os_20_opensource_os_for_ereaders_is_out/)

- [Release of Blender 3.6:](https://www.blender.org/download/releases/3-6/)

- [Release of Proxmox Backup Server 3.0:](https://forum.proxmox.com/threads/proxmox-backup-server-3-0-available.129716/)

- [Oracle automatic optimization:](https://blogs.oracle.com/linux/post/introducing-bpftune)

- [UDisks 2.10.0 with NVMe support:](https://github.com/storaged-project/udisks/releases/tag/udisks-2.10.0)

- [An additional 4 years for RHEL 7:](https://www.redhat.com/en/blog/announcing-4-years-extended-life-cycle-support-els-red-hat-enterprise-linux-7)

- [Release of Xonotic 0.8.6:](https://xonotic.org/posts/2023/xonotic-0-8-6-release/)

- [Release of  Nitrux 2.9:](https://nxos.org/changelog/release-announcement-nitrux-2-9-0/)

- [Release of I2P 2.3.0:](https://geti2p.net/en/blog/post/2023/06/25/new_release_2.3.0)

- [Darktable 4.4:](https://www.darktable.org/2023/06/darktable-4.4.0-released/)

- [Peppermint OS based on Debian:](https://peppermintos.com/2023/07/peppermint-os-debian-release/)

- [unsnap - tools for migration of Ubuntu from Snap to Flatpak:](https://github.com/popey/unsnap)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
