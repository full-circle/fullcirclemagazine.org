---
title: "Full Circle Weekly News 332"
date: 2023-09-24
draft: false
tags:
  - "servo"
  - "mojo"
  - "libreoffice"
  - "fheroes2"
  - "opensuse"
  - "syslinuxos"
  - "rclone"
  - "fedora"
  - "wine"
  - "dentos"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20332.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Development of Servo transferred to the Linux Foundation Europe:](https://servo.org/)

- [The Mojo programming language](https://www.modular.com/blog/mojo-its-finally-here)

- [Over 1.5 million copies of LibreOffice 7.6 downloaded in two weeks:](https://blog.documentfoundation.org/blog/2023/09/11/1-5-million-downloads-of-libreoffice-7-6/)

- [Release of fheroes2 - 1.0.8:](https://github.com/ihhub/fheroes2/releases/tag/1.0.8)

- [An openSUSE Slowroll distribution to replace openSUSE Leap:](https://en.opensuse.org/openSUSE)

- [Release of SysLinuxOS 12.1:](https://syslinuxos.com/syslinuxos-12-1-released/)

- [Release of rclone 1.64:](https://forum.rclone.org/t/rclone-v1-64-0-release/41649)

- [Fedora 40 plans to stop X11 support in KDE:](https://lists.fedoraproject.org/archives/list/devel-announce@lists.fedoraproject.org/thread/I3O6DZCT6WFGF5SQ2IPJSYAVBGZ3N47U/)

- [Ubuntu 23.10 and ZFS:](https://github.com/canonical/ubuntu-desktop-installer/issues/2312)

- [Free Download Manager backdoor:](https://www.freedownloadmanager.org/blog/?p=664)

- [Release of PostgreSQL 16 DBMS:](https://www.postgresql.org/about/news/postgresql-16-released-2715/)

- [Release of LKRG 0.9.7:](https://www.mail-archive.com/announce@lists.openwall.com/msg00193.html)

- [Release Wine 8.16:](https://www.winehq.org/announce/8.16)

- [DentOS 3.2 network operating system:](https://github.com/dentproject/dentOS/releases/tag/v3.2)

- [Libadwaita 1.4 library:](https://blogs.gnome.org/alicem/2023/09/15/libadwaita-1-4/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
