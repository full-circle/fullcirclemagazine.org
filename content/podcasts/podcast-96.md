---
title: "Full Circle Weekly News 96"
date: 2018-06-11
draft: false
tags:
  - "3-2"
  - "4-1"
  - "5-0"
  - "alexa"
  - "amazon"
  - "bug"
  - "canonical"
  - "chrome"
  - "dell"
  - "emacs"
  - "facebook"
  - "firefox"
  - "github"
  - "gitlab"
  - "gnome"
  - "gnu"
  - "kubernetes"
  - "librem"
  - "linux"
  - "mate"
  - "microsoft"
  - "mint"
  - "precision"
  - "purism"
  - "rhel"
  - "tara"
  - "torvalds"
  - "xfce"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2096.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

A bumper episode for you this week!

- [You know that silly fear about Alexa recording everything and leaking it online? It just happened](https://www.theregister.co.uk/2018/05/24/alexa_recording_couple/)

- [GNU Emacs 26.1 released](http://www.pro-linux.de/news/1/25935/gnu-emacs-261-freigegeben.html)

- [Firefox And Chrome Bug Leaked Facebook Profile Details For Almost A Year](https://fossbytes.com/firefox-chrome-side-channel-attack-leaked-facebook-profile-details/)

- [Support for Linux 3.2 and 4.1 finished](https://www.pro-linux.de/news/1/25965/unterst%C3%BCtzung-f%C3%BCr-)linux-32-und-41-beendet.html

- [Linus Torvalds decides world isn’t ready for Linux 5.0](https://www.theregister.co.uk/2018/06/04/linux_4_17_released/)

- [Dell Precision 'Developer Edition' mobile workstations run Ubuntu Linux and are RHEL certified](https://betanews.com/2018/05/27/dell-precision-developer-ubuntu-linux-rhel/)

- [Ubuntu 18.04-based Linux Mint 19 'Tara' Beta is here with Cinnamon, MATE, and Xfce](https://betanews.com/2018/06/04/linux-mint-19-tara-beta/)

- [Canonical Announces Ubuntu for Amazon’s Elastic Container Service for Kubernetes](https://news.softpedia.com/news/canonical-announces-ubuntu-for-amazon-s-elastic-container-service-for-kubernetes-521446.shtml)

- [Purism's Librem 5 Privacy, Security-Focused Linux Phone Arrives in January 2019](https://news.softpedia.com/news/purism-s-librem-5-privacy-security-focused-linux-phone-arrives-in-january-2019-521437.shtml)

- [Microsoft Buys Github](https://www.bloomberg.com/news/articles/2018-06-04/microsoft-agrees-to-buy-coding-site-github-for-7-5-billion)

- [GitLab Ultimate and GitLab Gold for open source projects for free](http://www.pro-linux.de/news/1/25968/gitlab-ultimate-und-gitlab-gold-f%C3%BCr-open-source-projekte-kostenlos.html)

- [GNOME transitions to GitLab](https://betanews.com/2018/06/01/gnome-transitions-to-gitlab/)
