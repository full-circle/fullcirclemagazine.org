---
title: "Full Circle Weekly News 324"
date: 2023-07-30
draft: false
tags:
  - "cmake"
  - "debian"
  - "slackware"
  - "voice"
  - "mozilla"
  - "libreboot"
  - "firmware"
  - "lazyvim"
  - "composefs"
  - "overlayfs"
  - "mysql"
  - "virtualbox"
  - "suricata"
  - "mir"
  - "zeek"
  - "whonix"
  - "creator"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20324.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Slackware Linux is 30 years old:](ftp://ftp.osuosl.org/pub/slackware/slackware64-current/ChangeLog.txt)

- [Voice Update for Mozilla Common Voice 14.0:](https://commonvoice.mozilla.org/)

- [GNU Boot, Libreboot boot firmware:](https://libreboot.org/news/gnuboot.html)

- [LazyVim 5:](https://github.com/LazyVim/LazyVim/releases/tag/v5.0.0)

- [Composefs on top of OverlayFS and EROFS:](https://github.com/containers/composefs/releases/tag/v0.1.4)

- [MySQL 8.1.0:](https://blogs.oracle.com/mysql/post/introducing-mysql-innovation-and-longterm-support-lts-versions)

- [VirtualBox 7.0.10:](https://www.virtualbox.org/)

- [Release of Suricata 7.0:](https://suricata.io/2023/07/18/suricata-7-0-0-released/)

- [Release of Mir 2.14:](https://discourse.ubuntu.com/t/mir-release-2-14-0/37082)

- [Release of the traffic analyzer Zeek 6.0.0:](https://zeek.org/2023/07/13/introducing-zeek-6/)

- [Release of Qt Creator 11:](https://www.qt.io/blog/qt-creator-11-released)

- [Whonix 17:](https://forums.whonix.org/t/whonix-17-has-been-released-debian-12-bookworm-based-major-release/16922)

- [Update of Debian 12.1:](https://www.debian.org/News/2023/20230722)

- [CMake 3.27.0 build system:](https://www.kitware.com/cmake-3-27-0-available-for-download/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
