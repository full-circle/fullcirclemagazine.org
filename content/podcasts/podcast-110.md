---
title: "Full Circle Weekly News 110"
date: 2018-10-07
draft: false
tags:
  - "beta"
  - "buster"
  - "debian"
  - "developers"
  - "distro"
  - "elementary"
  - "firefox"
  - "iso"
  - "juno"
  - "kernel"
  - "mutagen"
  - "nibiru"
  - "solus"
  - "sparkylinux"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20110.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu-based elementary OS 5.0 'Juno' Beta 2 Linux distro now available](https://betanews.com/2018/09/21/ubuntu-based-elementary-juno-beta-2-linux/)

- [Solus 3.9999 ISO Refresh Released](https://fossbytes.com/solus-3-9999-iso-refresh-released-download-features/)

- [New SparkyLinux 5.5 "Nibiru" ISOs Released with Latest Debian Buster Updates](https://news.softpedia.com/news/new-sparkylinux-5-5-nibiru-isos-released-with-latest-debian-buster-updates-522817.shtml)

- [New Firefox browser bug causes crashes on Windows, Mac and Linux](https://www.techradar.com/news/new-firefox-browser-bug-causes-crashes-on-windows-mac-and-linux)

- [Linux Developers Threaten to Pull "Kill Switch"](https://www.hardocp.com/news/2018/09/23/linux_developers_threaten_to_pull_kill_switch)

- [Concerns about the behavioral guidelines of Linux kernel developers](https://www.pro-linux.de/news/1/26336/bedenken-gegen-die-verhaltensrichtlinien-der-linux-kernel-entwickler.html)

- ['Mutagen Astronomy' Linux kernel vulnerability sighted](https://www.theregister.co.uk/2018/09/27/mutagen_astronomy_linux/)
