---
title: "Full Circle Weekly News 185"
date: 2020-10-11
draft: false
tags:
  - "4mlinux"
  - "amnesia"
  - "assets"
  - "browser"
  - "debian"
  - "edge"
  - "journal"
  - "kernel"
  - "lenovo"
  - "linux"
  - "lite"
  - "magazine"
  - "microsoft"
  - "ota-13"
  - "ota13"
  - "thinkpad"
  - "thinkstation"
  - "touch"
  - "ubports"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20185.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Lenovo Releases 27 Thinkpads and Thinkstations with Ubuntu](https://news.lenovo.com/pressroom/press-releases/lenovo-launches-linux-ready-thinkpad-and-thinkstation-pcs-preinstalled-with-ubuntu/)

- [Amnesia the Dark Descent, Without Assets, Now Open Source](https://frictionalgames.com/2020-09-amnesia-is-now-open-source/)

- [Linux Journal Is Back](https://www.linuxjournal.com/content/linux-journal-back)

- [Microsoft Confirms Edge On Linux in October](https://blogs.windows.com/windowsexperience/2020/09/22/whats-new-in-web-experiences-ignite-2020-need-to-secure-your-remote-workers-choose-microsoft-edge-as-your-browser-for-business/)

- [Ubuntu Touch OTA-13 Out](https://ubports.com/blog/ubport-blogs-news-1/post/ubuntu-touch-ota-13-release-3720)

- [Debian 10.6 Out](https://www.debian.org/News/2020/20200926)

- [Linux Lite 5.2 RC1 Out](https://www.linuxliteos.com/forums/release-announcements/linux-lite-5-2-rc1-released/)

- [4MLinux 34 Out](https://4mlinux-releases.blogspot.com/2020/09/4mlinux-340-stable-released.html)

- [Linux Kernel 5.9 RC7 Out](https://lkml.org/lkml/2020/9/27/449)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
