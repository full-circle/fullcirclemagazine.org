---
title: "Full Circle Weekly News 162"
date: 2020-01-29
draft: false
tags:
  - "arch"
  - "battlefield"
  - "calm"
  - "cart"
  - "compression"
  - "ea"
  - "gitbucket"
  - "kart"
  - "kernel"
  - "linux"
  - "manager"
  - "multiplayer"
  - "openbsd"
  - "rc5"
  - "smartmontools"
  - "supertux"
  - "window"
  - "zstandard"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20162.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Arch Linux Now Using ZStandard Instead of XZ for Compression](https://www.archlinux.org/news/now-using-zstandard-instead-of-xz-for-package-compression/)

- [EA Is Permanently Banning Linux Gamers in Battlefield V](https://itsfoss.com/ea-banning-linux-gamers/)

- [OpenBSD's Calm Window Manager version 6.6 is now available in a portable package](https://github.com/leahneukirchen/cwm)

- [Smartmontools 7.1 is available](https://www.smartmontools.org/browser/tags/RELEASE_7_1/smartmontools/NEWS)

- [Supertux Cart 1.1 is available with better multiplayer](http://blog.supertuxkart.net/2020/01/supertuxkart-11-released.html)

- [GitBucket has released version 4.33.0](https://gitbucket.github.io/gitbucket-news/gitbucket/2019/12/31/gitbucket-4.33.0.html)

- [Linux Kernel 5.5 rc5 is available](https://lkml.org/lkml/2020/1/5/185)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
