---
title: "Full Circle Weekly News 389"
date: 2024-10-27
draft: false
tags:
  - "inkscape"
  - "framework"
  - "solus"
  - "openssh"
  - "intel"
  - "amd"
  - "torvalds"
  - "rogue"
  - "legacy"
  - "fooyin"
  - "forgejo"
  - "mysql"
  - "ubuntu"
  - "coreboot"
  - "malibal"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20389.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Release of Inkscape 1.4:](https://inkscape.org/news/2024/10/13/inkscape-launches-version-14-powerful-new-accessib/)

- [Qt 6.8 Framework Release:](https://www.qt.io/blog/qt-6.8-released)

- [Solus 4.6 released:](https://getsol.us/2024/10/14/solus-4-6-released/)

- [OpenSSH, moves to sshd-auth:](https://marc.info/?l%3Dopenbsd-cvs%26m%3D172887095204232%26w%3D2)

- [Intel and AMD, with Linus Torvalds, Form x86 Ecosystem Advisory Group:](https://www.intel.com/content/www/us/en/newsroom/news/october-2024-intel-news.html)

- [Rogue Legacy Source Code Released:](https://x.com/CellarDoorGames/status/1846246914406195662)

- [Fooyin Music Player 0.8 Released:](https://github.com/fooyin/fooyin/releases)

- [Forgejo 9.0 Collaborative Development Platform:](https://forgejo.org/2024-10-release-v9-0/)

- [MySQL 9.1.0 Released:](https://dev.mysql.com/downloads/mysql/)

- [Release of F-Stack 1.24:](https://github.com/F-Stack/f-stack/releases/tag/1.24)

- [Ubuntu Linux Distribution Turns 20:](https://lists.ubuntu.com/archives/ubuntu-announce/2004-October/000003.html)

- [CoreBoot-based solutions for systems with Intel Xeon 6 processors:](https://community.intel.com/t5/Blogs/Tech-Innovation/Data-Center/Advancing-Open-Source-Firmware-on-Intel-Xeon-6-Based-Platforms/post/1636720)

- [Laptop maker Malibal attacks CoreBoot project:](https://www.malibal.com/features/dont-support-the-coreboot-project/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
