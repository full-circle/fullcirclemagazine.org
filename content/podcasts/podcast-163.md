---
title: "Full Circle Weekly News 163"
date: 2020-02-12
draft: false
tags:
  - "beta"
  - "endlessos"
  - "firefox"
  - "kali"
  - "kde"
  - "kernel"
  - "linux"
  - "openeuler"
  - "pi"
  - "plasma"
  - "raspberry"
  - "root"
  - "tails"
  - "torvalds"
  - "trident"
  - "user"
  - "vim"
  - "vim9"
  - "void"
  - "vvvvvv"
  - "zfs"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20163.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Kali Defaults to a Non-Root User](https://www.kali.org/news/kali-default-non-root-user/)

- [VVVVVV Now Open Source](https://github.com/TerryCavanagh/VVVVVV/blob/master/LICENSE.md)

- [An All New Firefox 72](https://www.mozilla.org/en-US/firefox/72.0/releasenotes/)

- [Linus Torvalds Weary of ZFS in the Kernel](https://www.phoronix.com/scan.php?page=news_item&px=Linus-Says-No-To-ZFS-Linux)

- [EndlessOS Will Be Available on the Raspberry Pi](https://www.tomshardware.com/news/endless-os-coming-to-raspberry-pi)

- [First Beta Image of the Void Linux Based Project Trident is now Available](https://project-trident.org/post/void-beta-available/)

- [Vim9 Promises to Speed Up Vim Scripts](https://groups.google.com/forum/#!msg/vim_dev/OPbZwpcBP98/n4AIcviUBwAJ)

- [Plasma 5.17.5 Releases with Plenty of Bug Fixes](https://kde.org/announcements/plasma-5.17.5.php)

- [Pixelorama 0.6 Is Out](https://www.orama-interactive.com/post/pixelorama-v0-6-is-out)

- [OpenEuler is Available](https://itsfoss.com/openeuler/)

- [Tails 4.2 Is Out](https://tails.boum.org/news/version_4.2/index.en.html)

- [Kernel 5.5 rc6 Is Ready](https://lkml.org/lkml/2020/1/12/213)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
