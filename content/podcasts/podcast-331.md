---
title: "Full Circle Weekly News 331"
date: 2023-09-17
draft: false
tags:
  - "ubuntu"
  - "apache"
  - "netbeans"
  - "samba"
  - "manjaro"
  - "tails"
  - "webos"
  - "encryption"
  - "kde"
  - "zenwalk"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20331.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Apache NetBeans 19:](https://github.com/apache/netbeans/releases/tag/19)

- [Samba 4.19.0:](https://lists.samba.org/archive/samba-announce/2023/000647.html)

- [Release of Manjaro Linux 23.0:](https://forum.manjaro.org/t/manjaro-23-0-uranos-released/147448)

- [Tails 5.17:](https://tails.boum.org/news/version_5.17/index.en.html)

- [webOS Open Source Edition 2.23:](https://www.webosose.org/blog/2023/09/07/webos-ose-2-23-0-release/)

- [Ubuntu will support full-disk encryption that uses TPM:](https://canonical.com//blog/tpm-backed-full-disk-encryption-is-coming-to-ubuntu)

- [Double Commander 1.1:](https://github.com/doublecmd/doublecmd/releases)

- [KDE 6 is scheduled for February next year:](https://mail.kde.org/pipermail/kde-devel/2023-September/002008.html)

- [Zenwalk 2023:](http://www.zenwalk.org/2023/09/zenwalk-current-2023-is-out.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
