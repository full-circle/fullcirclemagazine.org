---
title: "Full Circle Weekly News 323"
date: 2023-07-22
draft: false
tags:
  - "geary"
  - "gimp"
  - "suse"
  - "rhel"
  - "void"
  - "thunderbird"
  - "pale moon"
  - "botan"
  - "kernel"
  - "almalinux"
  - "pfsense"
  - "debian"
  - "podman"
  - "llvm"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20323.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Geary 44.0:](https://gitlab.gnome.org/GNOME/geary/-/tags/44.0)

- [GIMP 2.99.16:](https://www.gimp.org/news/2023/07/09/gimp-2-99-16-released/)

- [SUSE has announced the creation of its own fork of RHEL:](https://www.suse.com/news/SUSE-Preserves-Choice-in-Enterprise-Linux/)

- [Void Linux installation builds are available:](https://voidlinux.org/news/2023/07/new-images.html)

- [Release of Thunderbird 115:](https://blog.thunderbird.net/2023/07/our-fastest-most-beautiful-release-ever-thunderbird-115-supernova-is-here/)

- [Release of Pale Moon 32.3:](https://forum.palemoon.org/viewtopic.php?t=30014&p=240884#p240884)

- [Release of the Botan 3.1.0 cryptographic library](https://botan.randombit.net/news.html)

- [Linux 6.5 kernel includes a system call cachestat:](https://lore.kernel.org/lkml/20230626085035.e66992e96b4c6d37dad54bd9@linux-foundation.org/)

- [AlmaLinux moves away from RHEL cloning:](https://almalinux.org/blog/future-of-almalinux/)

- [Release of pfSense 2.7.0:](https://www.netgate.com/blog/pfsense-2.7.0-and-23.05)

- [Debian to stop supporting GNU/kFreeBS port:](https://lists.debian.org/ZLFXP7cjnG5crpN1@aurel32.net)

- [Release of Podman Desktop 1.2:](https://podman-desktop.io/blog/podman-desktop-release-1.2)

- [Vector instruction optimizer for LLVM:](https://arxiv.org/pdf/2306.00229.pdf)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
