---
title: "Full Circle Weekly News 33"
date: 2016-09-03
draft: false
tags:
  - "16-10"
  - "ai"
  - "arch"
  - "beta"
  - "blackarch"
  - "canonical"
  - "ethical"
  - "hacking"
  - "kernel"
  - "linux"
  - "maru"
  - "mate"
  - "movie"
  - "opensuse"
  - "os"
  - "ransomware"
  - "trailer"
  - "vulnerabilities"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2033.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu MATE 16.10 Beta 1 Released](http://www.mobipicker.com/ubuntu-mate-16-10-beta-1-released-part-yakkety-yak-launch/)

- [Maru OS is now open source (Turns Android phones into Linux desktops)](Turns Android phones into Linux desktops)

- [New ransomware threat deletes files from Linux web servers](http://www.computerworld.com/article/3113658/security/new-ransomware-threat-deletes-files-from-linux-web-servers.html)

- [A new OpenSUSE Linux is coming to town, and it's all about stability](http://www.pcworld.com/article/3114575/linux/a-new-opensuse-linux-is-coming-to-town-and-its-all-about-stability.html)

- [Watch the first ever movie trailer made by artificial intelligence](http://www.polygon.com/2016/9/1/12753298/morgan-trailer-artificial-intelligence)

- [Canonical Patches Eight Linux Kernel Vulnerabilities for Ubuntu 16.04 LTS](http://news.softpedia.com/news/canonical-patches-eight-linux-kernel-vulnerabilities-for-ubuntu-16-04-lts-507732.shtml)

- [BlackArch Linux Ethical Hacking OS Is Now Powered by Kernel 4.7.2](http://news.softpedia.com/news/blackarch-linux-ethical-hacking-os-is-now-powered-by-kernel-4-7-2-new-isos-out-507817.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
