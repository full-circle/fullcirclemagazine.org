---
title: "Full Circle Weekly News 114"
date: 2018-11-07
draft: false
tags:
  - "android"
  - "desktop"
  - "development"
  - "firmware"
  - "gnu"
  - "kde"
  - "linux"
  - "pine64"
  - "pinephone"
  - "plasma"
  - "smartphone"
  - "snap"
  - "stallman"
  - "torvalds"
  - "userland"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20114.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Want To Run Linux On Android Without Rooting? Using UserLAnd](https://fossbytes.com/userland-linux-apps-distros-on-android-no-rooting/)

- [PinePhone: Pine64 Is Making An Affordable Linux Smartphone Running KDE Plasma](https://fossbytes.com/pinephone-pine64-affordable-linux-smartphone-kde-plasma/)

- [KDE Plasma 5.14.2 Desktop Environment Improves Firmware Updates, Snap Support](https://news.softpedia.com/news/kde-plasma-5-14-2-desktop-environment-improves-firmware-updates-snap-support-523381.shtml)

- [Richard Stallman Announces “GNU Kind Communication Guidelines”](https://fossbytes.com/richard-stallman-gnu-kind-communication-guidelines/)

- [Linus Torvalds Discusses His Return To Linux Development](https://fossbytes.com/linus-torvalds-return-to-linux-development/)
