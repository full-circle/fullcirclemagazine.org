---
title: "Full Circle Weekly News 03"
date: 2016-02-06
draft: false
tags:
  - "alpha"
  - "bq"
  - "canonical"
  - "convergence"
  - "dell"
  - "eol"
  - "google"
  - "ipv6"
  - "kernel"
  - "linux"
  - "mate"
  - "scanners"
  - "search"
  - "skylake"
  - "tablet"
  - "torvalds"
  - "touch"
  - "ubuntu"
  - "xps13"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2003.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu MATE 16.04 Alpha 2 Released as the Biggest Update Ever](http://news.softpedia.com/news/ubuntu-mate-16-04-alpha-2-released-as-the-biggest-update-ever-499632.shtml)

- [Canonical reinvents the personal mobile computing experience](https://insights.ubuntu.com/2016/02/04/canonical-reinvents-the-personal-mobile-computing-experience/)

- [Linux Kernel 2.6.32 LTS Reaches End of Life in February 2016](http://news.softpedia.com/news/linux-kernel-2-6-32-lts-reaches-end-of-life-on-february-2016-499661.shtml)

- [Using IPv6 with Linux? You’ve likely been visited by scanners](http://arstechnica.com/security/2016/02/using-ipv6-with-linux-youve-likely-been-visited-by-shodan-and-other-scanners/)

- [Linux Kernel 4.5 Release Candidate Build](http://news.softpedia.com/news/linus-torvalds-announced-the-second-linux-kernel-4-5-release-candidate-build-499688.shtml)

- [Dell will ship XPS 13 Developer Edition laptops with Skylake chips](http://www.pcworld.com/article/3029193/laptop-computers/dell-to-ship-xps-13-developer-edition-linux-laptop-with-skylake.html)

- [Head of Google Search retires, artificial intelligence chief to take over](http://arstechnica.com/gadgets/2016/02/head-of-google-search-retires-artificial-intelligence-chief-to-take-over/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
