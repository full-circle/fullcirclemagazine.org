---
title: "Full Circle Weekly News 377"
date: 2024-08-04
draft: false
tags:
  - "openbsd"
  - "nxs"
  - "hostapd"
  - "midnightbsd"
  - "openssl"
  - "mint"
  - "selectel"
  - "server"
  - "multipass"
  - "opnsense"
  - "zulip"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20377.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [OpenBSD adds VA-API support:](https://marc.info/?l=openbsd-cvs&m=172139969119269&w=2)

- [Release of nxs-data-anonymizer 1.9.0:](https://github.com/nixys/nxs-data-anonymizer)

- [Release of hostapd and wpa_supplicant 2.11:](https://lists.infradead.org/pipermail/hostap/2024-July/042847.html)

- [Release of the MidnightBSD 3.2:](https://www.justjournal.com/users/mbsd/entry/33985)

- [Restructuring the OpenSSL project:](https://mta.openssl.org/pipermail/openssl-announce/2024-July/000313.html)

- [Release of Linux Mint 22:](https://blog.linuxmint.com/?p=4731)

- [Selectel has begun public beta testing of its own Linux server distribution:](https://selectel-ru.translate.goog/about/newsroom/news/selectel-anonsiroval-otkrytoe-beta-testirovanie-servernoj-operaczionnoj-sistemy-sobstvennoj-razrabotki/?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en-US&_x_tr_pto=wapp)

- [Release of Multipass 1.14:](https://discourse.ubuntu.com/t/announcing-the-multipass-1-14-0-release/46668)

- [Release of OPNsense 24.7:](https://forum.opnsense.org/index.php?topic%3D41700.0)

- [Zulip 9:](https://blog.zulip.com/2024/07/25/zulip-9-0-released/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
