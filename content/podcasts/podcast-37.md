---
title: "Full Circle Weekly News 37"
date: 2016-10-01
draft: false
tags:
  - "10"
  - "2016"
  - "ai"
  - "build"
  - "doom"
  - "edition"
  - "facebook"
  - "flaw"
  - "israeli"
  - "kernel"
  - "microwatt"
  - "online"
  - "redstone"
  - "security"
  - "summit"
  - "wattos"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2037.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Online Summit for Ubuntu 17.04 is Taking Place In Mid-November](http://www.mobipicker.com/ubuntu-online-summit-ubuntu-17-04-taking-place-mid-november/)

- [Ubuntu 16.04 Now Available in Latest Windows 10 Redstone 2 Build](http://news.softpedia.com/news/ubuntu-16-04-now-available-in-latest-windows-10-redstone-2-build-508775.shtml)

- [Facebook has built a world-class, murderous, Doom-playing AI](http://www.businessinsider.de/facebook-intel-win-vizdoom-contest-build-artificial-intelligence-ai-plays-doom-deathmatch-2016-9?r=UK&IR=T)

- [This is the Israeli company that can hack any iPhone and Android smartphone](http://bgr.com/2016/09/26/iphone-7-vs-android-hacks/)

- [wattOS 10 Microwatt Edition Comes with Less of Everything, Based on Ubuntu 16.04](http://news.softpedia.com/news/wattos-10-microwatt-edition-comes-with-less-of-everything-based-on-ubuntu-16-04-508631.shtml)

- [Is the Linux kernel a security problem?](http://arstechnica.com/security/2016/09/linux-kernel-security-needs-fixing/?comments=1)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
