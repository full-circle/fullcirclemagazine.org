---
title: "Full Circle Weekly News 376"
date: 2024-07-27
draft: false
tags:
  - "blender"
  - "kernel"
  - "nomadbsd"
  - "gnome"
  - "tail"
  - "apache"
  - "peertube"
  - "suse"
  - "opensuse"
  - "nftables"
  - "audacity"
  - "google"
  - "nvidia"
  - "kde"
  - "kaos"
  - "openmandriva"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20376.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux kernel 6.10 released:](https://lkml.org/lkml/2024/7/14/250)

- [Release of NomadBSD 141:](https://nomadbsd.org/index.html%2320240715)

- [GNOME Foundation Executive Director Leaves:](https://foundation.gnome.org/2024/07/12/gnome-foundation-announces-transition-of-executive-director/)

- [Release of Tails 6.5:](https://tails.net/news/version_6.5/)

- [Release of PeerTube 6.2;](https://joinpeertube.org/news/release-6.2)

- [SUSE has asked the openSUSE project to stop using the SUSE brand:](https://lists.opensuse.org/archives/list/project@lists.opensuse.org/message/7IVGVJOAO4NIQILUYI3ZUL7NHCVBDQO7/)

- [Release of nftables 1.1.0:](https://www.mail-archive.com/netfilter-announce@lists.netfilter.org/msg00265.html)

- [Release of Audacity 3.6:](https://www.audacityteam.org/blog/audacity-3-6/)

- [Google has opened an application for creating 3D models using virtual reality:](https://opensource.googleblog.com/2024/07/google-blocks-is-now-open-source.html)

- [Release of StartWine-Launcher 404:](https://github.com/RusNor/StartWine-Launcher)

- [Bcachefs implements the abilities:](https://lore.kernel.org/lkml/r75jqqdjp24gikil2l26wwtxdxvqxpgfaixb2rqmuyzxnbhseq@6k34emck64hv/)
and [update](https://lore.kernel.org/lkml/73rweeabpoypzqwyxa7hld7tnkskkaotuo3jjfxnpgn6gg47ly@admkywnz4fsp/)

- [Release of nxs-backup 3.9.0:](https://github.com/nixys/nxs-backup/releases)

- [NVIDIA summarized plans to transfer Linux drivers to open kernel modules:](https://developer.nvidia.com/blog/nvidia-transitions-fully-towards-open-source-gpu-kernel-modules/)

- [Apache Foundation Announces Upcoming Rebranding:](https://news.apache.org/foundation/entry/evolving-the-asf-brand)

- [KDE crashes fixed and Wayland support improved:](https://pointieststick.com/2024/07/19/this-past-two-weeks-in-kde-fixing-sticky-keys-and-the-worst-crashes/)

- [Kaos](https://kaosx.us/news/2024/kaos07/)

- [Release of labwc 0.7.4:](https://github.com/labwc/labwc/releases)

- [Release of OpenMandriva ROME 24.07:](https://www.openmandriva.org/en/news/article/openmandriva-rome-24-07-released)

- [Release of Blender 4.2:](https://studio.blender.org/blog/new-geometry-nodes-features-in-blender-42/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
