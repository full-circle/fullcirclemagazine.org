---
title: "Full Circle Weekly News 43"
date: 2016-11-19
draft: false
tags:
  - "android"
  - "china"
  - "chrome"
  - "chromeos"
  - "foundation"
  - "kernel"
  - "linux"
  - "lts"
  - "microsoft"
  - "msoffice"
  - "office"
  - "os"
  - "ota-14"
  - "ota14"
  - "released"
  - "supercomputers"
  - "top500"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2043.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Touch OTA-14 Slightly Delayed](http://news.softpedia.com/news/ubuntu-touch-ota-14-slightly-delayed-new-task-manager-gets-fuzzy-background-510180.shtml)

- [Microsoft joins the Linux Foundation, 15 years after Ballmer called it 'cancer'](http://www.theverge.com/2016/11/16/13651940/microsoft-linux-foundation-membership)

- [Almost all the world's fastest supercomputers run Linux](http://www.zdnet.com/article/almost-all-the-worlds-fastest-supercomputers-run-linux/)

- [Security bods find Android phoning home. Home being China](http://www.theregister.co.uk/2016/11/15/android_phoning_home_to_china/)

- [Linux Kernel 4.4.31 LTS Released with Multiple Updated Drivers, Various Fixes](http://news.softpedia.com/news/linux-kernel-4-4-31-lts-released-with-multiple-updated-drivers-various-fixes-510128.shtml)

- [Microsoft Office for Android will be supported on Chrome OS](http://indianexpress.com/article/technology/tech-news-technology/microsoft-office-for-android-will-be-supported-on-chrome-os-4373073/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
