---
title: "Full Circle Weekly News 62"
date: 2017-05-27
draft: false
tags:
  - "4-10"
  - "4-11"
  - "embedded"
  - "flaw"
  - "iot"
  - "kernel"
  - "lightdm"
  - "linux"
  - "login"
  - "malware"
  - "rakos"
  - "security"
  - "tails"
  - "tor"
  - "vmware"
  - "wifi"
  - "windows-10"
  - "workstation"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2062.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Login Screen Security Flaw Could Allow Anyone To Access Your Files](https://fossbytes.com/ubuntu-login-screen-security-flaw-lightdm/)

- [Linux Kernel 4.10 Reached End of Life, Users Urged to Move to Linux 4.11 Series](http://news.softpedia.com/news/linux-kernel-4-10-reached-end-of-life-users-urged-to-move-to-linux-4-11-series-515898.shtml)

- [Linux Distros Won’t Work On Windows 10 S](https://fossbytes.com/linux-distros-wont-work-on-windows-10-s/)

- [VMware Patches Multiple Security Issues in Workstation](https://threatpost.com/vmware-patches-multiple-security-issues-in-workstation/125805/)

- [Privacy-focused Linux-based operating system Tails 3.0 reaches RC status](https://betanews.com/2017/05/21/tails-debian-stretch-privacy-rc/)

- [This New Tech Helps You See Through Walls Using Wi-Fi](https://fossbytes.com/wi-fi-imaging-walls-hologram/)

- [How does Rakos malware attack embedded Linux systems?](http://searchsecurity.techtarget.com/answer/How-does-Rakos-malware-attack-embedded-Linux-systems)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
