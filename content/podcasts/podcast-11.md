---
title: "Full Circle Weekly News 11"
date: 2016-04-02
draft: false
tags:
  - "4-6"
  - "ai"
  - "av-linux"
  - "build"
  - "debian"
  - "dell"
  - "distribution"
  - "distro"
  - "gnome-3-2"
  - "inquiry"
  - "kernel"
  - "laptop"
  - "lubuntu"
  - "lxle"
  - "malware"
  - "orangefs"
  - "remaiten"
  - "robotics"
  - "stali"
  - "torvalds"
  - "xps"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2011.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Stali distribution smashes assumptions about Linux](http://www.infoworld.com/article/3048737/open-source-tools/stali-distribution-smashes-assumptions-about-linux.html)

- [Lubuntu-Based LXLE 14.04.4 Officially Released](http://news.softpedia.com/news/lubuntu-based-lxle-14-04-4-posh-paradigm-linux-os-officially-released-502149.shtml)

- [UK Launches Robotics And Artificial Intelligence Inquiry](http://www.techtimes.com/articles/144164/20160326/uk-launches-robotics-and-artificial-intelligence-inquiry-worried-about-robots-taking-over-the-world.htm)

- [Linus Torvalds Announces First Linux Kernel 4.6 RC Build, Introduces OrangeFS](http://news.softpedia.com/news/linus-torvalds-announces-first-linux-kernel-4-6-rc-build-introduces-orangefs-502217.shtml)

- [Ubuntu-Based Dell XPS 13 Developer Edition Laptop Launches in Europe](http://news.softpedia.com/news/ubuntu-based-dell-xps-13-developer-edition-laptop-launches-in-europe-502435.shtml)

- [Remaiten Linux bot combines malware features to target weak credentials](http://www.scmagazine.com/remaiten-linux-bot-uses-a-unique-method-of-distribution/article/486416/)

- [AV Linux 2016 Officially Released, a Debian OS Optimized for Audio Production](http://news.softpedia.com/news/av-linux-2016-officially-released-a-debian-os-optimized-for-audio-production-502266.shtml)

- [GNOME 3.2 Released](http://www.linuxjournal.com/content/gnome-32-released)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
