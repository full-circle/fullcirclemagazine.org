---
title: "Full Circle Weekly News 58"
date: 2017-04-22
draft: false
tags:
  - "17-10"
  - "18-04"
  - "amd"
  - "cpu"
  - "email"
  - "fedora"
  - "firefox"
  - "gnome"
  - "i2p"
  - "intel"
  - "linux"
  - "mozilla"
  - "redhat"
  - "tails"
  - "thunderbird"
  - "tor"
  - "ubuntu"
  - "wayland"
  - "xorg"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2058.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 17.10 To Have Wayland Display Server As Default](https://fossbytes.com/ubuntu-17-10-wayland-default-server/)

- [Ubuntu might retire Thunderbird](http://www.fudzilla.com/news/43433-ubuntu-might-retire-thunderbird)

- [Red Hat and Fedora Teams Welcome Ubuntu to GNOME and Wayland with Open Arms](http://news.softpedia.com/news/red-hat-and-fedora-teams-welcome-ubuntu-to-gnome-and-wayland-with-open-arms-514661.shtml)

- [Tails 2.12 Anonymous Live OS Is Out, Drops I2P as Alternative Anonymity Network](http://news.softpedia.com/news/tails-2-12-anonymous-live-os-is-out-drops-i2p-as-alternative-anonymity-network-515000.shtml)

- [Mozilla Firefox web browser may no longer be supported on your Linux computer](https://betanews.com/2017/04/20/mozilla-firefox-linux-intel-amd/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
