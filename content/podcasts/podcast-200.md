---
title: "Full Circle Weekly News 200"
date: 2021-02-14
draft: false
tags:
  - "20-04-2"
  - "21-04"
  - "almalinux"
  - "apache"
  - "beta"
  - "core"
  - "crypto"
  - "cryptography"
  - "darktable"
  - "endeavour"
  - "endeavouros"
  - "feral"
  - "google"
  - "hwe"
  - "kde"
  - "libreoffice"
  - "module"
  - "os"
  - "rust"
  - "solus"
  - "stack"
  - "ubuntu"
  - "update"
  - "warhammer-iii"
  - "wimpress"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20200.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 20.04.2 and Flavors with HWE Stack Released](https://lists.ubuntu.com/archives/ubuntu-announce/2021-February/000264.html)

- [Ubuntu To Replace Ubiquity Installer](https://discourse.ubuntu.com/t/refreshing-the-ubuntu-desktop-installer/20659)

- [Apache Project Hoping for New Rust Cryptography Module, with Help from Google](https://www.zdnet.com/article/google-funds-project-to-secure-apache-web-server-project-with-new-rust-component/)

- [Martin Wimpress to Leave Post at Canonical](https://www.omgubuntu.co.uk/2021/02/martin-wimpress-ubuntu-desktop-lead-leaving-canonical)

- [Ubuntu Core 20 Out](https://ubuntu.com/blog/ubuntu-core-20-secures-linux-for-iot)

- [AlmaLinux 8.3 Beta Out](https://blog.almalinux.org/introducing-almalinux-beta-a-community-driven-replacement-for-centos/)

- [EndeavourOS's first 2021 Update Release Out](https://endeavouros.com/news/our-first-release-of-2021-has-arrived/)

- [Solus 4.2 Out](https://getsol.us/2021/02/03/solus-4-2-released/)

- [Ubuntu's Yaru Theme with GTK4 Support Out](https://discourse.ubuntu.com/t/call-for-testing-yaru-gtk4-theme/20668)

- [Ubuntu 21.04 Artwork Out](https://twitter.com/sylvia_ritter/status/1356648612231536641?s=20)

- [KDE's Februrary App Update Out](https://kde.org/announcements/releases/2021-02-apps-update/)

- [LibreOffice 7.1 Out](https://blog.documentfoundation.org/blog/2021/02/03/libreoffice-7-1-community/)

- [Darktable 3.4.1 Out](https://www.darktable.org/2021/02/darktable-341-released/)

- [Feral Interactive Have Announced Linux Support for Warhammer III](https://twitter.com/feralgames/status/1356982376673509380?s=20)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
