---
title: "Full Circle Weekly News 67"
date: 2017-07-22
draft: false
tags:
  - "16-04"
  - "16-10"
  - "17-04"
  - "ai"
  - "android"
  - "bad-taste"
  - "kerberos"
  - "malware"
  - "msi"
  - "touch"
  - "ubports"
  - "vulnerability"
  - "windows"
  - "yak"
  - "yakkety"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2067.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 16.10 (Yakkety Yak) Is No Longer Supported, Upgrade to Ubuntu 17.04 Now](Yakkety Yak)

- [UBports Working Lately on Ubuntu Touch Port for Nexus 5, Based on Ubuntu 16.04](http://news.softpedia.com/news/ubports-working-lately-on-ubuntu-touch-port-for-nexus-5-based-on-ubuntu-16-04-517027.shtml)

- ["Bad Taste" Vulnerability Affects Linux Systems via Malicious Windows MSI Files](https://www.bleepingcomputer.com/news/security/-bad-taste-vulnerability-affects-linux-systems-via-malicious-windows-msi-files/)

- [Windows, Linux distros, macOS pay for Kerberos 21-year-old 'cryptographic sin'](http://www.zdnet.com/article/windows-linux-distros-macos-pay-for-kerberos-21-year-old-cryptographic-sin/)

- [AI bots will kill us all! Or at least may seriously inconvenience humans](https://www.theregister.co.uk/2017/07/17/ai_will_kill_us_all/)

- [This scary Android malware can record audio, video and steal your data](http://www.zdnet.com/article/this-scary-android-malware-can-record-audio-video-and-steal-your-data/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
