---
title: "Full Circle Weekly News 206"
date: 2021-04-19
draft: false
tags:
  - "2d"
  - "amazon"
  - "animation"
  - "beta"
  - "console"
  - "fix"
  - "fpga"
  - "freebsd"
  - "google"
  - "hypervisor"
  - "kernel"
  - "lxqt"
  - "mozilla"
  - "nginx"
  - "nnn"
  - "nvidia"
  - "opensearch"
  - "opentoonz"
  - "server"
  - "slackware"
  - "sway"
  - "system76"
  - "wayland"
  - "xen"
  - "xorg"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20206.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [OpenToonz 1.5, an open source 2D animation package](https://github.com/opentoonz/opentoonz/releases/tag/v1.5.0)

- [Sway 1.6 environment using Wayland released](https://github.com/swaywm/sway/releases/tag/1.6)

- [Xen 4.15 hypervisor released](https://www.xenproject.org/)

- [FPGA Open Source Development Initiative](https://osfpga.org/osfpga-foundation-launched/)

- [NVIDIA Invests $ 1.5M in Mozilla Common Voice Project](https://blog.mozilla.org/blog/2021/04/12/mozilla-partners-with-nvidia-to-democratize-and-diversify-voice-technology/)

- [Slackware 15 entered beta testing](http://www.slackware.com/changelog/current.php?cpu=x86_64)

- [Amazon Introduces OpenSearch, Forked Elasticsearch Platform](https://aws.amazon.com/blogs/opensource/introducing-opensearch/)

- [FreeBSD 13.0 released](https://www.freebsd.org/releases/13.0R/announce/)

- [System76 has announced the development of a custom COSMIC environment](https://blog.system76.com/post/648371526931038208/cosmic-to-arrive-in-june-release-of-popos-2104)

- [X.Org Server 1.20.11 update with vulnerability fix](https://www.mail-archive.com/xorg-announce@lists.x.org/msg01290.html)

- [Nginx 1.19.10 released](https://mailman.nginx.org/pipermail/nginx-announce/2021/000296.html)

- [Patches for randomizing Linux kernel stack addresses for system calls introduced](https://lkml.org/lkml/2021/3/30/1180)

- [Google unveils multi-level LRU patches for Linux](https://lore.kernel.org/lkml/20210413065633.2782273-1-yuzhao@google.com/)

- [Release of LXQt 0.17](https://github.com/lxqt/lxqt/releases/tag/0.17.0)

- [Console file manager nnn 4.0 available](https://github.com/jarun/nnn/releases/tag/v4.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
