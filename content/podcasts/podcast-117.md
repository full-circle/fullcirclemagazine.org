---
title: "Full Circle Weekly News 117"
date: 2018-12-12
draft: false
tags:
  - "27"
  - "android"
  - "captchas"
  - "fedora"
  - "gui"
  - "kernel"
  - "linus"
  - "monitoring"
  - "network"
  - "nutty"
  - "ota-6"
  - "ota6"
  - "phone"
  - "russia"
  - "slowdown"
  - "spectre"
  - "stibp"
  - "tool"
  - "torvalds"
  - "touch"
  - "ubports"
  - "yandex"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20117.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Rest in peace, Fedora Linux 27](https://betanews.com/2018/11/30/fedora-linux-27-eol/)

- [Russian search giant Yandex is releasing an Android phone](https://www.theverge.com/2018/12/5/18127431/russian-yandex-search-android-phone-release)

- [OTA-6: Ubuntu Touch OTA-6 Release](https://ubports.com/blog/ubports-blog-1/post/ubuntu-touch-ota-6-release-186)

- [STIBP, collaborate and listen: Linus floats Linux kernel that 'fixes' Intel CPUs' Spectre slowdown](https://www.theregister.co.uk/2018/12/04/linux_kernel_spectre_mitigation/)

- [Nutty: GUI Network Monitoring And Information Tool For Ubuntu, Linux Mint And elementary OS](https://www.linuxuprising.com/2018/12/nutty-gui-network-monitoring-and.html)

- [AI-Based Algorithm Developed by Researchers Could Make Text Captchas Obsolete Source](https://news.softpedia.com/news/ai-algorithm-developed-by-researchers-could-make-text-captchas-obsolete-524130.shtml)
