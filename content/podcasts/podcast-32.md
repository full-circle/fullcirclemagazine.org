---
title: "Full Circle Weekly News 32"
date: 2016-08-27
draft: false
tags:
  - "16-04"
  - "ai"
  - "botnet"
  - "budgie"
  - "china"
  - "chinese"
  - "core"
  - "cruise"
  - "iot"
  - "joule"
  - "lightdm"
  - "linux"
  - "missiles"
  - "openai"
  - "puppex"
  - "puppy"
  - "reddit"
  - "snappy"
  - "twitter"
  - "xenial"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2032.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [PuppEX Linux Live CD Now Based on Puppy Xenial, Compatible with Ubuntu 16.04](http://news.softpedia.com/news/puppex-linux-live-cd-now-based-on-puppy-xenial-compatible-with-ubuntu-16-04-507605.shtml)

- [Ubuntu Budgie Remix 16.10 ISO Coming Soon with New LightDM Greeter](http://news.softpedia.com/news/ubuntu-budgie-remix-16-10-yakkety-yak-iso-coming-soon-with-new-lightdm-greeter-507453.shtml)

- [Linux turns 25, is bigger and more professional than ever](http://arstechnica.com/information-technology/2016/08/on-linuxs-25th-anniversary-development-has-gone-corporate/)

- [OpenAI will use Reddit and a new supercomputer to teach artificial intelligence how to speak](http://www.zmescience.com/science/reddit-supercomp-59815/)

- [Intel's New Joule IoT Development Board Is Powered by Snappy Ubuntu Core](http://news.softpedia.com/news/intel-s-new-joule-iot-development-board-is-powered-by-snappy-ubuntu-core-507511.shtml)

- [Chinese cruise missiles to feature artificial intelligence](https://pacetoday.com.au/chinese-missiles-artificial-intelligence/)

- [Android botnet relies on Twitter for commands](http://www.computerworld.com/article/3112305/security/android-botnet-relies-on-twitter-for-commands.html)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
