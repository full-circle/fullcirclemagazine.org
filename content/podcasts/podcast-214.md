---
title: "Full Circle Weekly News 214"
date: 2021-06-14
draft: false
tags:
  - "backup"
  - "blender"
  - "boot"
  - "browser"
  - "display"
  - "gear"
  - "gnu"
  - "grub"
  - "icewm"
  - "kde"
  - "krita"
  - "lakka"
  - "mir"
  - "plasma"
  - "qmplay"
  - "regolith"
  - "rescuezilla"
  - "vivaldi"
  - "vsftpd"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20214.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Rescuezilla 2.2 Backup Distribution Released](https://github.com/rescuezilla/rescuezilla/releases/tag/2.2)

- [Lakka 3.1, a distribution for creating game consoles](http://www.lakka.tv/articles/2021/06/05/lakka-3.1/)

- [Blender 2.93 LTS released](https://www.blender.org/news/blender-2-93-release/)

- [Regolith Desktop 1.6 Released](https://github.com/regolith-linux/regolith-desktop/releases/tag/R1.6)

- [Vsftpd 3.0.4 released](https://security.appspot.com/vsftpd.html)

- [IceWM 2.4 window manager released](https://github.com/ice-wm/icewm/releases/tag/2.4.0)

- [KDE Plasma 5.22 Desktop Released](https://kde.org/announcements/plasma/5/5.22.0/)

- [Mir Display Server 2.4 released](https://discourse.ubuntu.com/t/release-2-4-0/22663)

- [GNU GRUB Boot Manager 2.06](https://lists.gnu.org/archive/html/grub-devel/2021-06/msg00022.html)

- [QMPlay2 release 21.06.07](https://github.com/zaps166/QMPlay2/releases/tag/21.06.07)

- [Vivaldi 4.0 released](https://vivaldi.com/blog/vivaldi-4-0/)

- [KDE Gear 21.04.2 released](https://9to5linux.com/kde-gear-21-04-gets-first-point-release-with-konsole-improvements-more)

- [Krita 4.4.5 Arrives – Last Bugfix Release Before Krita 5.0](https://9to5linux.com/krita-4-4-3-released-with-stability-and-performance-improvements-bug-fixes)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
