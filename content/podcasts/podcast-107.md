---
title: "Full Circle Weekly News 107"
date: 2018-09-17
draft: false
tags:
  - "18-10"
  - "4-20"
  - "5530"
  - "android"
  - "anonymous"
  - "bugs"
  - "cosmic"
  - "cpu"
  - "cuttlefish"
  - "dell"
  - "intel"
  - "kernel"
  - "librem"
  - "linux"
  - "nsa"
  - "phone"
  - "pi"
  - "precision"
  - "raspberry"
  - "tails"
  - "veracrypt"
  - "zerophone"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20107.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Dell Precision 5530 mobile workstation now available with Ubuntu](https://liliputing.com/2018/09/dell-precision-5530-mobile-workstation-now-available-with-ubuntu.html)

- [Ubuntu 18.10 Cosmic Cuttlefish WON’T Ship With Android Integration](https://fossbytes.com/ubuntu-18-10-cosmic-cuttlefish-no-android-integration/)

- [ZeroPhone Is “Coming Soon”: A Raspberry Pi-Based, Linux-Powered Phone For Just $50](https://fossbytes.com/zerophone-raspberry-pi-open-source-linux-crowdsupply/)

- [Librem 5 Linux Smartphone’s Release Date Pushed Back To April 2019](https://fossbytes.com/librem-5-linux-smartphones-release-pushed-april-2019/)

- [Linux Creator On Intel CPU Bugs: “It’s Unfair. We Have To Fix Someone Else’s Problems”](https://fossbytes.com/linus-torvalds-on-intel-cpu-bug-unfair/)

- [Linux Kernel 4.20 to drop NSA-developed Speck Algorithm](https://appuals.com/linux-kernel-4-20-to-drop-nsa-developed-speck-algorithmm/)

- [Tails Anonymous OS Gets Its Biggest Update Yet with VeraCrypt Integration](https://news.softpedia.com/news/tails-anonymous-os-gets-its-biggest-update-yet-with-veracrypt-integration-more-522543.shtml)

- [New AI capability helps empower DevSecOps teams](https://betanews.com/2018/09/06/ai-devsecops-teams/)
