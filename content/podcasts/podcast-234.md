---
title: "Full Circle Weekly News 234"
date: 2021-11-07
draft: false
tags:
  - "api"
  - "crabz"
  - "cygwin"
  - "gnu"
  - "graphics"
  - "linux"
  - "microkernel"
  - "muen"
  - "multithreaded"
  - "ncspot"
  - "openldap"
  - "raspberry"
  - "server"
  - "sniffglue"
  - "spotify"
  - "traffic"
  - "trident"
  - "trisuel"
  - "vulkan"
  - "xorg"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20234.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Muen 1.0, an open source microkernel](https://groups.google.com/g/muen-dev/c/mzd5E6lLomw)

- [OpenLDAP 2.6.0 released](https://www.openldap.org/software/release/announce.html)

- [Launch of ncspot, a console Spotify client](https://github.com/hrkfdn/ncspot/releases/tag/v0.9.0)

- [Vulkan 1.1 Graphics API Certified for Raspberry Pi 4](https://www.raspberrypi.com/news/vulkan-update-version-1-1-conformance-for-raspberry-pi-4/)

- [Crabz 0.7, multithreaded compression utility](https://github.com/sstadick/crabz)

- [X.Org Server 21.1 available](https://www.mail-archive.com/xorg@lists.x.org/msg06880.html)

- [Trisquel GNU / Linux 9.0.1](https://trisquel.info/en/release-announcement-trisquel-901-etiona-security-update)

- [New version of Cygwin 3.3.0](https://www.mail-archive.com/cygwin-announce@cygwin.com/msg09893.html)

- [Sniffglue traffic analyzer 0.14.0 released](https://github.com/kpcyrd/sniffglue)

- [Termination of Trident development](https://project-trident.org/post/2021-10-29_sunset/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
