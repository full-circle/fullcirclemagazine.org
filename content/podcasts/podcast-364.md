---
title: "Full Circle Weekly News 364"
date: 2024-05-05
draft: false
tags:
  - "lunatik"
  - "edgedb"
  - "audacity"
  - "netbsd"
  - "nmap"
  - "nginx"
  - "truenas"
  - "qemu"
  - "proxmox"
  - "nextcloud"
  - "ubuntu"
  - "genode"
  - "min"
  - "browser"
  - "ncurses"
  - "endeavour"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20364.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Lunatik:](https://github.com/luainkernel/lunatik)

- [Release of EdgeDB 5.0:](https://github.com/edgedb/edgedb/releases/tag/v5.0)

- [Release of Audacity 3.5:](https://www.audacityteam.org/blog/audacity-3-5/)

- [NetBSD 9.4 released:](https://www.netbsd.org/releases/formal-9/NetBSD-9.4.html)

- [Release of Nmap 7.95:](https://nmap.org/download)

- [Release of the Fedora Linux 40:](https://fedoramagazine.org/announcing-fedora-linux-40/)

- [Nginx 1.26.0 released with HTTP/3 support:](https://mailman.nginx.org/pipermail/nginx-announce/2024/EJFW6YFMCUODWQN7DZKIEKUTHKYXVZT2.html)

- [Release of TrueNAS SCALE 24.04:](https://www.truenas.com/blog/truenas-scale-dragonfish-release/)

- [Release of QEMU 9.0.0:](https://lists.nongnu.org/archive/html/qemu-devel/2024-04/msg03263.html)

- [Pale Moon browser 33.1.0:](https://forum.palemoon.org/viewtopic.php?t%3D31085%26p%3D251221%23p251221)

- [Release of Proxmox VE 8.2:](https://forum.proxmox.com/threads/proxmox-ve-8-2-released.145722/)

- [Nextcloud Hub 8 introduced:](https://nextcloud.com/blog/nextcloud-hub8/)

- [Release of Ubuntu 24.04 LTS:](https://lists.ubuntu.com/archives/ubuntu-announce/2024-April/000301.html)

- [Release of OSMC 2024.04-1:](https://osmc.tv/2024/04/osmcs-april-update-is-here-with-kodi-v20-5/)

- [The Genode project has published Sculpt 24.04:](https://genode.org/news/sculpt-os-release-24.04)

- [Web browser Min 1.32:](https://github.com/minbrowser/min/releases/tag/v1.32.0)

- [Release of ncurses 6.5:](https://www.mail-archive.com/info-gnu@gnu.org/msg03279.html)

- [Release of EndeavourOS 24.04:](https://endeavouros.com/news/plasma-6-with-wayland-or-x11-option-and-qt-6-ported-calamares-meet-gemini/%0D%0A)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
