---
title: "Full Circle Weekly News 369"
date: 2024-06-09
draft: false
tags:
  - "armbian"
  - "kaos"
  - "ubuntu"
  - "rhino"
  - "canonical"
  - "solaris"
  - "networkmanager"
  - "fedora"
  - "rocky"
  - "linux"
  - "kaspersky"
  - "lyx"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20369.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Armbian 24.5:](https://www.armbian.com/newsflash/armbian-24-5-1-havier/)

- [Release of KaOS 2024.05:](https://kaosx.us/news/2024/kaos05/)

- [Ubuntu 24.04 builds for the RISC-V board Milk-V Mars:](https://canonical.com/blog/canonical-enables-ubuntu-on-milk-v-mars)

- [Release of Rhino Linux 2024.1:](https://rhinolinux.org/news-13.html)

- [The XZ project published the result of a commit audit and the first update after the backdoor was identified:](https://www.mail-archive.com/xz-devel@tukaani.org/msg00681.html)

- [Canonical has published Real-time Ubuntu 24.04:](https://canonical.com//blog/real-time-24-04)

- [Major release of Solaris 11.4 SRU69:](https://blogs.oracle.com/solaris/post/whats-new-in-oracle-solaris-114-sru69)

- [Release of NetworkManager 1.48.0:](https://networkmanager.dev/blog/networkmanager-1-48/)

- [Fedora 41 intends to remove network-scripts and allow updating of atomic editions without a password:](https://www.mail-archive.com/devel-announce@lists.fedoraproject.org/msg03290.html)

- [Release of Rocky Linux 8.10:](https://rockylinux.org/news/rocky-linux-8-10-ga-release)

- [Kaspersky Lab has published a free malware scanner for Linux:](https://www.kaspersky.ru/blog/kvrt-for-linux/37571/)

- [LyX 2.4.0 published:](http://www.lyx.org/News)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
