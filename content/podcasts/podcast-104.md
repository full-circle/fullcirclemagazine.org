---
title: "Full Circle Weekly News 104"
date: 2018-08-27
draft: false
tags:
  - "beta"
  - "crypto"
  - "debian"
  - "dropbox"
  - "games"
  - "gcc"
  - "gnu"
  - "kernel"
  - "lenovo"
  - "linux"
  - "mint"
  - "nsa"
  - "ota-4"
  - "ota4"
  - "patches"
  - "stackleak"
  - "thinkpad"
  - "touch"
  - "ubports"
  - "valve"
  - "windows"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20104.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Debian GNU/Linux project to mark 25th birthday on Thursday](https://www.itwire.com/open-sauce/84040-)

- [Hands-on with Linux Mint Debian Edition 3 Beta](https://www.zdnet.com/article/hands-on-linux-mint-debian-edition-3-beta/)

- [Valve seems to be working on tools to get Windows games running on Linux](https://arstechnica.com/gaming/2018/08/valve-seems-to-be-working-on-tools-to-get-windows-games-running-on-linux/)

- [Lenovo launches its thinnest and lightest pro laptop – ThinkPad P1 Source](https://mybroadband.co.za/news/hardware/271715-lenovo-launches-its-thinnest-and-lightest-pro-laptop-thinkpad-p1.html)

- [UBports Releases Ubuntu Touch OTA-4](https://ubports.com/blog/ubports-blog-1/post/ubuntu-touch-ota-4-release-166)

- [Linux 4.18 And Zorin OS 12.4 Released With Big Changes](https://fossbytes.com/linux-kernel-4-18-zorin-os-12-4-download-features/)

- [New round of 64-bit ARM Patches Merged into Linux 4.19 Kernel, Includes GCC Stackleak Plugin Support](https://appuals.com/new-round-of-64-bit-arm-patches-merged-into-linux-4-19-kernel-includes-gcc-stackleak-plugin-support/)

- [New Linux kernel debuts, adds more suspect NSA-sourced crypto](https://www.itnews.com.au/news/new-linux-kernel-debuts-adds-more-suspect-nsa-sourced-crypto-500094)

- [Dropbox makes the cloud rain poop on Linux users](https://betanews.com/2018/08/11/dropbox-linux-doh/)
