---
title: "Full Circle Weekly News 242"
date: 2021-12-28
draft: false
tags:
  - "browser"
  - "debian"
  - "duckduckgo"
  - "elementary"
  - "font"
  - "gimp"
  - "init"
  - "jami"
  - "kart"
  - "krita"
  - "library"
  - "manjaro"
  - "mongoose"
  - "steam"
  - "supertux"
  - "system"
  - "taranis"
  - "theme"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20242.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Debian offers fnt font manager](https://bits.debian.org/2021/12/2000-fonts-debian.html)

- [Ubuntu 22.04 theme switched to orange](https://github.com/ubuntu/yaru/pull/3264)

- [Debian 11.2 Update](https://www.debian.org/News/2021/20211218)

- [Mongoose OS 2.20, an IoT device platform released](https://github.com/cesanta/mongoose-os/releases/tag/2.20.0)

- [Release of the GNU library libmicrohttpd 0.9.74](https://www.mail-archive.com/info-gnu@gnu.org/msg02977.html)

- [Release of helloSystem 0.7 using FreeBSD and similar to macOS](https://twitter.com/probonopd/status/1472595276942643200)

- [Release of the distribution kit Elementary OS 6.1](https://blog.elementary.io/elementary-os-6-1-available-now/)

- [Release of the graphics editor GIMP 2.10.30](https://www.gimp.org/)

- [80% of the 100 most popular games on Steam are running on Linux](https://www.protondb.com/)

- [Search Engine DuckDuckGo Desktop Web Browser](https://spreadprivacy.com/duckduckgo-2021-review/)

- [Release of service manager s6-rc 0.5.3.0 and init system s6-linux-init 1.0.7](https://github.com/skarnet/s6-rc/releases/tag/v0.5.3.0)

- [Manjaro Linux 21.2 Release](https://forum.manjaro.org/t/manjaro-21-2-0-qonos-released/95856)

- [SuperTux 0.6.3 Free Game Release](https://www.supertux.org/news/2021/12/23/0.6.3)

- [Release of Krita 5.0](https://krita.org/en/item/krita-5-0-released/)

- [Jami "Taranis" released](https://jami.net/taranis-a-major-release-of-jami/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
