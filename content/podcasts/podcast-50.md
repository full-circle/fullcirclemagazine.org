---
title: "Full Circle Weekly News 50"
date: 2017-01-21
draft: false
tags:
  - "16-04"
  - "ai"
  - "aio"
  - "all-in-one"
  - "biomedical"
  - "blind"
  - "ibm"
  - "iso"
  - "mac"
  - "puppy"
  - "quirky"
  - "vinux"
  - "xerus"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2050.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Puppy Linux Fork Quirky 8.1.6 "Xerus" Is Built from Ubuntu 16.04 Binary Packages](http://news.softpedia.com/news/puppy-linux-fork-quirky-8-1-6-xerus-is-built-from-ubuntu-16-04-binary-packages-511890.shtml)

- [You Can Now Have a Single ISO Image with All the Essential Ubuntu 16.10 Flavors](http://news.softpedia.com/news/you-can-now-have-a-single-iso-image-with-all-essential-ubuntu-16-10-flavors-exclusive-511788.shtml)

- [Ubuntu-Based Vinux Linux 5.1 Released for Blind and Partially Sighted People](http://news.softpedia.com/news/ubuntu-based-vinux-linux-5-1-released-for-blind-and-partially-sighted-people-511978.shtml)

- [How IBM Is Using Artificial Intelligence to Provide Cybersecurity](http://host.madison.com/business/investment/markets-and-stocks/how-ibm-is-using-artificial-intelligence-to-provide-cybersecurity/article_fb85c743-e534-535d-a454-93ad566dd31d.html)

- [Mac, Linux malware discovered targeting biomedical research](http://www.pcworld.com/article/3159248/macs/mac-malware-is-found-targeting-biomedical-research.html)

- [Beware! This Ruthless Malware Returned From The Dead Is Now A Virtual Machine Killer](https://fossbytes.com/second-shamoon-2-malware-virtual-machines/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
