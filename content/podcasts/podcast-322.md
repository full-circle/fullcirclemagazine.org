---
title: "Full Circle Weekly News 322"
date: 2023-07-16
draft: false
tags:
  - "perl"
  - "rclone"
  - "deltatouch"
  - "radix"
  - "kde"
  - "neon"
  - "steam"
  - "games"
  - "lxd"
  - "sourcegraph"
  - "openkylin"
  - "proxmox"
  - "zink"
  - "fedora"
  - "blendos"
  - "ubuntu"
  - "phosh"
  - "solus"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20322.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Perl 5.38.0 with class support:](https://www.nntp.perl.org/group/perl.perl5.porters/2023/07/msg266602.html)

- [Release of rclone 1.63:](https://forum.rclone.org/t/rclone-v1-63-0-release/39465)

- [DeltaTouch 1.0.0:](https://delta.chat/en/2023-07-02-deltatouch)

- [New release of Radix:](https://radix.pro/platform/install/)

- [KDE Neon:](https://blog.neon.kde.org/2023/07/04/kde-neon-plasma-6-isos/)

- [75% of Steam games play in Linux:](https://boilingsteam.com/valve-is-accelerating-the-validation-of-games-from-2023-for-the-steam-deck-like-never-before-including-games-not-even-released-yet/)

- [LXD seperation:](https://groups.google.com/a/lists.linuxcontainers.org/g/lxc-users/c/NE3cGaoaVUE)

- [The sourcegraph project moved from an open license to proprietary:](https://github.com/sourcegraph/sourcegraph)

- [OpenKylin 1.0:](https://www.openkylin.top/index-en.html)

- [Proxmox Mail Gateway 8.0:](https://forum.proxmox.com/threads/proxmox-mail-gateway-8-0-available.129793/)

- [Imagination uses Zink:](https://blog.imaginationtech.com/imagination-gpus-now-support-opengl-4.6)

- [Fedora considering telemetry:](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/55H3DT5CCL73HLMQJ6DK63KCAHZWO7SX/)

- [Ubuntu 23.10 new app manager:](https://discourse.ubuntu.com/t/pulse-13-ubuntu-desktop-engineering-update/36611)

- [blendOS 3:](https://blendos.co/blend-os-v3/)

- [Release Phosh 0.29:](https://phosh.mobi/releases/rel-0.29.0/)

- [Q4OS 5.2:](https://www.q4os.org/blog.html)

- [Release of Solus 4.4:](https://getsol.us/2023/07/08/solus-4-4-released/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
