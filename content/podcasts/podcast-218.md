---
title: "Full Circle Weekly News 218"
date: 2021-07-14
draft: false
tags:
  - "21.10"
  - "browser"
  - "dbms"
  - "easynas"
  - "firefox"
  - "kernel"
  - "Linux"
  - "mariadb"
  - "mint"
  - "mozilla"
  - "nas"
  - "network"
  - "nextcloud"
  - "nginx"
  - "openvms"
  - "patch"
  - "proxmox"
  - "snoop"
  - "systemd"
  - "tor"
  - "Ubuntu"
  - "virtuozzo"
  - "zstd"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20218.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The second edition of patches for the Linux kernel with support for Rust](https://lkml.org/lkml/2021/7/4/171)

- [Release of Virtuozzo Linux 8.4](https://www.virtuozzo.com/blog-review/details/blog/view/virtuozzo-vzlinux-84-now-available.html)

- [OpenVMS operating system for x86-64 architecture](https://vmssoftware.com/about/openvmsv9-1/)

- [Nextcloud Hub 22 Collaboration Platform Available](https://nextcloud.com/blog/nextcloud-hub-22-introduces-approval-workflows-integrated-knowledge-management-and-decentralized-group-administration/)

- [Tor Browser 10.5 released](https://blog.torproject.org/new-release-tor-browser-105)

- [Ubuntu 21.10 switches to using zstd algorithm for compressing deb packages](https://balintreczey.hu/blog/hello-zstd-compressed-debs-in-ubuntu/)

- [Mozilla stops development of Firefox Lite browser](https://support.mozilla.org/en-US/kb/end-support-firefox-lite)

- [Nginx 1.21.1 released](https://mailman.nginx.org/pipermail/nginx-announce/2021/000304.html)

- [Release of Proxmox VE 7.0](https://forum.proxmox.com/threads/proxmox-ve-7-0-released.92007/)

- [Systemd 249 system manager released](https://lists.freedesktop.org/archives/systemd-devel/2021-July/046672.html)

- [Release of Linux Mint 20.2](http://blog.linuxmint.com/)

- [Stable release of MariaDB 10.6 DBMS](https://mariadb.com/kb/en/mariadb-1063-release-notes/)

- [Snoop 1.3.0](https://github.com/snooppr/snoop/releases/tag/V1.3.0_10_July_2021)

- [Release of EasyNAS 1.0 network storage](https://easynas.org/2021/07/10/easynas-1-0/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
