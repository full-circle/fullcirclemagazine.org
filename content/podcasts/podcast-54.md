---
title: "Full Circle Weekly News 54"
date: 2017-03-04
draft: false
tags:
  - "ar"
  - "augmented"
  - "canonical"
  - "convergence"
  - "features"
  - "friendlyelec"
  - "kernel"
  - "linux-libre"
  - "mobile-world-congress"
  - "mwc"
  - "nano"
  - "pi"
  - "plus"
  - "raspberry"
  - "reality"
  - "robots"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2054.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu-Powered Robots and Augmented Reality Helmets to Be Showcased at MWC 2017](http://news.softpedia.com/news/ubuntu-powered-robots-and-augmented-reality-helmets-to-be-showcased-at-mwc-2017-513212.shtml)

- [Canonical wins award for convergence efforts](https://betanews.com/2017/02/28/canonical-ubuntu-linux-canonical-award-convergence/)

- [GNU Linux-libre 4.10 Kernel Officially Released for Users Who Want 100% Freedom](http://news.softpedia.com/news/gnu-linux-libre-4-10-kernel-officially-released-for-users-who-want-100-freedom-513380.shtml)

- [FriendlyElec releases Ubuntu Linux-ready NanoPi M1 Plus -- a $30 Raspberry Pi killer](https://betanews.com/2017/03/02/friendlyelec-linux-debian-ubuntu-nanopi-m1-plus-raspberry-pi/)

- [Linux Kernel 4.10 Released With New Features And Updated Drivers](https://fossbytes.com/linux-kernel-4-10-released-new-features/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
