---
title: "Full Circle Weekly News 258"
date: 2022-04-23
draft: false
tags:
  - "beta"
  - "browser"
  - "ceno"
  - "endeavor"
  - "endeavoros"
  - "fedora"
  - "framework"
  - "linux"
  - "lxd"
  - "matrix"
  - "microdnf"
  - "mx"
  - "openssh"
  - "raspberry"
  - "rhvoice"
  - "sftp"
  - "shell"
  - "speech"
  - "suse"
  - "thunderbird"
  - "turnkey"
  - "vim"

cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20258.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [New Release of Raspberry Pi OS:](https://www.raspberrypi.com/news/raspberry-pi-bullseye-update-april-2022/)

- [Thunderbird 100 beta release integrates Matrix protocol support:](https://www.thunderbird.net/en-US/thunderbird/100.0beta/releasenotes/)

- [Custom Material Shell 42:](https://github.com/material-shell/material-shell/releases/tag/42)

- [MX Linux release 21.1:](https://mxlinux.org/)

- [Release of OpenSSH 9.0 with transfer of scp to the SFTP protocol:](https://lists.mindrot.org/pipermail/openssh-unix-dev/2022-April/040174.html)

- [Minimal web browser links 2.26:](http://links.twibright.com/)

- [Utilities for managing SSD drives - nvme-cli 2.0:](https://github.com/linux-nvme/nvme-cli/releases/tag/v2.0)

- [EndeavorOS 22.1 released:](https://endeavouros.com/)

- [RHVoice 1.8.0 speech synthesizer released:](https://rhvoice.org/post/rhvoice-1.8.0/)

- [Release of LXD 5.0:](https://discuss.linuxcontainers.org/t/lxd-5-0-lts-has-been-released/13723)

- [Qt 6.3 framework released:](https://www.qt.io/blog/qt-6.3-released)

- [Progress in the development of open firmware for the Raspberry Pi:](https://github.com/librerpi)

- [Plans for the next generation of SUSE Linux:](https://lists.opensuse.org/archives/list/project@lists.opensuse.org/thread/N6TTE7ZBY7GFJ27XSDTXRF3MVLF6HW4W/)

- [Release of Turnkey Linux 17:](https://www.turnkeylinux.org/blog/v17.0-stable-core-and-tkldev)

- [CENO 1.4.0 censorship-focused web browser available:](https://github.com/censorship-no/ceno-browser/releases/tag/v1.4.0)

- [Fedora plans to replace DNF package manager with Microdnf:](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/JIRMSN6FKBAQQMHXYCWJZM4RG4T7N4K2/)

- [Release of Neovim 0.7.0, a modernized version of the Vim editor:](https://github.com/neovim/neovim/releases/tag/v0.7.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
