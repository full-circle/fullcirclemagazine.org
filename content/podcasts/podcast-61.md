---
title: "Full Circle Weekly News 61"
date: 2017-05-13
draft: false
tags:
  - "canonical"
  - "china"
  - "computer"
  - "computing"
  - "cyberattack"
  - "debian"
  - "fedora"
  - "fuchsia"
  - "gnu"
  - "google"
  - "ipo"
  - "linux"
  - "nhs"
  - "os"
  - "quantum"
  - "ransomware"
  - "store"
  - "suse"
  - "ubuntu"
  - "unity"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2061.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical starts IPO path](http://www.zdnet.com/article/canonical-starts-ipo-path/)

- [World’s First Quantum Computer Made By China — 24,000 Times Faster Than International Counterparts](https://fossbytes.com/worlds-first-quantum-computer-made-by-china/)

- [NHS cyberattack is 'biggest ransomware outbreak in history'](http://www.wired.co.uk/article/nhs-cyberattack-ransomware-security)

- [Debian GNU/Linux 8.8 Released With Tons Of Updates And Fixes](https://fossbytes.com/debian-gnu-linux-8-8-released-features-download-upgrade/)

- [Google’s “Fuchsia” smartphone OS dumps Linux, has a wild new UI](https://arstechnica.com/gadgets/2017/05/googles-fuchsia-smartphone-os-dumps-linux-has-a-wild-new-ui/)

- [Ubuntu, SUSE Linux, and Fedora are all coming to the Windows Store](https://www.theverge.com/circuitbreaker/2017/5/11/15625320/ubuntu-suse-linux-fedora-windows-store-microsoft-build-2017)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
