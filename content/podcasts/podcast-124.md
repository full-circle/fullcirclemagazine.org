---
title: "Full Circle Weekly News 124"
date: 2019-03-16
draft: false
tags:
  - "14-04"
  - "16-04"
  - "18-04"
  - "blockchain"
  - "canonical"
  - "document"
  - "encryption"
  - "kde"
  - "kernel"
  - "kubernetes"
  - "linux-lite"
  - "lts"
  - "maru"
  - "onlyoffice"
  - "ota-8"
  - "ota8"
  - "plasma"
  - "pureos"
  - "ransomware"
  - "torvalds"
  - "touch"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20124.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical Preps Emergency Point Releases for Ubuntu 16.04 LTS & Ubuntu 14.04 LTS](https://news.softpedia.com/news/canonical-preps-emergency-point-releases-for-ubuntu-16-04-lts-ubuntu-14-04-lts-525081.shtml)

- [ONLYOFFICE Announces Blockchain-Based End-to-End Document Encryption](https://news.softpedia.com/news/onlyoffice-announces-blockchain-based-end-to-end-document-encryption-525105.shtml)

- [KDE Plasma 5.15.2 Desktop Environment Released with 23 Bug Fixes, Update Now](https://news.softpedia.com/news/kde-plasma-5-12-2-desktop-environment-released-with-23-bug-fixes-update-now-525103.shtml)

- [Linux Lite 4.4 Slated for Release on April 1st, Based on Ubuntu 18.04.2 LTS](https://news.softpedia.com/news/linux-lite-4-4-slated-for-release-on-april-1st-based-on-ubuntu-18-04-2-lts-525118.shtml)

- [Ubuntu Touch OTA-8 Released for Ubuntu Phones with Multiple Improvements](https://news.softpedia.com/news/ubuntu-touch-ota-8-released-for-ubuntu-phones-with-multiple-improvements-525198.shtml)

- [Maru OS 0.6 brings updated Android/Linux convergence to more phones](https://www.slashgear.com/maru-os-0-6-brings-updated-android-linux-convergence-to-more-phones-06568945/)

- [PureOS: One Linux for both PCs and smartphones](https://www.zdnet.com/article/pureos-one-linux-for-both-pcs-and-smartphones/)

- [Canonical Improves Security and Robustness of Ubuntu Kubernetes with Containerd](https://news.softpedia.com/news/canonical-improves-security-and-robustness-of-ubuntu-kubernetes-with-containerd-525140.shtml)

- [Canonical Releases New Linux Kernel Security Update for Ubuntu 18.04 LTS](https://news.softpedia.com/news/canonical-releases-new-linux-kernel-security-update-for-ubuntu-18-04-lts-525195.shtml)

- [Linux 5.0 “Shy Crocodile” Arrives With Google’s Adiantum Encryption](https://www.howtogeek.com/406737/linux-5.0-shy-crocodile-arrives-with-googles-adiantum-encryption/)

- [Linus Torvalds pulls pin, tosses in grenade: x86 won, forget about Arm in server CPUs, says Linux kernel supremo](https://www.theregister.co.uk/2019/02/23/linus_torvalds_arm_x86_servers/)

- [B0r0nt0K Ransomware Wants $75,000 Ransom, Infects Linux Servers](https://www.bleepingcomputer.com/news/security/b0r0nt0k-ransomware-wants-75-000-ransom-infects-linux-servers/)
