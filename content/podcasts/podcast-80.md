---
title: "Full Circle Weekly News 80"
date: 2018-02-12
draft: false
tags:
  - "4-15"
  - "6-0"
  - "attack"
  - "china"
  - "chinese"
  - "coreos"
  - "crypto"
  - "firefox"
  - "government"
  - "hat"
  - "intel"
  - "libre"
  - "libreoffice"
  - "linux"
  - "meltdown"
  - "miner"
  - "monero"
  - "office"
  - "red"
  - "redhat"
  - "tool"
  - "us"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2080.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Firefox Addon Injects Monero Miner Source](https://www.bleepingcomputer.com/news/security/image-previewer-first-firefox-addon-that-injects-an-in-browser-miner/)

- [LibreOffice 6.0 Released With New Features Source](https://fossbytes.com/libreoffice-6-0-released-features-download/)

- [Red Hat buys CoreOS Source](http://news.softpedia.com/news/red-hat-buys-coreos-for-250m-to-expand-its-kubernetes-and-containers-leadership-519641.shtml)

- [Intel Disclosed Meltdown Bugs to Chinese Compaines before the US Govrenment Source](https://fossbytes.com/intel-disclosed-meltdown-bugs-to-chinese-companies-before-the-us-government/)

- [Good and Bad News about Linux 4.15 Source](http://www.zdnet.com/article/linux-4-15-good-news-and-bad-news-about-meltdown-and-spectre/)

- [Meltdown Attack Monitoring Tool Source](https://www.linuxinsider.com/story/Free-Linux-Tool-Monitors-Systems-for-Meltdown-Attacks-85094.html)
