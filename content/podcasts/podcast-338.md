---
title: "Full Circle Weekly News 338"
date: 2023-11-05
draft: false
tags:
  - "qbittorrent"
  - "agama"
  - "nginx"
  - "truenas"
  - "mysql"
  - "cinnamon"
  - "wayland"
  - "genode"
  - "kde"
  - "ubuntu"
  - "dagor"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20338.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of qBittorrent 4.6 with I2P support:](https://www.qbittorrent.org/news.php)

- [alternative installer Agama 5:](https://github.com/openSUSE/agama/releases/tag/v5)

- [Ubuntu LTS release support time increased to 10 years:](https://canonical.com/blog/linux-kernel-lts)

- [Release nginx 1.25.3, njs 0.8.2 and NGINX Unit 1.31.1:](http://nginx.org/#2023-10-24)

- [Release of TrueNAS SCALE 23.10:](https://www.truenas.com/blog/truenas-scale-23-10-is-released/)

- [MySQL DBMS available 8.2.0:](https://dev.mysql.com/downloads/mysql/)

- [The first Cinnamon porting results on Wayland:](https://blog.linuxmint.com/?p=4591)

- [Project Genode published OS Sculpt 23.10:](https://genode.org/news/sculpt-os-release-23.10)

- [KDE implements support for Wayland extensions for colour management:](https://pointieststick.com/2023/10/27/these-past-2-weeks-in-kde-wayland-color-management-the-desktop-cube-returns-and-optional-shadows-in-spectacle/)

- [Canoeboot (GNUboot?):](https://libreboot.org/news/canoeboot.html)

- [The Dagor game engine:](https://github.com/GaijinEntertainment/DagorEngine)


**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
