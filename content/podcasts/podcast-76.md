---
title: "Full Circle Weekly News 76"
date: 2018-01-14
draft: false
tags:
  - "android"
  - "eelo"
  - "freespire"
  - "kernel"
  - "leaking"
  - "linspire"
  - "linux"
  - "manjaro"
  - "meltdown"
  - "spectre"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2076.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

Some changes to the **Full Circle Weekly News**.

We have **Wayne** and **Joe** taking over as hosts of the FCWN. Big thanks to them for taking over as I (Ronnie) was finding it difficult to record a show each week.

- [Manjaro Linux 17.1.0 Released With Latest Packages](https://fossbytes.com/manjaro-linux-17-1-0-released-features-download/)

- ['Kernel memory leaking' Intel processor design flaw forces Linux, Windows redesign](https://www.theregister.co.uk/2018/01/02/intel_cpu_design_flaw/)

- [Black Lab Software Releases Freespire 3.0 & Linspire 7.0 Linux Operating Systems](http://news.softpedia.com/news/black-lab-software-releases-freespire-3-0-linspire-7-0-linux-operating-systems-519189.shtml)

- [Some Android games are quietly using your microphone to track your TV habits](https://www.theverge.com/2018/1/2/16842294/android-apps-microphone-access-listening-tv-habits)

- [Linux Kernels 4.14.11, 4.9.74, 4.4.109, 3.16.52, and 3.2.97 Patch Meltdown Flaw](http://news.softpedia.com/news/linux-kernels-4-14-11-4-9-74-4-4-109-3-16-52-and-3-2-97-patch-meltdown-flaw-519215.shtml)

- [eelo is a Google-free alternative for people focused on privacy](https://www.androidauthority.com/eelo-android-privacy-google-827275/)

**CREDITS**

Your hosts: "Wayne Out There" and "Joe In Here"
