---
title: "Full Circle Weekly News 227"
date: 2021-09-13
draft: false
tags:
  - "anastasis"
  - "antivirus"
  - "bullseye"
  - "clamav"
  - "debian"
  - "distro"
  - "encryption"
  - "finnix"
  - "frameworks"
  - "ghostbsd"
  - "gnu"
  - "gpu"
  - "kde"
  - "lakka"
  - "media"
  - "nvidia"
  - "openssl"
  - "pipewire"
  - "plasma"
  - "retroarch"
  - "server"
  - "tails"
  - "tarantool"
  - "wayland"
  - "whonix"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20227.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Lakka 3.4 and RetroArch 1.9.9 emulators](https://lakka.tv/articles/2021/09/06/lakka-3.4/)

- [ClamAV 0.104 Free Antivirus](https://blog.clamav.net/2021/09/clamav-01040-released.html)

- [Finnix 123 Linux Distro for System Administrators Is Out Based on Debian Bullseye](https://9to5linux.com/finnix-123-linux-distro-for-system-administrators-is-out-based-on-debian-bullseye)

- [Tarantool 2.8 released](https://github.com/tarantool/tarantool/releases/2.8.2)

- [Whonix 16 available](https://forums.whonix.org/t/whonix-16-0-1-7-kvm-debian-11-bullseye-based-major-stable-release/12264)

- [OpenSSL 3.0.0 Released](https://www.openssl.org/blog/blog/2021/09/07/OpenSSL3.Final/)

- [GhostBSD Release 21.09.06](http://ghostbsd.org/ghostbsd_21.09.06_iso_now_available)

- [Tails 4.22 released](https://tails.boum.org/news/version_4.22/index.en.html)

- [GNU Anastasis, a toolkit for backing up encryption keys](https://lists.gnu.org/archive/html/info-gnu/2021-09/msg00004.html)

- [PipeWire Media Server 0.3.35 Released](https://gitlab.freedesktop.org/pipewire/pipewire/-/releases/0.3.35)

- [KDE Frameworks 5.86 Improves Plasma Wayland for NVIDIA GPUs, Adds over 200 Changes](https://9to5linux.com/kde-frameworks-5-86-improves-plasma-wayland-for-nvidia-gpus-adds-over-200-changes)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
