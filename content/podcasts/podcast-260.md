---
title: "Full Circle Weekly News 260"
date: 2022-05-08
draft: false
tags:
  - "archinstall"
  - "bios"
  - "console"
  - "cosmic"
  - "elkhart"
  - "fedora"
  - "firmware"
  - "genode"
  - "github"
  - "gnunet"
  - "hare"
  - "kde"
  - "intel"
  - "mesa"
  - "messenger"
  - "mobile"
  - "opengl"
  - "plasma"
  - "pop!"
  - "programming"
  - "redis"
  - "redox"
  - "rust"
  - "spotify"
  - "steam"
  - "warcraft"
  - "wolfire"
  - "xpdf"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20260.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Mesa's Rust implementation of OpenCL now supports OpenCL 3.0:](https://twitter.com/neilt3d/status/1517541101648891906)

- [Intel Opens PSE Block Firmware Code for Elkhart Lake Chips:](https://github.com/intel/pse-fw)

- [DBMS libmdbx 0.11.7 Move Development to GitFlic After Lockdown on GitHub:](https://gitflic.ru/project/erthink/libmdbx)

- [Wolfire open source game Overgrowth:](https://github.com/WolfireGames/overgrowth)

- [The Warsmash project develops an alternative open source game engine for Warcraft III:](https://github.com/Retera/WarsmashModEngine/tree/experimental)

- [Release of GNUnet Messenger 0.7 and libgnunetchat 0.1 to create decentralized chats:](https://www.gnunet.org/en/news/2022-04-libgnunetchat-0.1.0.html)

- [The system programming language Hare:](https://harelang.org/blog/2022-04-25-announcing-hare/)

- [Updating the Steam OS distribution used on the Steam Deck gaming console:](https://store.steampowered.com/news/app/1675200/view/3216142491801797532)

- [Spotify donates 100,000 euros in open source awards:](https://engineering.atspotify.com/2022/04/announcing-the-spotify-foss-fund/)

- [Xpdf 4.04 released:](https://www.xpdfreader.com/)

- [Release of the Pop!\_OS 22.04 with the COSMIC desktop:](https://blog.system76.com/post/682519660741148672/popos-2204-lts-has-landed)

- [Technical committee rejects plan to end BIOS support in Fedora:](https://lists.fedoraproject.org/archives/list/meetingminutes@lists.fedoraproject.org/message/BD5Y5MYCAMHZQPQ4ZXR6IVHV6TIXAYT2/)

- [Release of the Archinstall 2.4:](https://github.com/archlinux/archinstall/releases/tag/v2.4.1)

- [KDE Plasma Mobile 22.04 Mobile Platform Available:](https://plasma-mobile.org/2022/04/26/plasma-mobile-gear-22-04/)

- [Redis 7.0 released:](https://github.com/redis/redis/releases/tag/7.0.0)

- [Mozilla Common Voice 9.0 Voice Update:](https://discourse.mozilla.org/t/common-voice-dataset-v-9/96539)

- [The Genode Project has published the Sculpt 22.04 General Purpose OS:](https://genode.org/news/sculpt-os-release-22.04)

- [Patent used to attack GNOME is invalidated:](https://blog.opensource.org/gnome-patent-troll-stripped-of-patent-rights/)

- [Release of the Redox OS 0.7 operating system written in Rust:](https://www.redox-os.org/news/release-0.7.0/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
