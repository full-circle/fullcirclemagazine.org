---
title: "Full Circle Weekly News 153"
date: 2019-11-12
draft: false
tags:
  - "19-10"
  - "android"
  - "bug"
  - "canonical"
  - "debian"
  - "desktop"
  - "ghost"
  - "gnome"
  - "kernel"
  - "linux"
  - "opensuse"
  - "ota-11"
  - "ota11"
  - "patch"
  - "patent"
  - "samsung"
  - "security"
  - "suse"
  - "tails"
  - "touch"
  - "troll"
  - "ubports"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20153.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [GNOME Files Defense Against Patent Troll](https://www.gnome.org/news/2019/10/gnome-files-defense-against-patent-troll/)

- [The Debian Project stands with the GNOME Foundation](https://bits.debian.org/2019/10/gnome-foundation-defense-patent-troll.html)

- [Vote on openSUSE Project name change](https://en.opensuse.org/openSUSE:Project_name_change_vote)

- [Tails 4.0 Is Out](https://tails.boum.org/news/version_4.0/index.en.html)

- [Ghost 3.0 Released](https://itsfoss.com/ghost-3-release/)

- [Samsung Discontinues Linux On DeX Starting With Android 10](https://fossbytes.com/samsung-discontinues-linux-on-dex-android-10/)

- [Unpatched Linux bug may open devices to serious attacks over Wi-Fi](https://arstechnica.com/information-technology/2019/10/unpatched-linux-flaw-may-let-attackers-crash-or-compromise-nearby-devices/)

- [Canonical Has a New Director of Ubuntu Desktop](https://www.omgubuntu.co.uk/2019/10/canonical-has-a-new-ubuntu-desktop-director)

- [Ubuntu 19.10 Gets First Linux Kernel Security Patch](https://news.softpedia.com/news/ubuntu-19-10-eoan-ermine-gets-first-linux-kernel-security-patch-update-now-527934.shtml)

- [Ubuntu Touch OTA-11 Released](https://ubports.com/blog/ubports-blog-1/post/ubuntu-touch-ota-11-release-252)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
