---
title: "Full Circle Weekly News 342"
date: 2023-12-03
draft: false
tags:
  - "vulkan"
  - "fedora"
  - "freebsd"
  - "rocky"
  - "owncast"
  - "endeavouros"
  - "claws"
  - "mail"
  - "archinstaller"
  - "arch"
  - "opensuse"
  - "proxmox"
  - "openmandriva"
  - "mandriva"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20342.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [NVK supports Vulkan 1.0:](https://www.collabora.com/news-and-blog/news-and-events/nvk-reaches-vulkan-conformance.html)

- [Fedora 40 plans to enable system service isolation:](https://lists.fedoraproject.org/archives/list/devel-announce@lists.fedoraproject.org/thread/ICCQ4GTH74UCR4LY3LWLOTTKW3RWKBMX/)

- [Release of FreeBSD 14.0:](https://www.freebsd.org/releases/14.0R/announce/)

- [Rocky Linux 9.3:](https://rockylinux.org/news/rocky-linux-9-3-ga-release/)

- [Release of owncast 0.1.2:](https://github.com/owncast/owncast/releases/tag/v0.1.2)

- [EndeavourOS 23.11:](https://endeavouros.com/news/slimmer-options-but-lean-and-in-a-new-live-environment-galileo-has-arrived/)

- [New versions of Claws Mail 3.20.0 and 4.2.0:](https://lists.claws-mail.org/pipermail/users/2023-November/032391.html)

- [Release of Archinstaller 2.7:](https://github.com/archlinux/archinstall/releases/tag/v2.7.0)

- [Qt Creator 12:](https://www.qt.io/blog/qt-creator-12-released)

- [The openSUSE project chooses a new logo:](https://news.opensuse.org/2023/11/23/selecting-the-new-face-of-os-is-underway/)

- [Release of Proxmox VE 8.1:](https://forum.proxmox.com/threads/proxmox-ve-8-1-released.136959/)

- [Release of MPV 0.37:](https://github.com/mpv-player/mpv/releases/tag/v0.37.0)

- [Release of Radix cross Linux 1.9.226:](https://radix.pro/platform/install/)

- [Release of OpenMandriva Lx 5.0:](https://www.openmandriva.org/en/news/article/openmandriva-lx-5-0-released)

- [PipeWire reaches 1.0.0:](https://github.com/PipeWire/pipewire/blob/master/NEWS)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
