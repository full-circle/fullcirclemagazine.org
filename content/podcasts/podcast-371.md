---
title: "Full Circle Weekly News 371"
date: 2024-06-23
draft: false
tags:
  - "icewm"
  - "parrot"
  - "perl"
  - "winlator"
  - "webos"
  - "systemd"
  - "opensuse"
  - "selks"
  - "ipfire"
  - "posix"
  - "kde"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20371.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Parrot 6.1:](https://parrotsec.org/blog/2024-06-05-parrot-6.1-release-notes/)

- [Perl 5.40.0 available:](https://www.nntp.perl.org/group/perl.perl5.porters/2024/06/msg268252.html)

- [Release of Winlator 7.0:](https://github.com/brunodev85/winlator/releases/tag/v7.0.0)

- [webOS Open Source Edition 2.26:](https://www.webosose.org/blog/2024/06/05/webos-ose-2-26-0-release/)

- [Release of systemd system manager 256 with run0 utility to replace sudo:](https://lists.freedesktop.org/archives/systemd-devel/2024-June/050407.html)

- [Release of openSUSE Leap 15.6:](https://news.opensuse.org/2024/06/12/leap-unveils-choices-for-users/)

- [SELKS 10 is available:](https://www.stamus-networks.com/pr/13-june-2024)

- [Releases of IPFire 2.29 Core 186:](https://www.ipfire.org/blog/ipfire-2-29-core-update-186-released)

- [POSIX 1003.1-2024 standard approved:](https://www.opengroup.org/austin/)

- [New UI Design Guidelines for KDE Applications:](https://pointieststick.com/2024/06/14/this-week-in-kde-final-plasma-6-1-polishing-and-new-features-for-6-2/)

- [Release of the IceWM 3.6.0:](https://github.com/ice-wm/icewm/releases/tag/3.6.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
