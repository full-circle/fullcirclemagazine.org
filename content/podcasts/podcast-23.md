---
title: "Full Circle Weekly News 23"
date: 2016-06-25
draft: false
tags:
  - "ai"
  - "amazon"
  - "android"
  - "aws"
  - "azure"
  - "china"
  - "chinese"
  - "chips"
  - "deb"
  - "flatpak"
  - "git"
  - "programming"
  - "rpm"
  - "services"
  - "snap"
  - "snapd"
  - "supercomputer"
  - "udacity"
  - "web"
  - "website"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2023.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux’s RPM/deb split could be replaced by Flatpak vs. snap](http://arstechnica.com/information-technology/2016/06/here-comes-flatpak-a-competitor-to-ubuntus-cross-platform-linux-apps/)

- [China builds world’s fastest supercomputer without U.S. chips - and it runs Linux](http://www.computerworld.com/article/3085483/high-performance-computing/china-builds-world-s-fastest-supercomputer-without-u-s-chips.html)

- [Google and Udacity launch a new Android programming course for beginners](https://techcrunch.com/2016/06/22/google-and-udacity-launch-a-new-android-programming-course-for-beginners/?ncid=mobilenavtrend)

- [Git 2.9 Released](http://www.linuxjournal.com/content/git-29-released)

- [Now You Can Use Artificial Intelligence to Build Your Website](http://www.cmswire.com/web-cms/now-you-can-use-artificial-intelligence-to-build-your-website/)

- [Amazon Web Services Expanding With Artificial Intelligence](http://www.gurufocus.com/news/422183/amazon-web-services-expanding-with-artificial-intelligence)

- [Nearly 1 in 3 Azure virtual machines now run Linux](http://www.cio.com/article/3087875/cloud-computing/one-third-of-azure-virtual-machines-now-run-linux.html)

- [Canonical Announces Snapd 2.0.9 with Full Snap Confinement on elementary OS 0.4](http://news.softpedia.com/news/canonical-announces-snapd-2-0-9-with-full-snap-confinement-on-elementary-os-0-4-505596.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
