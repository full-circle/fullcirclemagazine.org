---
title: "Full Circle Weekly News 328"
date: 2023-08-26
draft: false
tags:
  - "vim"
  - "ubuntu"
  - "store"
  - "devuan"
  - "gnu"
  - "ngnix"
  - "fheroes"
  - "crossover"
  - "loongarch"
  - "debian"
  - "clamav"
  - "sysvinit"
  - "siduction"
  - "suse"
  - "qutebrowser"
  - "budgie"
  - "freebsd"
  - "bazzite"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20328.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Midnight Commander 4.8.30](https://github.com/MidnightCommander/mc/releases/tag/4.8.30)

- [Vim future:](https://groups.google.com/g/vim_dev/c/dq9Wu5jqVTw)

- [Promoting the new Ubuntu Store app manager:](https://discourse.ubuntu.com/t/an-update-on-app-store-ubuntu-store/37770)

- [Release of Devuan 5.0:](https://www.devuan.org/os/announce/daedalus-release-announce-2023-08-14)

- [Debian GNU/Linux is 30 years old:](http://groups.google.com/group/comp.os.linux.development/msg/a32d4e2ef3bcdcc6)

- [Release nginx 1.25.2:](http://nginx.org/#2023-08-15)

- [Release of fheroes2 - 1.0.7:](https://github.com/ihhub/fheroes2/releases/tag/1.0.7)
- [Release of fheroes2 - 1.0.7:](https://store.steampowered.com/app/2147380/Silence_of_the_Siren/)

- [Release of CrossOver 23.0 for Linux:](https://www.codeweavers.com/support/forums/announce/?t=24;msg=286453)

- [Debian added support for LoongArch architecture:](https://lists.debian.org/debian-devel-announce/2023/08/msg00000.html)

- [New version of ClamAV:](https://blog.clamav.net/2023/07/2023-08-16-releases.html)

- [Release of SysVinit 3.08:](https://lists.nongnu.org/archive/html/sysvinit-devel/2023-08/msg00000.html)

- [Release of Siduction 2023.1:](https://siduction.org/2023/08/release-notes-2023-1-0-standing-on-the-shoulders-of-giants/)

- [SUSE will be converted from public to private company:](https://www.suse.com/news/EQT-announces-voluntary-public-purchase-offer-and-intention-to-delist-SUSE/)

- [qutebrowser 3.0:](https://github.com/qutebrowser/qutebrowser/releases/tag/v3.0.0)

- [KDE trends:](https://pointieststick.com/2023/08/18/this-week-in-kde-double-click-by-default/)

- [Release of Budgie 10.8:](https://buddiesofbudgie.org/blog/budgie-10.8-released%22)

- [Accelerate FreeBSD:](https://cgit.freebsd.org/src/commit/?id=9a7add6d01f3c5f7eba811e794cf860d2bce131d)

- [Release Bazzite 1.0:](https://universal-blue.org/blog/2023/08/20/bazzite-10/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
