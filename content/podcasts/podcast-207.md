---
title: "Full Circle Weekly News 207"
date: 2021-04-25
draft: false
tags:
  - "2021"
  - "21-04"
  - "chrome"
  - "endeavouros"
  - "firmware"
  - "fix"
  - "infinitime"
  - "java"
  - "javascript"
  - "kernel"
  - "kuroko"
  - "linux"
  - "microsoft"
  - "minnesota"
  - "nginx"
  - "node-js"
  - "openbsd"
  - "openssh"
  - "openvpn"
  - "pinetime"
  - "server"
  - "tetris"
  - "tetris-os"
  - "toarus"
  - "ubuntu"
  - "university"
  - "vulnerability"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20207.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [EndeavourOS 2021.04.17](https://endeavouros.com/news/our-april-release-is-available/)

- [OpenSSH 8.6 Release with Vulnerability fix](https://lists.mindrot.org/pipermail/openssh-unix-dev/2021-April/039306.html)

- [Nginx 1.20.0 released](http://nginx.org/)

- [Node.js 16.0 JavaScript Server Platform Released](https://nodejs.org/en/blog/release/v16.0.0/)

- [Tetris-OS - you guessed it](https://github.com/jdah/tetris-os)

- [University of Minnesota Suspended from Linux Kernel Development after Submitting Questionable Patches](https://lkml.org/lkml/2021/4/21/143)

- [OpenVPN 2.5.2 and 2.4.11 update](https://github.com/OpenVPN/openvpn/releases/tag/v2.5.2)

- [Microsoft begins testing support for running Linux GUI applications on Windows](https://devblogs.microsoft.com/commandline/the-initial-preview-of-gui-app-support-is-now-available-for-the-windows-subsystem-for-linux-2/)

- [Ubuntu 21.04 Distribution Release](https://releases.ubuntu.com/21.04/)

- [Chrome OS 90 released](https://chromereleases.googleblog.com/2021/04/stable-channel-update-for-chrome-os_21.html)

- [OpenBSD adds initial support for RISC-V architecture](https://marc.info/?l=openbsd-cvs&m=161914575319702&w=2)

- [First version of InfiniTime, firmware for open PineTime smartwatches](https://www.pine64.org/2021/04/22/its-time-infinitime-1-0/)

- [ToaruOS 1.14](https://github.com/klange/toaruos/releases/tag/v1.14.0)

- [Kuroko 1.1 programming language](https://github.com/kuroko-lang/kuroko/releases/tag/v1.1.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
