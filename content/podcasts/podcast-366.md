---
title: "Full Circle Weekly News 366"
date: 2024-05-19
draft: false
tags:
  - "gimp"
  - "peertube"
  - "gnome"
  - "pacstall"
  - "nebula"
  - "daphile"
  - "xpra"
  - "gittuf"
  - "eurolinux"
  - "vcmi"
  - "pingora"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20366.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of GIMP 2.10.38:](https://www.gimp.org/news/2024/05/05/gimp-2-10-38-released/)

- [Release of PeerTube 6.1:](https://joinpeertube.org/news/release-6.1)

- [The GNOME Project has published its financial report for 2023:](https://foundation.gnome.org/2024/05/07/2023-annual-report/)

- [Release of pacstall 5.0:](https://github.com/pacstall/pacstall/releases)

- [Release of Nebula 1.9:](https://github.com/slackhq/nebula/releases/tag/v1.9.0)

- [Daphile 24.05:](https://www.daphile.com/)

- [Release of Xpra 6.0:](https://github.com/Xpra-org/xpra/releases/tag/v6.0)

- [The gittuf project is at 0.4:](https://github.com/gittuf/gittuf/releases/tag/v0.4.0)

- [Release of EuroLinux 9.4:](https://docs.euro-linux.com/release-notes/9.4/)

- [Release of VCMI 1.5.0:](https://vcmi.eu/blog/2024/05/10/vcmi-150-released/)

- [Release of Pingora 0.2:](https://github.com/cloudflare/pingora/releases)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
