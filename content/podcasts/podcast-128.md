---
title: "Full Circle Weekly News 128"
date: 2019-04-29
draft: false
tags:
  - "14-04"
  - "19-04"
  - "19-10"
  - "5-0"
  - "apache"
  - "calamares"
  - "gnome"
  - "gpu"
  - "installer"
  - "kernel"
  - "kylin"
  - "lubuntu"
  - "lxqt"
  - "manjaro"
  - "netbeans"
  - "netrunner"
  - "nvidia"
  - "office"
  - "openjdk"
  - "pop_os"
  - "red-hat"
  - "redhat"
  - "rolling"
  - "skywalking"
  - "snapshot"
  - "system76"
  - "ubuntu"
  - "wps"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20128.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 14.04 Reaches End of Life on April 30](https://www.omgubuntu.co.uk/2019/04/ubuntu-14-04-end-of-life)

- [Ubuntu 19.04 comes refreshed with the Linux 5.0 kernel](https://www.zdnet.com/article/ubuntu-19-04-comes-refreshed-with-the-linux-5-0-kernel/)

- [Ubuntu 19.04 Delivers A Welcome Surprise For Nvidia GPU Owners ](https://www.forbes.com/sites/jasonevangelho/2019/04/15/ubuntu-19-04-delivers-a-welcome-surprise-for-nvidia-gpu-owners/#724a2e8b3c93)

- [Ubuntu Kylin 19.04 Adds a New Visual Experience, Latest WPS Office Suite ]([https://news.softpedia.com/news/ubuntu-kylin-19-04-adds-a-new-visual-experience-latest-wps-office-suite-525754.shtml)

- [Lubuntu 19.04 Released with Latest LXQt Desktop and Calamares Installer ](https://news.softpedia.com/news/lubuntu-19-04-released-with-latest-lxqt-desktop-and-latest-calamares-installer-525772.shtml)

- [System76 Releases Pop!\_OS 19.04 for Its Linux PCs, Based on Ubuntu 19.04 ](https://news.softpedia.com/news/system76-releases-pop-os-19-04-for-its-linux-pcs-based-on-ubuntu-19-04-525773.shtml)

- [Ubuntu 19.10 Development Has Started — Daily Build ISO Images Now Available ]([https://fossbytes.com/ubuntu-19-10-features-release-date-download-codename/)

- [Netrunner Rolling 2019.04 Delivers A Manjaro Linux-based Polished Desktop ](https://fossbytes.com/netrunner-rolling-2019-04-manjaro-linux/)

- [NetBeans and SkyWalking as new top-level projects of the Apache Foundation ](https://www.pro-linux.de/news/1/27002/netbeans-und-skywalking-als-neue-top-level-projekte-der-apache-foundation.html)

- [GNOME 3.34 Desktop Environment Development Kicks Off with First Snapshot ](https://news.softpedia.com/news/gnome-3-34-desktop-environment-development-kicks-off-with-first-snapshot-525791.shtml)

- [Leadership of OpenJDK 8 and OpenJDK 11 Transitions to Red Hat ](https://www.redhat.com/en/about/press-releases/leadership-openjdk-8-and-openjdk-11-transitions-red-hat)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
