---
title: "Full Circle Weekly News 346"
date: 2023-12-30
draft: false
tags:
  - "kernel"
  - "driver"
  - "rust"
  - "vulkan"
  - "video"
  - "qubes"
  - "zulip"
  - "dietpi"
  - "ssh"
  - "i2p"
  - "qemu"
  - "zorin"
  - "rhino"
  - "tails"
  - "tor"
  - "lazarus"
  - "enlightenment"
  - "darktable"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20346.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The Linux 6.8 kernel is scheduled to include the first network driver in Rust:](https://git.kernel.org/pub/scm/linux/kernel/git/netdev/net-next.git/commit/?id=d6beb085e8ff3d9547df8a5a55f15ccc7552c5d0)

- [The radv Mesa-driver supports Vulkan extensions for h.265 video encoding:](https://airlied.blogspot.com/2023/12/radv-vulkan-video-encode-status.html)

- [Release of Qubes 4.2.0 OS:](https://www.qubes-os.org/news/2023/12/18/qubes-os-4-2-0-has-been-released/)

- [I-mail exchange platform Zulip 8:](https://blog.zulip.com/2023/12/15/zulip-8-0-released/)

- [DietPi 8.25:](https://dietpi.com/docs/releases/v8_25/)

- [Using SSH over a UNIX socket instead of sudo to get rid of suid files:](https://tim.siosm.fr/blog/2023/12/19/ssh-over-unix-socket/)

- [Release of the anonymous I2P 2.4.0:](https://geti2p.net/en/blog/post/2023/12/18/i2p-release-2.4.0)

- [Release of the QEMU 8.2:](https://lists.nongnu.org/archive/html/qemu-devel/2023-12/msg02695.html)

- [Release of Zorin OS 17:](https://blog.zorin.com/2023/12/20/zorin-os-17-has-arrived/)

- [Release of Rhino Linux 2023.4:](https://rhinolinux.org/news-10.html)

- [Release of Tails 5.21:](https://tails.net/news/version_5.21/index.en.html)

- [Release Lazarus 3.0 IDE:](https://forum.lazarus.freepascal.org/index.php/topic,65612.0.html)

- [Release of labwc 0.7, composite server for Wayland:](https://github.com/labwc/labwc/releases/tag/0.7.0)

- [Release of Enlightenment 0.26:](https://www.enlightenment.org/news/2022-12-23-enlightenment-0.26.0)

- [Release of Darktable 4.6:](https://www.darktable.org/2023/12/darktable-4.6.0-released/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
