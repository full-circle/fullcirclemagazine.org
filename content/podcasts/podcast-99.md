---
title: "Full Circle Weekly News 99"
date: 2018-07-15
draft: false
tags:
  - "amazon"
  - "beta"
  - "blender"
  - "centos"
  - "eff"
  - "elementary"
  - "email"
  - "encrypt"
  - "firefox"
  - "firefoxos"
  - "kaios"
  - "linux"
  - "lts"
  - "riot-im"
  - "security"
  - "starttls"
  - "wifi"
  - "youtube"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2099.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical releases new infographic to show how Ubuntu Linux 'connects everything'](https://betanews.com/2018/07/10/canonical-infographic-ubuntu-linux/)

- [Google has made a $22m investment in KaiOS, a smartphone operating system designed for low-power devices that originally forked from Firefox OS](http://www.trustedreviews.com/news/google-kaios-firefox-os-3497173)

- [Announcing Amazon Linux 2 with Long Term Support](https://aws.amazon.com/about-aws/whats-new/2018/06/announcing-amazon-linux-2-with-long-term-support/)

- [The Linux Mint desktop continues to lead the rest](https://www.zdnet.com/article/the-linux-mint-desktop-continues-to-lead-the-rest/)

- [Linux Releases: elementary OS 5.0 Beta And CentOS 6.10 Are Here](https://fossbytes.com/centos-6-10-elementary-os-juno-beta-released/)

- [Riot.im browser-based Matrix client gets 'reply' feature](https://riot.im/app/)

- [YouTube Blocks Blender Videos Worldwide](https://www.blender.org/media-exposure/youtube-blocks-blender-videos-worldwide/)

- [Bill to save net neutrality is 46 votes short in US House](https://arstechnica.com/tech-policy/2018/06/bill-to-save-net-neutrality-is-46-votes-short-in-us-house/)

- [EFF has released STARTTLS Everywhere: free tools to encrypt email between mail servers](https://www.eff.org/deeplinks/2018/06/announcing-starttls-everywhere-securing-hop-hop-email-delivery)

- [Wi-Fi security is starting to get its biggest upgrade in over a decade](https://www.theverge.com/circuitbreaker/2018/6/26/17501594/wpa3-wifi-security-certification)
