---
title: "Full Circle Weekly News 385"
date: 2024-09-29
draft: false
tags:
  - "linux"
  - "freebsd"
  - "opensearch"
  - "amd"
  - "llvm"
  - "valkey"
  - "gnome"
  - "swift"
  - "valve"
  - "proton"
  - "zorin"
  - "ubuntu"
  - "criu"
  - "kernel"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20385.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Linux kernel 6.11 released:](https://lkml.org/lkml/2024/9/15/282)

- [FreeBSD 13.4 Released:](https://www.freebsd.org/releases/13.4R/announce/)

- [OpenSearch, a fork of the Elasticsearch platform, is now under the wing of the Linux Foundation:](https://aws.amazon.com/blogs/opensource/aws-welcomes-the-opensearch-foundation/)

- [AMD Engineer Proposes Easier Linux Kernel Management of CPU Vulnerability Locks:](https://lore.kernel.org/lkml/20240912190857.235849-1-david.kaplan@amd.com/T/)

- [LLVM 19 Compiler Suite Released:](https://discourse.llvm.org/t/llvm-19-1-0-released/81285)

- [Valkey 8.0 has been released:](https://valkey.io/blog/valkey-8-ga/)

- [GNOME Desktop Environment Release 47:](https://foundation.gnome.org/2024/09/18/introducing-gnome-47/)

- [Release of nxs-data-anonymizer 1.11.0:](https://github.com/nixys/nxs-data-anonymizer)

- [Swift 6.0 on Linux:](https://swift.org/download/%23releases)

- [Valve has released Proton 9.0-3:](https://github.com/ValveSoftware/Proton/releases/tag/proton-9.0-3)

- [Zorin OS 17.2 Released:](https://blog.zorin.com/2024/09/19/zorin-os-17.2-has-landed/)

- [Ubuntu 24.10 Beta Release:](https://lists.ubuntu.com/archives/ubuntu-announce/2024-September/000306.html)

- [Release of CRIU 4.0:](https://github.com/checkpoint-restore/criu/releases/tag/v4.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
