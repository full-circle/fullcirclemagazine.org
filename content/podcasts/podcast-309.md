---
title: "Full Circle Weekly News 309"
date: 2023-04-16
draft: false
tags:
  - "thread"
  - "tux"
  - "paint"
  - "fedora"
  - "encryption"
  - "wayland"
  - "ppp"
  - "openshot"
  - "dnf"
  - "fonoster"
  - "twilio"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20309.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [RT-Thread 5.0:](https://github.com/RT-Thread/rt-thread/releases/tag/v5.0.0)

- [Release of Tux Paint 0.9.29:](https://tuxpaint.org/latest/tuxpaint-0.9.29-press-release.php)

- [Fedora FS encryption:](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/LYUABL7F2DENO7YIYVD6TRYLWBMF2CFI/)

- [Available Wayland 1.22:](https://lists.freedesktop.org/archives/wayland-devel/2023-April/042647.html)

- [ppp 2.5.0:](https://github.com/ppp-project/ppp/releases/tag/ppp-2.5.0)

- [Release of OpenShot 3.1:](https://www.openshot.org/blog/2023/04/06/new_openshot_release_310/)

- [Release of DNF 4.15:](https://github.com/rpm-software-management/dnf/releases/tag/4.15.0)

- [Release of Fonoster 0.4, open alternative to Twilio:](https://github.com/fonoster/fonoster/releases/tag/v0.4.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
