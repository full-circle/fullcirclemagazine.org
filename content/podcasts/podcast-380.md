---
title: "Full Circle Weekly News 380"
date: 2024-08-25
draft: false
tags:
  - "wcurl"
  - "frigate"
  - "hyprland"
  - "rebecca black"
  - "tails"
  - "minetest"
  - "mesa"
  - "debian"
  - "deepin"
  - "outertale"
  - "godot"
  - "outwiker"
  - "labwc"
  - "minios"
  - "russia"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20380.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [wcurl has been accepted into the Curl project:](https://daniel.haxx.se/blog/2024/08/08/curl-welcomes-wcurl-to-the-team/)

- [Release of Frigate 0.14:](https://frigate.video/)

- [Release of Hyprland 0.42:](https://hyprland.org/news/update42/)

- [RebeccaBlackOS 2024-08-12:](https://sourceforge.net/projects/rebeccablackos/files/2024-08-12/)

- [Release of Tails 6.6:](https://tails.net/news/version_6.6/)

- [Release of Minetest 5.9.0:](https://blog.minetest.net/2024/08/12/5.9.0-released/)

- [Release of Mesa 24.2:](https://www.mesa3d.org/news/releases/mesa-24-2-0-is-released/)

- [Debian 11 has been moved to LTS maintenance stage:](https://www.debian.org/News/2024/20240814)

- [Release of Deepin 23:](https://www.deepin.org/en/deepin-v23-is-officially-released/)

- [Outertale game open source:](https://spacey-432.itch.io/outertale)

- [Release of Godot 4.3:](https://godotengine.org/article/godot-4-3-a-shared-effort/)

- [OutWiker 3.3 released:](https://github.com/Jenyay/outwiker/releases/tag/3.3.0-stable)

- [Russian Open OS Challenge 2024:](https://translate.google.com/website?sl=auto&tl=en&hl=en-US&client=webapp&u=https://openscaler.braim.org/)

- [Release of labwc 0.8.0:](https://github.com/labwc/labwc/releases/tag/0.8.0)

- [Release of MiniOS 3.3.4:](https://github.com/minios-linux/minios-live/releases/tag/v3.3.4)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
