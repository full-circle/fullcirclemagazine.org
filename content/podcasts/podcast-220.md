---
title: "Full Circle Weekly News 220"
date: 2021-07-28
draft: false
tags:
  - "binutils"
  - "boot"
  - "chessbase"
  - "driver"
  - "esp32"
  - "firewall"
  - "firewalld"
  - "gentoo"
  - "gnu"
  - "gpio"
  - "gui"
  - "handbrake"
  - "juliacon"
  - "kde"
  - "kernel"
  - "mobile"
  - "musescore"
  - "ntfs"
  - "nvidia"
  - "paragon"
  - "peertube"
  - "plasma"
  - "rust"
  - "sixtyfps"
  - "stockfish"
  - "systemd"
  - "torvalds"
  - "virtualbox"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20220.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [GNU Binutils 2.37 Released](https://www.mail-archive.com/info-gnu@gnu.org/msg02923.html)

- [HandBrake 1.4.0 Released](https://handbrake.fr/news.php?article=47)

- [Linus Torvalds commented on the situation with the NTFS driver from Paragon Software](https://lore.kernel.org/lkml/CAHk-=wjW7Up3KD-2EqVg7+ca8Av0-rC5Kd7yK+=m6Dwk3D4Q+A@mail.gmail.com/)

- [NVIDIA publishes driver 470.57.02, opens RTXMU code and adds Linux support to RTX SDK](https://forums.developer.nvidia.com/t/linux-solaris-and-freebsd-driver-470-57-02-production-branch-release/184049)

- [Linux kernel boot Implemented on ESP32 board](https://www.reddit.com/r/esp32/comments/om106r/boot_linux_500_on_esp32/)

- [Muse Group Seeks Closure of Musescore-Downloader Project GitHub Repository](https://github.com/Xmader/musescore-downloader/issues/5#issuecomment-882450335)

- [A GPIO driver written in Rust is proposed for the Linux kernel](https://lore.kernel.org/ksummit/YPV7DTFBRN4UFMH1@google.com/)

- [JuliaCon 2021 online conference will be held at the end of July](https://juliacon.org/2021/)

- [Stockfish sues ChessBase and revokes GPL license](https://stockfishchess.org/blog/2021/our-lawsuit-against-chessbase/)

- [VirtualBox 6.1.24 Released](https://www.mail-archive.com/vbox-announce@virtualbox.org/msg00212.html)

- [Linux kernel root vulnerability and systemd denial of service](https://blog.qualys.com/vulnerabilities-threat-research/2021/07/20/sequoia-a-local-privilege-escalation-vulnerability-in-linuxs-filesystem-layer-cve-2021-33909)

- [KDE Plasma Mobile 21.07 Released](https://www.plasma-mobile.org/2021/07/20/plasma-mobile-gear-21-07/)

- [SixtyFPS 0.1.0 GUI library available](https://github.com/sixtyfpsui/sixtyfps/releases/tag/v0.1.0)

- [Firewalld 1.0 released](https://firewalld.org/2021/07/firewalld-1-0-0-release)

- [Gentoo started generating additional assemblies based on Musl and systemd](https://www.gentoo.org/news/2021/07/20/more-downloads.html)

- [PeerTube 3.3 decentralized video broadcasting platform released](https://joinpeertube.org/news#release-3.3)

- [Hardware Health Assessment Initiative in a Future Release of Debian 11](https://github.com/linuxhw/TestCoverage/tree/main/Dist/Debian_11)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
