---
title: "Full Circle Weekly News 147"
date: 2019-10-01
draft: false
tags:
  - "16-04"
  - "18-04"
  - "19-10"
  - "4-15"
  - "canonical"
  - "juhraya"
  - "kaos"
  - "kernel"
  - "lilocked"
  - "linux"
  - "lts"
  - "lxle"
  - "manjaro"
  - "microsoft"
  - "plasma"
  - "ransomware"
  - "regression"
  - "servers"
  - "snapcraft"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20147.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Thousands Of Linux Servers Infected By Lilu (Lilocked) Ransomware](https://www.zdnet.com/article/thousands-of-servers-infected-with-new-lilocked-lilu-ransomware/)

- [Microsoft Teams is coming to Linux](https://www.zdnet.com/article/microsoft-teams-for-linux-client-its-happening/)

- [KaOS 2019.09 Linux Distro Released with KDE Plasma 5.16.5 and Linux Kernel 5.2](https://news.softpedia.com/news/kaos-2019-09-linux-released-with-kde-plasma-5-16-5-and-linux-kernel-5-2-527373.shtml)

- [Manjaro Is Taking the Next Step](https://betanews.com/2019/09/08/manjaro-linux-company/)

- [Manjaro 18.1.0 - Juhraya finally released!](https://forum.manjaro.org/t/manjaro-18-1-0-juhraya-finally-released/102668)

- [LXLE 18.04.3 Linux OS Released](https://news.softpedia.com/news/lxle-18-04-3-linux-os-released-for-old-pcs-it-s-based-on-ubuntu-18-04-3-lts-527319.shtml)

- [Ubuntu's Snapcraft Updated to 3.8](https://news.softpedia.com/news/ubuntu-s-snapcraft-snap-creator-tool-will-soon-get-a-windows-installer-527336.shtml)

- [Canonical Fixes Linux 4.15 Kernel Regression in Ubuntu 18.04 LTS and 16.04 LTS](https://news.softpedia.com/news/canonical-fixes-linux-4-15-kernel-regression-in-ubuntu-18-04-lts-and-16-04-lts-527359.shtml)

- [Ubuntu 19.10 Promises More Boot Speed Improvements](https://news.softpedia.com/news/ubuntu-19-10-eoan-ermine-promises-more-boot-speed-improvements-527358.shtml)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
