---
title: "Full Circle Weekly News 388"
date: 2024-10-20
draft: false
tags:
  - "tovalds"
  - "bcachefs"
  - "antix"
  - "opensuse"
  - "openbsd"
  - "libreboot"
  - "ubuntu"
  - "asahi"
  - "amazon"
  - "kernel"
  - "kde"
  - "distrobox"
  - "wayland"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20388.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Linus Torvalds is unhappy with Bcachefs commits again:](https://lore.kernel.org/lkml/172816780614.3194359.10913571563159868953.pr-tracker-bot@kernel.org/T/%23r631c24cd07f5820a4cbff8f25dff1d1a0c3cf2e7)

- [Release of antiX 23.2:](https://antixlinux.com/antix-23-2-released/)

- [Release of  MX Linux 23.4:](https://mxlinux.org/blog/mx-23-4-libretto-now-available/)

- [Development of openSUSE Leap 16.0:](https://news.opensuse.org/2024/10/07/leap-16-0-prealpha/)

- [OpenBSD 7.6 Released:](https://www.mail-archive.com/announce@openbsd.org/msg00535.html)

- [Libreboot 20241008 released:](https://libreboot.org/news/libreboot20241008.html)

- [Release of XCP-ng 8.3:](https://xcp-ng.org/blog/2024/10/07/xcp-ng-8-3/)

- [Ubuntu 24.10 release:](https://lists.ubuntu.com/archives/ubuntu-announce/2024-October/000307.html)

- [Asahi Linux Project Prepares for Running Windows Games:](https://rosenzweig.io/blog/aaa-gaming-on-m1.html)

- [Hyper-V host environment components into the Linux kernel:](https://lore.kernel.org/lkml/1727985064-18362-1-git-send-email-nunodasneves@linux.microsoft.com/)

- [Amazon's Open 3D Engine 09/24 Released:](https://github.com/o3de/o3de/releases/tag/2409.0)

- [Release of RPM 4.20:](http://rpm.org/)

- [KDE Neon builds based on Ubuntu 24.04:](https://blog.neon.kde.org/2024/10/10/kde-neon-rebased-on-ubuntu-24-04-lts/)

- [Distrobox 1.8 Released:](https://github.com/89luca89/distrobox/releases/tag/1.8.0)

- [Wayland-Protocols 1.38 Released:](https://lists.freedesktop.org/archives/wayland-devel/2024-October/043851.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
