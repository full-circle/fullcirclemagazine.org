---
title: "Full Circle Weekly News 95"
date: 2018-06-03
draft: false
tags:
  - "18-04"
  - "18-10"
  - "alpha"
  - "army"
  - "beta"
  - "bodhi"
  - "botnet"
  - "disto"
  - "ethical"
  - "hacked"
  - "hacking"
  - "kernel"
  - "libre"
  - "libreoffice"
  - "linux"
  - "lubuntu"
  - "lxqt"
  - "office"
  - "parrot"
  - "patch"
  - "routers"
  - "spectre"
  - "update"
  - "variant"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2095.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 18.04 LTS Gets First Kernel Update with Patch for Spectre Variant 4 Flaw](https://news.softpedia.com/news/ubuntu-18-04-lts-gets-first-kernel-update-with-patch-for-spectre-variant-4-flaw-521248.shtml)

- [500,000 Routers In 54 Countries Hacked To Create Massive Botnet Army](https://fossbytes.com/routers-hacked-botnet-army-vpnkill-malware/)

- [Parrot 4.0 Ethical Hacking Linux Distro Released](https://fossbytes.com/parrot-4-0-release-download-features-hacking-linux/)

- [Bodhi Linux 5.0 Enters Development Based on Ubuntu 18.04 LTS, First Alpha Is Out](https://news.softpedia.com/news/bodhi-linux-5-0-enters-development-based-on-ubuntu-18-04-lts-first-alpha-is-out-521219.shtml)

- [Hands-On with First Lubuntu 18.10 Build Featuring the LXQt Desktop by Default](https://news.softpedia.com/news/hands-on-with-first-lubuntu-18-10-build-featuring-the-lxqt-desktop-by-default-521200.shtml)

- [LibreOffice 6.1 Beta Arrives Next Week for Second Bug Hunting Session on May 28](https://news.softpedia.com/news/libreoffice-6-1-beta-arrives-next-week-for-second-bug-hunting-session-on-may-28-521256.shtml)
