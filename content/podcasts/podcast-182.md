---
title: "Full Circle Weekly News 182"
date: 2020-09-16
draft: false
tags:
  - "centaurus"
  - "fedora"
  - "firewall"
  - "flatpak"
  - "gorilla"
  - "groovy"
  - "htop"
  - "kart"
  - "lenovo"
  - "lfs"
  - "mint"
  - "nftables"
  - "pi"
  - "q4os"
  - "scratch"
  - "supertuxkart"
  - "thinkpad"
  - "tux"
  - "ubuntu"
  - "warpinator"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20182.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Groovy Gorilla Is In Feature Freeze](https://lists.ubuntu.com/archives/ubuntu-devel-announce/2020-August/001279.html)

- [Ubuntu Beginning the Switch to NFTables in Groovy Gorilla](https://lists.ubuntu.com/archives/ubuntu-devel/2020-August/041142.html)

- [IP Fire 2.25 Core Update 148 Released with Location-based Firewall](https://blog.ipfire.org/post/ipfire-2-25-core-update-148-released)

- [Lenovo to Ship Fedora on its Thinkpads](https://twitter.com/mattdm/status/1299718126175744000)

- [Raspberry Pi OS 2020-08-20 Out](https://www.raspberrypi.org/downloads/raspberry-pi-os/)

- [Q4OS 3.12, Centaurus Out](https://q4os.org/blog.html)

- [Linux from Scratch and Beyond LFS 10 Out](http://lists.linuxfromscratch.org/pipermail/lfs-support/2020-September/053845.html)

- [Linux Mint's Warpinator via Flatpak Out](https://flathub.org/apps/details/org.x.Warpinator)

- [SuperTuxKart 1.2 Out](https://blog.supertuxkart.net/2020/08/supertuxkart-12-release.html)

- [Htop 3.0 Out](https://groups.io/g/htop/topic/htop_3_0_0_released/76441967)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
