---
title: "Full Circle Weekly News 20"
date: 2016-06-04
draft: false
tags:
  - "android"
  - "anonymity"
  - "backbox"
  - "canonical"
  - "charge"
  - "chrome"
  - "chromeos"
  - "convergence"
  - "cryptography"
  - "desktop"
  - "enterprise"
  - "hacked"
  - "hacking"
  - "iot"
  - "iphone"
  - "linux"
  - "lithium"
  - "meizu"
  - "netos"
  - "parrot-security"
  - "pro"
  - "pro5"
  - "qnap"
  - "smartphone"
  - "tools"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2020.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu-Based BackBox Linux 4.6 Launches with Updated Hacking Tools, Kernel 4.2](http://news.softpedia.com/news/ubuntu-based-backbox-linux-4-6-launches-with-updated-hacking-tools-kernel-4-2-504550.shtml)

- [QNAP and Canonical Optimize Ubuntu For IoT Purposes](http://themerkle.com/qnap-and-canonical-optimize-ubuntu-for-iot-purposes/)

- [NetOS Enterprise Linux 8 Promises to Be a Worthy Alternative to Chrome OS](http://news.softpedia.com/news/netos-enterprise-linux-8-promises-to-be-a-worthy-alternative-to-chrome-os-504591.shtml)

- [Your iPhone Or Android Smartphone Can Get Hacked When You Charge Via USB And Laptop](http://www.techtimes.com/articles/161651/20160529/your-iphone-android-smartphone-can-get-hacked-when-you-charge-via-usb-and-laptop.htm)

- [Parrot Security OS 3.0 "Lithium" Is a Linux Distro for Cryptography & Anonymity](http://news.softpedia.com/news/parrot-security-os-3-0-lithium-is-linux-distro-for-cryptography-and-anonymity-504630.shtml)

- [Meizu Pro 5 is the first Ubuntu phone that’s also a desktop PC](http://liliputing.com/2016/06/meizu-pro-5-first-ubuntu-phone-thats-also-desktop-pc-thanks-ota-11-update.html)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
