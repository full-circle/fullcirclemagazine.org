---
title: "Full Circle Weekly News 398"
date: 2025-02-01
draft: false
tags:
  - "kernel"
  - "mesa"
  - "libre"
  - "toybox"
  - "9front"
  - "wine"
  - "veracrypt"
  - "sdl"
  - "ventoy"
  - "debian"
  - "mysql"
  - "facebook"
  - "dovecot"
  - "opensuse"
  - "shotcut"
  - "solus"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20398.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Linux kernel 6.13 released:](https://lkml.org/lkml/2025/1/19/281)

- [Mesa adopts amdgpu_virtio:](https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/21658)

- [Linux libre kernel 6.13:](https://www.fsfla.org/pipermail/linux-libre/2025-January/003569.html)

- [Release of Toybox 0.8.12:](https://landley.net/toybox/news.html)

- [9front release 10931:](http://9front.org/releases/2025/01/19/0/)

- [Wine 10.0 Stable Release:](https://www.winehq.org/news/2025012101)

- [Release of VeraCrypt 1.26.18:](https://www.veracrypt.fr/en/Release%2520Notes.html)

- [SDL 3 Released:](https://discourse.libsdl.org/t/announcing-the-sdl-3-official-release/57149)

- [Release of Ventoy 1.1.0:](https://www.ventoy.net/en/doc_news.html)

- [Debian 13 Packages Freeze Plan:](https://lists.debian.org/debian-devel-announce/2025/01/msg00004.html)

- [MySQL 9.2.0 Released:](https://dev.mysql.com/downloads/mysql/)

- [Release of Midnight Commander 4.8.33:](https://github.com/MidnightCommander/mc/releases/tag/4.8.33)

- [Facebook scamming everyone again:](https://www.fsf.org/blogs/licensing/llama-3-1-community-license-is-not-a-free-software-license)

- [Dovecot 2.4.0 Available:](https://dovecot.org/mailman3/archives/list/dovecot-news@dovecot.org/thread/UYNR6GBP25XEGFCS633SWPR4HXV3NSS3/)

- [openSUSE alternative installer for Agama 11:](https://agama-project.github.io/blog/2025/01/21/Agama-11)

- [Shotcut Video Editor Released:](https://shotcut.org/blog/new-release-250125/)

- [Solus 4.7 released:](https://getsol.us/2025/01/26/solus-4-7-released/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
