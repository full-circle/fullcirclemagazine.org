---
title: "Full Circle Weekly News 310"
date: 2023-04-23
draft: false
tags:
  - "minetest"
  - "openbsd"
  - "netlink"
  - "freebsd"
  - "wireguard"
  - "4mlinux"
  - "meson"
  - "nginx"
  - "nvidia"
  - "core"
  - "truenas"
  - "scale"
  - "libreboot"
  - "lxqt"
  - "pgadmin"
  - "solus"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20310.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Minetest 5.7.0:](https://blog.minetest.net/2023/04/08/5.7.0-released/)

- [Release of OpenBSD 7.3:](https://www.mail-archive.com/announce@openbsd.org/msg00476.html)

- [FreeBSD 13.2 with Netlink and WireGuard support:](https://www.freebsd.org/releases/13.2R/relnotes/)

- [4MLinux 42.0](https://4mlinux-releases.blogspot.com/2023/04/4mlinux-420-stable-released.html)

- [Release of Meson 1.1:](https://github.com/mesonbuild/meson/releases/tag/1.1.0)

- [New version of nginx 1.24.0:](http://nginx.org/#2023-04-11)

- [NVIDIA has published the code RTX Remix Runtime:](https://www.nvidia.com/en-us/geforce/news/rtx-remix-runtime-open-source-download/)

- [Release of Tiny Core Linux 14:](http://forum.tinycorelinux.net/index.php/topic,26201.0.html)

- [Release of TrueNAS SCALE 22.12.2:](https://www.truenas.com/blog/truenas-scale-gets-enterprise-features/)

- [Release of Libreboot 20230413:](https://libreboot.org/news/libreboot20230413.html)

- [Release of LXQt 1.3:](https://lxqt-project.org/release/2023/04/15/release-lxqt-1-3-0/)

- [Release of pgAdmin 4 7.0:](https://www.postgresql.org/about/news/pgadmin-4-v70-released-2620/)

- [The Solus Linux project changes development team:](https://www.reddit.com/r/SolusProject/comments/12ndrvt/righting_the_ship/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
