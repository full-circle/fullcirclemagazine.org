---
title: "Full Circle Weekly News 68"
date: 2017-08-18
draft: false
tags:
  - "birthday"
  - "destroyer"
  - "gui-gnome"
  - "space"
  - "spacex"
  - "stealth"
  - "subsystem-linux"
  - "supercomputer"
  - "uss"
  - "windows"
  - "wsl"
  - "zumwalt"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2068.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Windows Subsystem For Linux (WSL) Comes To Windows Server](WSL)

- [Linux desktop GUI GNOME celebrates its 20th birthday](http://www.zdnet.com/article/linux-desktop-gui-gnome-celebrates-its-20th-birthday/)

- [SpaceX Is ‘Sending A Linux-Powered Supercomputer To The Space’ For The First Time](https://fossbytes.com/spacex-hpe-supercomputer-space-mission/)

- [Inside the Stealth Destroyer USS Zumwalt, the Warship That Runs on Linux](http://www.popularmechanics.com/military/weapons/news/a27804/stealth-destroyer-uss-zumwalt-linux/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
