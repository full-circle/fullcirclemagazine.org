---
title: "Full Circle Weekly News 136"
date: 2019-06-24
draft: false
tags:
  - "32-bit"
  - "canonical"
  - "firefox"
  - "flaw"
  - "kde"
  - "mozilla"
  - "openmandriva"
  - "patch"
  - "plasma"
  - "security"
  - "ubuntu"
  - "update"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20136.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [OpenMandriva Lx 4.0 is here](https://betanews.com/2019/06/16/openmandriva-lx4-linux-amd/)

- [KDE Plasma 5.16 Gets First Point Release](https://news.softpedia.com/news/kde-plasma-5-16-desktop-environment-gets-first-point-release-update-now-526455.shtml)

- [Canonical Outs Important Security Update for All Ubuntu Releases](https://news.softpedia.com/news/canonical-outs-important-linux-kernel-security-update-for-all-ubuntu-releases-526440.shtml)

- [Canonical Will Drop Support for 32-bit Architectures in Future Ubuntu Releases](https://news.softpedia.com/news/canonical-will-drop-support-for-32-bit-architectures-in-future-ubuntu-releases-526439.shtml)

- [Canonical’s Snap Store Adds 11 Distro Specific Installation Pages for Every Single App](https://www.forbes.com/sites/jasonevangelho/2019/06/14/canonicals-snap-store-adds-10-distro-specific-installation-pages-for-every-single-app/#2f9f4bf65448)

- [Mozilla Patches Firefox Zero-Day Abused in the Wild](https://www.zdnet.com/article/mozilla-patches-firefox-zero-day-abused-in-the-wild/)

- [Mozilla Patches Second Zero-Day Flaw This Week](https://thehackernews.com/2019/06/firefox-0day-vulnerability.html)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
