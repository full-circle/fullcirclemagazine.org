---
title: "Full Circle Weekly News 21"
date: 2016-06-11
draft: false
tags:
  - "16-04"
  - "ai"
  - "aio"
  - "browser"
  - "creator"
  - "dvd"
  - "foundation"
  - "helicopters"
  - "iso"
  - "kali"
  - "kalibrowser"
  - "linux"
  - "lts"
  - "openswitch"
  - "russian"
  - "snap"
  - "snapcraft"
  - "snaps"
  - "tails"
  - "tor"
  - "ubuntu"
  - "wipro"
  - "zip"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2021.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [You Can Now Have All the Ubuntu 16.04 LTS Live DVDs in a Single ISO Image](http://news.softpedia.com/news/you-can-now-have-all-the-ubuntu-16-04-lts-live-dvds-into-a-single-iso-image-504867.shtml)

- [Canonical Releases Snapcraft 2.10 Snaps Creator with Support for ZIP Files, and More](http://news.softpedia.com/news/canonical-releases-snapcraft-2-10-snaps-creator-with-support-for-zip-files-more-505042.shtml)

- [OpenSwitch Moves Under Linux Foundation Umbrella](http://www.linuxinsider.com/story/83572.html?rss=1)

- [You Can Now Run Kali Linux Pentest OS in Your Web Browser](http://news.softpedia.com/news/you-can-now-run-backtrack-successor-kali-linux-pentest-os-in-your-web-browser-504809.shtml)

- [Russian Helicopters plan to switch to Linux](http://tass.ru/en/economy/880558)

- [3000 Software Engineers Freed Up At Wipro After AI Learns To Do Their Jobs](http://www.indiatimes.com/news/world/3000-software-engineers-might-get-fired-at-wipro-after-artificial-intelligence-learns-to-do-all-their-jobs-256519.html)

- [Tails, the anonymity-focused Linux distribution with deep Tor integration, reaches version 2.4](http://www.pcworld.com/article/3080525/linux/tails-the-anonymity-focused-linux-distribution-with-deep-tor-integration-reaches-version-24.html)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
