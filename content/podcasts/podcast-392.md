---
title: "Full Circle Weekly News 392"
date: 2024-11-17
draft: false
tags:
  - "tizen"
  - "nethsecurity"
  - "ffmpeg"
  - "doglinux"
  - "lxqt"
  - "firewalld"
  - "lima"
  - "webos"
  - "gimp"
  - "gsmartcontrol"
  - "touch"
  - "ubports"
  - "debian"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20392.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Tizen Studio 6.0 Development Environment Released:](https://developer.tizen.org/development/tizen-studio/)

- [Release of NethSecurity 8.3:](https://www.nethserver.org/)

- [FFmpeg speeds up some operations by 94 times:](https://twitter.com/FFmpeg/status/1852542388851601913)

- [Updating DogLinux build:](https://translate.google.com/website?sl=auto&tl=en&hl=en-US&client=webapp&u=https://gumanzoy.blogspot.com/2024/11/20241105-doglinux.html)

- [LXQt 2.1.0 Released:](https://lxqt-project.org/release/2024/11/05/release-lxqt-2-1-0/)

- [Firewalld 2.3.0 Released:](https://github.com/firewalld/firewalld/releases/tag/v2.3.0)

- [webOS Open Source Edition 2.27:](https://www.webosose.org/about/release-notes/webos-ose-2-27-0-release-notes/)

- [Lima 1.0:](https://github.com/lima-vm/lima/releases/tag/v1.0.0)

- [The first release candidate of GIMP 3.0:](https://www.gimp.org/news/2024/11/06/gimp-3-0-RC1-released/)

- [QNX free for non-commercial use:](https://forums.openqnx.com/t/topic/47580)

- [Release of GSmartControl 2.0:](https://github.com/ashaduri/gsmartcontrol/releases/tag/v2.0.0)

- [Ubuntu Touch OTA-6 Focal Mobile Platform Released:](https://ubports.com/en/blog/ubports-news-1/post/ubuntu-touch-ota-6-focal-release-3942)

- [Debian 12.8 Released:](https://www.debian.org/News/2024/20241109)

- [Release of iptables 1.8.11:](https://www.mail-archive.com/netfilter-announce@lists.netfilter.org/msg00270.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
