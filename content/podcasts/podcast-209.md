---
title: "Full Circle Weekly News 209"
date: 2021-05-12
draft: false
tags:
  - "apache"
  - "audacity"
  - "creator"
  - "editor"
  - "exim"
  - "gparted"
  - "group"
  - "heroes"
  - "kernel"
  - "libreoffice"
  - "linux"
  - "magic"
  - "mesa"
  - "might"
  - "muse"
  - "opengl"
  - "openoffice"
  - "qt"
  - "rhvoice"
  - "russia"
  - "shotcut"
  - "speech"
  - "syncthing"
  - "synthesizer"
  - "update"
  - "video"
  - "vulkan"
  - "vulnerability"
  - "wayland"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20209.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The Muse Group acquired the Audacity project](https://www.scoringnotes.com/news/muse-group-formed-to-support-musescore-ultimate-guitar-acquires-audacity/)

- [GParted 1.3 Partition Editor released](https://gparted.org/news.php?item=238)

- [Wayland-protocols 1.21 released](https://lists.freedesktop.org/archives/wayland-devel/2021-April/041815.html)

- [Exim 4.94.2 update fixes 10 remotely exploited vulnerabilities](https://www.openwall.com/lists/oss-security/2021/05/04/6)

- [Apache OpenOffice 4.1.10, fixing a vulnerability affecting LibreOffice](https://blogs.apache.org/OOo/entry/announcing-apache-openoffice-4-16)

- [Syncthing 1.16 released](https://forum.syncthing.net/t/syncthing-v1-16-0-v1-16-1/16758/)

- [Release of Shotcut Video Editor](https://www.shotcut.org/blog/new-release-210501/)

- [Qt Creator 4.15 Released](https://www.qt.io/blog/qt-creator-4.15-released)

- [Free Heroes of Might and Magic II 0.9.3 Released](https://github.com/ihhub/fheroes2/releases/tag/0.9.3)

- [Mesa 21.1, a free implementation of OpenGL and Vulkan released](https://lists.freedesktop.org/archives/mesa-dev/2021-May/225248.html)

- [RHVoice 1.2.4 speech synthesizer developed for Russian speakers](https://github.com/RHVoice/RHVoice/releases/tag/1.2.4)

- [Linux kernel developers complete audit of all patches from the University of Minnesota](https://lore.kernel.org/lkml/202105051005.49BFABCE@keescook/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
