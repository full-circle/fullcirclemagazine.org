---
title: "Full Circle Weekly News 308"
date: 2023-04-08
draft: false
tags:
  - "gnucash"
  - "blendergpt"
  - "blender"
  - "touch"
  - "ubports"
  - "debian"
  - "ventoy"
  - "porteus"
  - "kiosk"
  - "nginx"
  - "cinnamon"
  - "fresh"
  - "finnix"
  - "openmandriva"
  - "mandriva"
  - "server"
  - "dentos"
  - "star64"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20308.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of GnuCash 5.0:](https://lists.gnucash.org/pipermail/gnucash-announce/2023-March/000388.html)

- [BlenderGPT plugin announced:](https://github.com/gd3kr/BlenderGPT)

- [Ubuntu Touch OTA-1 Focal, translated on Ubuntu 20.04:](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-24-release-3872)

- [Debian 9 Stretch moved to archive:](https://lists.debian.org/debian-devel-announce/2023/03/msg00006.html)

- [Release Ventoy 1.0.90:](https://www.ventoy.net/en/doc_news.html)

- [Release of Porteus Kiosk 5.5.0:](https://porteus-kiosk.org/news.html#230327)

- [Release nginx 1.23.4 with default TLSv1.3:](http://nginx.org/#2023-03-28)

- [Ubuntu Cinnamon now an official edition of Ubuntu:](https://ubuntucinnamon.org/ubuntu-cinnamon-flavor-status-announcement/)

- [ROSA Fresh 12.4:](http://wiki.rosalab.ru/ru/index.php/ROSA_Fresh_12.4)

- [Release of Finnix 125:](https://blog.finnix.org/2023/03/28/finnix-125-released/)

- [Release of OpenMandriva ROME 23.03:](https://www.openmandriva.org/en/news/article/openmandriva-rome-23-03)

- [Release of Blender 3.5:](https://www.blender.org/download/releases/3-5/)

- [Upgrade X.Org Server 21.1.8 and xwayland 23.1.1:](https://www.mail-archive.com/xorg-announce@lists.x.org/msg01586.html)

- [IETF standardizes MLS end-to-end encryption protocol:](https://www.ietf.org/blog/mls-secure-and-usable-end-to-end-encryption/)

- [Beta-release of Ubuntu 23.04:](https://lists.ubuntu.com/archives/ubuntu-announce/2023-March/000288.html)

- [DentOS 3.0 is available:](https://github.com/dentproject/dentOS/releases/tag/v3.0)

- [Bloomberg has established a fund to pay grants to open projects:](https://www.bloomberg.com/company/stories/bloomberg-ospo-launches-foss-contributor-fund/)

- [The STAR64 goes on sale:](https://www.pine64.org/2023/04/01/march-update-tablet-bonanza/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
