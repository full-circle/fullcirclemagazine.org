---
title: "Full Circle Weekly News 180"
date: 2020-08-21
draft: false
tags:
  - "accounts"
  - "btrfs"
  - "device"
  - "distro"
  - "edge"
  - "endeavouros"
  - "enterprise"
  - "fedora"
  - "gnucash"
  - "ibm"
  - "ipfire"
  - "kde"
  - "keepass"
  - "keepassxc"
  - "mail"
  - "mint"
  - "pae"
  - "plasma"
  - "pulse"
  - "rancher"
  - "redhat"
  - "remix"
  - "serpent"
  - "suse"
  - "thunderbird-claws"
  - "tuxedo"
  - "ubuntued"
  - "upgrade"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20180.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Serpent OS, a New Distro](https://www.serpentos.com/blog/2020/06/30/first-post/)

- [IPFire Will Discontinue Support for 32-Bit with PAE](https://blog.ipfire.org/post/ipfire-2-25-core-update-146-released)

- [Ubuntu 19.10 Reaches End of Life](https://lists.ubuntu.com/archives/ubuntu-security-announce/2020-July/005494.html)

- [IBM Open Sources Their Edge Device Platform](https://www.datacenterknowledge.com/open-source/ibm-has-open-sourced-its-edge-device-platform-and-wishes-aws-and-microsoft-got-board)

- [Inclusive Langauge Is Gaining Popularity](https://itsfoss.com/linux-kernel-inclusive-code/)

- [Fedora 33 to Use Btrfs as Default Filesystem](https://fedoraproject.org/wiki/Releases/33/ChangeSet#Make_btrfs_the_default_file_system_for_desktop_variants)

- [SUSE Buys Rancher Labs](https://www.itprotoday.com/containers/suse-buys-rancher-labs-its-kubernetes-game)

- [UbuntuEd 20.04, a New Remix, Out](https://discourse.ubuntu.com/t/ubuntu-education-ubuntued/17063)

- [EndeavourOS' July Release Out](https://endeavouros.com/news/our-first-anniversary-the-july-release-and-whats-next/)

- [SUSE Linux Enterprise 15 sp2 Out](https://www.suse.com/c/suse-linux-enterprise-15-service-pack-2-is-generally-available/)

- [Plasma 5.19.3 Updates Out](https://kde.org/announcements/plasma-5.19.3)

- [Linux Mint 20 Upgrade Path Out](https://blog.linuxmint.com/?p=3946)

- [GnuCash 4.0 and 4.1 Out](https://www.gnucash.org/new_features-4.0.phtml)

- [KeePassXC 2.6.0 Out](https://keepassxc.org/blog/2020-07-07-2.6.0-released/)

- [Claws Mail 3.17.6 Out](https://www.claws-mail.org/news.php)

- [Thunderbird 78 Out](https://blog.thunderbird.net/2020/07/whats-new-in-thunderbird-78/)

- [Purism's Librem 14 Out](https://9to5linux.com/purism-unveils-the-librem-14-linux-laptop-now-available-for-pre-order)

- [TUXEDO Computers' Pulse 15 Out](https://9to5linux.com/tuxedo-computers-unveils-the-tuxedo-pulse-15-linux-ultrabook-with-amd-ryzen-4000h-series)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
