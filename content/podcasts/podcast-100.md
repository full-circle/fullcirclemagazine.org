---
title: "Full Circle Weekly News 100"
date: 2018-07-20
draft: false
tags:
  - "apps"
  - "bug"
  - "bypass"
  - "cloud"
  - "container"
  - "desktop"
  - "linux"
  - "lock"
  - "malware"
  - "touch"
  - "ubuntu"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20100.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Minimal Ubuntu for containers and clouds](https://www.zdnet.com/article/minimal-ubuntu-for-containers-and-clouds/)

- [Desktop Apps on Ubuntu Touch](https://distrowatch.com/weekly.php?issue=20180716#news)

- [This new dual-platform malware targets both Windows and Linux systems](https://www.techrepublic.com/article/this-new-dual-platform-malware-targets-both-windows-and-linux-systems/)

- [Ubuntu bug allows anyone with physical access to bypass your lock screen](https://www.neowin.net/news/ubuntu-bug-allows-anyone-with-physical-access-to-bypass-your-lock-screen)
