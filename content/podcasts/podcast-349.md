---
title: "Full Circle Weekly News 349"
date: 2024-01-21
draft: false
tags:
  - "kernel"
  - "kde"
  - "plasma"
  - "solus"
  - "chrome"
  - "arch"
  - "arti"
  - "godot"
  - "godotos"
  - "openssh"
  - "mint"
  - "firebird"
  - "pulseaudio"
  - "server"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20349.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux kernel 6.7 release:](https://lore.kernel.org/lkml/CAHk-=widprp4XoHUcsDe7e16YZjLYJWra-dK0hE1MnfPMf6C3Q@mail.gmail.com/)

- [KDE Plasma desktop in OpenBSD:](https://rsadowski.de/posts/2024-01-09-openbsd-kde/)

- [Release of Solus 4.5:](https://getsol.us/2024/01/08/solus-4-5-released/)

- [Chrome OS 120 released:](https://chromereleases.googleblog.com/2024/01/stable-channel-update-for-chromeos.html)

- [Arch Linux switched to using dbus-broker:](https://archlinux.org/news/making-dbus-broker-our-default-d-bus-daemon/)

- [Arti 1.1.12, an implementation of Tor in Rust:](https://blog.torproject.org/arti_1_1_12_released/)

-[The GodotOS project toy:](https://github.com/popcar2/GodotOS/releases/tag/1.0.0)

- [End of support for LTS Linux kernel 4.14:](https://lkml.org/lkml/2024/1/10/201)

- [First release candidate for KDE 6:](https://kde.org/announcements/megarelease/6/rc1/)

- [The OpenSSH Project plan to deprecate DSA support:](https://lists.mindrot.org/pipermail/openssh-unix-announce/2024-January/000156.html)

- [Release of Linux Mint 21.3:](https://blog.linuxmint.com/?p=4624)

- [Release of Firebird 5.0 DBMS:](https://firebirdsql.org/en/news/firebird-5-0-0-is-released/)

- [PulseAudio 17.0 sound server available:](https://lists.freedesktop.org/archives/pulseaudio-discuss/2024-January/032426.html)

- [The Linux 6.8 kernel to speed up TCP:](https://git.kernel.org/torvalds/c/3e7aeb78ab01c2c2f0e1f784e5ddec88fcd3d106)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
