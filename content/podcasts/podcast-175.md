---
title: "Full Circle Weekly News 175"
date: 2020-06-12
draft: false
tags:
  - "biohackathon"
  - "debian"
  - "eclipse"
  - "exfat"
  - "gnome"
  - "gnu"
  - "guix"
  - "hackathon"
  - "huawei"
  - "hurd"
  - "invention"
  - "media"
  - "mint"
  - "network"
  - "open"
  - "openeuler"
  - "openmediavault"
  - "paragon"
  - "patent"
  - "pinephone"
  - "rhel"
  - "theia"
  - "touch"
  - "ubports"
  - "ubuntu"
  - "ulyana"
  - "vault"
  - "wireguard"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20175.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Paragon Software Unhappy about exFAT in Kernel 5.7](https://arstechnica.com/information-technology/2020/03/the-exfat-filesystem-is-coming-to-linux-paragon-softwares-not-happy-about-it/)

- [Debian Announces Biohackathon](https://lists.debian.org/debian-devel-announce/2020/03/msg00010.html)

- [Linux Mint 20 will be named Ulyana](https://blog.linuxmint.com/?p=3887)

- [GNU Guix Plans Addition of Hurd Micro-Kernel](https://guix.gnu.org/blog/2020/a-hello-world-virtual-machine-running-the-hurd/)

- [Huawei Signs Non-Agression Patent Pact with Open Invention Network](https://www.theregister.com/2020/04/02/huawei_open_invention_network/)

- [Ubuntu 20.04 Beta Out](https://lists.ubuntu.com/archives/ubuntu-announce/2020-April/000255.html)

- [RHEL 7.8 Out](https://www.redhat.com/archives/rhelv6-list/2020-March/msg00000.html)

- [OpenMediaVault 5.0 Out](https://www.openmediavault.org/?p=2685)

- [Gnome 3.36.1 Out](https://mail.gnome.org/archives/gnome-announce-list/2020-April/msg00001.html)

- [Pinephone with UBPorts Out](https://www.pine64.org/2020/04/02/pinephone-ubports-community-edition-pre-orders-now-open/)

- [OpenEuler 20.03 Out](https://www.huawei.com/en/press-events/news/2020/3/openeuler-lts-open-source-operating-system)

- [Wireguard 1.0.0 Out](https://lists.zx2c4.com/pipermail/wireguard/2020-March/005206.html)

- [Eclipse Theia 1.0 Out](https://www.eclipse.org/org/press-release/20200331-theia.php)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
