---
title: "Full Circle Weekly News 394"
date: 2024-12-01
draft: false
tags:
  - "kernel"
  - "linux"
  - "ghostbsd"
  - "arch"
  - "ubuntu"
  - "canonical"
  - "freecad"
  - "github"
  - "reiserfs"
  - "mesa"
  - "maxx"
  
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20394.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Linux kernel 6.12 releases with Realtime support:](https://lkml.org/lkml/2024/11/17/326)

- [GhostBSD 24.10.1 Released:](https://ghostbsd.org/news/GhostBSD_24.10.1_Is_Now_Available)

- [Linux-libre 6.12 kernel available:  (Resolving licensing issues with Tuxedo drivers)](https://www.fsfla.org/pipermail/linux-libre/2024-November/003562.html)

- [Arch Linux Moves Package Build Scripts to 0BSD License:](https://archlinux.org/news/providing-a-license-for-package-sources/)

- [Ubuntu developers analyzed the effectiveness of enabling PGO optimization:](https://canonical.com/blog/profile-guided-optimization-a-case-study)

- [Canonical Announces First LTS Release of MicroCloud Tools:](https://canonical.com/blog/canonical-releases-microcloud-lts)

- [Release of FreeCAD 1.0:](https://blog.freecad.org/2024/11/19/freecad-version-1-0-released/)

- [GitHub's Initiative to Fund Security Improvements for Open Source Projects:](https://github.blog/news-insights/company-news/announcing-github-secure-open-source-fund/)

- [ReiserFS has been removed from the Linux kernel:](https://lore.kernel.org/all/173221228921.2032550.2839788690971460030.pr-tracker-bot@kernel.org/)

- [Mesa 24.3 is released:](https://lists.freedesktop.org/archives/mesa-announce/2024-November/000786.html)

- [The Linux Foundation Technical Committee has officially announced the temporary suspension of the author of BсacheFS:](https://lore.kernel.org/lkml/6740fc3aabec0_5eb129497@dwillia2-xfh.jf.intel.com.notmuch/)

- [MaXX Interactive Desktop 2.2:](https://docs.maxxinteractive.com/books/whats-new-release-notes/page/november-21st-2024-release-day-for-maxxdesktop-v220-aka-octane-alpha-1)


**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
