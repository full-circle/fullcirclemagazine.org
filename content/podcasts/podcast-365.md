---
title: "Full Circle Weekly News 365"
date: 2024-05-12
draft: false
tags:
  - "amarok"
  - "shotcut"
  - "run0"
  - "sde"
  - "opentofu"
  - "libreelec"
  - "redhat"
  - "enterprise"
  - "kde"
  - "dillo"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20365.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Amarok 3.0.0:](https://invent.kde.org/multimedia/amarok)

- [Release of Shotcut 24.04:](https://shotcut.org/blog/new-release-240428/)

- [Lennart Pottering introduces run0:](https://mastodon.social/@pid_eins/112353324518585654)

- [Release of meta-distribution T2 SDE 24.5:](https://www.mail-archive.com/t2@t2-project.org/msg04077.html)

- [Release of OpenTofu 1.7:](https://opentofu.org/blog/opentofu-1-7-0/)

- [Release of LibreELEC 12.0:](https://libreelec.tv/2024/05/01/libreelec-nexus-12-0-0/)

- [Release of Red Hat Enterprise Linux 9.4:](https://www.redhat.com/en/about/press-releases/red-hat-simplifies-standard-operating-environments-across-hybrid-cloud-latest-version-red-hat-enterprise-linux)

- [KDE has removed the ability to install GNOME icon themes:](https://pointieststick.com/2024/05/03/this-week-in-kde-looking-towards-plasma-6-1/)

- [The TSAC audio codec:](https://bellard.org/tsac/readme.txt)

- [Dillo 3.1 has been published:](https://dillo-browser.github.io/latest.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
