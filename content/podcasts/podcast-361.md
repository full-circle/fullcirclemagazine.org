---
title: "Full Circle Weekly News 361"
date: 2024-04-14
draft: false
tags:
  - "widelands"
  - "nitrux"
  - "rosa"
  - "mojo"
  - "libsystemd"
  - "redict"
  - "ceno"
  - "jpegli"
  - "ffmpeg"
  - "openbsd"
  - "netplan"
  - "kodi"
  - "gnome"
  - "xfce"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20361.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Widelands 1.2:](https://www.widelands.org/news/2024/Mar/30/widelands-1-2-released/)

- [Nitrux 3.4.0 is available:](https://nxos.org/changelog/release-announcement-nitrux-3-4-0/)

- [Release of the ROSA Fresh 12.5:](https://rosa.ru/rosa-anonsiruet-novuyu-versiyu-besplatnoj-os-dlya-domashnego-ispolzovaniya-rosa-fresh-12-5/)

- [Opening the Mojo language standard library:](https://www.modular.com/blog/whats-new-in-mojo-24-2-mojo-nightly-enhanced-python-interop-oss-stdlib-and-more)

- [Libsystemd dependency reduction initiative:](https://github.com/systemd/systemd/issues/32028)

- [Release of Redict 7.3.0:](https://redict.io/posts/2024-04-03-redict-7.3.0-released/)

- [CENO 2.1 web browser, uses a P2P network to access sites:](https://github.com/censorship-no/ceno-browser/releases/tag/v2.1.0)

- [Google introduced the jpegli library:](https://opensource.googleblog.com/2024/04/introducing-jpegli-new-jpeg-coding-library.html)

- [X.Org Server 21.1.12 update with 4 vulnerabilities fixed:](https://www.mail-archive.com/xorg-announce@lists.x.org/msg01706.html)

- [Schleswig-Holstein and Open source:](https://blog.documentfoundation.org/blog/2024/04/04/german-state-moving-30000-pcs-to-libreoffice/)

- [Release of the FFmpeg 7.0:](http://ffmpeg.org/download.html%23releases)

- [OpenBSD 7.5 release:](https://www.mail-archive.com/announce@openbsd.org/msg00522.html)

- [Netplan 1.0 is available:](https://canonical.com//blog/introducing-netplan-v1)

- [Release of Kodi 21.0 Open Media Center:](http://kodi.tv/)

- [Updates from the GNOME project:](https://thisweek.gnome.org/posts/2024/04/twig-142/)

- [AV Linux based on MX-23.2:](https://www.bandshed.net/2024/04/05/av-linux-mx-edition-23-2-iso-update/)

- [Presumably 14 million people use the Xfce desktop environment:](https://alexxcons.github.io/blogpost_9.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
