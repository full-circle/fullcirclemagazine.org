---
title: "Full Circle Weekly News 140"
date: 2019-07-22
draft: false
tags:
  - "18-10"
  - "19-2"
  - "5-2"
  - "attack"
  - "backdoor"
  - "beta"
  - "cinnamon"
  - "cosmic"
  - "cuttlefish"
  - "eol"
  - "evilgnome"
  - "force"
  - "gnu"
  - "infect"
  - "kernel"
  - "linux"
  - "mate"
  - "mint"
  - "mozilla"
  - "nas"
  - "ransomware"
  - "server"
  - "ssh"
  - "tails"
  - "tina"
  - "ubuntu"
  - "xfce"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20140.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [GNU Linux-Libre 5.2 Kernel Released](https://news.softpedia.com/news/gnu-linux-libre-5-2-kernel-released-for-those-seeking-100-freedom-for-their-pcs-526671.shtml)

- [Tails 3.15 Fixes Critical Bugs](https://tails.boum.org/news/version_3.15/index.en.html)

- [Mozilla’s Add-Ons Outage Post-Mortem Result](https://hacks.mozilla.org/2019/07/add-ons-outage-post-mortem-result/)

- [Ransomware uses Brute-Force SSH Attacks to Infect Linux-Based NAS Servers](https://thehackernews.com/2019/07/ransomware-nas-devices.html)

- [Linux Mint 19.2 “Tina” Beta Is Here WIth Cinnamon, Mate and XFCE](https://betanews.com/2019/07/16/linux-mint-192-tina-beta-ubuntu/)

- [New EvilGnome Backdoor Spies on Linux Users, Steals Their Files](https://www.bleepingcomputer.com/news/security/new-evilgnome-backdoor-spies-on-linux-users-steals-their-files/)

- [Ubuntu 18.10 ‘Cosmic Cuttlefish’ Reaches End of Life](https://www.theinquirer.net/inquirer/news/3079174/ubuntu-1810-end-of-life)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
