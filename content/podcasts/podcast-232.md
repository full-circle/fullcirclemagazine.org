---
title: "Full Circle Weekly News 232"
date: 2021-10-21
draft: false
tags:
  - "21.10"
  - "arkime"
  - "assembly"
  - "backup"
  - "cdrtools"
  - "debian"
  - "desktop"
  - "devuan"
  - "fresh"
  - "geany"
  - "grammar"
  - "indexing"
  - "kde"
  - "languagetool"
  - "Linux"
  - "lutris"
  - "microsoft"
  - "openbsd"
  - "plasma"
  - "punctuation"
  - "qbs"
  - "redorescue"
  - "rosa"
  - "spelling"
  - "sysmon"
  - "systemd"
  - "traffic"
  - "ubuntu"
  - "web"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20232.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Geany 1.38 IDE](https://www.geany.org/news/geany-138-is-out/)

- [Redo Rescue 4.0.0, backup and recovery distribution](https://github.com/redorescue/redorescue/releases/tag/4.0.0)

- [Arkime 3.1 network traffic indexing system](https://github.com/arkime/arkime/releases/tag/v3.1.0)

- [Release of ROSA Fresh 12 on the new rosa2021.1 platform (IN RUSSIAN)](http://wiki.rosalab.ru/ru/index.php/ROSA_Fresh_12)

- [The author of cdrtools passed away](https://minnie.tuhs.org/pipermail/tuhs/2021-October/024523.html)

- [Lutris Platform 0.5.9 Released](https://lutris.net/)

- [Release of assembly tools Qbs 1.20](https://www.qt.io/blog/qbs-1.20-released)

- [Open Source Security Improvement Foundation Receives $ 10 Million Funding](https://www.linuxfoundation.org/press-release/open-source-security-foundation-raises-10-million-in-new-commitments-to-secure-software-supply-chains/)

- [LanguageTool 5.5, a grammar, spelling, punctuation and style corrector, released](https://forum.languagetool.org/t/ann-languagetool-5-5/7278)

- [KDE Plasma 5.23 Desktop Release](https://kde.org/announcements/plasma/5/5.23.0/)

- [Ubuntu 21.10 released](https://lists.ubuntu.com/archives/ubuntu-announce/2021-October/000274.html)

- [Devuan 4.0 release, Debian fork without systemd](https://www.devuan.org/os/announce/chimaera-release-announce-2021-10-14)

- [Microsoft ported Sysmon to Linux and made it open source](https://techcommunity.microsoft.com/t5/azure-sentinel/automating-the-deployment-of-sysmon-for-linux-and-azure-sentinel/ba-p/2847054)

- [OpenBSD 7.0 released](https://www.mail-archive.com/announce@openbsd.org/msg00399.html)

- [Ubuntu Web 20.04.3 Released](https://discourse.ubuntu.com/t/ubuntu-web-remix/19394/92)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
