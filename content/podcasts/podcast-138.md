---
title: "Full Circle Weekly News 138"
date: 2019-07-08
draft: false
tags:
  - "19-10"
  - "buster"
  - "debian"
  - "distro"
  - "freedos"
  - "gnu"
  - "linux"
  - "microsoft"
  - "mint"
  - "mintbox"
  - "pi"
  - "purismmageia"
  - "raspberry"
  - "rush"
  - "server"
  - "ubuntu"
  - "wallpaper"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20138.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [purism’s Security Key Will Generate Keys Directly on the Device, Made in the USA](https://news.softpedia.com/news/purism-s-security-key-will-generate-keys-directly-on-the-device-made-in-the-usa-526570.shtml)

- [Mageia 7 Linux Distro Available for Download](https://betanews.com/2019/07/01/mageia-7-linux-seven-mageia7/)

- [Debian 10 “Buster” Released](https://www.debian.org/News/2019/20190706)

- [FreeDOS turns 25](https://liliputing.com/2019/07/freedos-turns-25-open-source-dos-compatible-operating-system.html)

- [Linux Mint 20 Will Drop Support for 32-bit Installations](https://news.softpedia.com/news/linux-mint-20-and-future-releases-will-drop-support-for-32-bit-installations-526601.shtml)

- [MintBox 3 Linux Mint-Powered Mini PC Announced as the Most Powerful MintBox Ever](https://news.softpedia.com/news/mintbox-3-linux-mint-powered-mini-pc-announced-as-the-most-powerful-mintbox-ever-526602.shtml)

- [GNU Rush 2.0 Released](https://www.phoronix.com/scan.php?page=news_item&px=GNU-Rush-2.0-Released)

- [Ubuntu 19.10 Wallpaper Competition Is Now Open for Submissions](https://news.softpedia.com/news/ubuntu-19-10-eoan-ermine-wallpaper-competition-is-now-open-for-submissions-526599.shtml)

- [Linux Overtakes Windows Server As Most Used Operating System on Azure](https://www.onmsft.com/news/linux-overtakes-windows-server-as-most-used-operating-system-on-azure)

- [Microsoft Asks To Join Private Linux Security Developer List](https://www.zdnet.com/article/microsoft-asks-to-join-private-linux-security-developer-list/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
