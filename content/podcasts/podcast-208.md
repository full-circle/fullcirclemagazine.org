---
title: "Full Circle Weekly News 208"
date: 2021-05-09
draft: false
tags:
  - "akira"
  - "browser"
  - "budgie"
  - "calculate"
  - "compiler"
  - "fedora"
  - "finit"
  - "fix"
  - "gcc"
  - "ghostbsd"
  - "gnu"
  - "godot"
  - "init"
  - "kernel"
  - "leap"
  - "linux"
  - "malware"
  - "nano"
  - "openbsd"
  - "openindiana"
  - "opensolaris"
  - "opensuse"
  - "palemoon"
  - "postfix"
  - "proxmox"
  - "qemu"
  - "redhat"
  - "rocky"
  - "rotajakiro"
  - "samba"
  - "systemd"
  - "thinkpenguin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20208.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Godot 3.3](http://www.godotengine.org/)

- [Linux kernel 5.12 released](https://lkml.org/lkml/2021/4/25/225)

- [Release of Akira 0.0.14](http://akiraux.org/)

- [Finit 4.0 init system available](https://github.com/troglobit/finit/releases/tag/4.0)

- [Fedora 34 Linux distribution released](https://fedoramagazine.org/announcing-fedora-34/)

- [Pale Moon 29.2 Browser Released](https://forum.palemoon.org/viewtopic.php?t=26690&p=213782#p213782)

- [Budgie Desktop 10.5.3 Released](https://getsol.us/2021/04/27/fashionable-gnome-forty/)

- [GCC Compiler Set 11 Released](https://gcc.gnu.org/gcc-11/changes.html)

- [Calculate Linux 21 released](https://forum.calculate-linux.org/t/calculate-linux-21/10619)

- [OpenSUSE Leap 15.3 RC](https://news.opensuse.org/2021/04/28/opensuse-leap-153-enters-rc-phase/)

- [New versions of Samba 4.14.4, 4.13.8 and 4.12.15 with vulnerability fix](https://www.mail-archive.com/samba-announce@lists.samba.org/msg00548.html)

- [Release of the GNU nano text editor 5.7](https://www.nano-editor.org/news.php)

- [Proxmox VE 6.4 released](https://www.proxmox.com/en/news/listid-1/mailid-177-proxmox-virtual-environment-6-4-released#h10)

- [RotaJakiro - new Linux malware masquerading as systemd process](https://blog.netlab.360.com/stealth_rotajakiro_backdoor_en/)

- [QEMU 6.0 emulator release](https://lists.nongnu.org/archive/html/qemu-devel/2021-04/msg06210.html)

- [GhostBSD Release 04/21/27](https://ghostbsd.org/GhostBSD_21.04.27_ISO%27s_are_now_available)

- [The Free Software Foundation certifies ThinkPenguin TPE-R1300 Wireless Router](https://www.fsf.org/news/free-software-wireless-n-mini-router-v3-from-thinkpenguin-inc-now-fsf-certified-to-respect-your-freedom)

- [Postfix 3.6.0 mail server released](https://www.postfix.org/)

- [OpenBSD 6.9 Released](https://www.mail-archive.com/announce@openbsd.org/msg00371.html)

- [OpenIndiana 2021.04, continuing the development of OpenSolaris](http://docs.openindiana.org/release-notes/2021.04-release-notes/)

- [The first test release of Rocky Linux](https://rockylinux.org/news/rocky-linux-8-3-rc1-release/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
