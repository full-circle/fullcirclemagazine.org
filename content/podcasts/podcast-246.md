---
title: "Full Circle Weekly News 246"
date: 2022-01-30
draft: false
tags:
  - "alphaplot"
  - "arch"
  - "archlabs"
  - "arti"
  - "centos"
  - "deepin"
  - "essence"
  - "ffmpeg"
  - "ghostbsd"
  - "kernel"
  - "lighttpd"
  - "mumble"
  - "ocr"
  - "onlyoffice"
  - "plotting"
  - "radio"
  - "rust"
  - "server"
  - "shell"
  - "suse"
  - "systemrescue"
  - "tor"
  - "wine"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20246.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Arti 0.0.3, a Rust implementation of the Tor client](https://gitlab.torproject.org/tpo/core/arti/)

- [SystemRescue 9.0.0 released](https://www.system-rescue.org/)

- [Release of GhostBSD 01/22/12](https://ghostbsd.org/ghostbsd_22.01.12_iso_is_now_available)

- [Release of Mumble 1.4](https://www.mumble.info/)

- [Essence - an original operating system with its own kernel and graphical shell](https://nakst.gitlab.io/essence)

- [FFmpeg 5.0 released](https://ffmpeg.org/index.html#news)

- [Release of GNU Radio 3.10.0](https://www.gnuradio.org/news/2022-01-17-gnuradio-v3.10.0.0-release/)

- [Release of the Deepin 20.4](http://www.deepin.org/?language=en)

- [ONLYOFFICE Docs 7.0 office suite](https://www.onlyoffice.com/blog/2022/01/onlyoffice-docs-7-0/)

- [Stable release of Wine 7.0](https://www.winehq.org/announce/7.0)

- [Release of AlphaPlot, a scientific plotting program](https://github.com/narunlifescience/AlphaPlot/releases/tag/1.02)

- [Lighttpd http server release 1.4.64](https://blog.lighttpd.net/articles/2022/01/19/lighttpd-1.4.64-released/)

- [SUSE develops its own replacement for CentOS 8](https://www.suse.com/products/suse-liberty-linux/)

- [GNU Ocrad 0.28 OCR released](https://www.mail-archive.com/info-gnu@gnu.org/msg02997.html)

- [New version of Monitorix 3.14.0](http://www.monitorix.org/news.html?n=20220118)

- [ArchLabs release 2022.01.18](https://forum.archlabslinux.com/t/archlabs-2022-01-18-available-for-download/6136)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
