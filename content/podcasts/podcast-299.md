---
title: "Full Circle Weekly News 299"
date: 2023-02-08
draft: false
tags:
  - "hellosystem"
  - "nolibc"
  - "signals"
  - "gstreamer"
  - "opensuse"
  - "codec"
  - "tangram"
  - "palemoon"
  - "openvpn"
  - "ondsel"
  - "xine"
  - "wine"
  - "vulkan"
  - "canonical"
  - "opnsense"
  - "openenroth"
  - "carbonyl"
  - "browser"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20299.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of helloSystem 0.8 BSD:](https://fosstodon.org/@probono/109729091492664929)

- [The nolibc C-library is supporting signals:](https://lkml.org/lkml/2023/1/8/79)

- [Tangram 2.0, web browser based on WebKitGTK:](https://github.com/sonnyp/Tangram/releases/tag/v2.0)

- [GStreamer 1.22.0:](http://gstreamer.freedesktop.org/releases/1.22/)

- [The openSUSE simplified the H.264 codec installation process:](https://news.opensuse.org/2023/01/24/opensuse-simplifies-codec-install/)

- [Release of Pale Moon 32:](https://forum.palemoon.org/viewtopic.php?t=29363&p=235914#p235914)

- [OpenVPN 2.6.0 is available:](https://openvpn.net/)

- [Ondsel to promote professional use of FreeCAD:](https://wiki.freecadweb.org/Path_Workbench)

- [New xine 1.2.13:](https://sourceforge.net/projects/xine/files/xine-lib/1.2.13/README.txt/view)

- [Wine added support for HDR for Vulkan:](https://source.winehq.org/git/wine.git/commitdiff/90416a48af01c07b19b620474c4a65888415fc9e)

- [Canonical announces the availability of the Ubuntu Pro service:](https://canonical.com//blog/ubuntu-pro-enters-ga)

- [Release of OPNsense 23.1:](https://www.deciso.com/opnsense-23-1-quintessential-quail-released/)

- [OpenEnroth - an open engine for Might and Magic VI-VIII games](https://github.com/OpenEnroth/OpenEnroth)

- [Carbonyl console browser:](https://fathy.fr/carbonyl)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
