---
title: "Full Circle Weekly News 19"
date: 2016-05-28
draft: false
tags:
  - "android"
  - "bug"
  - "chromebook"
  - "chromium"
  - "compact"
  - "destroyer"
  - "display"
  - "fancy"
  - "google"
  - "millenium"
  - "mir"
  - "oems"
  - "pc"
  - "powered"
  - "security"
  - "support"
  - "torvalds"
  - "unix"
  - "vulkan"
  - "zumwalt"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2019.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux powered Zumwalt Destroyer delivered to US navy](http://www.techworm.net/2016/05/linux-powered-zumwalt-destroyer-delivered-us-navy-3-billion.html)

- [Chromebooks are about to take over](http://www.techinsider.io/chromebooks-to-run-android-apps-2016-5)

- [Vulkan Support May Soon Be Implemented in Ubuntu Linux's Mir Display Server](http://news.softpedia.com/news/vulkan-support-might-be-implemented-in-ubuntu-linux-s-mir-display-server-soon-504359.shtml)

- [Torvalds unhappy with sloppy Unix Millennium Bug](http://www.neowin.net/news/torvalds-unhappy-with-sloppy-unix-millennium-bug-patches-for-linux-kernel)

- [Fancy is a compact PC that runs Chromium, Ubuntu, or Android](http://liliputing.com/2016/05/fancy-225-compact-pc-runs-chromium-ubuntu-windows.html)

- [Google pressuring OEMs and carriers to speed up Android updates, security patches](http://9to5google.com/2016/05/25/android-security-os-updates/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
