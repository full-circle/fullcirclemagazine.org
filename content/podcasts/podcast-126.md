---
title: "Full Circle Weekly News 126"
date: 2019-04-02
draft: false
tags:
  - "16-04"
  - "18-04-02"
  - "3-13"
  - "beta"
  - "canonical"
  - "comet"
  - "cpu"
  - "desktop"
  - "flatpak"
  - "fortitude"
  - "intel"
  - "kernel"
  - "linux"
  - "lts"
  - "mate"
  - "patch"
  - "solus"
  - "suse"
  - "tails"
  - "tor"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20126.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Zorin OS 15 Enters Beta with Flatpak Support, Based on Ubuntu 18.04.2 LTS](https://news.softpedia.com/news/zorin-os-15-enters-beta-with-flatpak-support-based-on-ubuntu-18-04-2-lts-525373.shtml)

- [Solus 4 "Fortitude" Officially Released, It's Now Available for Download](https://news.softpedia.com/news/solus-4-fortitude-officially-released-it-s-now-available-for-download-525323.shtml)

- [MATE 1.22 Linux Desktop Is Here With Improvements And Fixes Source](https://fossbytes.com/mate-1-22-linux-desktop-features-update/)

- [SUSE, The First Enterprise Linux Company, Is Again Independent](https://fossbytes.com/suse-open-source-linux-company-independent/)

- [Tails 3.13 closes Lücḱen in Tor Browser and Thunderbird](https://www.pro-linux.de/news/1/26885/tails-313-schlie%C3%9Ft-l%C3%BCc%E1%B8%B1en-in-tor-browser-und-thunderbird.html)

- [Canonical Releases Important Linux Kernel Patch for Ubuntu 16.04 LTS, Update Now](https://news.softpedia.com/news/canonical-releases-important-linux-kernel-patch-for-ubuntu-16-04-lts-update-now-525339.shtml)

- [Intel Comet Lake Processors To Feature Up To 10 Cores: Linux Support List](https://fossbytes.com/intel-comet-lake-processors-to-feature-up-to-10-cores-linux-support-list/)
