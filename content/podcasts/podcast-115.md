---
title: "Full Circle Weekly News 115"
date: 2018-11-18
draft: false
tags:
  - "18-04"
  - "19-1"
  - "4-20"
  - "5-0"
  - "azure"
  - "bsd"
  - "bug"
  - "canonical"
  - "debian"
  - "github"
  - "hat"
  - "ibm"
  - "kde"
  - "kernel"
  - "linux"
  - "mint"
  - "open"
  - "red"
  - "redhat"
  - "root"
  - "shuttleworth"
  - "source"
  - "spectre"
  - "spectrersb"
  - "stretch"
  - "support"
  - "system76"
  - "thelio"
  - "torvalds"
  - "ubuntu"
  - "virtualbox"
  - "x-org"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20115.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Open Source Software: 20-Plus Years of Innovation](https://www.linuxinsider.com/story/Open-Source-Software-20-Plus-Years-of-Innovation-85646.html)

- [IBM Buys Linux & Open Source Software Distributor Red Hat For $34 Billion](https://fossbytes.com/ibm-buys-red-hat-open-source-linux/)

- [We (may) now know the real reason for that IBM takeover. A distraction for Red Hat to axe KDE](https://www.theregister.co.uk/2018/11/02/rhel_deprecates_kde/)

- [Ubuntu Founder Mark Shuttleworth Has No Plans Of Selling Canonical](https://fossbytes.com/ubuntu-founder-mark-shuttleworth-has-no-plans-of-selling-canonical/)

- [Mark Shuttleworth reveals Ubuntu 18.04 will get a 10-year support lifespan](https://www.zdnet.com/article/mark-shuttleworth-reveals-ubuntu-18-04-will-get-a-10-year-support-lifespan/)

- [Debian GNU/Linux 9.6 "Stretch" Released with Hundreds of Updates](https://news.softpedia.com/news/debian-gnu-linux-9-6-stretch-released-with-hundreds-of-updates-download-now-523739.shtml)

- [Fresh Linux Mint 19.1 Arrives This Christmas](https://www.forbes.com/sites/jasonevangelho/2018/11/01/fresh-linux-mint-19-1-arrives-this-christmas/#6c64618d293d)

- [Linux-friendly company System76 shares more open source Thelio computer details Source](https://betanews.com/2018/10/26/system76-open-source-thelio-linux/)

- [Linus Torvalds Says Linux 5.0 Comes in 2019, Kicks Off Development of Linux 4.20](https://news.softpedia.com/news/linus-torvalds-is-back-kicks-off-the-development-of-linux-kernel-4-20-523622.shtml)

- [Canonical Adds Spectre V4, SpectreRSB Fixes to New Ubuntu 18.04 LTS Azure Kernel](https://news.softpedia.com/news/canonical-adds-spectre-v4-spectrersb-fixes-to-new-ubuntu-18-04-lts-azure-kernel-523533.shtml)

- [Trivial Bug in X.Org Gives Root Permission on Linux and BSD Systems](https://www.bleepingcomputer.com/news/security/trivial-bug-in-xorg-gives-root-permission-on-linux-and-bsd-systems/)

- [Security Researcher Drops VirtualBox Guest-to-Host Escape Zero-Day on GitHub](https://news.softpedia.com/news/security-researcher-drops-virtualbox-guest-to-host-escape-zero-day-on-github-523660.shtml)
