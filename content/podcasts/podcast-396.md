---
title: "Full Circle Weekly News 396"
date: 2025-01-11
draft: false
tags:
  - "budgie"
  - "wayland"
  - "almalinux"
  - "seamonkey"
  - "orangepi"
  - "scribus"
  - "tails"
  - "opensuse"
  - "cosmic"
  - "ebian"
  - "enlightenment"
  - "torvalds"
  - "kernel"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20396.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Budgie 10.10 Will Only Retain Wayland Support:](https://buddiesofbudgie.org/blog/state-of-the-budgie-2024)

- [Release of fheroes2 1.1.5:](https://github.com/ihhub/fheroes2/releases/tag/1.1.5)

- [AlmaLinux Kitten 10 update:](https://almalinux.org/blog/2025-01-07-almalinux-os-kitten-10-updates/)

- [SeaMonkey 2.5 Suite 3.20 Released:](https://blog.seamonkey-project.org/2025/01/07/seamonkey-2-53-20-is-out/)

- [OrangePi Neo Gaming Console Comes with Manjaro Linux:](https://neo.manjaro.org/)

- [Scribus 1.6.3 Update:](https://www.scribus.net/scribus-1-6-3-released/)

- [Tails 6.11 release with fixes for issues identified by security audit:](https://tails.net/news/version_6.11/index.en.html)

- [January release of openSUSE Slowroll:](https://news.opensuse.org/2025/01/09/ny-starts-with-slowroll-vb/)

- [Fifth alpha release of the COSMIC desktop environment:](https://blog.system76.com/post/cosmic-alpha-5-released)

- [ebian 12.9 Released:](https://www.debian.org/News/2025/20250111)

- [Release of Enlightenment 0.27 and EFL 1.28 libraries:](https://www.enlightenment.org/news/2025-01-11-enlightenment-0.27.0)

- [Linus Torvalds to Give Away a Guitar Pedal of His Own Design Among Kernel Developers:](https://lkml.org/lkml/2025/1/12/429)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
