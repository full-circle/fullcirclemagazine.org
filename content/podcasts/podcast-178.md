---
title: "Full Circle Weekly News 178"
date: 2020-07-18
draft: false
tags:
  - "2020"
  - "alpha"
  - "ardour"
  - "audacity"
  - "blackarch"
  - "clonezilla"
  - "endless"
  - "gnome"
  - "inkscape"
  - "kali"
  - "kaos"
  - "kde"
  - "kid3"
  - "libre"
  - "libreoffice"
  - "linux"
  - "office"
  - "oracle"
  - "patch"
  - "pi"
  - "pixelorama"
  - "plasma"
  - "pop"
  - "raspberry"
  - "raspbian"
  - "studio"
  - "tails"
  - "transmission"
  - "ubuntu"
  - "virtualbox"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20178.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Oracle's Patch Reduces Boot Times By Almost Half](https://www.phoronix.com/scan.php?page=news_item&px=Oracle-Faster-Linux-Boot-PADATA)

- [Inkscape 1.0 Released, Finally](https://inkscape.org/news/2020/05/04/introducing-inkscape-10/)

- [Ubuntu Studio 20.10 To Ship With Plasma](https://ubuntustudio.org/2020/05/progress-on-plasma/)

- [Ubuntu 20.04 Certifies the Pi](https://ubuntu.com/blog/ubuntu-20-04-lts-is-certified-for-the-raspberry-pi)

- [Audacity Released 2.4, Withdrew It, Then Released It Again](https://www.audacityteam.org/audacity-2-4-0-released/)

- [Kid3 Goes from Hosted to Official KDE Application](https://kde.org/announcements/releases/2020-05-apps-update/)

- [Patent Dispute with Gnome Settled](https://www.gnome.org/news/2020/05/patent-case-against-gnome-resolved/)

- [Raspbian Changed to Raspberry Pi OS](https://www.raspberrypi.org/blog/8gb-raspberry-pi-4-on-sale-now-at-75/)

- [Linux Kernel 5.7 Released](http://lkml.iu.edu/hypermail/linux/kernel/2005.3/09342.html)

- [Pop! OS 20.04 LTS Out](https://blog.system76.com/post/616861064165031936/whats-new-with-popos-2004-lts)

- [Clonezilla Live 2.6.6 Out](https://sourceforge.net/p/clonezilla/news/2020/05/stable-clonezilla-live-266-15-released/)

- [KaOS 2020.05 Out](https://kaosx.us/news/2020/kaos05/)

- [Tails 4.6 Out](https://tails.boum.org/news/version_4.6/index.en.html)

- [Kali Linux 2020.2 Out](https://www.kali.org/news/kali-linux-2020-2-release/)

- [Endless OS 3.8.1 Out](https://community.endlessos.com/t/release-endless-os-3-8-1/13010)

- [BlackArch 2020.06.01 Out](https://9to5linux.com/latest-blackarch-linux-iso-adds-more-than-150-new-hacking-tools-linux-5-6)

- [KDE Plasma 5.18.5 LTS Out](https://kde.org/announcements/plasma-5.18.4-5.18.5-changelog)

- [Gnome 3.36.2 Out](https://mail.gnome.org/archives/gnome-announce-list/2020-May/msg00002.html)

- [LibreOffice 6.4.4 Out](https://blog.documentfoundation.org/blog/2020/05/21/libreoffice-644/)

- [LibreOffice 7.0 Alpha 1 Out](https://qa.blog.documentfoundation.org/2020/05/12/libreoffice-7-0-alpha1-is-ready-for-testing/)

- [Virtualbox 6.1.8 Out](https://www.virtualbox.org/wiki/Changelog-6.1#v8)

- [Transmission 3.0 Out](https://github.com/transmission/transmission/releases/tag/3.00)

- [Ardour 6.0 Released](https://ardour.org/news/6.0.html)

- [Pixelorama 0.7 Out](https://www.orama-interactive.com/post/pixelorama-v0-7-is-out)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
