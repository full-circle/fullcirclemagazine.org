---
title: "Full Circle Weekly News 379"
date: 2024-08-18
draft: false
tags:
  - "binutils"
  - "nitrux"
  - "vortex"
  - "puppeteer"
  - "cosmic"
  - "desktop"
  - "librecuda"
  - "kernel"
  - "ladybird"
  - "swift"
  - "firefox"
  - "haiku"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20379.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of GNU Binutils 2.43:](https://www.mail-archive.com/info-gnu@gnu.org/msg03314.html)

- [Release of the Nitrux 3.6.0:](https://nxos.org/changelog/release-announcement-nitrux-3-6-0/)

- [Vortex 2.2 is available:](https://github.com/vortexgpgpu/vortex/releases/tag/v2.2)

- [Release of Puppeteer 23:](https://hacks.mozilla.org/2024/08/puppeteer-support-for-firefox/)

- [First alpha release of the COSMIC desktop environment:](https://system76.com/cosmic)

- [LibreCUDA project to run CUDA code on NVIDIA GPUs without proprietary Runtime:](https://github.com/mikex86/LibreCuda)

- [Ubuntu is moving to the latest kernel versions in upcoming releases:](https://discourse.ubuntu.com/t/kernel-version-selection-for-ubuntu-releases/47007)

- [Ladybird browser moves to Swift:](https://x.com/awesomekling/status/1822236888188498031)

- [Firefox ported to Haiku OS:](https://discuss.haiku-os.org/t/progress-on-porting-firefox/13493/143)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
