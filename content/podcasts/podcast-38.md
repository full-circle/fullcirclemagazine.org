---
title: "Full Circle Weekly News 38"
date: 2016-10-08
draft: false
tags:
  - "alwsl"
  - "arch"
  - "beta"
  - "linux"
  - "lite"
  - "mini"
  - "mint"
  - "mintbox"
  - "mirai"
  - "pro"
  - "rescue"
  - "subsystem"
  - "system"
  - "systemd"
  - "systemrescuecd"
  - "trojan"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2038.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Mintbox Mini Pro: A Cheap Linux Machine With Compelling Specs](https://fossbytes.com/mintbox-mini-pro-a-cheap-linux-machine-with-compelling-specs/)

- [alwsl Project Lets You Install Arch Linux in the Windows Subsystem for Linux](http://news.softpedia.com/news/alwsl-project-lets-you-install-arch-linux-in-the-windows-subsystem-for-linux-508956.shtml)

- [Linux Lite 3.2 Enters Beta, Now Plays Nice with Other Distributions](http://news.softpedia.com/news/linux-lite-3-2-enters-beta-now-plays-nice-with-other-gnu-linux-distributions-508998.shtml)

- [SystemRescueCd 4.8.3 Released](http://news.softpedia.com/news/systemrescuecd-4-8-3-released-with-x-org-server-1-18-4-linux-kernel-4-4-23-lts-508905.shtml)

- [You can crash Linux Systemd with a single command line](http://www.techworm.net/2016/10/can-crash-linux-systemd-single-tweet.html)

- [Meet the Linux.Mirai Trojan, a DDoS nightmare](https://www.hackread.com/linux-mirai-trojan-a-ddos-nightmare/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
