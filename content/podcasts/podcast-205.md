---
title: "Full Circle Weekly News 205"
date: 2021-04-11
draft: false
tags:
  - "android"
  - "audio"
  - "cobol"
  - "codec"
  - "compiler"
  - "engine"
  - "ffmpeg"
  - "firefox"
  - "game"
  - "gateway"
  - "gnupgp"
  - "google"
  - "hack"
  - "haruna"
  - "ibm"
  - "icewm"
  - "java"
  - "linux"
  - "live"
  - "lyra"
  - "mail"
  - "manager"
  - "mozilla"
  - "oracle"
  - "php"
  - "proxmail"
  - "server"
  - "speech"
  - "storm"
  - "tex"
  - "video"
  - "webos"
  - "webrender"
  - "window"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20205.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Proxmox Mail Gateway 6.4 released](https://www.proxmox.com/en/proxmox-mail-gateway)

- [Haruna 0.6.0 video player available](https://github.com/g-fb/haruna)

- [TeX Live 2021 released](https://www.mail-archive.com/cygwin-announce@cygwin.com/msg09615.html)

- [IceWM 2.3 window manager released](https://ice-wm.org/)

- [WebOS Open Source Edition 2.10 Release](https://www.webosose.org/blog/2021/04/02/webos-ose-2-10-0-release/)

- [Google wins Java and Android litigation with Oracle](https://www.supremecourt.gov/opinions/20pdf/18-956_d18f.pdf)

- [KDE has taken over the maintenance of the public Qt 5.15 branch](https://dot.kde.org/2021/04/06/announcing-kdes-qt-5-patch-collection)

- [Lyra audio codec for speech transmission with poor connection quality](https://opensource.googleblog.com/2021/04/lyra-enabling-voice-calls-for-next-billion-users.html)

- [Firefox decided not to remove compact mode and enable WebRender for all Linux environments](https://bugzilla.mozilla.org/show_bug.cgi?id=1703254)

- [IBM to publish COBOL compiler for Linux](https://www-01.ibm.com/common/ssi/ShowDoc.wss?docURL=/common/ssi/rep_ca/9/872/ENUSAP21-0019/index.html&request_locale=en)

- [GnuPG 2.3.0 Released](https://lists.gnupg.org/pipermail/gnupg-devel/2021-April/034828.html)

- [Release of FFmpeg 4.4](http://ffmpeg.org/)

- [PHP server hack report](https://externals.io/message/113981)

- [Storm game engine open sourced](https://store.steampowered.com/news/app/223330/view/3013444995188538670)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
