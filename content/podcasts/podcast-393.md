---
title: "Full Circle Weekly News 393"
date: 2024-11-24
draft: false
tags:
  - "curl"
  - "cachyos"
  - "luanti"
  - "dxvk"
  - "red hat"
  - "redhat"
  - "debian"
  - "phosh"
  - "openwrt"
  - "archinstall"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20393.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [The tools wget 1.25 and Curl 8.11 are now available:](https://daniel.haxx.se/blog/2024/11/07/rock-solid-curl/) [and](https://translate.google.com/website?sl=auto&tl=en&hl=en-US&u=https://www.mail-archive.com/info-gnu@gnu.org/msg03333.html)

- [CachyOS 241110 Released:](https://cachyos.org/blog/2411-november-release/)

- [Release of Luanti 5.10.0:](https://forum.minetest.net/viewtopic.php?t%3D31123)

- [DXVK 2.5 Release:](https://github.com/doitsujin/dxvk/releases/tag/v2.5)

- [Red Hat Enterprise Linux 10 Beta Release and RHEL 9.5 Release:](https://www.redhat.com/en/blog/red-hat-enterprise-linux-95-release)

- [Debian Junior Project:](https://lists.debian.org/debian-jr/2024/11/msg00009.html)

- [Phosh 0.43.0,:](https://phosh.mobi/releases/rel-0.43.0/)

- [OpenWrt switches to APK package manager:](https://forum.openwrt.org/t/major-change-notice-new-package-manager/215682)

- [Release of Archinstall 3.0.0:](https://github.com/archlinux/archinstall/releases/tag/v3.0.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
