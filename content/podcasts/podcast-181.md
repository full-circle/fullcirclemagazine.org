---
title: "Full Circle Weekly News 181"
date: 2020-09-03
draft: false
tags:
  - "20-04"
  - "boothole"
  - "debian"
  - "firefox"
  - "fix"
  - "gnome"
  - "kali"
  - "kde"
  - "kernel"
  - "libre"
  - "libreoffice"
  - "linux"
  - "lts"
  - "mozilla"
  - "mx"
  - "neon"
  - "office"
  - "point"
  - "radeon"
  - "rc"
  - "red-hat"
  - "redhat"
  - "release"
  - "rhino"
  - "rolling"
  - "ubuntu"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20181.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu LTS Point Releases Are Here](http://fridge.ubuntu.com/2020/08/06/ubuntu-20-04-1-lts-released/)

- [Rolling Rhino Turns Ubuntu 20.04 into a Rolling Release](https://github.com/wimpysworld/rolling-rhino)

- [Boothole, A Linux Security Vulnerability](https://eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/)

- [Red Hat's Boothole Fix Causes Issues](https://www.zdnet.com/article/red-hat-enterprise-linux-runs-into-boothole-patch-trouble/)

- [Firefox Cuts Jobs Again](https://arstechnica.com/information-technology/2020/08/firefox-maker-mozilla-lays-off-250-workers-says-covid-19-lowered-revenue/)

- [Debian 10.5 Out](https://www.debian.org/News/2020/20200801)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
