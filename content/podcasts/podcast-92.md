---
title: "Full Circle Weekly News 92"
date: 2018-05-06
draft: false
tags:
  - "18-04"
  - "chromebooks"
  - "debian"
  - "distro"
  - "escalation"
  - "flaw"
  - "huawei"
  - "kali"
  - "kernel"
  - "linux"
  - "lite"
  - "lts"
  - "patch"
  - "pop_os"
  - "twitter"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2092.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Kali Linux 2018.2 Ethical Hacking Distro Is Here](https://fossbytes.com/kali-linux-2018-2-download-iso-torrent-features/)

- [Major Debian Linux Kernel Patch Fixes 8-Year-Old Privilege Escalation Flaw](https://news.softpedia.com/news/major-debian-linux-kernel-patch-fixes-8-year-old-privilege-escalation-flaw-520942.shtml)

- [Twitter Wants 336 Million Users To Change Passwords, Bug Exposed Them In Plain Text](https://fossbytes.com/twitter-bug-exposed-password-plain-text/)

- [Linux Apps Are Coming to Chromebooks and You Can Try Them Right Now](https://news.softpedia.com/news/linux-apps-are-coming-to-chromebooks-and-you-can-try-them-now-in-chrome-os-dev-520966.shtml)

- [Linux Lite 4.0 OS Enters Beta with New Look and Feel, Based on Ubuntu 18.04 LTS](https://news.softpedia.com/news/linux-lite-4-0-os-enters-beta-with-new-look-and-feel-based-on-ubuntu-18-04-lts-520892.shtml)

- [Pop!\_OS 18.04 Released — Get System76’s Beautiful Ubuntu-based Linux Distro](https://fossbytes.com/pop_os-18-04-release-download-features/)

- [Huawei could be working on its own OS as an Android alternative](https://www.techradar.com/news/huawei-could-be-working-on-its-own-os-as-an-android-alternative)
