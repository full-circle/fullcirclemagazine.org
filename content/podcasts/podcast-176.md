---
title: "Full Circle Weekly News 176"
date: 2020-06-18
draft: false
tags:
  - "baker"
  - "ceo"
  - "compose"
  - "docker"
  - "driver"
  - "firefox"
  - "foliate"
  - "freerdp"
  - "git"
  - "license"
  - "licensing"
  - "mozilla"
  - "nvidia"
  - "opensuse"
  - "president"
  - "qt"
  - "red-hat"
  - "redhat"
  - "suse"
  - "tails"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20176.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Red Hat Names Paul Cormier President and CEO](https://www.businesswire.com/news/home/20200406005139/en)

- [Mitchell Baker Named CEO of Mozilla](https://blog.mozilla.org/blog/2020/04/08/mitchell-baker-named-ceo-of-mozilla/)

- [Docker Users Targeted with Crypto Malware](https://blog.aquasec.com/threat-alert-kinsing-malware-container-vulnerability)

- [Docker Compose Is Now Open Source](https://www.docker.com/blog/announcing-the-compose-specification/)

- [Gnome Announces Community Engagement Challenge](https://itsfoss.com/gnome-community-engagement-challenge/)

- [QT Makes Changes to Licensing](https://mail.kde.org/pipermail/kde-community/2020q2/006098.html)

- [OpenSUSE Wants to Close the Leap Gap](https://lists.opensuse.org/opensuse-announce/2020-04/msg00000.html)

- [Git Turns 15](https://www.itwire.com/the-linux-distillery/git-version-control-system-hits-15-year-milestone.html)

- [Firefox 75 Out](https://www.mozilla.org/en-US/firefox/75.0/releasenotes/)

- [Nvidia Long Lived Linux Driver 440.82 Out](https://www.gamingonlinux.com/articles/nvidia-released-the-44082-stable-long-lived-linux-driver-helps-doom-eternal-on-steam-play-proton.16382)

- [Foliate 2.0 Out](https://www.linuxuprising.com/2020/04/foliate-linux-gtk-ebook-reader-20.html)

- [Tails 4.5 Out](https://tails.boum.org/news/version_4.5/index.en.html)

- [FreeRDP 2.0 Out](http://www.freerdp.com/2020/04/09/2_0_0-released)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
