---
title: "Full Circle Weekly News 301"
date: 2023-02-18
draft: false
tags:
  - "openttd"
  - "mythtv"
  - "fedora"
  - "flathub"
  - "transmission"
  - "kernel"
  - "endless"
  - "ocular"
  - "freetype"
  - "gtk5"
  - "networkmanager"
  - "google"
  - "telemetry"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20301.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of OpenTTD 13.0:](https://www.openttd.org/news/2023/02/05/openttd-13-0)

- [Release of MythTV 33:](https://www.mythtv.org/news/173/v33.1%20Released)

- [Fedora 38 to offer unlimited support for Flathub:](https://pagure.io/fesco/issue/2939)

- [New version of Transmission 4.0.0:](http://www.transmissionbt.com/)

- [Linux 6.1 kernel is long term support:](https://kernel.org/category/releases.html)

- [Release of Endless OS Distribution 5.0:](https://community.endlessos.com/t/release-endless-os-5-0-0/19857)

- [Release of Ocular GOST:](http://okulargost.ru/)

- [Release of FreeType 2.13:](https://www.mail-archive.com/freetype-announce@nongnu.org/msg00136.html)

- [Work on the GTK5 will begin at the end of the year:](https://blog.gtk.org/2023/02/09/updates-from-inside-gtk/)

- [Release of NetworkManager 1.42.0:](https://networkmanager.dev/blog/networkmanager-1-42/)

- [Google intends to add telemetry to the Go language:](https://github.com/golang/go/discussions/58409)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
