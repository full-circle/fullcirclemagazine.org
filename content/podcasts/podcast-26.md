---
title: "Full Circle Weekly News 26"
date: 2016-07-16
draft: false
tags:
  - "16-10"
  - "ai"
  - "canonical"
  - "creation"
  - "creator"
  - "gtk"
  - "microsoft"
  - "mycroft"
  - "nautilus"
  - "nsa"
  - "ota-12"
  - "ota12"
  - "radiance"
  - "skype"
  - "snap"
  - "snapcraft"
  - "snaps"
  - "theme"
  - "tool"
  - "tor"
  - "tracking"
  - "yak"
  - "yakkety"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2026.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical Releases Snapcraft 2.12.1 Snap Creator Tool for Ubuntu 16.04](http://news.softpedia.com/news/canonical-releases-snapcraft-2-12-1-snap-creator-tool-for-ubuntu-16-04-lts-506287.shtml)

- [Use Linux or Tor? The NSA might just be tracking you](http://www.trustedreviews.com/news/nsa-use-linux-or-tor-you-re-probably-a-terrorist)

- [Ubuntu 16.10 Getting Nautilus 3.20 Soon, Radiance Theme Fully Ported to GTK 3.20](http://news.softpedia.com/news/ubuntu-16-10-getting-nautilus-3-20-soon-radiance-theme-fully-ported-to-gtk-3-20-506314.shtml)

- [Mycroft Uses Ubuntu and Snaps to Deliver a Free Intelligent Personal Assistant](http://news.softpedia.com/news/mycroft-uses-ubuntu-and-snaps-to-deliver-a-free-intelligent-personal-assistant-506097.shtml)

- [Ubuntu Touch OTA-12 Lands Next Wednesday, OTA-13 Brings Libertine Improvements](http://news.softpedia.com/news/ubuntu-touch-ota-12-to-land-next-wednesday-ota-13-brings-libertine-improvements-506215.shtml)

- [Microsoft is replacing Skype’s ancient Linux client with a web app](http://www.pcworld.com/article/3095391/linux/microsoft-is-replacing-skype-s-ancient-linux-client-with-a-web-app-sort-of.html)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
