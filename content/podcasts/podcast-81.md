---
title: "Full Circle Weekly News 81"
date: 2018-02-19
draft: false
tags:
  - "canonical"
  - "cloudlinux"
  - "data"
  - "diamond"
  - "firefox"
  - "hacking"
  - "jade"
  - "kali"
  - "kde"
  - "kernelcare"
  - "linux"
  - "meltdown"
  - "mint"
  - "mobile"
  - "opera"
  - "oracle"
  - "plasma"
  - "quantum"
  - "redhat"
  - "skype"
  - "slimbook"
  - "snap"
  - "spectre"
  - "touch"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2081.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Why KDE's Plasma Mobile is the ideal platform for Linux fans and developers](https://www.techrepublic.com/article/why-kdes-plasma-mobile-is-the-ideal-platform-for-linux-fans-and-developers/)

- [KDE Slimbook II is Cheaper and More Powerful](http://news.softpedia.com/news/kde-slimbook-ii-plasma-based-linux-ultrabook-laptop-is-cheaper-more-powerful-519729.shtml)

- [Skype Released as Snap on Ubuntu, Linux Mint](http://news.softpedia.com/news/microsoft-loves-linux-skype-released-as-snap-on-ubuntu-linux-mint-519665.shtml)

- [Opera 51 Released It’s 38% Faster Than Firefox Quantum 58](https://fossbytes.com/opera-51-released-its-38-faster-than-firefox-quantum/)

- [Ubuntu Touch Jade Diamond is a Safe-for-Kids web browser](https://github.com/bhdouglass/jadediamond)

- [Meltdown/Spectre Status for Red Hat and Oracle](http://www.linuxjournal.com/content/meltdownspectre-status-red-hat-and-oracle)

- [CloudLinux's KernelCare Promises to Fix Meltdown & Spectre Flaws without Reboots](http://news.softpedia.com/news/cloudlinux-s-kernelcare-promises-to-fix-meltdown-spectre-flaws-without-reboots-519728.shtml)

- [Kali Linux 2018.1 Released For Ethical Hackers](https://fossbytes.com/kali-linux-2018-1-released-download-features/)

- [Canonical wants to collect Anonymous User Data](http://www.techradar.com/news/ubuntu-plans-to-collect-data-on-desktop-pcs-unless-you-tell-it-not-to)
