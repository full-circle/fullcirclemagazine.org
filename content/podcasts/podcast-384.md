---
title: "Full Circle Weekly News 384"
date: 2024-09-28
draft: false
tags:
  - "gtk"
  - "redox"
  - "droidian"
  - "radicle"
  - "gentoo"
  - "ardour"
  - "ubuntu"
  - "maestro"
  - "shotcut"
  - "haiku"
  - "nextcloud"
  - "samba"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20384.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [GTK 4.16 is available:](https://gitlab.gnome.org/GNOME/gtk/-/tags/4.16.0)

- [Release of Redox OS 0.9:](https://www.redox-os.org/news/release-0.9.0/)

- [Droidian 99, has been released:](https://github.com/droidian-images/droidian/releases/tag/droidian%252F99)

- [Radicle 1.0 released:](https://radicle.xyz/2024/09/10/radicle-1.0.html)

- [Gentoo Linux Significantly Improves Support for MIPS and Alpha:](https://www.gentoo.org/news/2024/09/11/Improved-MIPS-and-Alpha-support.html)

- [Release of Ardour 8.7:](https://ardour.org/whatsnew.html)

- [Ubuntu 22.04.5 LTS Released:](https://lists.ubuntu.com/archives/ubuntu-announce/2024-September/000305.html)

- [Maestro console music player unveiled:](https://github.com/PrajwalVandana/maestro-cli)

- [Shotcut Video Editor Released:](https://shotcut.org/blog/new-release-240913/)

- [Haiku R1 Operating System Beta 5:](https://www.haiku-os.org/news/2024-09-13_haiku_r1_beta5/)

- [Nextcloud Hub 9 is released:](https://github.com/nextcloud/server/releases/tag/v30.0.0)

- [Sovereign Fund invests 688 thousand euros in the development of the Samba project:](https://lists.samba.org/archive/samba/2024-September/249751.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
