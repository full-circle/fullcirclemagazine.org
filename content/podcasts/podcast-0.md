---
title: "Full Circle Weekly News 00"
date: 2016-01-16
draft: false
tags:
  - "foss"
  - "linux"
  - "news"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2000.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

**The beta test version.**
This all began because I (Ronnie) was looking for a news only podcast for FOSS/Linux/Ubuntu, and found nothing. So, like the fool I am, I thought: why not start one! And here it is.
**DISCLAIMER**
I'm no professional announcer/podcaster, as you'll hear, so I'm still new at this. Constructive criticism is welcome.

Is this something you'd listen to, and I should keep doing? Let me know via email: [ronnie@fullcirclemagazine.org](mailto:ronnie@fullcirclemagazine.org).

**SHOW NOTES**
Episode #00
News compiled by Arnfried Walbrecht, and read by Ronnie Tucker.

- [Ubuntu aims for true convergence across devices in 2016](http://www.neowin.net/news/ubuntu-aims-for-true-convergence-across-devices-in-2016)

- [Linux Foundation to Develop Hyperledger along with Others](http://www.newsbtc.com/2015/12/27/linux-foundation-hyperledger/)

- [Mycroft Open Sources Artificial Intelligence Library for IoT Devices](http://thevarguy.com/open-source-application-software-companies/mycroft-open-sources-artificial-intelligence-library-iot-)

- [Ubuntu Touch to Support Encryption of User Data](http://news.softpedia.com/news/ubuntu-touch-to-support-encryption-of-user-data-498694.shtml)

- [What's new and nifty in Linux 4.4](http://www.zdnet.com/article/whats-new-and-nifty-in-linux-4-4/)

- [AT&T Adopts Canonical’s Ubuntu in Push to Replace Proprietary Systems with Open-Source Tech](http://blogs.wsj.com/cio/2016/01/13/att-adopts-canonicals-ubuntu-in-push-to-replace-proprietary-systems-with-open-source-tech/)

- [Google chairman Eric Schmidt says artificial intelligence platforms can 'change the world'](http://telecom.economictimes.indiatimes.com/news/google-chairman-eric-schmidt-says-artificial-intelligence-platforms-can-change-the-world/50556584)

- [China’s Baidu Releases Its AI Code](http://www.technologyreview.com/news/545486/chinas-baidu-releases-its-ai-code/)

**CREDITS**:

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- [Teletype - Halleck](http://freesound.org/people/Halleck/sounds/19488/)
