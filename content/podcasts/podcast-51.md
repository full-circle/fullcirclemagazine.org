---
title: "Full Circle Weekly News 51"
date: 2017-01-28
draft: false
tags:
  - "16-04-2"
  - "2-0"
  - "2-10"
  - "4-10"
  - "4-8"
  - "access"
  - "developers"
  - "handbrake"
  - "kernel"
  - "linux"
  - "point"
  - "release"
  - "root"
  - "systemd"
  - "tails"
  - "transcoder"
  - "tutorials"
  - "wine"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2051.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 16.04.2 LTS Point Release Coming On Feb 2 With Linux Kernel 4.8](https://fossbytes.com/ubuntu-16-04-2-second-point-release-date/)

- [Canonical launches Ubuntu Tutorials](http://betanews.com/2017/01/22/canonical-ubuntu-tutorials-linux-open-source/)

- [Ubuntu Developers Now Tracking Linux Kernel 4.10 for Ubuntu 17.04 (Zesty Zapus)](Zesty Zapus)

- [HandBrake 1.0.2 Video Transcoder Released for Linux, Mac and Windows](http://news.softpedia.com/news/handbrake-1-0-2-open-source-video-transcoder-released-for-linux-mac-and-windows-512134.shtml)

- [Linux Systemd Flaw Gives Attackers Root Access](https://www.bleepingcomputer.com/news/security/linux-systemd-flaw-gives-attackers-root-access/)

- [Wine 2.0 released: Run Windows apps in Linux](http://www.infoworld.com/article/3161704/linux/wine-2-0-released-run-windows-apps-in-linux.html)

- [Worried about your online privacy? Download Tails 2.10](https://betanews.com/2017/01/25/linux-tails-online-privacy-download/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
