---
title: "Full Circle Weekly News 59"
date: 2017-04-29
draft: false
tags:
  - "2038"
  - "aardvark"
  - "artful"
  - "clocks"
  - "gnome"
  - "halium-sailfish"
  - "kali"
  - "linux"
  - "malware"
  - "phone"
  - "touch"
  - "unix"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2059.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 17.10 (Artful Aardvark) Hits the Streets on October 19, with GNOME 3.26](Artful Aardvark)

- [Canonical Shuts Down Support For Ubuntu Phone, Updates Ending In June](https://fossbytes.com/canonical-shuts-down-support-for-ubuntu-phone-updates-ending-in-june/)

- [Hackers uncork experimental Linux-targeting malware](https://www.theregister.co.uk/2017/04/25/linux_malware/)

- [Kali Linux 2017.1 Released With New Features](https://fossbytes.com/kali-linux-2017-1-features-download-torrent-iso/)

- [What Is The Year 2038 Problem In Linux? Will Unix Clocks Fail On Jan. 19, 2038?](https://fossbytes.com/year-2038-problem-linux-unix/)

- [Linux on Android smartphones: Project Halium wants your handset to run Ubuntu, Sailfish](http://www.zdnet.com/article/linux-on-android-smartphones-project-halium-wants-your-handset-to-run-ubuntu-sailfish/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
