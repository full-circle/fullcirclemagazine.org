---
title: "Full Circle Weekly News 265"
date: 2022-07-18
draft: false
tags:
  - "ubuntu"
  - "apple"
  - "armbian"
  - "deepin"
  - "distrobox"
  - "driver"
  - "filter"
  - "focal"
  - "gedit"
  - "gnome"
  - "ipad"
  - "linuxfx"
  - "lxle"
  - "mandriva"
  - "nftables"
  - "nitrux"
  - "nix"
  - "nixos"
  - "nvidia"
  - "openmandriva"
  - "perl"
  - "powerdns"
  - "recursor"
  - "steam"
  - "steamos"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20265.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of LXLE Focal:](https://lxle.net/articles/?post=lxle-focal-released)

- [OpenMandriva rock n roll:](https://forum.openmandriva.org/t/new-lxqt-isos-for-rock-rolling/4476)

- [PowerDNS Recursor 4.7.0 Caching DNS Server Released:](https://blog.powerdns.com/2022/05/30/powerdns-recursor-4-7-0-released/)

- [Release of the NixOS 22.05 distribution using the Nix package manager:](https://nixos.org/blog/announcements.html#22.05)

- [Ubuntu 22.10 will replace GEdit with the new GNOME text editor:](https://discourse.ubuntu.com/t/proposal-gnome-text-editor-as-default-text-editor/28286/2)

- [NVIDIA proprietary driver release 515.48.07:](https://forums.developer.nvidia.com/t/linux-solaris-and-freebsd-driver-515-48-07-production-branch-release/216112)

- [nftables packet filter 1.0.3 released:](https://www.mail-archive.com/netfilter-announce@lists.netfilter.org/msg00242.html)

- [Perl 7 will smoothly continue the development of Perl 5 without breaking backwards compatibility:](http://blogs.perl.org/users/psc/2022/05/what-happened-to-perl-7.html)

- [Release of Steam OS 3.2:](https://store.steampowered.com/news/app/1675200/view/3297210455204145216)

- [A hardwired password for accessing the user base has been revealed in the Linuxfx distribution:](https://kernal.eu/posts/linuxfx/)

- [Release of the Deepin 20.6:](https://www.deepin.org/en/2022/05/31/deepin-20-6-release/)

- [Release of Nitrux 2.2 distribution with NX Desktop:](https://nxos.org/changelog/release-announcement-nitrux-2-2-0/)

- [Distrobox 1.3, a toolkit for nested distributions:](https://github.com/89luca89/distrobox/releases/tag/1.3.0)

- [Armbian distribution release 22.05:](https://www.armbian.com/newsflash/armbian-22-05/)

- [Linux ported for Apple iPad tablets on A7 and A8 chips:](https://twitter.com/konradybcio/status/1532106368936706051)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
