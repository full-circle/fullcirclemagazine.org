---
title: "Full Circle Weekly News 262"
date: 2022-05-22
draft: false
tags:
  - "deadbeef"
  - "docker"
  - "eurolinux"
  - "fedora"
  - "gnome"
  - "kernel"
  - "linux"
  - "litestream"
  - "microsoft"
  - "multipass"
  - "nvidia"
  - "onlyoffice"
  - "pale moon"
  - "patches"
  - "photoflare"
  - "red hat"
  - "redhat"
  - "rust"
  - "sqlite"
  - "toybox"
  - "webrtc"
  - "weron"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20262.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Toybox 0.8.7:](https://github.com/landley/toybox/releases/tag/0.8.7)

- [Patches for the Linux kernel with Rust language support:](https://lkml.org/lkml/2022/5/7/13)

- [The Weron project developing VPN based on the WebRTC protocol:](https://github.com/pojntfx/weron/releases)

- [Photoflare 1.6.10 image editor released:](https://github.com/PhotoFlare/photoflare/releases/tag/v1.6.10)

- [GNOME Project Strategy in 2022:](https://discourse.gnome.org/t/evolving-a-strategy-for-2022-and-beyond/9759)

- [The Litestream project, implementing a replication system for SQLite:](https://fly.io/blog/all-in-on-sqlite-litestream/)

- [Microsoft releases CBL-Mariner 2.0:](https://github.com/microsoft/CBL-Mariner/releases/tag/2.0.20220426-2.0)

- [Fedora Linux 36 distribution released:](https://fedoramagazine.org/announcing-fedora-36/)

- [Red Hat Enterprise Linux 9:](https://www.redhat.com/en/about/press-releases/red-hat-defines-new-epicenter-innovation-red-hat-enterprise-linux-9)

- [Docker Desktop is available for Linux:](https://www.docker.com/blog/the-magic-of-docker-desktop-is-now-available-on-linux/)

- [Pale Moon Browser 31.0 Released:](https://forum.palemoon.org/viewtopic.php?t=28245&p=227357#p227357)

- [EuroLinux 8.6 released, compatible with RHEL:](https://en.euro-linux.com/blog/eurolinux-8-6-released/)

- [NVIDIA open source video drivers for Linux kernel:](https://developer.nvidia.com/blog/nvidia-releases-open-source-gpu-kernel-modules/)

- [Multipass 1.9, a toolkit for deploying Ubuntu in virtual machines:](https://ubuntu.com//blog/linux-on-mac-with-multipass-1-9)

- [Google launches team to help open source projects improve security:](https://blog.google/technology/safety-security/shared-success-in-building-a-safer-open-source-community/)

- [Release of ONLYOFFICE Docs 7.1 office suite:](https://www.onlyoffice.com/blog/2022/05/discover-onlyoffice-docs-v7-1/)

- [DeaDBeeF 1.9.0:](https://deadbeef.sourceforge.io/posts/deadbeef_1.9.0_is_out.html)

- [Alt Workstation K 10.0:](https://lists.altlinux.org/pipermail/altlinux-announce-ru/2022/000033.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
