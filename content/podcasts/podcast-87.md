---
title: "Full Circle Weekly News 87"
date: 2018-04-03
draft: false
tags:
  - "attack"
  - "branchscope"
  - "danos"
  - "facebook"
  - "firefox"
  - "foundation"
  - "goscanssh"
  - "linus"
  - "linux"
  - "malware"
  - "meltdown"
  - "mint"
  - "spectre"
  - "tara"
  - "torvalds"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2087.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Mint 19 "Tara" Will Ship in June, Pre-Installed on the Mintbox Mini 2 PCs](http://news.softpedia.com/news/linux-mint-19-tara-will-ship-in-june-pre-installed-on-the-mintbox-mini-2-pcs-520378.shtml)

- [Linus Torvalds says new Linux lands next week and he’s sticking to that … for now](https://www.theregister.co.uk/2018/03/26/linux_4_16_rc7/)

- [New Firefox Extension Builds a Wall Around Facebook](https://www.linuxinsider.com/story/New-Firefox-Extension-Builds-a-Wall-Around-Facebook-85233.html)

- [The Linux Foundation launches a deep learning foundation](https://techcrunch.com/2018/03/26/the-linux-foundation-launches-a-deep-learning-foundation/)

- [GoScanSSH malware targets Linux systems but avoids government servers](https://www.csoonline.com/article/3266586/security/goscanssh-malware-targets-linux-systems-but-avoids-government-servers.html)

- [After Meltdown and Spectre, Intel CPUs Are Now Vulnerable to BranchScope Attacks](http://news.softpedia.com/news/after-meltdown-and-spectre-intel-cpus-are-now-vulnerable-to-branchscope-attacks-520433.shtml)

- [Linux Foundation Introduces Network Operating System DANOS](https://www.pro-linux.de/news/1/25755/linux-foundation-stellt-netzwerkbetriebssystem-danos-vor.html)
