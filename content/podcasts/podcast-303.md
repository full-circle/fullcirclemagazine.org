---
title: "Full Circle Weekly News 303"
date: 2023-03-04
draft: false
tags:
  - "kernel"
  - "linux"
  - "fish"
  - "shell"
  - "rust"
  - "opensuse"
  - "jami"
  - "intel"
  - "sqlite"
  - "dbms"
  - "flatpak"
  - "gnome"
  - "opengl"
  - "ambient"
  - "github"
  - "microsoft"
  - "canonical"
  - "corbos"
  - "netbeans"
  - "mesa"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20303.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Linux kernel 6.2:](https://lkml.org/lkml/2023/2/19/309)

- [Release of Budgie 10.7.1:](https://blog.buddiesofbudgie.org/budgie-10-7-1-released/)

- [Fish shell is being rewritten in Rust language:](https://github.com/fish-shell/fish-shell/pull/9512)

- [openSUSE Leap beta release 15.5:](https://news.opensuse.org/2023/02/21/leap-reaches-beta-phase/)

- [Jami "Vilagfa" is available:](https://jami.net/vil/)

- [A plan to promote a Linux driver, Xe for Intel GPU:](https://lore.kernel.org/dri-devel/Y+%2Fo2a21Eym3ee%2Fa@phenom.ffwll.local/T/#m3fee000d74a8744ed65c02124587d1ada346e8bd)

- [Release of SQLite DBMS 3.41:](https://www.sqlite.org/changes.html)

- [Ubuntu will stop supporting Flatpak in basic delivery:](https://discourse.ubuntu.com/t/ubuntu-flavor-packaging-defaults/34061)

- [GNOME Mutter will stop supporting old versions of OpenGL:](https://gitlab.gnome.org/GNOME/mutter/-/commit/b2579750a7173eaf20ef6a908046285a977ad204)

- [The first release of the open multiplayer game engine Ambient:](https://www.ambient.run/post/introducing-ambient)

- [GitHub showing its Microsoft colours:](https://github.com/github/site-policy/pull/648)

- [Ubuntu 22.04.2 LTS with updated graphical stack and Linux kernel:](https://lists.ubuntu.com/archives/ubuntu-announce/2023-February/000286.html)

- [Canonical and Elektrobit introduced EB corbos Linux:](https://ubuntu.com/blog/elektrobit-and-canonical-announce-eb-corbos-linux-built-on-ubuntu)

- [GParted 1.5 and GParted Live 1.5:](https://gparted.org/news.php?item=247)

- [Release of Apache NetBeans 17:](https://blogs.apache.org/netbeans/entry/announce-apache-netbeans-17-released)

- [Release of Mesa 23.0:](https://lists.freedesktop.org/archives/mesa-dev/2023-February/225930.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
