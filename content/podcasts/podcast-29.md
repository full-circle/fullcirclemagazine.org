---
title: "Full Circle Weekly News 29"
date: 2016-08-06
draft: false
tags:
  - "18"
  - "ai"
  - "anonymity"
  - "botnet"
  - "bypassed"
  - "crypt"
  - "crypto"
  - "darpa"
  - "debian"
  - "dropbox"
  - "elon-musk"
  - "hacked"
  - "https"
  - "mint"
  - "paper"
  - "project"
  - "sarah"
  - "skynet"
  - "spacex"
  - "tor"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2029.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Mint 18 Xfce Edition Is Now Available for Download](http://news.softpedia.com/news/linux-mint-18-xfce-edition-is-now-available-for-download-here-s-what-s-new-506828.shtml)

- [The rise of the Linux botnet](http://betanews.com/2016/08/02/rise-linux-botnet/)

- [Elon Musk Says DARPA A.I. Hacking Challenge Will Lead to Skynet](https://www.inverse.com/article/18301-elon-musk-warns-that-darpa-artificial-intelligence-security-challenge-will-yield-skynet)

- [Debian Project Enhances the Anonymity and Security of Debian Linux Users via Tor](http://news.softpedia.com/news/debian-project-enhances-the-anonymity-and-security-of-debian-linux-users-via-tor-506909.shtml)

- [HTTPS Bypassed On Windows, Mac, And Linux](http://www.valuewalk.com/2016/07/https-bypassed-windows-mac-linux/)

- [Chinese Android smartphone packs a dedicated crypto chip](http://www.theregister.co.uk/2016/08/02/chinese_android_smartphone/)

- [Dropbox's answer to Google Docs is now available on iOS and Android](http://www.theverge.com/2016/8/4/12373696/dropbox-paper-ios-android-sharing-docs)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
