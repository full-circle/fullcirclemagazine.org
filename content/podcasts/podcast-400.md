---
title: "Full Circle Weekly News 400"
date: 2025-02-16
draft: false
tags:
  - "void"
  - "minios"
  - "mylibrary"
  - "freebsd"
  - "maxx"
  - "openwrt"
  - "libreoffice"
  - "picolibc"
  - "onlyoffice"
  - "min"
  - "tuxtape"
  - "sysvinit"
  - "pale"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20400.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Void Linux update:](https://voidlinux.org/news/2025/02/new-images.html)

- [MiniOS 4.1 released:](https://github.com/minios-linux/minios-live/releases/tag/v4.1.0)

- [Release of MyLibrary 3.1:](https://github.com/ProfessorNavigator/mylibrary/releases/tag/v3.1)

- [First results of the project to improve FreeBSD performance on laptops:](https://freebsdfoundation.org/blog/laptop-support-and-usability-project-update-first-monthly-report-community-initiatives/)

- [MaXX Interactive Desktop 2.2 Released:](https://docs.maxxinteractive.com/books/whats-new-release-notes/page/january-31th-2025-maxxdesktop-octane-220-release)

- [OpenWrt 24.10 is available:](https://lists.openwrt.org/pipermail/openwrt-announce/2025-February/000067.html)

- [Release of LibreOffice 25.2:](https://blog.documentfoundation.org/blog/2025/02/06/libreoffice-25-2/)

- [Release of PicoLibc Standard C Library 1.8.9:](https://github.com/picolibc/picolibc/releases/tag/1.8.9)

- [ONLYOFFICE 8.3 is available:](https://www.onlyoffice.com/blog/2025/02/onlyoffice-docs-8-3-released)

- [Min 1.34:](https://github.com/minbrowser/min/releases/tag/v1.34.0)

- [TuxTape project:](https://github.com/geico/tuxtape/releases/tag/v0.1)

- [Release of SysVinit 3.14:](https://lists.nongnu.org/archive/html/sysvinit-devel/2025-02/msg00000.html)

- [Pale Moon 33.6.0 Released:](https://forum.palemoon.org/viewtopic.php?t%3D32070%26p%3D259424%23p259424)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
