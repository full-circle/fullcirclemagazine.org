---
title: "Full Circle Weekly News 298"
date: 2023-02-04
draft: false
tags:
  - "mxlinux"
  - "libreelec"
  - "losslesscut"
  - "kodi"
  - "restic"
  - "firejail"
  - "plop"
  - "lakka"
  - "virtualbox"
  - "gcompris"
  - "gpupdate"
  - "gnome"
  - "telemetry"
  - "pandoc"
  - "seamonkey"
  - "tor"
  - "apple"
  - "lisa"
  - "archlabs"
  - "kde"
  - "plasma"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20298.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of MX Linux 21.3:](https://mxlinux.org/blog/mx-21-3-wildflower-released/)

- [Release of LibreELEC 10.0.4:](https://libreelec.tv/2023/01/15/libreelec-matrix-10-0-4/)

- [Release of LosslessCut 3.49.0:](https://github.com/mifi/lossless-cut/releases/tag/v3.49.0)

- [Release of Kodi 20.0:](http://kodi.tv/)

- [Backup system restic 0.15:](https://restic.net/blog/2023-01-12/restic-0.15.0-released/)

- [Release of Firejail 0.9.72:])https://github.com/netblue30/firejail/releases/tag/0.9.72)

- [Release of Plop Linux 23:](http://www.plop.at/)

- [Release of Lakka 4.3:](https://www.lakka.tv/articles/2023/01/18/lakka-4.3/)

- [Release of VirtualBox 7.0.6:](https://www.mail-archive.com/vbox-announce@virtualbox.org/msg00223.html)

- [Release of GCompris 3.0:](https://gcompris.net/news/2023-01-18-en.html)

- [Release of the gpupdate group policy tool 0.9.12:](https://github.com/altlinux/gpupdate)

- [The GNOME report summarizing data obtained after telemetry collection:](https://blogs.gnome.org/aday/2023/01/18/gnome-info-collect-what-we-learned/)

- [Release pandoc 3.0:](https://pandoc.org/releases.html)

- [Release of SeaMonkey 2.53.15 and Tor Browser 12.0.2:](https://blog.seamonkey-project.org/2023/01/20/seamonkey-2-53-15-final-is-released/)[Tor:](https://blog.torproject.org/new-release-tor-browser-1202/)

- [Source code of the Apple Lisa computer OS:](https://computerhistory.org/blog/the-lisa-apples-most-influential-failure/)

- [Distributed ArchLabs 2023.01.20:](https://forum.archlabslinux.com/t/archlabs-2023-01-20-release/7082)

- [KDE Plasma 5.27 desktop testing:](https://kde.org/announcements/plasma/5/5.26.90/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
