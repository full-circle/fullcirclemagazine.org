---
title: "Full Circle Weekly News 341"
date: 2023-11-26
draft: false
tags:
  - "redhat"
  - "red hat"
  - "almalinux"
  - "vivaldi"
  - "wireshark"
  - "handbrake"
  - "blender"
  - "eurolinux"
  - "openwrt"
  - "linux"
  - "pfsense"
  - "oracle"
  - "canonical"
  - "inkscape"

cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20341.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Red Hat Enterprise Linux 9.3:](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux)

- [AlmaLinux 9.3:](https://www.almalinux.org/)

- [Vivaldi browser has appeared on Flathub:](https://flathub.org/apps/com.vivaldi.Vivaldi)

- [Release of Wireshark 4.2:](https://www.wireshark.org/news/20231115b.html)

- [HandBrake 1.7.0:](https://github.com/HandBrake/HandBrake/releases/tag/1.7.0)

- [Release of Blender 4.0:](https://www.blender.org/press/blender-4-0-release/)

- [Release of EuroLinux 9.3:](https://en.euro-linux.com/blog/eurolinux-9-3-released/)

- [OpenWrt 23.05.2:](https://lists.openwrt.org/pipermail/openwrt-announce/2023-November/000048.html)

- [The Linux 6.6 core is classified as long term support:](https://kernel.org/category/releases.html)

- [Release of pfSense 2.7.1:](https://www.netgate.com/blog/netgate-releases-pfsense-ce-software-version-2.7.1)

- [Release of Oracle Linux 9.3:](https://blogs.oracle.com/linux/post/oracle-linux-9-update-3)

- [Canonical introduced MicroCloud:](https://canonical.com/blog/canonical_releases_microcloud)

- [Release TR1X 3.0, open engine for Tomb Raider:](https://github.com/LostArtefacts/TR1X/releases/tag/3.0)

- [Inkscape 1.3.1:](https://inkscape.org/news/2023/11/18/big-small-release-inkscape-131-is-out/)

- [Release of Red Hat Enterprise Linux 8.9:](https://www.redhat.com/en/blog/standardize-while-delivering-flexibility-red-hat-enterprise-linux-89)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
