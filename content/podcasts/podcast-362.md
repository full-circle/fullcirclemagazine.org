---
title: "Full Circle Weekly News 362"
date: 2024-04-21
draft: false
tags:
  - "bcachefs"
  - "stow"
  - "pumpkinos"
  - "palmos"
  - "pivpn"
  - "pingora"
  - "fedora"
  - "rivendell"
  - "openssl"
  - "kubuntu"
  - "i2p"
  - "gentoo"
  - "openziti"
  - "oracle"
  - "ardor"
  - "lakka"
  - "lutris"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20362.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Bcachefs patches to fix the FS:](https://lore.kernel.org/lkml/wwkqc7ugdewzde6gdej5bi6kb3bsvoqzqkexxejcl64d5r3pow@46qmmqq5wx4y/)

- [Release of GNU Stow 2.4:](https://www.mail-archive.com/info-gnu@gnu.org/msg03275.html)

- [The PumpkinOS project develops PalmOS reincarnation:](https://github.com/migueletto/PumpkinOS)

- [Release of apt-mirror2 4:](https://gitlab.com/apt-mirror2/apt-mirror2/-/releases/v4)

- [PiVPN project discontinued:](https://github.com/pivpn/pivpn/releases/tag/v4.6.0)

- [First edition of Pingora:](https://github.com/cloudflare/pingora/releases)

- [Fedora 41 approved DNF5:](https://pagure.io/fesco/issue/3191#comment-904697)

- [Rivendell 4.2 is available:](https://lists.linuxaudio.org/hyperkitty/list/linux-audio-announce@lists.linuxaudio.org/thread/WJFB554MUA2BHOZMYIANTDAG7WCESIC2/)

- [Release of nxs-backup 3.4.0:](https://github.com/nixys/nxs-backup/releases/tag/v3.4.0)

- [Release of OpenSSL 3.3.0:](https://www.mail-archive.com/openssl-announce@openssl.org/msg00445.html)

- [Kubuntu project presented an updated logo and branding elements:](https://kubuntu.org/news/celebrating-creativity-announcing-the-winners-of-the-kubuntu-contests/)

- [Release of I2P 2.5.0 anonymous network:](https://geti2p.net/en/blog/post/2024/04/08/new_release_i2p_2.5.0)

- [Project Gentoo and SPI:](https://www.gentoo.org/news/2024/04/10/SPI-associated-project.html)

- [OpenZiti 1.0:](https://blog.openziti.io/announcing-openziti-v1)

- [OS Zone Initiative Revokes 54 Anti-Open Software Patents:](https://openinventionnetwork.com/founding-open-source-zone-members-oin-the-linux-foundation-and-microsoft-mark-five-years-of-successes-in-protecting-open-source-software-from-patent-assertion-entities/)

- [Oracle has published DTrace 2.0.0-1.14 for Linux](https://lore.kernel.org/all/ZhBRSM2j0v7cOLn%252F@oracle.com/T/%23u)

- [Release of Ardor 8.5:](https://ardour.org/whatsnew.html)

- [Ubuntu 24.04 beta release:](https://fridge.ubuntu.com/2024/04/12/ubuntu-24-04-lts-noble-numbat-beta-released/)

- [Release of KDE Frameworks 6.1.0:](https://translate.google.com/website?sl=auto&tl=en&hl=en-US&client=webapp&u=https://kde.org/announcements/frameworks/6/6.1.0/)

- [Lakka 5.0:](https://lakka.tv/articles/2024/04/13/lakka-5.0/)

- [Release of Lutris 0.5.17:](https://github.com/lutris/lutris/releases/tag/v0.5.17)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
