---
title: "Full Circle Weekly News 48"
date: 2017-01-07
draft: false
tags:
  - "17-04"
  - "beta"
  - "canonical"
  - "device"
  - "fairphone"
  - "killdisk"
  - "malware"
  - "nexus"
  - "ota-15"
  - "ota15"
  - "phone"
  - "ransomware"
  - "touch"
  - "tux4ubuntu"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2048.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 17.04 Skips First Alpha for Opt-In Flavors](http://news.softpedia.com/news/ubuntu-17-04-skips-first-alpha-for-opt-in-flavors-gcc-6-3-0-hits-the-repository-511456.shtml)

- [Nexus 5 Now a Fully Working Ubuntu Phone, Fairphone 2 Gets Voice Call Support](http://news.softpedia.com/news/nexus-5-now-a-fully-working-ubuntu-phone-fairphone-2-gets-voice-call-support-511448.shtml)

- [Canonical Clarifies the Current State of Ubuntu Phones and Ubuntu Touch Updates](http://news.softpedia.com/news/canonical-clarifies-the-current-state-of-ubuntu-phones-and-ubuntu-touch-updates-511577.shtml)

- [KillDisk Ransomware Now Targets Linux, Prevents Boot-Up, Has Faulty Encryption](https://www.bleepingcomputer.com/news/security/killdisk-ransomware-now-targets-linux-prevents-boot-up-has-faulty-encryption/)

- [Tux4Ubuntu: Tuxify Your Ubuntu Linux This New Year](https://fossbytes.com/tux4ubuntu-tux-penguin-ubuntu-linux/)

- [Linux 2017: With great power comes great responsibility](http://www.zdnet.com/article/linux-2017-with-great-power-comes-great-responsibility/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
