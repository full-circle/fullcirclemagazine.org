---
title: "Full Circle Weekly News 73"
date: 2017-12-02
draft: false
tags:
  - "arch"
  - "automation"
  - "blackarch"
  - "bug"
  - "cinnamon"
  - "console"
  - "edition"
  - "factory"
  - "game"
  - "gameshell"
  - "hackers"
  - "handheld"
  - "industry"
  - "linux"
  - "mate"
  - "microsoft"
  - "mint"
  - "office"
  - "retro"
  - "word"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2073.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Mint 18.3 Cinnamon And MATE Editions Released](https://fossbytes.com/linux-mint-18-3-released-download-torrent-iso/)

- [BlackArch Linux 2017.11.24 Available For Download](https://fossbytes.com/blackarch-linux-2017-11-24-download-features-update/)

- [GameShell Is An Open Source And Linux-powered Retro Game Console](https://fossbytes.com/gameshell-open-source-linux-retro-game-console/)

- [Linux for the Industry 4.0 era: New distro for factory automation](https://www.networkworld.com/article/3238727/linux/linux-for-factory-automation.html)

- [Hackers Can Control Your PC With This 17 Year Old Microsoft Word Bug](https://fossbytes.com/hackers-can-control-pc-microsoft-bug-cobalt-malware/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
