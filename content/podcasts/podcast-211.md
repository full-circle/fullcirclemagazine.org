---
title: "Full Circle Weekly News 211"
date: 2021-05-24
draft: false
tags:
  - "1password"
  - "bodhi"
  - "console"
  - "cups"
  - "distro"
  - "doglinux"
  - "enterprise"
  - "game"
  - "geckolinux"
  - "kde"
  - "lakka"
  - "netbs"
  - "openprinting"
  - "perl"
  - "plasma"
  - "please"
  - "redhat"
  - "rust"
  - "solaris"
  - "vulnerability"
  - "wayward"
  - "weston"
  - "wrapper"
  - "zabbix"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20211.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Bodhi 6 64-bit release](https://www.bodhilinux.com/2021/05/12/bodhi-linux-6-0-0-released/)

- [KDE Plasma 5.22 testing has begun](https://kde.org/announcements/plasma/5/5.21.90)

- [Zabbix 5.4 release](https://www.zabbix.com/rn/rn5.4.0)

- [NetBSD 9.2 released](https://blog.netbsd.org/tnf/entry/netbsd_9_2_released)

- [The OpenPrinting project and CUPS printing system](https://openprinting.github.io/OpenPrinting-News-March-2021/)

- [Vulnerabilities in Please, the Rust alternative to sudo](https://github.com/edneville/please)

- [First release of DogLinux](https://gumanzoy.blogspot.com/2021/05/liveusb-1100mb-doglinux-debian-11.html)

- [Solaris 11.4 SRU33 available](https://blogs.oracle.com/solaris/announcing-oracle-solaris-114-sru33)

- [1Password password manager offers full Linux support](https://blog.1password.com/welcoming-linux-to-the-1password-family/)

- [GeckoLinux Distribution Release](https://github.com/geckolinux/geckolinux-project/releases/tag/210517.999)

- [Wayward – a custom wrapper based on Weston composite server](https://github.com/varmd/wayward)

- [Perl 5.34.0 released](https://www.nntp.perl.org/group/perl.perl5.porters/2021/05/msg260110.html)

- [Red Hat Enterprise Linux 8.4 Released](https://www.redhat.com/en/blog/stability-plus-innovation-red-hat-enterprise-linux-84-now-ga)

- [Lakka 3.0, for creating game consoles](https://www.lakka.tv/articles/2021/05/22/lakka-3.0/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
