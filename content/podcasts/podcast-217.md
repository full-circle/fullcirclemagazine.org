---
title: "Full Circle Weekly News 217"
date: 2021-07-07
draft: false
tags:
  - "abiword"
  - "almalinux"
  - "arm64"
  - "audacity"
  - "cosmic"
  - "cura"
  - "desktop"
  - "detection"
  - "draid"
  - "ibm"
  - "intrusion"
  - "kernel"
  - "ltsm"
  - "mixing"
  - "mixxx"
  - "music"
  - "neovim"
  - "openzfs"
  - "paint"
  - "pop!_os"
  - "qutebrowser"
  - "redhat"
  - "seamonkey"
  - "suite"
  - "support"
  - "suricata"
  - "ubuntu"
  - "ultimaker"
  - "whitehurst"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20217.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux 5.13 kernel release](https://lkml.org/lkml/2021/6/27/202)

- [LTSM proposed](https://github.com/AndreyBarmaley/linux-terminal-service-manager)

- [Release of Mixxx 2.3, the free music mixing app](http://mixxx.org/)

- [Ubuntu is moving away from dark headers and light backgrounds](https://github.com/ubuntu/yaru/pull/2922)

- [Ultimaker Cura 4.10 released](https://ultimaker.com/learn/an-improved-engineering-workflow-with-ultimaker-cura-4-10)

- [Pop!\_OS 21.04 distribution offers new COSMIC desktop](https://system76.com/pop)

- [SeaMonkey 2.53.8 Integrated Internet Application Suite Released](https://www.seamonkey-project.org/news#2021-06-30)

- [Suricata Intrusion Detection System Update](https://suricata.io/2021/06/30/new-suricata-6-0-3-and-5-0-7-releases/)

- [AlmaLinux includes support for ARM64](https://wiki.almalinux.org/release-notes/8.4-arm.html)

- [Qutebrowser 2.3 released](https://lists.schokokeks.org/pipermail/qutebrowser-announce/2021-June/000104.html)

- [Tux Paint 0.9.26 is released](http://www.tuxpaint.org/latest/tuxpaint-0.9.26-press-release.php)

- [Jim Whitehurst, head of Red Hat, steps down as president of IBM](https://www.cnbc.com/quotes/IBM)

- [OpenZFS 2.1 release with dRAID support](https://github.com/openzfs/zfs/releases/tag/zfs-2.1.0)

- [Neovim 0.5, available](https://github.com/neovim/neovim/releases/tag/v0.5.0)

- [Audacity’s new privacy policy allows data collection for the benefit of government authorities](https://news.ycombinator.com/item?id=27724389)

- [AbiWord 3.0.5 update](http://www.abisource.com/release-notes/3.0.5.phtml)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
