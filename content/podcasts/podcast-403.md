---
title: "Full Circle Weekly News 403"
date: 2025-03-09
draft: false
tags:
  - "lutris"
  - "apache"
  - "netbeans"
  - "mythtv"
  - "aqualung"
  - "xorg"
  - "nextcloud"
  - "freebsd"
  - "opencloud"
  - "linux"
  - "kernel"
  - "fish"
  - "networkmanager"
  - "brushshe"
  - "ghostbsd"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20403.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Lutris 0.5.19 Released:](https://github.com/lutris/lutris/releases/tag/v0.5.19)

- [Apache NetBeans 25 IDE:](https://netbeans.apache.org/front/main/blogs/entry/announce-apache-netbeans-25-released/)

- [Release of MythTV 35:](https://www.mythtv.org/news/175/v35.0%2520Released)

- [Aqualung 2.0 Music Player Ported to GTK3 Released:](https://github.com/jeremyevans/aqualung/releases/tag/2.0)

- [X.Org Server 21.1.16 Update Fixes 8 Vulnerabilities:](https://gitlab.freedesktop.org/xorg/xserver/-/tags/)

- [Nextcloud Hub 10 is released:](https://github.com/nextcloud/server/releases/tag/v31.0.0)

- [A Project to Run FreeBSD Programs on Linux:](https://www.freebsd.org/status/report-2024-10-2024-12/)

- [OpenCloud 1.0 Now Available:](https://opencloud.eu/en/news/opencloud-now-available-new-open-source-alternative-microsoft-sharepoint)

- [Linux kernel 6.15 to remove SystemV filesystem:](https://lore.kernel.org/all/20250221-dienlich-metapher-c2755e73b3f7@brauner/T/%23mf04463bbb9bb2d9461ea135955cd33ac29ebc0cd)

- [Electronic Arts Opens Command & Conquer Games Under GPL License:](https://www.ea.com/games/command-and-conquer/command-and-conquer-remastered/news/steam-workshop-support)

- [Fish 4.0 Shell Rewritten in Rust:](https://fishshell.com/blog/new-in-40/)

- [NetworkManager 1.52.0 Released:](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/tags/1.52.0)

- [Release of Brushshe 1.2.0:](https://github.com/limafresh/Brushshe/releases/tag/v1.2.0)

- [GhostBSD Release 25.01:](https://ghostbsd.org/news/GhostBSD_25.01-R14.2p1_Now_Available)



**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
