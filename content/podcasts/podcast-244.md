---
title: "Full Circle Weekly News 244"
date: 2022-01-13
draft: false
tags:
  - "bittorrent"
  - "distro"
  - "editor"
  - "gecko"
  - "kde"
  - "kernel"
  - "lazarus"
  - "mint"
  - "ota-21"
  - "ota21"
  - "pinta"
  - "remnants"
  - "slackel"
  - "solus"
  - "touch"
  - "ubports"
  - "wayland"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20244.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Slackel 7.5 distribution released](http://www.slackel.gr/forum/viewtopic.php?f=3&t=688)

- [Pinta 2.0 graphics editor released](https://www.pinta-project.com/releases/2-0)

- [Solus project drama](https://twitter.com/JoshStrobl/status/1477391260340137988)

- [Speed up building the Linux kernel by 50-80%](https://lkml.org/lkml/2022/1/2/187)

- [KDE plans to achieve parity between Wayland and X11 sessions in 2022](https://pointieststick.com/2022/01/03/kde-roadmap-for-2022/)

- [UbuntuDDE 21.10 Release](https://ubuntudde.com/blog/ubuntudde-remix-21-10-impish-release-note/)

- [Remnants of the Precursors game released](https://rayfowler.itch.io/remnants-of-the-precursors/devlog/330806/remnants-of-the-precursors-is-released)

- [Release of Lazarus 2.2.0](https://forum.lazarus.freepascal.org/index.php/topic,57752.0.html)

- [Twenty-first firmware update for Ubuntu Touch](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-21-release-3798)

- [QBittorrent 4.4 released with BitTorrent v2 support](https://www.qbittorrent.org/news.php)

- [New versions of GeckoLinux](https://github.com/geckolinux/geckolinux-project/releases/)

- [Release of Linux Mint 20.3](http://blog.linuxmint.com/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
