---
title: "Full Circle Weekly News 146"
date: 2019-09-24
draft: false
tags:
  - "blackarch"
  - "buster"
  - "debian"
  - "deepin"
  - "distro"
  - "ethical"
  - "exfat"
  - "extix"
  - "firmware"
  - "gnome"
  - "hacking"
  - "kali"
  - "kernel"
  - "linux"
  - "lite"
  - "lts"
  - "microsoft"
  - "neptune"
  - "sparky"
  - "sparkylinux"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20146.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 18.04.3 LTS Makes It Easier to Patch the Linux Kernel without Rebooting](https://news.softpedia.com/news/ubuntu-18-04-3-lts-makes-it-easier-to-patch-the-linux-kernel-without-rebooting-527132.shtml)

- [GNOME Wants to Make Linux Firmware Updates Easier to Deploy with New Tool](https://news.softpedia.com/news/gnome-wants-to-make-linux-firmware-updates-easier-to-deploy-with-new-tool-527174.shtml)

- [Microsoft wants to bring exFAT to the Linux kernel](https://techcrunch.com/2019/08/28/microsoft-wants-to-bring-exfat-to-the-linux-kernel/?guccounter=1&guce_referrer_us=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8&guce_referrer_cs=X1_tmdwHBuxKjixGrInI7Q)

- [Neptune 6.0 Linux Distro Released, It's Based on Debian GNU/Linux 10 "Buster"](https://news.softpedia.com/news/neptune-6-0-linux-distro-released-it-s-based-on-debian-gnu-linux-10-buster-527129.shtml)

- [Drauger OS Makes a Capable Linux Game Console Platform](https://www.linuxinsider.com/story/86210.html)

- [Kali Linux Ethical Hacking OS Switches to Linux 5.2, Now Supports OnePlus 7](https://news.softpedia.com/news/kali-linux-ethical-hacking-os-switches-to-linux-5-2-now-supports-oneplus-7-527239.shtml)

- [BlackArch Linux Ethical Hacking OS Adds over 150 New Tools in Latest Release](https://news.softpedia.com/news/blackarch-linux-ethical-hacking-os-adds-over-150-new-tools-in-latest-release-527173.shtml)

- [ExTiX 19.8 "The Ultimate Linux System" Ditches Ubuntu & Debian for Deepin Linux](https://news.softpedia.com/news/extix-19-8-the-ultimate-linux-system-ditches-ubuntu-debian-for-deepin-linux-527208.shtml)

- [Linux Lite 4.6 Officially Released, It's Based on Ubuntu 18.04.3 LTS](https://news.softpedia.com/news/linux-lite-4-6-officially-released-it-s-based-on-ubuntu-18-04-3-lts-527222.shtml)

- [Open source Kodi 18.4 'Leia' now available for Windows, macOS, Linux, and more](https://www.kodi.tv/article/kodi-leia-184-release)

- [SparkyLinux 2019.09 with Xfce 4.14 available](https://sparkylinux.org/sparky-2019-09/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
