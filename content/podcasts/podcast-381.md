---
title: "Full Circle Weekly News 381"
date: 2024-09-01
draft: false
tags:
  - "catchy"
  - "update"
  - "cockroaches"
  - "minuet"
  - "allegretto"
  - "solaris"
  - "office"
  - "kde"
  - "gimp"
  - "freeze"
  - "midnight"
  - "commander"
  - "unreal"
  - "surreal"
  - "kernel"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20381.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [A Catchy update:](https://cachyos.org/blog/2408-august-release/)

- [Cockroaches close up shop:](https://www.cockroachlabs.com/blog/enterprise-license-announcement/)

- [Minuet in Allegretto:](http://www.menuetos.net/index.htm)

- [A ship named Solaris:](https://blogs.oracle.com/solaris/post/announcing-oracle-solaris-114-sru72)

- [Office freedom:](https://blog.documentfoundation.org/blog/2024/08/22/libreoffice-248/)

- [Gear'd up:](https://kde.org/announcements/gear/24.08.0/)

- [Freeze, put your hands in the air:](https://discourse.gnome.org/t/gimps-master-branch-string-freeze/22895)

- [Twelve bells, CO:](https://github.com/MidnightCommander/mc/releases/tag/4.8.32)

- [Phantasmagorical news:](https://github.com/dpjudas/SurrealEngine)

- [Caching bees:](https://lore.kernel.org/lkml/sctzes5z3s2zoadzldrpw3yfycauc4kpcsbpidjkrew5hkz7yf@eejp6nunfpin/)

- [Saving the best for last:](http://www.cs.cmu.edu/~awb/linux.history.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
