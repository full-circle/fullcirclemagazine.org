---
title: "Full Circle Weekly News 311"
date: 2023-04-30
draft: false
tags:
  - "tails"
  - "kernel"
  - "postfix"
  - "deepin"
  - "digikam"
  - "vivaldi"
  - "fedora"
  - "qemu"
  - "ubuntu"
  - "opus"
  - "sway"
  - "kaos"
  - "manjaro"
  - "shotwell"
  - "rakudo"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20311.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Vulnerability in the Linux 6.2 kernel:](https://github.com/google/security-research/security/advisories/GHSA-mj4w-6495-6crx)

- [Postfix 3.8.0 available:](https://www.mail-archive.com/postfix-announce@postfix.org/msg00052.html)

- [Release of the Deepin 20.9:](https://www.deepin.org/en/deepin-20-9-is-officially-released/)

- [Release of digiKam 8.0:](https://www.digikam.org/news/2023-04-16-8.0.0_release_announcement/)

- [Release of Vivaldi 6.0:](https://vivaldi.com/ru/blog/vivaldi-on-desktop-6-0/)

- [Release of Fedora Linux 38:](https://fedoramagazine.org/announcing-fedora-38/)

- [Tails 5.12:](https://tails.boum.org/news/version_5.12/index.en.html)

- [Release of QEMU 8.0:](https://lists.nongnu.org/archive/html/qemu-devel/2023-04/msg02755.html)

- [Available now /e/OS 1.10:](https://e.foundation/leaving-apple-celebrate-earth-day-with-a-sustainable-choice/)

- [Release of Ubuntu 23.04:](https://ubuntu.com/blog/ubuntu-desktop-23-04-release-roundup)

- [Opus 1.4 is available:](http://lists.xiph.org/pipermail/opus/2023-April/004578.html)

- [Release of Ubuntu Sway Remix 23.04:](https://github.com/Ubuntu-Sway/Ubuntu-Sway-Remix)

- [KaOS 2023.04:](https://kaosx.us/news/2023/kaos04/)

- [Manjaro Linux 22.1:](https://forum.manjaro.org/t/manjaro-22-1-talos-released/139155)

- [Shotwell 0.32 is available:](https://gitlab.gnome.org/GNOME/shotwell/-/tags/shotwell-0.32.0)

- [Release of Rakudo 2023.04:](https://rakudo.org/post/announce-rakudo-release-2023.04)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
