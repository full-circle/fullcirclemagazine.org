---
title: "Full Circle Weekly News 357"
date: 2024-03-17
draft: false
tags:
  - "nixbsd"
  - "arti"
  - "opus"
  - "opensuse"
  - "freebsd"
  - "azure"
  - "mint"
  - "fedora"
  - "zorin"
  - "postfix"
  - "lxqt"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20357.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [NixBSD develops a variant of NixOS with a kernel from FreeBSD:](https://discourse.nixos.org/t/is-nixbsd-a-posibility/29612/34)

- [Release of Arti 1.2:](https://translate.google.com/website?sl=auto&tl=en&hl=en-US&u=https://blog.torproject.org/arti_1_2_0_released/)

- [Opus 1.5 audio codec available:](http://lists.xiph.org/pipermail/opus/2024-March/004589.html)

- [openSUSE Tumbleweed & systemd-boot:](https://news.opensuse.org/2024/03/05/systemd-boot-integration-in-os/)

- [FreeBSD 13.3:](https://www.freebsd.org/releases/13.3R/announce/)

- [Azure Linux:](https://github.com/microsoft/azurelinux/pull/8136)

- [Linux Mint develops new chat app after shutting down HexChat:](https://blog.linuxmint.com/?p%3D4650)

- [Fedora 41 is slated to remove the X11 session for GNOME:](https://pagure.io/fedora-workstation/issue/414%23comment-899128)

- [Beta release of openSUSE Leap 15.6:](https://news.opensuse.org/2024/03/07/leap-reaches-beta-phase/)

- [Release of Zorin OS 17.1:](https://blog.zorin.com/2024/03/07/zorin-os-17.1-is-released/)

- [Postfix 3.9.0 mail server published:](https://www.mail-archive.com/postfix-announce@postfix.org/msg00098.html)

- [Bruce Perens published a draft version of the Post-Open license:](https://perens.com/2024/03/08/post-open-license-first-draft/)

- [The LXQt desktop ready for Wayland:](https://mastodon.social/@LXQt/112065354791368936)

- [Release of LibreSSL 3.9.0:](https://www.mail-archive.com/announce@openbsd.org/msg00517.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
