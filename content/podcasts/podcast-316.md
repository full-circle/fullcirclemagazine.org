---
title: "Full Circle Weekly News 316"
date: 2023-06-04
draft: false
tags:
  - "top500"
  - "nau"
  - "paint"
  - "podman"
  - "oracle"
  - "linux"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20316.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The highest-performance supercomputers:](https://www.top500.org/news/frontier-remains-sole-exaflop-machine-and-retains-top-spot-improving-upon-its-previous-hpl-score/)

- [VKontakte's open game engine - Nau Engine:](https://vk.com/wall-220611591_1)

- [Release of Tux Paint 0.9.30:](https://tuxpaint.org/latest/tuxpaint-0.9.30-press-release.php)

- [Podman Desktop 1.0:](https://developers.redhat.com/articles/2023/05/23/podman-desktop-now-generally-available)

- [Release of Oracle Linux 8.8 and 9.2:](https://blogs.oracle.com/linux/post/oracle-announces-general-availability-of-latest-oracle-linux-releases)


**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
