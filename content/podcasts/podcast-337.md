---
title: "Full Circle Weekly News 337"
date: 2023-10-29
draft: false
tags:
  - "opensilver"
  - "openbsd"
  - "virtualbox"
  - "gnome"
  - "mediagoblin"
  - "kubuntu"
  - "plasma"
  - "intel"
  - "ospray"
  - "asterisk"
  - "geany"
  - "libreboot"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20337.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of the OpenSilver 2.0 platform:](https://opensilver.net/announcements/2-0/)

- [OpenBSD 7.4:](https://www.mail-archive.com/announce@openbsd.org/msg00498.html)

- [Release of VirtualBox 7.0.12:](https://www.mail-archive.com/vbox-announce@virtualbox.org/msg00227.html)

- [New CEO of GNOME Foundation:](https://foundation.gnome.org/2023/10/17/foundation-welcomes-new-executive-director/)

- [Release of MediaGoblin 0.13:](https://mediagoblin.org/news/mediagoblin-0.13.0-release.html)

- [For Kubuntu 22.04 packages with KDE Plasma 5.27:](https://kubuntu.org/news/plasma-5-27-lts-for-jammy-22-04-lts-available-via-ppa)

- [Intel has released OSPRay 3.0 engine:](https://github.com/ospray/ospray/releases/tag/v3.0.0)

- [Release of Asterisk 21:](https://www.asterisk.org/asterisk-news/asterisk-21-0-0-now-available/)

- [Integrated development environment Geany 2.0 is available:](https://www.geany.org/news/geany-20-is-out/)

- [Release of the http server Apache 2.4.58:](https://www.mail-archive.com/announce@httpd.apache.org/msg00167.html)

- [Release of Libreboot 20231021:](https://libreboot.org/news/libreboot20231021.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
