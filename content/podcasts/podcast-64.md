---
title: "Full Circle Weekly News 64"
date: 2017-06-17
draft: false
tags:
  - "access"
  - "ai"
  - "cia"
  - "developer"
  - "firmware"
  - "grid"
  - "guest"
  - "language"
  - "linux"
  - "non-human"
  - "routers"
  - "samba"
  - "smb"
  - "windows"
  - "wsl"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2064.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Now You Can Run Linux On Windows 10 Without Enabling Developer Mode](https://fossbytes.com/linux-on-windows-10-without-developer-mode/)

- [Nearly One Million Systems Provide "Guest" SMB Access, Most Are Linux](https://www.bleepingcomputer.com/news/security/nearly-one-million-systems-provide-guest-smb-access-most-are-linux/)

- [Advanced CIA firmware has been infecting Wi-Fi routers for years](https://arstechnica.com/security/2017/06/advanced-cia-firmware-turns-home-routers-into-covert-listening-posts/)

- [Security firms warn of new cyber threat to electric grid](http://cio.economictimes.indiatimes.com/news/digital-security/security-firms-warn-of-new-cyber-threat-to-electric-grid/59120351)

- [An Artificial Intelligence Developed Its Own Non-Human Language](https://www.theatlantic.com/technology/archive/2017/06/artificial-intelligence-develops-its-own-non-human-language/530436/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
