---
title: "Full Circle Weekly News 07"
date: 2016-03-05
draft: false
tags:
  - "ai"
  - "artificial"
  - "attack"
  - "budgie"
  - "canonical"
  - "desktop"
  - "drown"
  - "google"
  - "hat"
  - "health"
  - "healthcare"
  - "intelligence"
  - "kernel"
  - "key"
  - "linux"
  - "meizu"
  - "openssl"
  - "pre-order"
  - "pro"
  - "pro5"
  - "red"
  - "redhat"
  - "regression"
  - "remix"
  - "shashlik"
  - "skeleton"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2007.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Google's artificial intelligence arm moves in to healthcare](http://www.beckershospitalreview.com/healthcare-information-technology/google-s-artificial-intelligence-arm-breaks-into-healthcare.html)

- [Security Researchers At Red Hat And Google Warn Of Serious Linux Skeleton Key Vulnerability](http://hothardware.com/news/security-researchers-at-red-hat-and-google-warn-of-serious-linux-skeleton-key-vulnerability)

- [Meizu PRO 5 Ubuntu Edition now Available for Pre-Order](http://www.chinatopix.com/articles/78398/20160301/ubuntu-%E2%80%93based-meizu-pro-5-edition-now-available-pre-order.htm)

- [Millions of OpenSSL secured websites at risk of new DROWN attack](http://www.zdnet.com/article/dont-let-your-openssl-secured-web-sites-drown/)

- [Shashlik brings Android apps to Linux](http://www.pcworld.com/article/3040093/android/shashlik-brings-android-apps-to-linux-and-you-can-try-it-today.html)

- [Canonical Patches Ubuntu 15.10 Kernel Regression That Broke Graphics Displays](http://news.softpedia.com/news/canonical-patches-ubuntu-15-10-kernel-regression-that-broke-graphics-displays-501063.shtml)

- [Budgie-Remix Could Become Ubuntu Budgie](http://news.softpedia.com/news/budgie-remix-could-become-ubuntu-budgie-download-and-test-it-501231.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
