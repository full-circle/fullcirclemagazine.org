---
title: "Full Circle Weekly News 167"
date: 2020-04-11
draft: false
tags:
  - "20-04"
  - "coreos"
  - "development"
  - "engine"
  - "eol"
  - "godot"
  - "kde"
  - "kernel"
  - "linux"
  - "lts"
  - "tlp"
  - "ubuntu"
  - "update"
  - "wine"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20167.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [EOL of CoreOS Linux](https://coreos.com/os/eol/)

- [Kernel 5.4 Marked as LTS](https://9to5linux.com/linux-kernel-5-4-is-now-an-official-lts-release-supported-until-2022)

- [Ubuntu 20.04 Will Ship with the 5.4 LTS Kernel](https://www.omgubuntu.co.uk/2020/02/ubuntu-20-04-kernel-5-4-lts)

- [Godot Engine Receives Epic MegaGrant](https://godotengine.org/article/godot-engine-was-awarded-epic-megagrant)

- [Wine 5.1 Development Released](https://www.winehq.org/announce/5.1)

- [TLP 1.3 Released](https://linrunner.de/en/tlp/docs/tlp-linux-advanced-power-management.html#features)

- [KDE's 19.2 February Updates Released](https://kde.org/announcements/releases/2020-02-apps-update/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
