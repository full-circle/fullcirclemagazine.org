---
title: "Full Circle Weekly News 297"
date: 2023-01-21
draft: false
tags:
  - "obs"
  - "openwrt"
  - "freecol"
  - "duelyst"
  - "hammer2"
  - "netbsd"
  - "freebsd"
  - "apache"
  - "iptables"
  - "xubuntu"
  - "toybox"
  - "ventoy"
  - "chrome"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20297.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of the OBS Studio 29:](https://github.com/obsproject/obs-studio/releases/tag/29.0.0)

- [Update OpenWrt 22.03.3:](https://lists.infradead.org/pipermail/openwrt-devel/2023-January/040194.html)

- [Release of FreeCol 1.0:](https://www.freecol.org/news/freecol-1.0.0-released.html)

- [Source code of Duelyst game is open:](https://www.youtube.com/watch?v=h3Cuz6d_7x0)

- [HAMMER2 file system port available for NetBSD and FreeBSD:](https://github.com/kusumi/)

- [Indians seek to rename Apache project:](https://blog.nativesintech.org/apache-appropriation/)

- [Release of I2P 2.1.0:](https://geti2p.net/en/blog/post/2023/01/09/2.1.0-Release)

- [Release of iptables 1.8.9:](https://www.mail-archive.com/netfilter-announce@lists.netfilter.org/msg00253.html)

- [Xubuntu minimalistic builds:](https://lists.ubuntu.com/archives/ubuntu-release/2023-January/005521.html)

- [Release of Toybox 0.8.9:](https://github.com/landley/toybox/releases/tag/0.8.9)

- [Release Ventoy 1.0.88:](https://github.com/ventoy/Ventoy/releases)

- [Chrome OS 109 available:](https://chromereleases.googleblog.com/2023/01/stable-channel-update-for-chromeos_13.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
