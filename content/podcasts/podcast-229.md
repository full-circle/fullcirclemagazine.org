---
title: "Full Circle Weekly News 229"
date: 2021-09-26
draft: false
tags:
  - "14.04"
  - "16.04"
  - "21.10"
  - "apache"
  - "bespoke"
  - "beta"
  - "bsd"
  - "coreutils"
  - "criu"
  - "gnome"
  - "gnu"
  - "google"
  - "headscale"
  - "hiba"
  - "kernel"
  - "midnightbsd"
  - "module"
  - "netbeans"
  - "openvpn"
  - "ota-19"
  - "ota19"
  - "samba"
  - "smb"
  - "synth"
  - "synthesizer"
  - "sysvinit"
  - "tailscale"
  - "touch"
  - "ubports"
  - "ubuntu"
  - "waydroid"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20229.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Bespoke Synth 1.0 software sound synthesizer released](https://github.com/BespokeSynth/BespokeSynth/releases/tag/v1.0.0)

- [Headscale develops an open server for Tailscale](https://github.com/juanfont/headscale)

- [Samba 4.15.0 Released](https://www.mail-archive.com/samba-announce@lists.samba.org/msg00562.html)

- [Sysvinit 3.0 released](https://lists.nongnu.org/archive/html/sysvinit-devel/2021-09/msg00000.html)

- [Ubuntu 14.04 and 16.04 support time extended to 10 years](https://ubuntu.com/blog/ubuntu-14-04-and-16-04-lifecycle-extended-to-ten-years)

- [Google publishes HIBA](https://opensource.googleblog.com/2021/09/announcing-hiba-host-identity-based-authorization-for-SSH.html)

- [A kernel module that can speed up OpenVPN](https://openvpn.net/blog/openvpn-data-channel-offload/)

- [Apache NetBeans IDE 12.5 Released](https://blogs.apache.org/netbeans/entry/announce-apache-netbeans-12-5)

- [Ubuntu Touch Nineteenth Firmware Update](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-19-release-3779)

- [Release of the GNOME 41 user environment](https://foundation.gnome.org/2021/09/22/gnome-41-release/)

- [The Waydroid project develops a package to run Android on GNU / Linux distributions](https://waydro.id/)

- [Release of CRIU 3.16](http://criu.org/)

- [MidnightBSD 2.1 released](https://www.midnightbsd.org/notes/)

- [Ubuntu 21.10 beta release](https://lists.ubuntu.com/archives/ubuntu-announce/2021-September/000273.html)

- [HackerOne pays for identifying vulnerabilities in Open Source Software](https://www.hackerone.com/press-release/hackerone-expands-internet-bug-bounty-improve-collective-security-software-supply)

- [GNU Coreutils 9.0 released](https://www.mail-archive.com/info-gnu@gnu.org/msg02953.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
