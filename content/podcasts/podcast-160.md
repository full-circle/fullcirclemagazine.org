---
title: "Full Circle Weekly News 160"
date: 2019-12-29
draft: false
tags:
  - "dxvk"
  - "firefox"
  - "kde"
  - "linux"
  - "microsoft"
  - "oracle"
  - "teams"
  - "ubuntu"
  - "update"
  - "virtualbox"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20160.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Zorin OS 15.1 is Released](https://zoringroup.com/blog/2019/12/12/zorin-os-15-1-is-released-a-better-way-to-work-learn-and-play/)

- [Firefox 71 is Now Available for All Supported Ubuntu Releases](https://news.softpedia.com/news/mozilla-firefox-71-is-now-available-for-all-supported-ubuntu-linux-releases-528537.shtml)

- [KDE's December 2019 Apps Update](https://kde.org/announcements/releases/2019-12-apps-update/)

- [Oracle Virtualbox 6.1 Now Available](https://blogs.oracle.com/virtualization/oracle-vm-virtualbox-61-now-available)

- [Microsoft Teams is Now Available for Linux](https://www.ostechnix.com/microsoft-teams-is-now-officially-available-for-linux/)

- [DXVK to Enter Maintenance Mode](https://www.linuxuprising.com/2019/12/dxvk-to-enter-maintenance-mode-because.html)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
