---
title: "Full Circle Weekly News 329"
date: 2023-09-03
draft: false
tags:
  - "bodhi"
  - "linux"
  - "inkscape"
  - "ubuntudde"
  - "libreoffice"
  - "office"
  - "qemu"
  - "openmandriva"
  - "kali"
  - "almalinux"
  - "debian"
  - "terraform"
  - "opentf"
  - "mageia"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20329.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Bodhi Linux 7.0:](https://www.bodhilinux.com/2023/08/21/introducing-bodhi-linux-7-0-a-landmark-release/)

- [P2P VPN 0.10:](https://gitlab.com/Monsterovich/p2pvpn/-/releases/0.10)

- [Traces of malicious activity on the Inkscape download server:](https://discourse.nixos.org/t/inkscape-possible-domain-compromise/32008)
- [Update here:](https://gitlab.com/inkscape/inkscape-web/-/issues/647)

- [Release of UbuntuDDE 23.04:](https://ubuntudde.com/blog/ubuntudde-remix-23-04-lunar-release-note/)

- [The LibreOffice project date-linked versioning:](https://wiki.documentfoundation.org/ReleaseNotes/24.2)

- [Release of QEMU 8.1:](https://lists.nongnu.org/archive/html/qemu-devel/2023-08/msg03802.html)

- [OpenMandriva ROME 23.08:](https://www.openmandriva.org/en/news/article/openmandriva-rome-23-08-and-a-glimpse-of-omlx-5-0)

- [Release of Kali Linux 2023.3:](https://www.kali.org/blog/kali-linux-2023-3-release/)

- [AlmaLinux repository with additional packages:](https://almalinux.org/blog/new-repositories-for-almalinux-os-synergy-and-testing/)

- [Debian and Ubuntu created a repository with fresh versions of the Linux kernel:](https://stgraber.org/2023/08/24/stable-linux-mainline-builds/)

- [Ubuntu Desktop development plans:](https://canonical.com/blog/ubuntu-desktop-charting-a-course-for-the-future)

- [OpenTF has created a fork of Terraform:](https://opentf.org/announcement)

- [Release of Mageia 9:](https://www.mageia.org/en/9/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
