---
title: "Full Circle Weekly News 93"
date: 2018-05-14
draft: false
tags:
  - "18-10"
  - "32-bit"
  - "android"
  - "budgie"
  - "chrome"
  - "chromeos"
  - "cosmic"
  - "cuttlefish"
  - "iso"
  - "kaos"
  - "kde"
  - "linux"
  - "mate"
  - "phone"
  - "support"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2093.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu MATE And Ubuntu Budgie Dropping 32-bit Hardware Support](https://fossbytes.com/ubuntu-mate-ubuntu-budgie-drop-32-bit-support/)

- [Now You Can Officially Run Linux Apps On Chrome OS](https://fossbytes.com/linux-apps-on-chrome-os/)

- [KaOS KDE-focused rolling Linux distro celebrates 5th birthday with updated ISO](https://betanews.com/2018/05/04/kaos-kde-linux-5th-birthday-iso/)

- [Sophisticated Android malware tracks all your phone activities](https://www.engadget.com/2018/05/07/zoopark-android-malware-exfiltration/)

- [Ubuntu 18.10 Operating System Dubbed "Cosmic Cuttlefish" by Mark Shuttleworth](https://news.softpedia.com/news/ubuntu-18-10-operating-system-dubbed-cosmic-cuttlefish-by-mark-shuttleworth-521029.shtml)
