---
title: "Full Circle Weekly News 36"
date: 2016-09-24
draft: false
tags:
  - "21"
  - "4"
  - "ai"
  - "block"
  - "browser"
  - "debian"
  - "decode"
  - "images"
  - "installs"
  - "jessie"
  - "kernel"
  - "lenovo"
  - "lts"
  - "microsoft"
  - "omega2"
  - "photos"
  - "pixelated"
  - "server"
  - "tor"
  - "unblur"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2036.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Debian 8.6 "Jessie" Officially Released with over 90 Security Fixes](http://news.softpedia.com/news/debian-gnu-linux-8-6-jessie-officially-released-brings-over-90-security-fixes-508413.shtml)

- [Tor Browser 6.0.5 Released](https://fossbytes.com/tor-browser-6-0-5-released-windows-linux-mac-os-x/)

- [Linux Kernel 4.4.21 LTS Is a Big Update with Over 200 Changes](http://news.softpedia.com/news/linux-kernel-4-4-21-lts-is-a-big-update-with-over-200-changes-update-now-508357.shtml)

- [Artificial Intelligence can Decode and Unblur Pixelated Images](https://www.hackread.com/artificial-intelligence-unblur-pixelated-images/)

- [Lenovo denies claims it plotted with Microsoft to block Linux installs](http://www.theregister.co.uk/2016/09/21/lenovo_denies_plot_with_microsoft_to_block_linux_installs/)

- [Start-up sells a stamp-sized Linux server for $5](http://www.itworld.com/article/3122248/computer-hardware/start-up-sells-a-stamp-sized-linux-server-for-5.html)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
