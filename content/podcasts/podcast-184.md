---
title: "Full Circle Weekly News 184"
date: 2020-10-04
draft: false
tags:
  - "arm"
  - "beta"
  - "braveheart"
  - "deepin"
  - "endeavouros"
  - "firefox"
  - "firmware"
  - "fossapup"
  - "gnome"
  - "kde"
  - "kernel"
  - "linux"
  - "mozilla"
  - "multi-boot"
  - "multiboot"
  - "notes"
  - "nvidia"
  - "pinephone"
  - "puppy"
  - "send"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20184.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Gnome 3.38 Release](https://www.gnome.org/news/2020/09/gnome-3-38-released/)

- [Gnome 40 Is Next](https://discourse.gnome.org/t/new-gnome-versioning-scheme/4235)

- [Nvidia Buys ARM](https://nvidianews.nvidia.com/news/nvidia-to-acquire-arm-for-40-billion-creating-worlds-premier-computing-company-for-the-age-of-ai)

- [The Pinephone Braveheart and Above Can Multi-Boot](https://xnux.eu/log/#014)

- [Mozilla Sunsets Firefox Send and Firefox Notes](https://blog.mozilla.org/blog/2020/09/17/update-on-firefox-send-and-firefox-notes/)

- [Deepin 20 Released; with new dual 5.4 and 5.7 kernel options](https://www.deepin.org/en/2020/09/11/deepin-20-innovation-is-ongoing/)

- [EndeavourOS September Release Out; with major updates to the Welcome app](https://endeavouros.com/news/the-september-release-and-endeavouros-arm-arrived/)

- [Puppy Linux 9.5 FossaPup Out; with switchable kernel, applications and firmware](https://forum.puppylinux.com/viewtopic.php?f=40&t=88&i=1)

- [KDE 5.20 Beta Out; with a final release expected Mid October](https://9to5linux.com/kde-plasma-5-20-desktop-enters-beta-final-release-expected-on-october-13)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
