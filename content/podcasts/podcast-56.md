---
title: "Full Circle Weekly News 56"
date: 2017-04-01
draft: false
tags:
  - "17-04"
  - "4-10"
  - "4-4"
  - "4-9"
  - "antivirus"
  - "app"
  - "elon-musk"
  - "gnome"
  - "google"
  - "ixmaps"
  - "kernel"
  - "linux"
  - "lts"
  - "malware"
  - "maps"
  - "neuralink"
  - "nsa"
  - "ok"
  - "open"
  - "ransomware"
  - "russian"
  - "source"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2056.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 17.04 inches closer to production](https://www.theregister.co.uk/2017/03/26/ubuntu_1704_final_beta/)

- [Linux Kernels 4.10.6, 4.9.18 LTS and 4.4.57 LTS Released with Updated Drivers](http://news.softpedia.com/news/linux-kernels-4-10-6-4-9-18-lts-and-4-4-57-lts-released-with-updated-drivers-514312.shtml)

- [Google Launches New Open Source Website](https://fossbytes.com/google-open-source-website/)

- [IXmaps: This Map Tells If Your Web Traffic Is Being Spied On By The NSA](https://fossbytes.com/ixmaps-map-web-traffic-nsa-listening-port/)

- [Elon Musk Launches New Startup Neuralink To Connect Human Brains With Computers](https://fossbytes.com/elon-musk-neuralink-connect-human-brains-computers/)

- [New Android Ransomware Goes Undetected by All Antivirus Programs](http://news.softpedia.com/news/new-android-ransomware-goes-undetected-by-all-antivirus-programs-514440.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
