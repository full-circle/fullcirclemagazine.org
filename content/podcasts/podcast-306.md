---
title: "Full Circle Weekly News 306"
date: 2023-03-26
draft: false
tags:
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20306.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of  Scilab 2023.0.0:](https://www.scilab.org/news/scilab-2023.0.0-has-been-released)

- [OpenChatKit, tools for creating chatbots:](https://www.together.xyz/blog/openchatkit)

- [Release of the Git 2.40:](https://lkml.org/lkml/2023/3/13/1087)

- [Release of nftables 1.0.7:](https://www.mail-archive.com/netfilter-announce@lists.netfilter.org/msg00256.html)

- [Qubes 4.1.2:](https://www.qubes-os.org/news/2023/03/15/qubes-4-1-2/)

- [Shattered Pixel Dungeon 2.0:](https://shatteredpixel.com/blog/shattered-pixel-dungeon-v200.html)

- [Fedora Linux 38 has moved to beta testing:](https://fedoramagazine.org/announcing-fedora-38-beta/)

- [Release of Kali Linux for 2023:](https://www.kali.org/blog/kali-linux-2023-1-release/)

- [Docker Hub scraps free service for organizations developing open projects:](https://blog.alexellis.io/docker-is-deleting-open-source-images/)

- [Amazon Linux 2023:](https://aws.amazon.com/blogs/aws/amazon-linux-2023-a-cloud-optimized-linux-distribution-with-long-term-support/)

- [Debian 12 moved to soft freeze:](https://lists.debian.org/debian-devel-announce/2023/02/msg00003.html)

- [NordVPN opens Linux client and library code with MeshNet implementation:](https://nordvpn.com/blog/nordvpn-linux-open-source/)

- [Release of WebKitGTK 2.40.0 and Epiphany 44 web browser:](https://webkitgtk.org/2023/03/17/webkitgtk2.40.0-released.html)

- [Release of LLVM 16.0:](https://discourse.llvm.org/t/llvm-16-0-0-release/69326)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
