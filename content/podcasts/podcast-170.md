---
title: "Full Circle Weekly News 170"
date: 2020-05-07
draft: false
tags:
  - "android"
  - "arch"
  - "defender"
  - "dns"
  - "dock"
  - "dxvk"
  - "fire"
  - "firefox"
  - "freespire"
  - "gimp"
  - "gnu"
  - "https"
  - "ipfire"
  - "kernel"
  - "latte"
  - "linux"
  - "lomiri"
  - "manjaro"
  - "microsoft"
  - "netrunner"
  - "news"
  - "pi"
  - "project"
  - "raspberry"
  - "ubuntu"
  - "unity"
  - "wayland"
  - "wine"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20170.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The Structure and Administration of the GNU Project Announced](https://www.gnu.org/gnu/gnu-structure.html)

- [CTO calls for patience after devs complain promised donations platform has stalled](https://www.theregister.co.uk/2020/02/22/npm_funding_source/)

- [Arch Linux Has a New Leader](https://www.archlinux.org/news/the-future-of-the-arch-linux-project-leader/)

- [Microsoft Previews Defender ATP for Linux](https://www.zdnet.com/article/microsoft-previews-microsoft-defender-atp-for-linux/)

- [Unity 8 Renamed to Lomiri](https://ubports.com/blog/ubports-blog-1/post/lomiri-new-name-same-great-unity8-265)

- [Raspberry Pi 4 with 2GB RAM Reduced to $35](https://www.raspberrypi.org/blog/new-price-raspberry-pi-4-2gb/)

- [DNS Over HTTPS Default for Firefox Users in USA](https://blog.mozilla.org/blog/2020/02/25/firefox-continues-push-to-bring-dns-over-https-by-default-for-us-users/)

- [Freespire 6.0 is Out](https://www.freespirelinux.com/2020/02/freespire-60-released.html)

- [Kernel 5.6 rc3 is Out](https://lkml.org/lkml/2020/2/23/336)

- [Wayland 1.20 is Out](https://lists.freedesktop.org/archives/wayland-devel/2020-February/041269.html)

- [DXVK 1.55 is Out](https://github.com/doitsujin/dxvk/releases/tag/v1.5.5)

- [Wine 5.3 is Out](https://www.winehq.org/announce/5.3)

- [IP Fire 2.25 Update 141 is Out](https://blog.ipfire.org/post/ipfire-2-25-core-update-141-release)

- [Latte Dock 0.9.9 is Out](https://psifidotos.blogspot.com/2020/02/latte-bug-fix-release-v099.html)

- [Manjaro 19, Kyria, is Out](https://forum.manjaro.org/t/manjaro-19-0-released-gnome-kde-xfce-architect/126010)

- [GIMP 2.10.18 is Out](https://www.gimp.org/news/2020/02/24/gimp-2-10-18-released/)

- [Netrunner "Twenty" is Out](https://www.netrunner.com/netrunner-20-01-twenty-released/)

- [Android x86 9.0-r1 is Out](https://www.android-x86.org/releases/releasenote-9-0-r1.html)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
