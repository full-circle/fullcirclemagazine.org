---
title: "Full Circle Weekly News 45"
date: 2016-12-10
draft: false
tags:
  - "ai"
  - "amazon"
  - "aws"
  - "canonical"
  - "north-korea"
  - "ota-14"
  - "ota14"
  - "phone"
  - "pinebook"
  - "rule"
  - "rule-41"
  - "rule41"
  - "services"
  - "snoopers"
  - "tor"
  - "touch"
  - "web"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2045.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical to sue cloud provider over Ubuntu images](http://www.itwire.com/cloud/76039-canonical-to-sue-cloud-provider-over-ubuntu-images.html)

- [“Just 1 Link” To Hack North Korea’s “Terrible” Linux Distro](https://fossbytes.com/hack-north-korea-linux-distro-red-star-3/)

- [Ubuntu Touch OTA-14 Officially Released](http://news.softpedia.com/news/ubuntu-touch-ota-14-officially-released-with-revamped-unity-8-interface-fixes-510787.shtml)

- [Tor Phone Is The “Super-secure Version Of Android”, Developed By Tor Project](https://fossbytes.com/dollar-89-pinebook-linux-laptop-64-bit/https://fossbytes.com/tor-phone-secure-android-copperhead/)

- [Your Entire Internet History Is Now Exposed, Thanks To The Snoopers](https://fossbytes.com/uk-snoopers-charter-internet-connection-record/)

- [Meet Pinebook, A Low Cost Linux Laptop That Looks Like A MacBook](https://fossbytes.com/dollar-89-pinebook-linux-laptop-64-bit/)

- [Amazon Web Services Releases 3 Artificial Intelligence Tools to the Public](http://www.businessnewsdaily.com/9604-amazon-artificial-intelligence-tools.html)

- [Rule 41 — The FBI Just Got Unlimited Power To Hack Any Computer In The World](https://fossbytes.com/rule-41-fbi-hacking-power-computer-warrant/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
