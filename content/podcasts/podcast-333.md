---
title: "Full Circle Weekly News 333"
date: 2023-10-01
draft: false
tags:
  - "webkitgtk"
  - "angie"
  - "nginx"
  - "rpm"
  - "kernel"
  - "linux"
  - "bcachefs"
  - "apache"
  - "pinot"
  - "opentf"
  - "opentofu"
  - "gnome"
  - "fedora"
  - "yescrypt"
  - "visopsys"
  - "geckos"
  - "retroarch"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20333.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of WebKitGTK 2.42.0:](https://webkitgtk.org/2023/09/15/webkitgtk2.42.0-released.html)

- [Angie 1.3.0, a Russian Nginx fork:](https://github.com/webserver-llc/angie/releases/tag/1.3.0)

- [Release of RPM 4.19 Package Manager:](http://rpm.org/)

- [It's been 32 years since the first release of the Linux 0.01 kernel:](https://lkml.org/lkml/2023/9/17/253)

- [Bcachefs added to the Linux-next kernel:](https://bcachefs.org/)

- [Apache Pinot 1.0:](https://pinot.apache.org/blog/2023/09/19/Annoucing-Apache-Pinot-1-0/)

- [OpenTF renamed OpenTofu:](https://www.terraform.io/)

- [GNOME 45:](https://foundation.gnome.org/2023/09/20/introducing-gnome-45/)

- [Fedora Linux 39 beta:](https://fedoramagazine.org/announcing-fedora-39-beta/)

- [Local vulnerability in the network subsystem of the Linux kernel:](https://www.openwall.com/lists/oss-security/2023/09/18/3)

- [Beta-release of Ubuntu 23.10:](https://lists.ubuntu.com/archives/ubuntu-announce/2023-September/000295.html)

- [Arch Linux moves to yescrypt for password hashes:](https://archlinux.org/news/changes-to-default-password-hashing-algorithm-and-umask-settings/)

- [Release of Visopsys 0.92:](https://visopsys.org/news/)

- [Release of GeckOS 2.1:](https://github.com/fachat/GeckOS-V2/tree/v2.1.0)

- [Release of RetroArch 1.16:](https://www.libretro.com/index.php/retroarch-1-16-0-release/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
