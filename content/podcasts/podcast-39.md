---
title: "Full Circle Weekly News 39"
date: 2016-10-15
draft: false
tags:
  - "16-10"
  - "alexa"
  - "amazon"
  - "beta"
  - "black"
  - "bodhi"
  - "canonical"
  - "email"
  - "freebsd"
  - "kernel"
  - "lab"
  - "module"
  - "netos"
  - "patch"
  - "yahoo"
  - "yak"
  - "yakkety"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2039.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 16.10 released](https://wiki.ubuntu.com/YakketyYak/ReleaseNotes)

- [Canonical Patches New Linux Kernel Vulnerabilities in All Supported Ubuntu OSes](http://news.softpedia.com/news/canonical-patches-new-linux-kernel-vulnerabilities-in-all-supported-ubuntu-oses-509171.shtml)

- [Yahoo email scanning done with a Linux kernel module](https://www.engadget.com/2016/10/08/reuters-yahoo-email-scanning-done-using-a-linux-kernel-module/)

- [Amazon's Alexa Can Now Run on Your Windows, Mac, or Linux Machine](http://gadgets.ndtv.com/internet/news/amazons-alexa-can-now-run-on-your-windows-mac-or-linux-machine-1472377)

- [Bodhi Linux 4.0.0 Beta Out, Final Release Lands This Month Based on Ubuntu 16.04](http://news.softpedia.com/news/bodhi-linux-4-0-0-beta-out-final-release-lands-this-month-based-on-ubuntu-16-04-509145.shtml)

- [Black Lab Linux Goes Commercial Due to Lack of Funding, netOS Discontinued](http://news.softpedia.com/news/black-lab-linux-goes-commercial-due-to-lack-of-founding-netos-discontinued-509198.shtml)

- [FreeBSD 11 Released with New Features](https://fossbytes.com/freebsd-11-released-the-open-source-operating-system-gets-new-features/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
