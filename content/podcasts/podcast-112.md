---
title: "Full Circle Weekly News 112"
date: 2018-10-21
draft: false
tags:
  - "android"
  - "bpftrace"
  - "coc"
  - "code"
  - "conduct"
  - "desktop"
  - "dtrace"
  - "google"
  - "kde"
  - "microsoft"
  - "patents"
  - "plasma"
  - "punkt"
  - "release"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20112.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [KDE Plasma 5.14 Released: What’s New In The Popular Linux Desktop](https://fossbytes.com/kde-plasma-5-14-release-linux-new-features/)

- [Punkt: A minimalist Android for the paranoid](https://www.theregister.co.uk/2018/10/09/punkt_mp02/)

- [Google+ Is Shutting Down After Data Breach](https://fossbytes.com/google-shutting-down-massive-data-breach/)

- [Microsoft: we've got your back, Linux, here are 60,000 patents to protect you](https://www.neowin.net/news/microsoft-weve-got-your-back-linux-here-are-60000-patents-to-protect-you/)

- [Linux Code Of Conduct Might See Some Changes Before 4.19 Release](https://fossbytes.com/linux-code-of-conduct-changes-before-4-19-release/)

- [Bpftrace should become Dtrace successor for Linux](https://www.golem.de/news/software-analyse-bpftrace-soll-dtrace-nachfolger-fuer-linux-werden-1810-137039.html)
