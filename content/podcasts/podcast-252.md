---
title: "Full Circle Weekly News 252"
date: 2022-03-13
draft: false
tags:
  - "almalinux"
  - "anbox"
  - "arti"
  - "canonical"
  - "coreboot"
  - "gnunet"
  - "hyperbola"
  - "lakka"
  - "mplayer"
  - "oracle"
  - "p2p"
  - "rust"
  - "scratch"
  - "seamonkey"
  - "smartphone"
  - "solaris"
  - "sqlite"
  - "tor"
  - "vodafone"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20252.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [GNUnet P2P Platform Release 0.16.0](https://www.gnunet.org/en/news/2022-02-0.16.0.html)

- [Release of SQLite 3.38 and sqlite-utils 3.24](http://sqlite.org/)

- [Release of MPlayer 1.5 media player](http://mplayerhq.hu/design7/news.html)

- [Coreboot 4.16 released](https://blogs.coreboot.org/blog/2022/02/26/announcing-coreboot-4-16/)

- [Canonical and Vodafone develop cloud smartphone technology using Anbox Cloud](https://ubuntu.com/blog/vodafone-cloud-smartphone-based-on-anbox-cloud)

- [AlmaLinux distribution option available for PowerPC architecture](https://repo.almalinux.org/almalinux/8.5/isos/ppc64le/)

- [Armbian release 22.02](https://www.armbian.com/newsflash/armbian-22-02-pig-release-announcement/)

- [Release of Hyperbola 0.4](https://www.hyperbola.info/)

- [Linux From Scratch 11.1 and Beyond Linux From Scratch 11.1 published](https://www.linuxfromscratch.org/lfs/view/11.1/)(https://www.linuxfromscratch.org/blfs/view/11.1)

- [SeaMonkey 2.53.11 Suite Released](https://www.seamonkey-project.org/)

- [First beta release of Arti, a Rust implementation of Tor](https://blog.torproject.org/arti_010_released/)

- [Release of Lakka 3.7](https://www.lakka.tv/articles/2022/03/01/lakka-3.7/)

- [Oracle Unveils Solaris 11.4 CBE, Free Use Edition](https://blogs.oracle.com/solaris/post/announcing-the-first-oracle-solaris-114-cbe)

- [First release of sdl12-compat, the SDL 1.2 compatibility layer running through SDL 2](https://github.com/libsdl-org/sdl12-compat/releases)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
