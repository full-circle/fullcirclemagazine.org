---
title: "Full Circle Weekly News 15"
date: 2016-04-30
draft: false
tags:
  - "budgie"
  - "c"
  - "code"
  - "format"
  - "mele"
  - "microsoft"
  - "package"
  - "pgc02u"
  - "python"
  - "research"
  - "sensor"
  - "snap"
  - "stick"
  - "windows-10"
  - "wolfram"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2015.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux expert Matthew Garrett says Ubuntu 16.04's new Snap format is a security risk](http://www.zdnet.com/article/linux-expert-matthew-garrett-ubuntu-16-04s-new-snap-format-is-a-security-risk/)

- [Ubuntu Budgie 16.04 Officially Released Based on Ubuntu 16.04 and Budgie Desktop](http://news.softpedia.com/news/ubuntu-budgie-16-04-officially-released-based-on-ubuntu-16-04-and-budgie-desktop-503414.shtml)

- [Microsoft reveals all about Windows 10's Linux subsystem](http://betanews.com/2016/04/24/windows-10-linux-subsystem/)

- [New functional programming language can generate C and Python code](http://www.infoworld.com/article/3060838/application-development/new-functional-programming-language-can-generate-c-python-code-for-apps.html)

- [Wolfram Research turns your Ubuntu phone into an IoT sensor](http://www.theverge.com/2016/4/27/11520410/wolfram-research-ubuntu-iot-data-sensor-cloud-)

- [This $70 computer stick is designed for Ubuntu](http://www.theverge.com/circuitbreaker/2016/4/27/11518642/mele-pgc02u-computer-stick-ubuntu)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
