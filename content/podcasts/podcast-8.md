---
title: "Full Circle Weekly News 08"
date: 2016-03-11
draft: false
tags:
  - "45"
  - "bmw"
  - "email"
  - "firefox"
  - "jasper"
  - "kernel"
  - "linux"
  - "microsoft"
  - "mozilla"
  - "ota-10"
  - "ota10"
  - "sql"
  - "tomlinson"
  - "torvalds"
  - "touch"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2008.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Kernel 4.5 to Land Next Week As Linus Torvalds Announces the Last RC Build](http://news.softpedia.com/news/linux-kernel-4-5-to-land-next-week-as-linus-torvalds-announces-the-last-rc-build-501416.shtml)

- [JasPer Vulnerabilities Fixed in Ubuntu](http://linux.softpedia.com/blog/jasper-vulnerabilities-fixed-in-ubuntu-501346.shtml)

- [Microsoft Opens Its Corporate Data Software to Linux](http://www.nytimes.com/2016/03/08/technology/microsoft-opens-its-corporate-data-software-to-linux.html?_r=0)

- [Ray Tomlinson Inventor of email dies aged 74](http://www.independent.co.uk/news/people/ray-tomlinson-dead-inventor-of-email-dies-aged-74-a6916496.html)

- [Ubuntu Touch OTA-10 Update to Land in April, Supports the New BQ Tablet](http://news.softpedia.com/news/ubuntu-touch-ota-10-update-to-land-in-april-supports-the-new-bq-ubuntu-tablet-501510.shtml)

- [Mozilla Firefox 45.0 Lands in All Supported Ubuntu OSes](http://news.softpedia.com/news/mozilla-firefox-45-0-lands-in-all-supported-ubuntu-oses-without-gtk3-integration-501550.shtml)

- [BMW To Build The ‘Most Intelligent Car’](http://www.ubergizmo.com/2016/03/bmw-to-build-the-most-intelligent-car/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
