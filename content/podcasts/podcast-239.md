---
title: "Full Circle Weekly News 239"
date: 2021-12-10
draft: false
tags:
  - "3d"
  - "4mlinux"
  - "amazon"
  - "anti-virus"
  - "antivirus"
  - "boot"
  - "centos"
  - "cups"
  - "disk"
  - "engine"
  - "ghostbsd"
  - "library"
  - "multimedia"
  - "neovim"
  - "nix"
  - "nixos"
  - "open"
  - "openprinting"
  - "paint"
  - "rescuepack"
  - "sdl"
  - "sqlite"
  - "stream"
  - "tux"
  - "webos"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20239.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [GhostBSD Release 21.11.24](https://ghostbsd.org/ghostbsd_21.11.24_iso_is_now_available)

- [Tux Paint 0.9.27 is released](https://tuxpaint.org/latest/tuxpaint-0.9.27-press-release.php)

- [SQLite 3.37 Released](https://www.sqlite.org/changes.html)

- [The OpenPrinting project has released CUPS 2.4.0](https://openprinting.github.io/cups-2.4.0/)

- [WebOS Open Source Edition 2.14 Released](https://www.webosose.org/blog/2021/11/30/webos-ose-2-14-0-release/)

- [4MLinux 38.0 in the wild](https://4mlinux-releases.blogspot.com/2021/11/4mlinux-380-stable-released.html)

- [SDL 2.0.18 Multimedia Library Released](https://discourse.libsdl.org/t/sdl-2-0-18-released/33768)

- [Ubuntu RescuePack 21.11 Anti-Virus Boot Disk Update](https://ualinux.com/ru/news/update-ubuntu-rescuepack)

- [NixOS 21.11 using the Nix package manager](https://nixos.org/blog/announcements.html#21.11)

- [Release of Neovim 0.6.0](https://github.com/neovim/neovim/releases/tag/v0.6.0)

- [The first release of the Open 3D Engine game engine opened by Amazon](https://o3de.org/blog/posts/o3de-2111-announcement/)

- [CentOS Stream 9 distribution is officially presented](https://blog.centos.org/2021/12/introducing-centos-stream-9/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
