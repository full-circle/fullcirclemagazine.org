---
title: "Full Circle Weekly News 305"
date: 2023-03-19
draft: false
tags:
  - "libreelec"
  - "apt"
  - "audacious"
  - "apache"
  - "samba"
  - "flathub"
  - "flatpak"
  - "p10"
  - "alt"
  - "octave"
  - "retroarch"
  - "hellosystem"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20305.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of LibreELEC 11.0:](https://libreelec.tv/2023/03/06/libreelec-nexus-11-0-0/)

- [Release of APT 2.6:](https://github.com/Debian/apt/releases/tag/2.6.0)

- [Release of Audacious 4.3:](https://audacious-media-player.org/news/56-audacious-4-3-released)

- [Release of Apache 2.4.56:](https://downloads.apache.org/httpd/Announcement2.4.html)

- [New version of Samba 4.18.0:](https://lists.samba.org/archive/samba-announce/2023/000630.html)

- [Plan to promote Flathub as an independent app store:](https://discourse.flathub.org/t/flathub-in-2023/3808)

- [ALT p10 update:](https://lists.altlinux.org/pipermail/community/2023-March/688890.html)

- [Release of GNU Octave 8:](https://octave.org/news/release/2023/03/07/octave-8.1.0-released.html)

- [Release of RetroArch 1.15:](https://www.libretro.com/index.php/retroarch-1-15-0-release/)

- [Release of helloSystem 0.8.1:](https://github.com/helloSystem/ISO/releases/tag/r0.8.1)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
