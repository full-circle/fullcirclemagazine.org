---
title: "Full Circle Weekly News 10"
date: 2016-03-26
draft: false
tags:
  - "aqaris"
  - "beta"
  - "bq"
  - "bsd"
  - "core"
  - "driver"
  - "endless"
  - "final"
  - "freebsd"
  - "freeze"
  - "m10"
  - "mini"
  - "mir"
  - "nvidia"
  - "openstack"
  - "owncloud"
  - "pi"
  - "raspberry"
  - "snappy"
  - "tablet"
  - "tele2"
  - "ubuntubsd"
  - "vulkan"
  - "wayland"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2010.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [ownCloud Pi Device to Run on Snappy Ubuntu Core 16.04 LTS and Raspberry Pi 3](http://news.softpedia.com/news/owncloud-pi-device-to-run-on-snappy-ubuntu-core-16-04-lts-and-raspberry-pi-3-501904.shtml)

- [Endless Mini Computer Succeeds](http://www.examiner.com/review/the-endless-mini-endless-possibilities)

- [Nvidia 364.12 Linux Driver Out Now with Vulkan 1.0, Wayland, and Mir Support](http://news.softpedia.com/news/nvidia-364-12-linux-driver-out-now-with-vulkan-1-0-support-wayland-improvements-501989.shtml)

- [UbuntuBSD Brings Ubuntu And FreeBSD Together](http://itsfoss.com/ubuntubsd-ubuntu-freebsd/)

- [Ubuntu 16.04 LTS Final Beta Now in Feature Freeze, Lands March 24](http://news.softpedia.com/news/ubuntu-16-04-lts-xenial-xerus-final-beta-now-in-feature-freeze-lands-march-24-502020.shtml)

- [The Ubuntu Tablet, BQ Aquaris M10, Will Be Available for Pre-Order on March 28](http://news.softpedia.com/news/the-ubuntu-tablet-bq-aquaris-m10-will-be-available-for-pre-order-on-march-28-502091.shtml)

- [Tele2 Adopts Canonical's Ubuntu Open Source OpenStack Cloud for NFV](http://thevarguy.com/open-source-application-software-companies/tele2-adopts-canonicals-ubuntu-open-source-openstack-clou)

**CREDITS**

- Intro: ["Weapons" - Cory Gray] (http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
