---
title: "Full Circle Weekly News 254"
date: 2022-03-27
draft: false
tags:
  - "agpl"
  - "blender"
  - "browser"
  - "console"
  - "debian"
  - "delivery"
  - "domain"
  - "drama"
  - "framework"
  - "libressl"
  - "neo4j"
  - "offpunk"
  - "openssl"
  - "redhat"
  - "red hat"
  - "trademark"
  - "updates"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20254.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Blender 3.1](https://www.youtube.com/watch?v=BCi0QRM1ADY)

- [TUF 1.0 is available, a framework for secure delivery of updates](https://github.com/theupdateframework/python-tuf/blob/v1.0.0/docs/1.0.0-ANNOUNCEMENT.md)

- [First release of Offpunk console browser](https://tildegit.org/ploum/AV-98-offline)

- [Debian 12 package base freeze date determined](https://lists.debian.org/debian-devel-announce/2022/03/msg00006.html)

- [Red Hat tried to take away the WeMakeFedora.org domain under the guise of trademark infringement](https://www.adrforum.com/DomainDecisions/1980642.htm)

- [Debian maintainer leaves Debian, more drama](https://www.preining.info/)

- [Results of the legal proceedings related to the Neo4j project and the AGPL license](https://storage.courtlistener.com/recap/gov.uscourts.cand.335295/gov.uscourts.cand.335295.140.0.pdf)

- [Release of Pale Moon Browser 30.0](https://forum.palemoon.org/viewtopic.php?t=27956&p=224775)

- [Vulnerability in OpenSSL and LibreSSL](https://www.openssl.org/news/openssl-3.0-notes.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
