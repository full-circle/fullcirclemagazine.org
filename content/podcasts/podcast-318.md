---
title: "Full Circle Weekly News 318"
date: 2023-06-18
draft: false
tags:
  - "blink"
  - "owncast"
  - "plane"
  - "asahi"
  - "opensuse"
  - "leap"
  - "postmarketos"
  - "cinnamon"
  - "fciv"
  - "freeciv"
  - "puzzlefs"
  - "debian"
  - "easyos"
  - "bookworm"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20318.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Blink, high-performance system emulator:](https://github.com/jart/blink/releases/tag/1.0.0)

- [Release of Owncast 0.1.0:](https://owncast.online/releases/owncast-0.1.0/)

- [Plane:](https://github.com/makeplane/plane/releases/tag/v0.7-dev)

- [Asahi Linux:](https://asahilinux.org/2023/06/opengl-3-1-on-asahi-linux/)

- [Release of openSUSE Leap 15.5:](https://www.opensuse.org/)

- [PostmarketOS 23.06:](https://postmarketos.org/blog/2023/06/07/v23.06-release/)

- [Release of Cinnamon 5.8:](http://cinnamon.linuxmint.com/)

- [Project Fciv.net develops 3D version of the strategy game Freeciv:](https://github.com/fciv-net/fciv-net)

- [PuzzleFS file system for Linux kernel:](https://lore.kernel.org/rust-for-linux/20230609063118.24852-1-amiculas@cisco.com)

- [Release of Debian 12 "Bookworm":](https://www.debian.org/News/2023/20230610)

- [Release of EasyOS 5.4:](https://bkhome.org/news/202306/easyos-kirkstone-series-version-54-released.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
