---
title: "Full Circle Weekly News 109"
date: 2018-09-30
draft: false
tags:
  - "14-04"
  - "18-04-1"
  - "2028"
  - "canonical"
  - "ceo"
  - "cod"
  - "code"
  - "conduct"
  - "databas"
  - "esm"
  - "facebook"
  - "google"
  - "linux"
  - "malware"
  - "ubuntu"
  - "windows"
  - "wobbly"
  - "xbash"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20109.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Wobbly Windows Are Making a Comeback ](https://www.omgubuntu.co.uk/2018/09/wobbly-windows-new-libanimation-linux-library)

- [Canonical Releases Ubuntu 18.04.1 Desktop Image Optimized for Microsoft Hyper-V](https://news.softpedia.com/news/canonical-releases-ubuntu-18-04-1-desktop-image-optimized-for-microsoft-hyper-v-522743.shtml)

- [Canonical reveals Ubuntu Linux 14.04 LTS 'Trusty Tahr' Extended Security Maintenance (ESM) plans](https://betanews.com/2018/09/19/canonical-ubuntu-linux-trusty-tahr-esm/)

- [Unit 42 Researchers Discover Xbash – Malware Which Destroys Linux and Windows Based Databases](https://appuals.com/unit-42-researchers-discover-xbash-malware-which-destroys-linux-and-windows-based-databases/)

- [You Gave Facebook Your Number For Security. They Used It For Ads.](https://www.eff.org/deeplinks/2018/09/you-gave-facebook-your-number-security-they-used-it-ads)

- [Linux Adopts A New Code Of Conduct](https://fossbytes.com/linux-code-of-conduct/)

- [Ex-Google CEO: There Will Be Two Versions of The Internet by 2028](https://fossbytes.com/eric-schmidt-two-versions-of-internet-by-2028-china-led-us-led/)
