---
title: "Full Circle Weekly News 55"
date: 2017-03-25
draft: false
tags:
  - "12-04"
  - "adobe"
  - "antivirus"
  - "attack"
  - "canonical"
  - "cisco"
  - "doubleagent"
  - "edge"
  - "hacked"
  - "linux"
  - "lts"
  - "malware"
  - "munich"
  - "pown2own"
  - "reader"
  - "safari"
  - "support"
  - "switches"
  - "ubuntu"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2055.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

Apologies for the lack of shows for the past two weeks. I had a heavy cold and assumed you wouldn't want me coughing and sniffling in your ears. :)

- [Canonical extends Ubuntu 12.04 support for paying customers](http://www.zdnet.com/article/canonical-extends-ubuntu-12-04-support-for-paying-customers/)

- [Ubuntu Linux, Safari, Adobe Reader, And Edge Hacked At Pwn2Own 2017](https://fossbytes.com/ubuntu-linux-safari-adobe-edge-hacked-pwn2own-2017/)

- [Linux in Munich: 'No compelling technical reason to return to Windows,' says city's IT chief](http://www.techrepublic.com/article/linux-in-munich-no-compelling-technical-reason-to-return-to-windows-says-citys-it-chief/)

- [DoubleAgent Attack Turns Your Antivirus Into Malware And Hijacks Your PC](https://fossbytes.com/doubleagent-attack-antivirus-into-malware/)

- [Critical Bug Allows CIA To Control 318 Cisco Switch Models, No Fix Available](https://fossbytes.com/cisco-switches-critical-vulnerability-cmp/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
