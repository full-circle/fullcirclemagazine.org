---
title: "Full Circle Weekly News 139"
date: 2019-07-15
draft: false
tags:
  - "amd"
  - "android"
  - "anubis"
  - "banking"
  - "desktop"
  - "driver"
  - "firefox"
  - "lts"
  - "macos"
  - "malware"
  - "mozilla"
  - "nvidia"
  - "pyoxidizer"
  - "python"
  - "ryzen"
  - "system76"
  - "thelio"
  - "ubuntu"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20139.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [System 76’s Linux-powered Thelio desktop now available with 3rd gen AMD Ryzen Processors](https://betanews.com/2019/07/07/system76-linux-thelio-amd-ryzen3/)

- [PyOxidizer Can Turn Python Code Into Apps for Windows, MacOS, Linux](https://fossbytes.com/pyoxidizer-can-turn-python-code-apps-for-windows-macos-linux/)

- [Thousands of Android Apps Can Track Your Phone -- Even if You Deny Permissions](https://www.theverge.com/2019/7/8/20686514/android-covert-channel-permissions-data-collection-imei-ssid-location)

- [Anubis Android Banking Malware Returns with Extensive Financial App Hit List](https://www.zdnet.com/article/anubis-android-banking-malware-returns-with-a-bang/)

- [Mozilla Firefox and the Nomination for Internet Villain Award](https://itsfoss.com/mozilla-internet-villain/)

- [Ubuntu LTS Will Now Get the Latest Nvidia Driver Updates](https://itsfoss.com/ubuntu-lts-latest-nvidia-drivers/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
