---
title: "Full Circle Weekly News 157"
date: 2019-12-17
draft: false
tags:
  - "acbackdoor"
  - "backdoor"
  - "centos"
  - "china"
  - "chrome"
  - "ddos"
  - "debian"
  - "edge"
  - "enterprise"
  - "hacked"
  - "hacking"
  - "ipfire"
  - "linux"
  - "lite"
  - "nextcry"
  - "ransomware"
  - "red-hat"
  - "redhat"
  - "safari"
  - "server"
  - "system-76-laptop"
  - "system76"
  - "webmin"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20157.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Updated Debian 10: 10.2 released](https://www.debian.org/News/2019/20191116)

- [Zorin OS 15 Lite is Here](https://zoringroup.com/blog/2019/11/20/zorin-os-15-lite-is-here-breathe-new-life-into-your-old-computers/)

- [IPFire 2.23, Core Update 137 released](https://blog.ipfire.org/post/ipfire-2-23-core-update-137-released)

- [China Extends Lead in Number of TOP500 Supercomputers](https://www.top500.org/news/china-extends-lead-in-number-of-top500-supercomputers-us-holds-on-to-performance-advantage/)

- [System76 Will Build Its Own Linux Laptops From January 2020](https://www.forbes.com/sites/jasonevangelho/2019/11/20/system76-will-start-designing-and-building-its-own-linux-laptops-beginning-january-2020/#41e955b73e16)

- [Red Hat Enterprise Linux and CentOS Now Patched Against Latest Intel CPU Flaws](https://news.softpedia.com/news/red-hat-enterprise-linux-and-centos-now-patched-against-latest-intel-cpu-flaws-528177.shtml)

- [Chrome, Edge, Safari Hacked at Elite Chinese Hacking Contest](https://www.zdnet.com/article/chrome-edge-safari-hacked-at-elite-chinese-hacking-contest/)

- [ACBackdoor, a New Multiplatform Backdoor](https://www.intezer.com/blog-acbackdoor-analysis-of-a-new-multiplatform-backdoor/)

- [NextCry Ransomware Goes After Linux Servers](https://www.tomshardware.com/news/nextcry-ransomware-nextcloud-linux-nginx-cybersecurity)

- [Linux Servers Running Webmin App Targeted By DDoS Attacks](https://fossbytes.com/linux-servers-webmin-targeted-ddos-attacks/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
