---
title: "Full Circle Weekly News 188"
date: 2020-11-03
draft: false
tags:
  - "20-10"
  - "antix"
  - "budgie"
  - "build"
  - "edge"
  - "etonia"
  - "firefox"
  - "google"
  - "gorilla"
  - "groovy"
  - "kde"
  - "kernel"
  - "kubuntu"
  - "linux"
  - "lubnutu"
  - "mate"
  - "microsoft"
  - "mozilla"
  - "plasma"
  - "pop_os"
  - "preview"
  - "studio"
  - "tails"
  - "trisquel"
  - "ubuntu"
  - "virtualbox"
  - "xubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20188.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 20.10 Groovy Gorilla and Flavors Released](https://discourse.ubuntu.com/t/groovy-gorilla-release-notes/15533)

- [It's Fedora Test Week for Kernel 5.9](https://fedoramagazine.org/fedora-kernel-5-9-test-week/)

- [Microsoft Edge Preview Builds Available](https://blogs.windows.com/msedgedev/2020/10/20/microsoft-edge-dev-linux/)

- [Mozilla Reacts to the U.S. v. Google Antitrust Lawsuit](https://blog.mozilla.org/blog/2020/10/20/mozilla-reaction-to-u-s-v-google/)

- [Pop!\_OS 20.10 Released](https://blog.system76.com/post/632781631953027072/whats-new-in-popos-2010)

- [Tails 4.12 Out](https://tails.boum.org/news/version_4.12/index.en.html)

- [AntiX 19.3 Out](https://antixlinux.com/antix-19-isos-available/)

- [Trisquel 9.0 Etonia Out](https://trisquel.info/en/trisquel-90-etiona-release-announcement-and-100-plans)

- [Linux Kernel 5.10 rc1 Out](https://www.lkml.org/lkml/2020/10/25/267)

- [KDE Plasma 5.20.1 Out](https://kde.org/announcements/plasma-5.20.0-5.20.1-changelog/)

- [Firefox 82 Out](https://www.mozilla.org/en-US/firefox/82.0/releasenotes/)

- [Virtualbox 6.1.16 Out](https://www.virtualbox.org/wiki/Changelog)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
