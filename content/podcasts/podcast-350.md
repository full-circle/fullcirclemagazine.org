---
title: "Full Circle Weekly News 350"
date: 2024-01-28
draft: false
tags:
  - "lutris"
  - "opensuse"
  - "cosmic"
  - "shell"
  - "virtualbox"
  - "mysql"
  - "xorg"
  - "tesseract"
  - "gnu"
  - "emacs"
  - "foxconn"
  - "wayland"
  - "ocrad"
  - "fuchsia"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20350.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Lutris 0.5.15:](https://github.com/lutris/lutris/releases/tag/v0.5.15)

- [openSUSE Leap 16 will be built on the ALP platform using containers:](https://news.opensuse.org/2024/01/15/clear-course-is-set-for-os-leap/)

- [COSMIC Custom Shell:](https://blog.system76.com/post/cosmic-the-road-to-alpha)

- [VirtualBox 7.0.14 released:](https://www.mail-archive.com/vbox-announce@virtualbox.org/msg00229.html)

- [MySQL 8.3.0 DBMS:](https://dev.mysql.com/downloads/mysql/)

- [X.Org Server 21.1.11:](https://lists.x.org/archives/xorg/2024-January/061526.html)

- [Release of Tesseract 5.3.4:](https://github.com/tesseract-ocr/tesseract/releases/tag/5.3.4)

- [Release of GNU Emacs 29.2:](https://www.mail-archive.com/info-gnu@gnu.org/msg03249.html)

- [Foxconn joins initiative to protect Linux from patent claims:](https://openinventionnetwork.com/hon-hai-technology-group-foxconn-joins-open-invention-network-community/)

- [Release of Wayland-Protocols 1.33:](https://lists.freedesktop.org/archives/wayland-devel/2024-January/043400.html)

- [KDE has improved scaling support and added autosaving in Dolphin:](https://pointieststick.com/2024/01/19/this-week-in-kde-auto-save-in-dolphin-and-better-fractional-scaling/)

- [Release of GNU Ocrad OCR 0.29:](https://www.mail-archive.com/info-gnu@gnu.org/msg03251.html)

- [Fuchsia Workstation OS Development Program Cancelled:](https://bugs.chromium.org/p/chromium/issues/detail?id%3D1509109)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
