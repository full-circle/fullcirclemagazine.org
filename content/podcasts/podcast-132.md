---
title: "Full Circle Weekly News 132"
date: 2019-05-27
draft: false
tags:
  - "android"
  - "antergos"
  - "gcc"
  - "git"
  - "google"
  - "huawei"
  - "kali"
  - "korea"
  - "linux"
  - "malware"
  - "peppermint"
  - "tails"
  - "winnti"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20132.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [GCC Switching to Git Is Approaching](https://www.pro-linux.de/news/1/27071/gcc-umstieg-auf-git-kommt-n%C3%83%C2%A4her.html)

- [Google Suspends Huawei’s Android Support](https://www.engadget.com/2019/05/19/google-pulls-android-support-from-huawei/)

- [Security Researchers Discover Linux Version of Winnti Malware](https://www.zdnet.com/article/security-researchers-discover-linux-version-of-winnti-malware/)

- [South Korea Wants to Switch from Windows 7 to Linux](https://www.golem.de/news/verwaltung-suedkorea-will-von-windows-7-auf-linux-wechseln-1905-141406.html)

- [Antergos Linux Project Is Dead](https://fossbytes.com/antergos-linux-dead-alternatives/)

- [Tails 3.14 Released](https://www.pro-linux.de/news/1/27088/tails-314-ver%C3%B6ffentlicht.html)

- [Peppermint 10 Officially Released](https://news.softpedia.com/news/peppermint-10-operating-system-officially-released-based-on-ubuntu-18-04-lts-526146.shtml)

- [Kali Linux 2019.2 Released with Nethunter and New Kernel](https://fossbytes.com/kali-linux-2019-2-released-nethunter-download/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
