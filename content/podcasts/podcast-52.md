---
title: "Full Circle Weekly News 52"
date: 2017-02-11
draft: false
tags:
  - "3-0"
  - "ai"
  - "antitrust"
  - "bt"
  - "canonical"
  - "criminals"
  - "devices"
  - "drop"
  - "eu"
  - "fairphone"
  - "fairphone2"
  - "google"
  - "iot"
  - "mirai"
  - "ota-15"
  - "ota15"
  - "port"
  - "support"
  - "tails"
  - "touch"
  - "trojan"
  - "ubports"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2052.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Touch OTA-15 Has Been Officially Released for Ubuntu Phones and Tablets](http://news.softpedia.com/news/ubuntu-touch-ota-15-has-been-officially-released-for-ubuntu-phones-and-tablets-512689.shtml)

- [UBports Community Successfully Ports Canonical’s Ubuntu OS to the Fairphone 2](http://news.softpedia.com/news/ubports-community-successfully-ports-canonical-s-ubuntu-os-to-the-fairphone-2-512737.shtml)

- [Tails 3.0 will drop 32-bit processor support](https://betanews.com/2017/02/03/tails-3-32-bit-processor/)

- [Mirai: Windows Trojan helps hackers infect Linux-based devices with IoT malware](http://www.theinquirer.net/inquirer/news/3004242/mirai-windows-trojan-helps-hackers-infect-linux-based-devices-with-iot-malware)

- [BT defends Google and Android in EU antitrust case](https://www.engadget.com/2017/02/06/bt-google-android-antitrust-eu-letter/)

- [How criminals use Artificial Intelligence and Machine Learning](https://betanews.com/2017/02/08/how-criminals-use-artificial-intelligence-and-machine-learning/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
