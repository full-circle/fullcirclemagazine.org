---
title: "Full Circle Weekly News 82"
date: 2018-02-26
draft: false
tags:
  - "16-04-4"
  - "attack"
  - "cpu"
  - "flaw"
  - "kde"
  - "lts"
  - "meltdown"
  - "plasma"
  - "spectre"
  - "ubuntu"
  - "unity"
  - "usb"
  - "xenial"
  - "xerus"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2082.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical Plans to Release Ubuntu 16.04.4 LTS (Xenial Xerus) on March 1, 2018](http://news.softpedia.com/news/canonical-plans-to-release-ubuntu-16-04-4-lts-xenial-xerus-on-march-1-2018-519817.shtml)

- [Unity 8 on the Desktop](https://ubports.com/blog/ubports-blog-1/post/unity8-on-the-desktop-95)

- [KDE Plasma Linux Desktop Is No Longer Vulnerable to USB Attacks](http://news.softpedia.com/news/kde-plasma-linux-desktop-is-no-longer-vulnerable-to-usb-attacks-update-now-519767.shtml)

- [Hate to ruin your day, but... Boffins cook up fresh Meltdown, Spectre CPU design flaw exploits](https://www.theregister.co.uk/2018/02/14/meltdown_spectre_exploit_variants/)
