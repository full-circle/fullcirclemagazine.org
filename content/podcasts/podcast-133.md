---
title: "Full Circle Weekly News 133"
date: 2019-06-03
draft: false
tags:
  - "blackarch"
  - "cloud"
  - "editor"
  - "fedora"
  - "gparted"
  - "hiddenwasp"
  - "krita"
  - "linux"
  - "systems"
  - "unity"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20133.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [GParted Reaches 1.0](https://news.softpedia.com/news/gparted-open-source-partition-editor-reaches-1-0-milestone-after-almost-15-years-526221.shtml)

- [Krita 4.2 Released](https://www.linuxuprising.com/2019/05/free-raster-graphics-editor-krita-420.html)

- [Unity Editor Now Officially Available for Linux](https://itsfoss.com/unity-editor-linux/_)

- [Google Cloud Goes Down](https://www.zdnet.com/article/google-cloud-goes-down-taking-youtube-gmail-snapchat-and-others-with-it/)

- [Fedora 28 Linux OS Reached End of Life](https://news.softpedia.com/news/fedora-28-linux-os-reached-end-of-life-users-urged-to-upgrade-to-fedora-30-526220.shtml)

- [BlackArch Gets New Release](https://news.softpedia.com/news/blackarch-linux-ethical-hacking-os-gets-new-release-with-more-than-150-new-tools-526162.shtml)

- [HiddenWasp Malware Found Targeting Linux Systems](https://www.zdnet.com/article/new-hiddenwasp-malware-found-targeting-linux-systems/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
