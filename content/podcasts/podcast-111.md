---
title: "Full Circle Weekly News 111"
date: 2018-10-14
draft: false
tags:
  - "18-10"
  - "ai"
  - "arch"
  - "beta"
  - "cosmic"
  - "cuttlefish"
  - "darpa"
  - "debian"
  - "emmabuntus"
  - "flaw"
  - "israel"
  - "kernel"
  - "linux"
  - "microsoft"
  - "security"
  - "stretch"
  - "ubuntu"
  - "web"
  - "xcom"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20111.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 18.10 “Cosmic Cuttlefish” Beta Released: Download All Flavors Here](ttps://fossbytes.com/ubuntu-18-10-cosmic-cuttlefish-beta-download-features/)

- [Emmabuntüs Releases Debian Edition 2 1.03](https://emmabuntus.sourceforge.io/mediawiki/index.php/October_2018_EmmaDE2_1_03)

- [Solid: Web inventor launches free web platform](https://www.pro-linux.de/news/1/26354/solid-web-)

- [XCOM 2: War of the Chosen - Tactical Legacy Pack Is Coming to Linux and macOS](https://news.softpedia.com/news/xcom-2-war-of-the-chosen-tactical-legacy-pack-is-coming-to-linux-on-october-9-523012.shtml)

- [Major Debian GNU/Linux 9 "Stretch" Linux Kernel Patch Fixes 18 Security Flaws](https://news.softpedia.com/news/major-debian-gnu-linux-9-stretch-linux-kernel-patch-fixes-18-security-flaws-523011.shtml)

- [Arch Linux's October 2018 ISO Snapshot Released with Linux Kernel 4.18.9](https://news.softpedia.com/news/arch-linux-s-october-2018-iso-snapshot-released-with-linux-kernel-4-18-9-more-522992.shtml)

- [DARPA introduces ‘third wave’ of artificial intelligence](https://www.artificialintelligence-news.com/2018/09/28/darpa-third-wave-artificial-intelligence/)

- [Israel to end licensing agreement with Microsoft](https://www.reuters.com/article/us-israel-microsoft/israel-to-end-licensing-agreement-with-microsoft-idUSKCN1L61DX)
