---
title: "Full Circle Weekly News 127"
date: 2019-04-22
draft: false
tags:
  - "android"
  - "arch"
  - "beta"
  - "debian"
  - "desktop"
  - "environment"
  - "fedora"
  - "gnome"
  - "linux"
  - "mx-linux"
  - "privacyidea"
  - "red-hat"
  - "redhat"
  - "spurv"
  - "taipei"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20127.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

**First episode with new host Leo Chavez**

- [First Arch Linux ISO Snapshot Powered by Linux Kernel 5.0 Is Here](https://news.softpedia.com/news/first-arch-linux-iso-snapshot-powered-by-linux-kernel-5-0-is-here-525604.shtml)

- [MX Linux 18.2 is here -- download the Debian-based operating system now](https://betanews.com/2019/04/09/mx-linux-182-debvian/)

- [Fedora 30 Beta Released For Bleeding-edge Linux Experience](https://fossbytes.com/fedora-30-beta-linux-features-download/)

- [GNOME 3.32 "Taipei" Desktop Environment Gets First Point Release, Update Now](https://news.softpedia.com/news/gnome-3-32-desktop-environment-gets-first-point-release-update-now-525634.shtml)

- [Red Hat And Fedora Working To Bring Linux-powered ARM Laptops](https://fossbytes.com/red-hat-fedora-bring-linux-arm-laptops/)

- [Ubuntu MATE 19.04 and 18.04.2 Are Now Available for GPD Pocket & GDP Pocket 2](https://news.softpedia.com/news/ubuntu-mate-19-04-and-18-04-2-are-now-available-for-gpd-pocket-and-gdp-pocket-2-525603.shtml)

- [‘SPURV’ Project Lets You Run Android Apps On Desktop Linux](https://fossbytes.com/run-android-apps-on-desktop-linux-with-spurv/)

- [privacyIDEA 3.0 released](https://www.pro-linux.de/news/1/26965/privacyidea-30-erschienen.html)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
