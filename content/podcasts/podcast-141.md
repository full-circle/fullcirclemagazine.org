---
title: "Full Circle Weekly News 141"
date: 2019-07-30
draft: false
tags:
  - "18-04"
  - "19-04"
  - "5g"
  - "bt"
  - "btrfs"
  - "canonical"
  - "cloud"
  - "core"
  - "coreboot"
  - "dropbox"
  - "ecryptfs"
  - "exim"
  - "firefox"
  - "flaw"
  - "jira"
  - "kernel"
  - "linux"
  - "media"
  - "mozilla"
  - "openstack"
  - "security"
  - "server"
  - "tor"
  - "ubuntu"
  - "vlc"
  - "xfs"
  - "zfs"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20141.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Mozilla Firefox Could Soon Get a “Tor mode” Add-on](https://news.softpedia.com/news/mozilla-firefox-could-soon-get-a-tor-mode-add-on-526774.shtml)

- [Critical Flaw in VLC Media Player Discovered by German Cybersecurity Agency](https://news.softpedia.com/news/critical-flaw-in-vlc-media-player-discovered-by-german-cybersecurity-agency-526768.shtml)

- [Hackers Exploit Jira and Exim Linux Servers to “Keep the Internet Safe”](https://www.bleepingcomputer.com/news/security/hackers-exploit-jira-exim-linux-servers-to-keep-the-internet-safe/)

- [Dropbox Is Bringing Back Support for ZFS, XFS, BTRFS, and eCryptFS on Linux](https://itsfoss.com/dropbox-brings-back-linux-filesystem-support/)

- [Announcing Coreboot 4.10](https://blogs.coreboot.org/blog/2019/07/22/announcing-coreboot-4-10/)

- [Canonical Outs New Linux Kernel Security Updates for Ubuntu 19.04 and 18.04](https://news.softpedia.com/news/canonical-outs-new-linux-kernel-security-updates-for-ubuntu-19-04-and-18-04-lts-526818.shtml)

- [Ubuntu OpenStack Architecture to Empower BT’s Next-Gen 5g Cloud Core](https://news.softpedia.com/news/canonical-s-ubuntu-openstack-architecture-to-empower-bt-s-next-gen-5g-cloud-core-526834.shtml)

- [Virtualbox 6.0.10 Adds UEFI Secure Boot Driver Signing Support on Ubuntu and Debian](https://news.softpedia.com/news/virtualbox-6-0-10-adds-uefi-secure-boot-driver-signing-support-on-ubuntu-debian-526817.shtml)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
