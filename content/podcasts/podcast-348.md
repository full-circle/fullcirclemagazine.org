---
title: "Full Circle Weekly News 348"
date: 2024-01-14
draft: false
tags:
  - "nodverse"
  - "gnuplot"
  - "scribus"
  - "snoop"
  - "vim"
  - "ipfire"
  - "maestro"
  - "rust"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20348.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Update to Nodeverse:](https://content.minetest.net/packages/aerkiaga/nodeverse/)

- [Release of Gnuplot 6.0:](https://gnuplot.sourceforge.net/ReleaseNotes_6_0_0.html)

- [Release of Scribus 1.6.0:](https://www.scribus.net/scribus-1-6-0-released/)

- [Release of Snoop 1.4.0:](https://github.com/snooppr/snoop)

- [Release of Vim 9.1:](https://www.vim.org/news/news.php)

- [Release of IPFire 2.27 Core 182:](https://blog.ipfire.org/post/ipfire-2-27-core-update-182-released)

- [Maestro core, written in Rust and partially compatible with Linux:](https://blog.lenot.re/a/introduction)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
