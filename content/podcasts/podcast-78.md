---
title: "Full Circle Weekly News 78"
date: 2018-01-28
draft: false
tags:
  - "core"
  - "google"
  - "recognition"
  - "rubyminer"
  - "speech"
  - "transmission"
  - "ubuntu"
  - "wine"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2078.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Core: A secure open source OS for IoT](http://internetofthingsagenda.techtarget.com/feature/Ubuntu-Core-A-secure-open-source-OS-for-IoT)

- [Google Moves Away from Ubuntu](https://www.theinquirer.net/inquirer/news/3024623/google-ditches-ubuntu-for-debian-from-internal-engineering-environment)

- [Improved Speech Recognition hopes for Ubuntu](https://hackaday.com/2018/01/17/speech-recognition-for-linux-gets-a-little-closer/)

- [RubyMiner](https://www.bleepingcomputer.com/news/security/linux-and-windows-servers-targeted-with-rubyminer-malware/)

- [Transmission Bug](https://www.theinquirer.net/inquirer/news/3024494/bittorrent-flaw-lets-hackers-take-control-of-windows-linux-pcs)

- [Wine 3.0](https://fossbytes.com/wine-3-0-released-features-download/)
