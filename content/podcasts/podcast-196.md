---
title: "Full Circle Weekly News 196"
date: 2021-01-15
draft: false
tags:
  - "apple"
  - "bleachbit"
  - "deepin"
  - "driver"
  - "extix"
  - "firefox"
  - "frameworks"
  - "games"
  - "heroic"
  - "kde"
  - "launcher"
  - "linux"
  - "m1"
  - "mint"
  - "nitrux"
  - "nvidia"
  - "paint"
  - "plasma"
  - "port"
  - "puppy"
  - "tux"
  - "ulyssa"
  - "update"
  - "upgrade"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20196.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Qt 5.15 Fixes No Longer Open](https://www.theregister.com/2021/01/05/qt_lts_goes_commercial_only/)

- [The Linux port for Apple's M1 Gets a Name](https://asahilinux.org/about/)

- [Firefox Gets a Visual Refresh](https://www.debugpoint.com/2021/01/firefox-proton/)

- [Linux Mint 20.1 Ulyssa and Upgrade Paths Out](https://blog.linuxmint.com/?p=4011)

- [ExTix Deepin 21.1 Out](http://www.extix.se/?p=772)

- [Puppy Linux 7.0 Out](http://distro.ibiblio.org/puppylinux/puppy-slacko-7.0/32/release-Slacko-7.0.htm)

- [Nitrux 1.3.6 Out](https://nxos.org/changelog/changelog-nitrux-1-3-6/)

- [Bleachbit 4.2.0 Out](https://www.bleachbit.org/news/bleachbit-420)

- [KDE Plasma 5.20.5 Out](https://kde.org/announcements/plasma/5/20.5/)

- [KDE's January App Updates Out](https://kde.org/announcements/releases/2021-01-apps-update/)

- [KDE Frameworks 5.78.0 Out](https://kde.org/announcements/kde-frameworks-5.78.0/)

- [Tux Paint 0.9.25 for Flatpak Out](http://www.tuxpaint.org/)

- [Nvidia Driver 460.32.03 Out](https://www.nvidia.com/Download/driverResults.aspx/168347/en-us)

- [Heroic Games Launcher 1.0 Out](https://github.com/flavioislima/HeroicGamesLauncher/releases/tag/v1.0.0)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
