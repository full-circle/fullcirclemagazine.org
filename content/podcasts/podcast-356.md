---
title: "Full Circle Weekly News 356"
date: 2024-03-10
draft: false
tags:
  - "tiny"
  - "core"
  - "kicad"
  - "tails"
  - "thesus"
  - "zentyal"
  - "rar"
  - "distrobox"
  - "vivaldi"
  - "browser"
  - "musl"
  - "grml"
  - "proxmox"
  - "menuetos"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20356.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Tiny Core Linux 15.0:](https://forum.tinycorelinux.net/index.php/topic,26861.0.html)

- [Release of CAD KiCad 8.0:](https://www.kicad.org/blog/2024/02/Version-8.0.0-Released/)

- [Tails 6.0, switched to Debian 12 and GNOME 43:](https://tails.net/news/version_6.0/index.en.html)

- [Theseus Ship 6.0:](https://subdiff.org/blog/2024/kwinft-becomes-theseus-ship)

- [Release of Zentyal 8.0:](https://zentyal.com/news/zentyal-8-0-changelog/)

- [Release of RAR 7.0:](https://www.rarlab.com/rarnew.htm)

- [The Open Collective Foundation has decided to close:](https://blog.opencollective.com/open-collective-official-statement-ocf-dissolution/)

- [Release of Distrobox 1.7:](https://github.com/89luca89/distrobox/releases/tag/1.7.0)

- [Release of Vivaldi 6.6:](https://www.askvg.com/vivaldi-new-web-browser/)

- [Release of /e/OS 1.20:](https://e.foundation/leaving-apple-google-welcome-e-os-1-20-and-the-improved-advanced-privacy/)

- [Release of Musl 1.2.5 standard C library:](https://www.openwall.com/lists/musl/2024/03/01/2)

- [Release of Live distribution Grml 2024.02:](https://blog.grml.org/archives/413-Grml-new-stable-release-2024.02-available.html)

- [Release of Proxmox Mail Gateway 8.1:](https://forum.proxmox.com/threads/proxmox-mail-gateway-8-1-available.142509/)

- [MenuetOS 1.50:](http://www.menuetos.net/index.htm)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
