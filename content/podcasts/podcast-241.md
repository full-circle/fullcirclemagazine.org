---
title: "Full Circle Weekly News 241"
date: 2021-12-25
draft: false
tags:
  - "cloud"
  - "download"
  - "hypervisor"
  - "intel"
  - "peertube"
  - "reactos"
  - "release"
  - "toaruos"
  - "ventoy"
  - "youtube"
  - "youtube-dl"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20241.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Ventoy 1.0.62](https://github.com/ventoy/Ventoy/releases/tag/v1.0.62)

- [ToaruOS 2.0 operating system released](https://github.com/klange/toaruos/releases/tag/v2.0.0)

- [Intel Moves Cloud Hypervisor Development to Linux Foundation](https://www.linuxfoundation.org/press-release/linux-foundation-to-host-the-cloud-hypervisor-project-creating-a-performant-lightweight-virtual-machine-monitor-for-modern-cloud-workloads/)

- [PeerTube 4.0 released](https://github.com/Chocobozzz/PeerTube/releases/tag/v4.0.0)

- [ReactOS 0.4.14 operating system released](https://reactos.org/project-news/reactos-0414-released/)

- [Release youtube-dl 2021.12.17](https://github.com/ytdl-org/youtube-dl/releases/tag/2021.12.17)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
