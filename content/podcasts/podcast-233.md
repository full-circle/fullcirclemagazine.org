---
title: "Full Circle Weekly News 233"
date: 2021-10-24
draft: false
tags:
  - "apache"
  - "bareflank"
  - "centos"
  - "commander"
  - "dash"
  - "desktop"
  - "discord"
  - "dock"
  - "doublefile"
  - "fosscord"
  - "gimp"
  - "gpl"
  - "hypervisor"
  - "kiosk"
  - "lawsuit"
  - "linux"
  - "manager"
  - "mx linux"
  - "openttd"
  - "porteus"
  - "rakudo"
  - "rancher"
  - "redcore"
  - "release"
  - "violation"
  - "vizio"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20233.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [First release of Fosscord's Discord-compatible communication platform](https://github.com/fosscord/fosscord-server/releases/tag/v0.0.3)

- [Apache Foundation moves away from mirrors in favor of CDN](https://blogs.apache.org/foundation/entry/apache-software-foundation-moves-to)

- [Rancher Desktop 0.6.0 Released(Including Linux support)](https://github.com/rancher-sandbox/rancher-desktop/releases/tag/v0.6.0)

- [Dash to Dock 70 released](https://micheleg.github.io/dash-to-dock/release/2021/10/17/new-release-v70-gnome-40-support.html)

- [Double Commander 1.0.0 File Manager Release](https://sourceforge.net/p/doublecmd/news/2021/10/double-commander-100-beta-released/)

- [Release of Porteus Kiosk 5.3.0](https://porteus-kiosk.org/news.html#211018)

- [Release of OpenTTD 12.0](https://www.openttd.org/news/2021/10/17/openttd-12-0)

- [CentOS Leader Announces Retirement From Governing Board](https://karan.org/posts/stepping-down/)

- [Vizio named in GPL violation lawsuit](https://sfconservancy.org/copyleft-compliance/vizio.html)

- [Redcore Linux 2102 released](https://redcorelinux.org/news/redcore-linux-hardened-2102-polaris-stable)

- [The fourth pre-release of GIMP 3.0](https://www.gimp.org/news/2021/10/20/gimp-2-99-8-released/)

- [Bareflank 3.0 hypervisor released](https://github.com/Bareflank/hypervisor/releases/tag/v3.0.0)

- [MX Linux 21](https://mxlinux.org/blog/mx-21-wildflower-released/)

- [Rakudo compiler release 2021.10](https://github.com/rakudo/rakudo/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
