---
title: "Full Circle Weekly News 164"
date: 2020-02-19
draft: false
tags:
  - "braveheart"
  - "centos"
  - "coreos"
  - "fedora"
  - "gnu"
  - "guile"
  - "kernel"
  - "linux"
  - "lite"
  - "mir"
  - "mozilla"
  - "pinephone"
  - "theme"
  - "torvalds"
  - "ubuntu"
  - "xfce"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20164.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Zorin Announces Zorin Grid](https://zoringroup.com/blog/2020/01/16/introducing-zorin-grid-manage-all-of-your-organizations-computers-as-easily-as-one/)

- [Xfce 4.16 Getting a Major UI Change](https://simon.shimmerproject.org/2020/01/14/xfce-4-14-maintenance-and-4-15-updates/)

- [Mozilla Lays Off About 70 Employees](https://techcrunch.com/2020/01/15/mozilla-lays-off-70-as-it-waits-for-subscription-products-to-generate-revenue/)

- [Ubuntu Theme Development for 20.04](https://ubuntu.com/blog/new-ubuntu-theme-in-development-for-20-04)

- [Fedora CoreOS Out of Preview](https://fedoramagazine.org/fedora-coreos-out-of-preview/)

- [PinePhone Braveheart Edition Ships](https://www.pine64.org/2020/01/15/pinephones-start-shipping-all-you-want-to-know/)

- [Linus Torvalds Releases Linux Kernel 5.5 rc7](https://lkml.org/lkml/2020/1/19/237)

- [GNU Guile 3.0.0 Released](https://www.gnu.org/software/guile/news/gnu-guile-300-released.html)

- [Linux Lite 4.8 Released](https://www.linuxliteos.com/forums/release-announcements/linux-lite-4-8-final-released/)

- [CentOS 8.1 Released](https://wiki.centos.org/Manuals/ReleaseNotes/CentOS8.1911)

- [Mir 1.7 Released](https://discourse.ubuntu.com/t/mir-1-7-0-release/14048)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
