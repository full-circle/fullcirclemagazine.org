---
title: "Full Circle Weekly News 270"
date: 2022-07-23
draft: false
tags:
  - "ubuntu"
  - "bacula"
  - "coreboot"
  - "debian"
  - "encryption"
  - "github"
  - "inspection"
  - "kde"
  - "kernel"
  - "libreboot"
  - "linux"
  - "mate"
  - "microsoft"
  - "ndpi"
  - "oracle"
  - "packet"
  - "pi"
  - "porteus"
  - "pottering"
  - "quantum"
  - "raspberry"
  - "rclone"
  - "red hat"
  - "redhat"
  - "slimbook"
  - "spacevm"
  - "toolkit"
  - "vim"
  - "wxwidgets"
  - "zabbix"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20270.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [SFC urges open source projects to stop using GitHub:](https://sfconservancy.org/blog/2022/jun/30/give-up-github-launch/)

- [Porteus 5.0 distribution released:](https://forum.porteus.org/viewtopic.php?f=35&t=10183)

- [Release of Zabbix 6.2:](https://www.zabbix.com/documentation/6.2/manual/introduction/whatsnew620)

- [The KDE project introduced their fourth generation of KDE Slimbooks:](https://kde.slimbook.es/)

- [Oracle Linux 9 and Unbreakable Enterprise Kernel 7 available:](https://blogs.oracle.com/linux/post/announcing-oracle-linux-9-general-availability)

- [NIST Approves Quantum Resistant Encryption Algorithms:](https://groups.google.com/a/list.nist.gov/g/pqc-forum/c/G0DoD7lkGPk)

- [Lennart Pottering left Red Hat and joined Microsoft:](https://www.phoronix.com/scan.php?page=news_item&px=Systemd-Creator-Microsoft)

- [Release of SpaceVim 2.0:](https://spacevim.org/SpaceVim-release-v2.0.0/)

- [Ubuntu MATE distribution has generated builds for the Raspberry Pi:](https://ubuntu-mate.community/t/ubuntu-mate-22-04-lts-for-raspberry-pi-is-out-now/25634)

- [wxWidgets 3.2.0 graphical toolkit:](https://wxwidgets.org/news/2022/07/wxwidgets-3.2.0-final-release/)

- [Bacula 13.0.0 Available:](https://www.bacula.org/bacula-release-13-0-0/)

- [Microsoft introduces a ban on the sale of open source software through the Microsoft Store:](https://sfconservancy.org/blog/2022/jul/07/microsoft-bans-commerical-open-source-in-app-store/)

- [nDPI 4.4 Deep Packet Inspection Released:](https://www.ntop.org/ndpi/introducing-ndpi-4-4-many-new-protocols-improvements-and-cybersecurity-features/)

- [Debian 11.4 update:](https://www.debian.org/News/2022/20220709)

- [rclone 1.59  released:](https://forum.rclone.org/t/rclone-1-59-0-release/31808)

- [Release of Libreboot 20220710, a completely free distribution of Coreboot:](https://libreboot.org/news/libreboot20220710.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
