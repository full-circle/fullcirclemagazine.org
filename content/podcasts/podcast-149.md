---
title: "Full Circle Weekly News 149"
date: 2019-10-15
draft: false
tags:
  - "16-04"
  - "18-04"
  - "autonomous"
  - "canonical"
  - "corporate"
  - "foundation"
  - "gnome"
  - "kernel"
  - "lawsuit"
  - "librem"
  - "live"
  - "oracle"
  - "os"
  - "parrot"
  - "patch"
  - "patent"
  - "phone"
  - "rothschild"
  - "server"
  - "ucs"
  - "univention"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20149.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Oracle Autonomous Linux Is The World's First Autonomous OS](https://www.oracle.com/corporate/pressrelease/oow19-oracle-autonomous-linux-091619.html)

- [Linux Kernel 5.3 Gets First Point Release](https://news.softpedia.com/news/linux-kernel-5-3-gets-first-point-release-it-s-now-ready-for-mass-deployments-527518.shtml)

- [Parrot 4.7 Released with Linux Kernel 5.2](https://news.softpedia.com/news/parrot-4-7-ethical-hacking-os-released-with-linux-kernel-5-2-mate-1-22-desktop-527520.shtml)

- [Canonical Releases New Kernel Live Patch for Ubuntu 18.04 and 16.04](https://news.softpedia.com/news/canonical-releases-new-kernel-live-patch-for-ubuntu-18-04-lts-and-16-04-lts-527536.shtml)

- [Zorin OS 15 Education Edition Officially Released](https://news.softpedia.com/news/zorin-os-15-education-edition-officially-released-based-on-ubuntu-18-04-lts-527562.shtml)

- [First Librem 5 Linux Phones Start Shipping to Customers Around the World](https://news.softpedia.com/news/first-librem-5-linux-phones-start-shipping-to-customers-around-the-world-527544.shtml)

- [GNOME Foundation Facing Lawsuit from Rothschild Patent Imaging](https://www.gnome.org/news/2019/09/gnome-foundation-facing-lawsuit-from-rothschild-patent-imaging/)

- [Univention Corporate Server Publishes Second Point Release for UCS 4.4](https://www.univention.com/blog-en/2019/09/ucs-4-4-2-second-point-release/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
