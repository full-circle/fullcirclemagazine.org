---
title: "Full Circle Weekly News 353"
date: 2024-02-18
draft: false
tags:
  - "kubuntu"
  - "kaos"
  - "mipi"
  - "cameras"
  - "chasquid"
  - "smtp"
  - "clamav"
  - "virtualbox"
  - "xfce"
  - "wayland"
  - "microsoft"
  - "sudo"
  - "word"
  - "mythtv"
  - "debian"
  - "arkime"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20353.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Kubuntu switches to Calamares installer:](https://kubuntu.org/news/kubuntu-council-meeting-30th-january-2024/)

- [Release of KaOS 2024.01:](https://kaosx.us/news/2024/kaos01/)

- [Fully open stack for MIPI cameras introduced:](https://hansdegoede.dreamwidth.org/28153.html)

- [Chasquid SMTP server 1.13 available:](https://groups.google.com/g/chasquid/c/ztlFgik2mPA)

- [ClamAV 1.3.0 fixed a dangerous vulnerability:](https://blog.clamav.net/2023/11/clamav-130-122-105-released.html)

- [VirtualBox to run on top of the KVM hypervisor:](https://cyberus-technology.de/articles/vbox-kvm-public-release)

- [Xfce Project Updates Plans for Wayland Support:](https://wiki.xfce.org/releng/wayland_roadmap)

- [Microsoft has published sudo for Windows. OpenBSD responded by creating Word:](https://devblogs.microsoft.com/commandline/introducing-sudo-for-windows/)

- [Release of MythTV 34:](https://www.mythtv.org/news/174/v34.0%2520Released)

- [Debian 12.5 and 11.9 update:](https://www.debian.org/News/2024/20240210)

- [Release of Arkime 5.0:](https://arkime.com/release-v5)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
