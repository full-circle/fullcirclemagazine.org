---
title: "Full Circle Weekly News 358"
date: 2024-03-24
draft: false
tags:
  - "kernel"
  - "tenv"
  - "openssh"
  - "openai"
  - "gtk"
  - "obs"
  - "studio"
  - "ntfs"
  - "driver"
  - "void"
  - "linux"
  - "mineclonia"
  - "minetest"
  - "tileos"
  - "rocky"
  - "oracle"
  - "suse"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20358.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux kernel 6.8 released:](https://lkml.org/lkml/2024/3/10/243)

- [Release of tenv 1.2.0:](https://github.com/tofuutils/tenv/releases/tag/v1.2.0)

- [Release of OpenSSH 9.7:](https://lists.mindrot.org/pipermail/openssh-unix-dev/2024-March/041264.html)

- [The OpenAI project has opened Transformer Debugger:](https://github.com/openai/transformer-debugger)

- [GTK 4.14 with new engines for OpenGL and Vulkan:](https://gitlab.gnome.org/GNOME/gtk/-/tags/4.14.0)

- [OBS Studio 30.1 released:](https://github.com/obsproject/obs-studio/releases/tag/30.1.0)

- [The old NTFS driver & Linux kernel 6.9:](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id%3D77417942e49017ff6d0b3d57b8974ab1d63d592c)

- [PDP-10 mainframe on Raspberry Pi 5:](https://obsolescence.dev/pidp10)

- [Updating Void Linux installation builds:](https://voidlinux.org/news/2024/03/new-images.html)

- [Update of Mineclonia 0.97:](https://content.minetest.net/packages/ryvnf/mineclonia/)

- [First release of TileOS:](https://tile-os.com/)

- [Release of Libadwaita 1.5:](https://blogs.gnome.org/alicem/2024/03/15/libadwaita-1-5/)

- [Rocky, Oracle and SUSE will provide further support for the Linux 4.14 kernel:](https://openela.org)

- [WebKitGTK 2.44.0 browser engine:](https://webkitgtk.org/2024/03/16/webkitgtk2.44.0-released.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
