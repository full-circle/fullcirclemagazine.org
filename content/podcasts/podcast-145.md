---
title: "Full Circle Weekly News 145"
date: 2019-09-11
draft: false
tags:
  - "backdoor"
  - "beta"
  - "board"
  - "buster"
  - "cutiepie"
  - "debian"
  - "desktop"
  - "gnome"
  - "knoppix"
  - "opensuse"
  - "pi"
  - "raspberry"
  - "servers"
  - "system76"
  - "tablet"
  - "unix"
  - "webmin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20145.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Knoppix 8.6 is Now Based on Debian 10 “Buster”](https://news.softpedia.com/news/knoppix-live-gnu-linux-system-is-now-based-on-debian-gnu-lin)

- [System76 Unveils Graphical Firmware Updater for All Debian-Based Linux Distros](https://news.softpedia.com/news/system76-unveils-graphical-firmware-updater-for-all-debian-ba)

- [A Raspberry Pi Based Open Source Tablet is in the Making and It’s Called CutiePie](https://itsfoss.com/cutiepi-open-source-tab/)

- [Gnome 3.34 Desktop Gets a Second Beta\*\* [https://news.softpedia.com/news/gnome-3-34-desktop-gets-a-second-beta-final-release-lands-s](https://news.softpedia.com/news/gnome-3-34-desktop-gets-a-second-beta-final-release-lands-s) eptember-11th-527078.shtml

- [Backdoor Found in Webmin, a Popular Web-based Utility for Managing Unix Servers](https://www.zdnet.com/article/backdoor-found-in-webmin-a-popular-web-based-utility-for-manag)

- [Changing the Chair of the openSUSE Board](https://news.opensuse.org/2019/08/19/changing-the-chair-of-the-opensuse-board/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
