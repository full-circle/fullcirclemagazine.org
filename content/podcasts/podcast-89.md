---
title: "Full Circle Weekly News 89"
date: 2018-04-16
draft: false
tags:
  - "16-04"
  - "18-04"
  - "beaver"
  - "beta"
  - "bionic"
  - "connect"
  - "gnome"
  - "kde"
  - "lts"
  - "nautilus"
  - "patches"
  - "system76"
  - "upgrade"
  - "zfs"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2089.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 18.04 LTS Bionic Beaver “Final Beta” Released](https://fossbytes.com/ubuntu-18-04-lts-bionic-beaver-final-beta-release-download-links/)

- [Should Ubuntu Linux Replace Alpha/Beta Release Model With “Testing Weeks”?](https://fossbytes.com/ubuntu-linux-replace-alpha-beta-release-model-test-weeks/)

- [System76 becomes GNOME Foundation Advisory Board member](https://betanews.com/2018/04/11/system76-gnome-foundation-advisory-board/)

- [KDE Connect has a Nautilus Connection](http://www.pro-linux.de/news/1/25784/kde-connect-erh%C3%83%C2%A4lt-nautilus-anbindung.html)

- [Update for Ubuntu 16.04 LTS patches security vulnerabilities](https://mybroadband.co.za/news/security/255213-update-for-ubuntu-16-04-lts-patches-security-vulnerabilities.html)

- [Disappearing data under ZFS on Linux sparks small, swift upgrade](https://www.theregister.co.uk/2018/04/10/zfs_on_linux_data_loss_fixed/)
