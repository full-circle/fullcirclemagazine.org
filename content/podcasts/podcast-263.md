---
title: "Full Circle Weekly News 263"
date: 2022-05-29
draft: false
tags:
  - "apache"
  - "beta"
  - "engine"
  - "enterprise"
  - "filter"
  - "freebsd"
  - "game"
  - "inkscape"
  - "iptables"
  - "kernel"
  - "leap"
  - "mesa"
  - "micro"
  - "mybee"
  - "network"
  - "networkmanager"
  - "openmeetings"
  - "opensuse"
  - "oracle"
  - "packet"
  - "qt"
  - "red hat"
  - "redhat"
  - "rocky"
  - "serious sam"
  - "truenas"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20263.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [NetworkManager 1.38.0 released:](https://networkmanager.dev/blog/networkmanager-1-38/)

- [iptables packet filter release 1.8.8:](https://www.mail-archive.com/netfilter-announce@lists.netfilter.org/msg00241.html)

- [Release of TrueNAS 13.0:](https://www.ixsystems.com/blog/meet-truenas-13-your-datas-new-home/)

- [Serious Sam Classic game engine updated for Linux:](https://github.com/tx00100xt/SeriousSamClassic)

- [Release of Rocky Linux 8.6:](https://rockylinux.org/news/rocky-linux-8-6-ga-release/)

- [Release of Inkscape 1.2:](https://inkscape.org/news/2022/05/16/inkscape-12/)

- [FreeBSD 13.1 release:](https://download.freebsd.org/ftp/releases/ISO-IMAGES/13.1/)

- [Qt Company's CTO and Qt Lead Maintainer leave the project:](https://lists.qt-project.org/pipermail/development/2022-May/042477.html)

- [First release of the openSUSE Leap Micro distribution:](https://lists.opensuse.org/archives/list/project@lists.opensuse.org/thread/Q7Q54ZGTLFHRAHD43I7KWQ2CXKNHM5OB/)

- [Red Hat Enterprise Linux 9 Available for Download:](https://access.redhat.com/announcements/6958409)

- [Oracle Linux 8.6 Release and Unbreakable Enterprise Kernel 7 Beta:](https://blogs.oracle.com/linux/post/announcing-the-release-of-oracle-linux-8-update-6)

- [Release of Mesa 22.1:](https://lists.freedesktop.org/archives/mesa-dev/2022-May/225791.html)

- [MyBee 13.1.0, has been published:](https://myb.convectix.com/)

- [Apache OpenMeetings 6.3 web conferencing server available:](https://blogs.apache.org/openmeetings/entry/openmeetings-v6-3-0-fixes)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
