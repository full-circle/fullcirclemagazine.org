---
title: "Full Circle Weekly News 387"
date: 2024-10-13
draft: false
tags:
  - "qbittorrent"
  - "torrent"
  - "ffmpeg"
  - "phosh"
  - "apple"
  - "macos"
  - "oracle"
  - "nitrux"
  - "manjaro"
  - "openwrt"
  - "ardour"
  - "samsung"
  - "tizen"
  - "doglinux"
  - "firmware"
  - "cage"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20387.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [qBittorrent 5.0 Released:](https://www.qbittorrent.org/news.php)

- [FFmpeg 7.1 Released:](https://ffmpeg.org/index.html%23news)

- [Phosh 0.42.0 released:](https://phosh.mobi/releases/rel-0.42.0/)

- [Apple has published the open source components used in macOS 15:](https://opensource.apple.com/releases/)

- [Oracle Releases Unbreakable Enterprise Kernel R7U3:](https://blogs.oracle.com/linux/post/unbreakable-enterprise-kernel-release-7-update-3-delivers-enhanced-performance-and-security)

- [Nitrux 3.7.0 release:](https://nxos.org/changelog/release-announcement-nitrux-3-7-0/)

- [Release of Manjaro Linux 24.1:](https://forum.manjaro.org/t/manjaro-24-1-xahea-released/168699)

- [OpenWrt One Router:](https://banana-pi.org/en/product-news/557.html)

- [Release of Ardour 8.8:](https://github.com/Ardour/ardour/releases/tag/8.8)

- [Samsung Adapts Tizen Mobile Platform to RISC-V Architecture:](https://www.sifive.com/blog/samsung-highlights-work-to-bring-risc-v-to-tizen-)

- [DogLinux build to check hardware:](https://gumanzoy.blogspot.com/2024/10/20241004-doglinux.html)

- [Qmmp Music Player:](http://qmmp.ylsoftware.com/index.php)

- [The fwupd 2.0.0 firmware tool is now available:](https://blogs.gnome.org/hughsie/2024/10/04/fwupd-2-0-0/)

- [Mitmproxy 11 HTTPS analyzer releases with HTTP/3 support:](https://mitmproxy.org/posts/releases/mitmproxy-11/)

- [Release of Cage 0.2:](https://github.com/cage-kiosk/cage/releases/tag/v0.2.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
