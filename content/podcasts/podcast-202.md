---
title: "Full Circle Weekly News 202"
date: 2021-03-16
draft: false
tags:
  - "canonical"
  - "coreboot"
  - "doss"
  - "elementaryos"
  - "firefox"
  - "flutter"
  - "kde"
  - "kernel"
  - "linux-mint"
  - "mesa"
  - "mint"
  - "onlyoffice"
  - "plasma"
  - "root"
  - "star-labs"
  - "steam"
cover: "covers/podcasts/fallback.webp"
mp3: https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20202.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical Chooses Flutter](https://twitter.com/ubuntu/status/1367063203600031746?s=20)

- [Linux Mint to Make System Updates a Priority](https://blog.linuxmint.com/?p=4037)

- [Steam Link Now Available on Linux](https://steamcommunity.com/app/353380/discussions/10/3106892760562833187/)

- [ElementaryOS 6 Changing Some of the Look and Feel](https://blog.elementary.io/look-and-feel-changes-elementary-os-6/)

- [Root Access and Denial of Service Flaws Found and Fixed in Kernel 5.11](https://www.zdnet.com/article/linux-network-security-holes-found-fixed/)

- [Kernel 5.12 Testing Off to a Rough Start](https://lore.kernel.org/lkml/CAHk-=whH-stL2zLCf02HZaOeQgS4oGa7eEiHeYZGj-orK-PX0g@mail.gmail.com/T/#u)

- [ONLYOFFICE Docs 6.2 Out](https://www.onlyoffice.com/blog/2021/03/onlyoffice-docs-v6-2-with-data-validation-and-table-of-figures-released/)

- [Firefox 86.0.1 Out](https://www.mozilla.org/en-US/firefox/86.0.1/releasenotes/)

- [KDE March Apps Update Out](https://kde.org/announcements/releases/2020-03-apps-update/)

- [Mesa 21 Out](https://docs.mesa3d.org/relnotes/21.0.0.html)

- [Star Labs Announces Coreboot Support for the Mk III and Mk IV](https://support.starlabs.systems/kb/guides/installing-coreboot)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
