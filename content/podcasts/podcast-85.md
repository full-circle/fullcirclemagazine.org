---
title: "Full Circle Weekly News 85"
date: 2018-03-19
draft: false
tags:
  - "18-04"
  - "9-4"
  - "amd"
  - "beaver"
  - "bionic"
  - "compression"
  - "debian"
  - "kde"
  - "kodi"
  - "krypton"
  - "libreelec"
  - "neptune"
  - "plasma"
  - "rolling"
  - "sparkylinux"
  - "stretch"
  - "torvalds"
  - "ubuntu"
  - "vulnerabilities"
  - "zstd"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2085.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 18.04 LTS “Bionic Beaver” Beta 1 Released For Opt-In Flavors](https://fossbytes.com/ubuntu-18-04-beta-1-bionic-beaver-release-download-features/)

- [New Ubuntu Installs Could Be Speed Up by 10% with the Zstd Compression Algorithm](http://news.softpedia.com/news/new-ubuntu-installs-could-be-speed-up-by-10-with-the-zstd-compression-algorithm-520177.shtml)

- [Debian 9.4 Stretch GNU/Linux Released With 150+ Fixes](https://fossbytes.com/debian-9-4-stretch-download-features-update/)

- [SparkyLinux 5.3 Rolling Linux OS Debuts Based on Debian GNU/Linux 10 "Buster"](http://news.softpedia.com/news/sparkylinux-5-3-rolling-linux-os-debuts-based-on-debian-gnu-linux-10-buster-520185.shtml)

- [Neptune 5.0 Linux OS Released with KDE Plasma 5.12 LTS, Based on Debian Stretch](http://news.softpedia.com/news/neptune-5-0-linux-os-released-with-kde-plasma-5-12-lts-based-on-debian-stretch-520215.shtml)

- [LibreELEC (Krypton) 8.2.4 Kodi-focused Linux distro is ready for Raspberry Pi 3 Model B+](https://betanews.com/2018/03/14/libreelec-krypton-824-kodi-linux/)

- [Linux Beats Windows To Become The Most Popular Development Platform: Stack Overflow Survey 2018](https://fossbytes.com/linux-most-preferred-development-platform/)

- [Linus Torvalds Roasts CTS Labs After They Exposed AMD Chip Vulnerabilities](https://fossbytes.com/linus-torvalds-roasts-cts-labs-amd-cpu-vulnerabilty-report/)
