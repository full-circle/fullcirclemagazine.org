---
title: "Full Circle Weekly News 41"
date: 2016-11-05
draft: false
tags:
  - "4-9"
  - "ai"
  - "bug"
  - "canonical"
  - "core"
  - "encryption"
  - "fakefile"
  - "google"
  - "kernel"
  - "linux"
  - "live"
  - "patch"
  - "patching"
  - "rc3"
  - "snappy"
  - "torvalds"
  - "trojan"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2041.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Core 16 released](http://www.theinquirer.net/inquirer/news/2476362/ubuntu-core-16-coming-to-a-raspberry-pi-near-you)

- [Canonical announces live kernel patching for Ubuntu](http://news.softpedia.com/news/canonical-patches-ancient-dirty-cow-kernel-bug-in-all-supported-ubuntu-oses-509507.shtml)

- [Linux Kernels Patched Against "Dirty COW" bug](http://news.softpedia.com/news/linux-kernels-3-16-38-3-12-66-3-10-104-and-3-2-83-patched-against-dirty-cow-509537.shtml)

- [FakeFile Trojan Opens Backdoors on Linux Computers](http://news.softpedia.com/news/fakefile-trojan-opens-backdoors-on-linux-computers-except-opensuse-509526.shtml)

- [Linus Torvalds Announces Linux Kernel 4.9 RC3](http://news.softpedia.com/news/linus-torvalds-announces-linux-kernel-4-9-rc3-things-are-getting-bigger-509791.shtml)

- [How Google's AI taught itself to create its own encryption](http://www.wired.co.uk/article/google-artificial-intelligence-encryption)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
