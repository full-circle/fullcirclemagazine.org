---
title: "Full Circle Weekly News 314"
date: 2023-05-21
draft: false
tags:
  - "llvm"
  - "mojo"
  - "systemd"
  - "alpine"
  - "cadzinho"
  - "opentoonz"
  - "thunderbird"
  - "lighttpd"
  - "red hat"
  - "redhat"
  - "almalinux"
  - "mesa"
  - "simply"
  - "synthstrom"
  - "audible"
  - "shepherd"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20314.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Creator of LLVM develops a new programming language Mojo:](https://www.modular.com/blog/a-unified-extensible-platform-to-superpower-your-ai)

- [Lennart Pottering offered to add a soft reboot mode to systemd:](https://mastodon.social/@pid_eins/110272799283345055)

- [Release of Alpine Linux 3.18:](https://alpinelinux.org/posts/Alpine-3.18.0-released.html)

- [Free 2D CAD CadZinho 0.3:](https://github.com/zecruel/CadZinho/releases/tag/0.3.0)

- [Release OpenToonz 1.7:](https://github.com/opentoonz/opentoonz/releases/tag/v1.7.0)

- [Thunderbird releases 2022 financials:](https://blog.thunderbird.net/2023/05/thunderbird-is-thriving-our-2022-financial-report/)

- [Release of Lighttpd 1.4.70:](https://blog.lighttpd.net/articles/2023/05/10/lighttpd-1.4.70-released/)

- [Release of Red Hat Enterprise Linux 9.2:](https://www.redhat.com/en/about/press-releases/red-hat-delivers-latest-releases-red-hat-enterprise-linux)

- [AlmaLinux 9.2:](https://almalinux.org/blog/almalinux-92-now-available/)

- [Release of Mesa 23.1:](https://lists.freedesktop.org/archives/mesa-dev/2023-May/226007.html)

- [Simply Linux RV:](https://lists.altlinux.org/pipermail/riscv-devel/2023-May/000038.html)

- [Synthstrom Audible will open the firmware code of Deluge:](https://synthstrom.com/open/)

- [Release of GNU Shepherd 0.10:](https://www.mail-archive.com/info-gnu@gnu.org/msg03181.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
