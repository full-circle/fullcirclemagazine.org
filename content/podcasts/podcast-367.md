---
title: "Full Circle Weekly News 367"
date: 2024-05-26
draft: false
tags:
  - "linux"
  - "kernel"
  - "viola"
  - "debian"
  - "keepassxc"
  - "rescuezilla"
  - "nvidia"
  - "manjaro"
  - "aldos"
  - "fedora"
  - "endless"
  - "oracle"
  - "gnome"
  - "winamp"
  - "neovim"
  - "kde"
  - "opensuse"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20367.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux kernel 6.9 release:](https://lkml.org/lkml/2024/5/12/224)

- [Viola Workstation K 10.3:](https://lists.altlinux.org/pipermail/altlinux-announce-ru/2024/000045.html)

- [Debian switches to a stripped-down version of the KeePassXC:](https://fosstodon.org/@keepassxc/112417353193348720)

- [Rescuezilla 2.5:](https://github.com/rescuezilla/rescuezilla/releases/tag/2.5)

- [An NVIDIA representative answered questions related to the transfer of drivers to open kernel modules:](https://forums.developer.nvidia.com/t/clarifying-560-series-drivers-open-sourcedness-vs-kernel-module-type-proprietary-CFD0C5CECEC5D4/292698/2)

- [Release of Manjaro Linux 24.0:](https://forum.manjaro.org/t/manjaro-24-0-wynsdey-released/161527)

- [ALDOS a variant of Fedora without systemd:](https://www.reddit.com/r/linux/comments/1cs8xr7/aldos_fedora_without_systemd/)

- [Release of Endless OS 6.0:](https://www.endlessos.org/post/getting-started-with-endless-os-6)

- [Release of Oracle Linux 9.4:](https://blogs.oracle.com/linux/post/oracle-linux-9-update-4-is-generally-available)

- [GNOME OS switches to atomic updates using systemd-sysupdate:](https://www.codethink.co.uk/articles/2024/GNOME-OS-systemd-sysupdate/)

- [An SSH backdoor installed during the kernel.org hack remained undetected for two years:](https://www.welivesecurity.com/en/eset-research/ebury-alive-unseen-400k-linux-servers-compromised-cryptotheft-financial-gain/)

- [Opening of Winamp code announced:](https://about.winamp.com/press/article/winamp-open-source-code)

- [Release of Neovim 0.10:](https://gpanders.com/blog/whats-new-in-neovim-0.10/)

- [Release 7-Zip 24.05:](https://sourceforge.net/p/sevenzip/discussion/45797/thread/b92679e642/)

- [Changes to improve the display of KDE applications in GNOME and Xfce:](https://pointieststick.com/2024/05/17/this-week-in-kde-all-about-those-apps)

- [The openSUSE project has published the Agama 8 installer, freed from binding to Cockpit:](https://yast.opensuse.org/blog/2024-05-17/agama-8)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
