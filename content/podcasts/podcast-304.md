---
title: "Full Circle Weekly News 304"
date: 2023-03-12
draft: false
tags:
  - "ubuntu"
  - "mini"
  - "kde"
  - "plasma"
  - "apache"
  - "openoffice"
  - "armbian"
  - "ffmpeg"
  - "gimp"
  - "debian"
  - "firmware"
  - "openra"
  - "godot"
  - "scummvm"
  - "gnome"
  - "gtk4"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20304.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu mini installation:](https://lists.ubuntu.com/archives/ubuntu-devel/2023-February/042485.html)

- [Release EasyOS 5.0:](https://bkhome.org/news/202302/easyos-kirkstone-series-version-50.html)

- [KDE Plasma switches to Qt 6:](https://mail.kde.org/pipermail/kde-devel/2023-February/001699.html)

- [Release of Apache OpenOffice 4.1.14:](https://blogs.apache.org/OOo/entry/announcing-apache-openoffice-4-110)

- [Release of Armbian 23.02:](https://www.armbian.com/newsflash/armbian-23-02/)

- [Release of FFmpeg 6.0:](http://ffmpeg.org/download.html#releases)

- [GIMP 2.10.34:](https://www.gimp.org/news/2023/02/27/gimp-2-10-34-released/)

- [Debian 12 launched a separate repository with firmware:](https://lists.debian.org/debian-devel/2023/02/msg00335.html)

- [Release of OpenRA 20230225:](http://www.openra.net/news/release-20230225/)

- [Release of Godot 4.0:](https://godotengine.org/article/godot-4-0-sets-sail/)

- [Release of ScummVM 2.7.0:](https://www.scummvm.org/news/20230226/)

- [GNOME Shell and Mutter have completed the transition to GTK4:](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/2864)

- [GTK 4.10 is available:](https://gitlab.gnome.org/GNOME/gtk/-/tags)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
