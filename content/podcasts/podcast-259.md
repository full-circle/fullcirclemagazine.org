---
title: "Full Circle Weekly News 259"
date: 2022-05-01
draft: false
tags:
  - "22.04"
  - "box64"
  - "box86"
  - "celestial"
  - "core"
  - "coreutils"
  - "debian"
  - "emulation"
  - "gear"
  - "git"
  - "gnu"
  - "kde"
  - "lxqt"
  - "openbsd"
  - "openwrt"
  - "ovirt"
  - "ppa"
  - "proton"
  - "qemu"
  - "rsync"
  - "sdl"
  - "silero"
  - "snap"
  - "ubuntu"
  - "valve"
  - "wayland"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20259.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of the GNU Coreutils 9.1 set of core system utilities:](https://savannah.gnu.org/forum/forum.php?forum_id=10158)

- [LXQt 1.1 User Environment Released:](https://lxqt-project.org/release/2022/04/15/lxqt-1-1-0/)

- [Rsync 3.2.4 Released:](https://lists.samba.org/archive/rsync-announce/2022/000110.html)

- [Celestial shuns snaps:](https://www.reddit.com/r/Ubuntu/comments/u4jqt5/ubuntu_with_flatpaks_without_the_snaps_celestial/)

- [The SDL developers have canceled the default Wayland switch in the 2.0.22 release:](https://discourse.libsdl.org/t/sdl-revert-video-prefer-wayland-over-x11/35376)

- [New versions of Box86 and Box64 emulators that allow you to run x86 games on ARM systems:](https://github.com/ptitSeb/box64)(https://github.com/ptitSeb/box86)

- [Release of the QEMU 7.0 emulator:](https://lists.nongnu.org/archive/html/qemu-devel/2022-04/msg02245.html)

- [PPA proposed for Ubuntu to improve Wayland support in Qt:](https://launchpad.net/~ci-train-ppa-service/+archive/ubuntu/4829)

- [Movement to include proprietary firmware in the Debian distribution:](https://blog.einval.com/2022/04/19)

- [Git 2.36 source control released:](https://lore.kernel.org/all/xmqqh76qz791.fsf@gitster.g/)

- [oVirt 4.5.0 Virtualization Infrastructure Management System Release:](https://blogs.ovirt.org/2022/04/ovirt-4-5-0-is-now-generally-available/)

- [New versions of OpenWrt 21.02.3 and 19.07.10:](https://lists.infradead.org/pipermail/openwrt-devel/2022-April/038491.html)

- [Ubuntu 22.04 LTS distribution release:](https://discourse.ubuntu.com/t/jammy-jellyfish-release-notes/24668)

- [Valve has released Proton 7.0-2, for running Windows games on Linux:](https://github.com/ValveSoftware/Proton/releases/tag/proton-7.0-2)

- [Release of OpenBSD 7.1:](https://www.mail-archive.com/announce@openbsd.org/msg00429.html)

- [Summary ofresults of the election of the leader of the Debian project:](https://lists.debian.org/debian-devel-announce/2022/04/msg00007.html)

- [New release of the Silero speech synthesis system:](https://github.com/snakers4/silero-models#text-to-speech)

- [Release of KDE Gear 22.04:](https://kde.org/info/releases-22.04.0.php)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
