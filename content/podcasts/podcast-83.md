---
title: "Full Circle Weekly News 83"
date: 2018-03-05
draft: false
tags:
  - "18-04"
  - "antsie"
  - "beaver"
  - "bionic"
  - "calamares"
  - "chrome"
  - "exploit"
  - "freebsd"
  - "hackers"
  - "intel"
  - "kde"
  - "libre"
  - "libreoffice"
  - "meltdown"
  - "miner"
  - "office"
  - "openbsd"
  - "os"
  - "plasma"
  - "purism"
  - "rtorrent"
  - "unix"
  - "vm"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2083.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Soon You’ll Be Able To Run Linux Apps In VM On Chrome OS](https://fossbytes.com/linux-apps-in-vm-chrome-os/)

- [Ubuntu Adds New “Minimal Installation” Option For Fewer Preinstalled Packages](https://fossbytes.com/ubuntu-new-minimal-installation/)

- [Ubuntu 18.04 LTS Bionic Beaver](https://fossbytes.com/ubuntu-18-04-bionic-beaver-release-date-features/)

- [Calamares 3.2 Linux Installer Will Integrate a Module for the KDE Plasma Desktop](http://news.softpedia.com/news/calamares-3-2-linux-installer-will-integrate-a-module-for-the-kde-plasma-desktop-519977.shtml)

- [Purism Now Sells the Most Secure Linux Laptops with Heads Integrated TPM Chips](http://news.softpedia.com/news/purism-now-sells-the-most-secure-linux-laptops-with-heads-integrated-tpm-chips-519976.shtml)

- [LibreOffice 6.0 Open-Source Office Suite Passes 1 Million Downloads Mark](http://news.softpedia.com/news/libreoffice-6-0-open-source-office-suite-passes-1-million-downloads-mark-519831.shtml)

- [Meet the Antsle: The perfect out-of-the box virtual machine solution](https://www.techrepublic.com/article/meet-the-antsle-the-perfect-out-of-the-box-virtual-machine-solution/)

- [Hackers exploiting rTorrent to install Unix coin miner have netted $4k so far](https://arstechnica.com/information-technology/2018/03/hackers-exploiting-rtorrent-to-install-unix-coin-miner-have-netted-4k-so-far/)

- [Hackers Take Control of Tesla’s Cloud Account To Mine Cryptocurrency](https://fossbytes.com/hackers-control-tesla-cloud-account-mine-cryptocurrency/)

- [Intel Returns With Another Spectre Patch For 6th, 7th, 8th Gen Core Processors](https://fossbytes.com/spectre-patch-intel-core-chips-skylake-kaby-lake-coffe-lake/)

- [Spectre and Meltdown Mitigations Now Available for FreeBSD and OpenBSD Systems](http://news.softpedia.com/news/spectre-and-meltdown-mitigations-now-available-for-freebsd-and-openbsd-systems-519919.shtml)

- [Canonical Outs New Ubuntu Kernel Update with Compiler-Based Retpoline Mitigation](http://news.softpedia.com/news/canonical-outs-new-ubuntu-kernel-update-with-compiler-based-retpoline-mitigation-519909.shtml)
