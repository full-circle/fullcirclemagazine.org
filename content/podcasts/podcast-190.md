---
title: "Full Circle Weekly News 190"
date: 2020-11-15
draft: false
tags:
  - "400"
  - "amazon"
  - "aws"
  - "collabora"
  - "debian"
  - "dell"
  - "docker"
  - "drivers"
  - "edition"
  - "elementary"
  - "emmabuntus"
  - "kde"
  - "kernel"
  - "librem-mini"
  - "linux"
  - "lxqt"
  - "nitrux"
  - "odin"
  - "office"
  - "os"
  - "pi"
  - "purism"
  - "raspberry"
  - "sparky"
  - "updates"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20190.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [AWS Creates Its Own Docker Images](https://www.zdnet.com/article/aws-preps-its-own-library-of-public-docker-container-images/)

- [Dell Adds Privacy Drivers to the Kernel](https://www.debugpoint.com/2020/11/dell-privacy-driver-linux/)

- [Elementary OS Is Making Progress with Dark Mode in Odin](https://blog.elementary.io/dark-style-progress/)

- [Raspberry Pi 400's New Form Factor](https://www.raspberrypi.org/blog/raspberry-pi-400-the-70-desktop-pc/)

- [KDE Announces Updates and Updates](https://kde.org/announcements/releases/2020-11-apps-update/)

- [Nitrux 1.3.4 Out](https://nxos.org/changelog/changelog-nitrux-1-3-4/)

- [Emmabuntus Debian Edition 1.03 Outinstallation features](https://emmabuntus.org/on-november-2020-emmade3-1-03-focuses-on-the-reuse-for-all/)

- [Sparky Linux 5.13 Out](https://sparkylinux.org/sparky-5-13/)

- [LXQT 0.16 Out](https://github.com/lxqt/lxqt/releases/tag/0.16.0)

- [Collabora Office 6.4 Out](https://www/)

- [Second Gen Librem Mini from Purism Out](https://9to5linux.com/purism-launches-2nd-gen-librem-mini-linux-pc-with-a-10th-gen-intel-core-cpu)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
