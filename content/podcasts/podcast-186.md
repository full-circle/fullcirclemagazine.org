---
title: "Full Circle Weekly News 186"
date: 2020-10-18
draft: false
tags:
  - "20-1"
  - "20-10"
  - "apps"
  - "beta"
  - "calibre"
  - "fedora"
  - "firefox"
  - "gui"
  - "kaos"
  - "microsoft"
  - "mint"
  - "nitrux"
  - "tails"
  - "ubuntu"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20186.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux GUI Apps Coming to Windows](https://www.zdnet.com/article/linux-graphical-apps-coming-to-windows-subsystem-for-linux/)

- [Linux Mint 20.1 Will Arrive Mid-December](https://blog.linuxmint.com/?p=3969)

- [Ubuntu 20.10, Groovy Gorilla \](https://9to5linux.com/ubuntu-20-10-beta-is-now-available-for-download)

- [Fedora 33 Beta Out](https://fedoramagazine.org/announcing-the-release-of-fedora-33-beta/)

- [Tails 4.11 Out](https://tails.boum.org/news/version_4.11/index.en.html)

- [Nitrux 1.3.3 Out](https://nxos.org/changelog/changelog-nitrux-1-3-3/)

- [Firefox 81.0.1 Out](https://www.mozilla.org/en-US/firefox/81.0.1/releasenotes/)

- [Calibre 5.0 Out](https://calibre-ebook.com/new-in/fourteen)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
