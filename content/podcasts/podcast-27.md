---
title: "Full Circle Weekly News 27"
date: 2016-07-23
draft: false
tags:
  - "10"
  - "ai"
  - "android"
  - "backdoor"
  - "banks"
  - "bash"
  - "desktop"
  - "dev"
  - "flaw"
  - "shell"
  - "trojan"
  - "unity"
  - "vbulletin"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2027.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu's Unity desktop shows the limits of Windows 10's Bash shell](http://www.pcworld.com/article/3094422/windows/ubuntus-unity-desktop-shows-the-limits-of-windows-10s-bash-shell.html)

- [This Android Trojan blocks victims from alerting banks](http://www.pcworld.com/article/3095965/security/this-android-trojan-blocks-the-victim-from-alerting-banks.html)

- [Flaw in vBulletin add-on leads to Ubuntu Forums database breach](http://www.pcworld.com/article/3095902/security/flaw-in-vbulletin-add-on-leads-to-ubuntu-forums-database-breach.html)

- [Microsoft silently kills dev backdoor that boots Linux on locked-down Windows RT tablets](http://www.theregister.co.uk/2016/07/15/windows_fix_closes_rt_unlock_loophole/)

- [You Could Soon Run Windows Apps on Android and Chrome OS](http://gadgets.ndtv.com/apps/news/you-could-soon-run-windows-apps-on-android-and-chrome-os-862267)

- [Russia on Verge of Major Breakthrough in Artificial Intelligence](http://sputniknews.com/science/20160719/1043305617/artificial-intelligence-breakthrough.html)

- [Google cut its electricity bill by 40pc using artificial intelligence](http://www.telegraph.co.uk/technology/2016/07/20/google-cut-its-electricity-bill-by-40pc-using-artificial-intelli/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
