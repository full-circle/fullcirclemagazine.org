---
title: "Full Circle Weekly News 102"
date: 2018-08-05
draft: false
tags:
  - "16-04"
  - "18-04"
  - "boot"
  - "canonical"
  - "chip"
  - "cinnamon"
  - "cpu"
  - "debian"
  - "fallout"
  - "india"
  - "linux"
  - "lts"
  - "mint"
  - "neptune"
  - "processor"
  - "risc-v"
  - "shakti"
  - "slax"
  - "usb"
  - "yaru"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20102.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Mint developers planning big Cinnamon 4.0 improvements](https://betanews.com/2018/07/20/linux-mint-cinnamon-4/)

- [Latest Neptune OS 5.4 brings many application improvements and bugfixed](https://appuals.com/latest-neptune-os-5-4-brings-many-application-improvements-and-bugfixes/)

- [Debian-Based Slax 9.5.0 Released, Now Available on Hardware-Encrypted USB Keys](https://news.softpedia.com/news/debian-based-slax-9-5-0-released-with-usb-key-with-hardware-based-aes-encryption-522072.shtml)

- [Everything About Ubuntu’s New Default Theme “Yaru”](https://fossbytes.com/ubuntu-yaru-theme-new-try/)

- [Canonical Fixes Boot Failures on Ubuntu 18.04 LTS and 16.04 LTS, Update Now](https://news.softpedia.com/news/canonical-fixes-boot-failures-on-ubuntu-18-04-lts-and-16-04-4-lts-update-now-522073.shtml)

- [Linux 4.18-RC6 Brings Network and Driver Fixes Including 32-Bit VM Fallout Fix](https://appuals.com/linux-4-18-rc6-brings-network-and-driver-fixes-including-32-bit-vm-fallout-fix/)

- [India's first RISC-V based Chip is Here: Linux boots on Shakti processor!](http://www.geekdave.in/2018/07/indias-first-risc-v-is-here-linux-boots.html?m=1)
