---
title: "Full Circle Weekly News 312"
date: 2023-05-07
draft: false
tags:
  - "kernel"
  - "audacity"
  - "barsoom"
  - "opera"
  - "browser"
  - "solus"
  - "budgie"
  - "cudatext"
  - "debian"
  - "sculpt"
  - "fedora"
  - "onyx"
  - "nitrux"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20312.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"

---

- [Linux kernel 6.3 release:](https://lkml.org/lkml/2023/4/23/284)

- [Audacity 3.3:](https://github.com/audacity/audacity/releases/tag/Audacity-3.3.0)

- [Victory for Barsoom:](https://lists.debian.org/debian-devel-announce/2023/04/msg00005.html)

- [Opera One, replacing the current browser:](https://press.opera.com/2023/04/25/opera-one-developer/)

- [Solus 5 coming full circle:](https://getsol.us/2023/04/18/a-new-voyage/)

- [Update of Budgie 10.7.2:](https://blog.buddiesofbudgie.org/budgie-10-7-2/)

- [Update of CudaText:](https://cudatext.github.io/download.html)

- [Update of Debian 11.7:](https://www.debian.org/News/2023/20230429)

- [Sculpt OS 23.04:](https://genode.org/news/sculpt-os-release-23.04)

- [Fedora Onyx:](https://fedoraproject.org/wiki/Changes/Fedora_Onyx)

- [Release of the Nitrux 2.8:](https://nxos.org/changelog/release-announcement-nitrux-2-8-0/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
