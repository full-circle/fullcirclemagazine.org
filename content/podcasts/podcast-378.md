---
title: "Full Circle Weekly News 378"
date: 2024-08-11
draft: false
tags:
  - "oflinks"
  - "sysvinit"
  - "firmware"
  - "ubuntu"
  - "touch"
  - "grub2"
  - "funtoo"
  - "arm"
  - "azure"
  - "amarok"
  - "manjaro"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20378.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release ofLinks 2.30:](http://links.twibright.com/download.php)

- [Release of SysVinit 3.10:](https://lists.nongnu.org/archive/html/sysvinit-devel/2024-07/msg00016.html)

- [Firmware release for Ubuntu Touch OTA-5 Focal:](https://ubports.com/en/blog/ubports-news-1/post/ubuntu-touch-ota-5-focal-release-3933)

- [Super Grub2 Disk 2.06s4, published:](https://www.supergrubdisk.org/2024/07/30/super-grub2-disk-2-06s4-released/)

- [Funtoo Linux going to the big server in the sky:](https://forums.funtoo.org/topic/5182-all-good-things-must-come-to-an-end/)

- [Plan to end support for older ARM processors in the Linux kernel:](https://lkml.org/lkml/2024/7/31/1242)

- [Azure Linux 3.0:](https://github.com/microsoft/azurelinux/releases/tag/3.0.20240727-3.0?changelog_tagCFD0C5CECEC5D4)

- [Amarok music player 3.1.0:](https://blogs.kde.org/2024/08/02/amarok-3.1-tricks-of-the-light-released/)

- [Open homomorphic encryption library:](https://www.swift.org/blog/announcing-swift-homomorphic-encryption/)

- [Manjaro Linux atomically updated version:](https://manjaro.org/news/2024/manjaro-immutable-testing)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
