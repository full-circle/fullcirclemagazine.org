---
title: "Full Circle Weekly News 354"
date: 2024-02-25
draft: false
tags:
  - "ubuntu"
  - "touch"
  - "ubports"
  - "freebsd"
  - "regolith"
  - "vmware"
  - "vsphere"
  - "openvpn"
  - "ghostbsd"
  - "freenginx"
  - "mixxx"
  - "opensuse"
  - "denoise"
  - "duckdb"
  - "ugrep"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20354.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [A new model for generating Ubuntu Touch releases:](https://ubports.com/en/blog/ubports-news-1/post/announcement-of-release-model-changes-3920)

- [FreeBSD will stop supporting 32-bit:](https://lists.freebsd.org/archives/freebsd-announce/2024-February/000117.html)

- [Regolith Desktop Environment 3.1:](https://www.freelists.org/post/regolith-linux/Regolith-Desktop-31-release-announcement)

- [Free versions of VMware vSphere Hypervisor has ceased:](https://kb.vmware.com/s/article/2107518)

- [OpenVPN 2.6.9 update with license change:](https://github.com/OpenVPN/openvpn/releases/tag/v2.6.9)

- [GhostBSD Release 24.01.1:](https://www.ghostbsd.org/news/GhostBSD_24.01.1_Now_Available)

- [POC for uninstalled application handler in Ubuntu:](https://www.aquasec.com/blog/snap-trap-the-hidden-dangers-within-ubuntus-package-suggestion-system/)

- [FreeNginx:](https://freenginx.org/en/support.html)

- [Auto-cpufreq 2.2.0:](https://github.com/AdnanHodzic/auto-cpufreq/releases/tag/v2.2.0)

- [Mixxx 2.4:](https://mixxx.org/news/2024-02-16-mixxx-2-4-0-features/)

- [The openSUSE project has revealed plans for the development of a new Agama installer](https://news.opensuse.org/2024/02/16/exploring-agamas-roadmap/)

- [Open Image Denoise 2.2:](https://github.com/OpenImageDenoise/oidn/releases/tag/v2.2.0)

- [Release of DuckDB 0.10.0:](https://duckdb.org/2024/02/13/announcing-duckdb-0100.html)

- [ugrep 5.0:](https://github.com/Genivia/ugrep/releases/tag/v5.0.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
