---
title: "Full Circle Weekly News 04"
date: 2016-02-13
draft: false
tags:
  - "16-04"
  - "168"
  - "algorithm"
  - "android"
  - "canonical"
  - "chip"
  - "core"
  - "debian"
  - "deepmind"
  - "firefox"
  - "font"
  - "kernel"
  - "library"
  - "linux"
  - "maru"
  - "mit"
  - "nuc"
  - "os"
  - "processing"
  - "russia"
  - "switch"
  - "vulnerabilities"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2004.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Google's DeepMind AI Develops an Algorithm](http://www.techtimes.com/articles/131162/20160205/googles-deepmind-artificial-intelligence-develops-algorithm-enabling-navigate-3d-maze.htm)

- [Maru Is an Android OS on the Phone and Debian Linux When Connected to a PC](http://news.softpedia.com/news/maru-is-an-android-lollipop-on-the-phone-and-debian-when-connected-to-pc-500028.shtml)

- [Vulnerabilities in Font Processing Library Impact Firefox and Linux](http://gadgets.ndtv.com/internet/news/vulnerabilities-in-font-processing-library-impact-firefox-linux-report-799808)

- [Ubuntu Core now supports Intel’s NUC mini computer](http://liliputing.com/2016/02/ubuntu-core-now-supports-intels-150-or-less-nuc-mini-computer.html)

- [MIT's 168-Core Chip Could Bring AI to Smartphones and IoT Devices](http://www.eweek.com/networking/mits-168-core-chip-could-bring-ai-to-smartphones-iot-devices.html)

- [Russia's internet guru says no to Windows, yes to Linux](http://www.theregister.co.uk/2016/02/11/putins_internet_guru_says_nyet_to_windows/)

- [Canonical reckons Android phone-makers will switch to Ubuntu](http://www.theregister.co.uk/2016/02/09/canonical_ubuntu_google_android_partners/)

- [Ubuntu 16.04 LTS Is Now Using the Latest Linux Kernel 4.4.1](http://news.softpedia.com/news/ubuntu-16-04-lts-is-now-using-the-latest-linux-kernel-4-4-1-500201.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
