---
title: "Full Circle Weekly News 215"
date: 2021-06-21
draft: false
tags:
  - "alpha"
  - "alpine"
  - "cinnamon"
  - "cloudfare"
  - "database"
  - "debian"
  - "developer"
  - "editor"
  - "fsf"
  - "glibc"
  - "gnu"
  - "kde"
  - "linux"
  - "nano"
  - "networkmanager"
  - "php"
  - "rqlite"
  - "rust"
  - "server"
  - "sme"
  - "sqlite"
  - "stable"
  - "text"
  - "tor"
  - "warp"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20215.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [PHP 8.1 alpha testing has begun](https://www.php.net/index.php#id2021-06-10-1)

- [rqlite 6.0, a distributed fault-tolerant SQLite-based database management system](https://www.philipotoole.com/rqlite-6-0-0-building-for-the-future/)

- [New stable Tor 0.4.6 branch](https://blog.torproject.org/node/2041)

- [GNU nano text editor 5.8](https://nano-editor.org/)

- [SME Server 10.0 Linux server](https://lists.contribs.org/pipermail/updatesannounce/2021-June/000477.html)

- [Debian’s Cinnamon maintainer switches to KDE](https://www.preining.info/blog/2021/06/future-of-cinnamon-in-debian/)

- [Alpine Linux 3.14 released](https://alpinelinux.org/posts/Alpine-3.14.0-released.html)

- [Glibc Developers Consider stopping Code Transfer To Free Software Foundation](https://sourceware.org/pipermail/libc-alpha/2021-June/127581.html)

- [NetworkManager 1.32.0 released](https://mail.gnome.org/archives/networkmanager-list/2021-June/msg00044.html)

- [Cloudflare publishes WARP for Linux](https://blog.cloudflare.com/announcing-warp-for-linux-and-proxy-mode/)

- [Rust 1.53 released](https://blog.rust-lang.org/2021/06/17/Rust-1.53.0.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
