---
title: "Full Circle Weekly News 65"
date: 2017-07-01
draft: false
tags:
  - "cern"
  - "debian"
  - "kaby-lake"
  - "laptops"
  - "librem"
  - "pop_os"
  - "purism"
  - "qubes"
  - "skylake"
  - "supercomputing"
  - "system76"
  - "systemd"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2065.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux owns supercomputing](http://www.zdnet.com/article/linux-owns-supercomputing/)

- [Security-Focused Purism Librem 13 & Librem 15 Linux Laptops with Qubes OS](http://news.softpedia.com/news/security-focused-purism-librem-13-15-linux-laptops-go-mainstream-with-qubes-os-516673.shtml)

- [System76 Announces Its Own Linux Distribution Named Pop!\_OS](https://fossbytes.com/pop-os-linux-distro-system76/)

- [Debian Linux reveals Intel Skylake and Kaby Lake processors have broken hyper-threading](http://www.zdnet.com/article/debian-linux-reveals-intel-skylake-kaby-lake-processors-have-broken-hyper-threading/)

- [An Advanced AI Has Been Deployed to Fight Against Hackers](https://futurism.com/an-advanced-ai-has-been-deployed-to-fight-against-hackers/)

- [Entroware Launches Two New Ubuntu Laptops, for Linux Gaming and Office Use](http://news.softpedia.com/news/entroware-launches-two-new-ubuntu-laptops-for-linux-gaming-and-office-use-516703.shtml)

- [New systemd Vulnerability Affects Ubuntu 17.04 and Ubuntu 16.10, Update Now](http://news.softpedia.com/news/new-systemd-vulnerability-affects-ubuntu-17-04-and-ubuntu-16-10-update-now-516721.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
