---
title: "Full Circle Weekly News 125"
date: 2019-03-21
draft: false
tags:
  - "14-04"
  - "3-32"
  - "5-0"
  - "5-1"
  - "5-15-3"
  - "apt"
  - "bug"
  - "canonical"
  - "desktop"
  - "distro"
  - "extix"
  - "firefox"
  - "flatpak"
  - "gnome"
  - "kde"
  - "kernel"
  - "kodi"
  - "linux"
  - "lts"
  - "mozilla"
  - "parrot"
  - "plasma"
  - "send"
  - "trusty-tahir"
  - "ubuntu"
  - "xfce"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20125.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [ExTix 19.3 Linux Distro Released With Kernel 5.0, Kodi 18.2, And Xfce 4.13](https://fossbytes.com/extix-19-3-linux-released-features-download/)

- [Ubuntu 14.04.6 Trusty Tahr Released With High-impact APT Bug Fix](https://fossbytes.com/ubuntu-14-04-6-trusty-tahr-released-download/)

- [KDE Plasma 5.15.3 Desktop Environment Released with Flatpak Improvements](https://news.softpedia.com/news/kde-plasma-5-15-3-desktop-environment-released-with-flatpak-improvements-more-525273.shtml)

- [GNOME 3.32 'Taipei' is finally here! The best Linux desktop environment gets even better](https://betanews.com/2019/03/13/gnome-332-taipei-linux/)

- [Mozilla introduces Private Encrypted File Exchange with Firefox Send](https://www.pro-linux.de/news/1/26862/mozilla-stellt-mit-firefox-send-privaten-verschl%C3%BCsselten-dateitausch-vor.html)

- [Canonical Releases Minor Linux Kernel Security Update for Ubuntu 14.04 LTS](https://news.softpedia.com/news/canonical-releases-minor-linux-kernel-security-update-for-ubuntu-14-04-lts-525308.shtml)

- [Linux 5.1 Might Add Support For Using Persistant Memory As System RAM](https://fossbytes.com/linux-5-1-kernel-persistant-memory-as-system-ram/)

- [Parrot Home: Enjoy the Privacy Extras](https://www.linuxinsider.com/story/Parrot-Home-Enjoy-the-Privacy-Extras-85886.html)(
