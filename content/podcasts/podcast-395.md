---
title: "Full Circle Weekly News 395"
date: 2025-01-04
draft: false
tags:
  - "opensuse"
  - "sysvint"
  - "kernel"
  - "linux"
  - "darktable"
  - "siduction"
  - "scumm"
  - "fixbrowser"
  - "serpent"
  - "alpha"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20395.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [OpenSUSE develops YQPkg:](https://news.opensuse.org/2024/12/20/new-pkg-mgmt-tool-debuts/)

- [Release of SysVinit 3.12:](https://github.com/slicer69/sysvinit/releases/tag/3.12)

- [Linux 6.12 kernel tweaks cause zRAM issues in some distros:](https://github.com/void-linux/void-packages/issues/53580)

- [Darktable 5.0.0 released:](https://github.com/darktable-org/darktable/releases/tag/release-5.0.0)

- [Siduction 2024.1 release:](https://siduction.org/2024/12/release-notes-for-siduction-2024-1-0-shine-on/)

- [Release ScummVM 2.9.0:](https://www.scummvm.org/news/20241222/)

- [First test release of FixBrowser:](https://www.fixbrowser.org)

- [Serpent OS distribution enters alpha testing stage:](https://serpentos.com/blog/2024/12/23/serpent-os-enters-alpha/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
