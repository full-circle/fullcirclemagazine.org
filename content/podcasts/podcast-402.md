---
title: "Full Circle Weekly News 402"
date: 2025-03-02
draft: false
tags:
  - "fheroes"
  - "postfix"
  - "openssh"
  - "mesa"
  - "xenoeye"
  - "obs"
  - "ubuntu"
  - "mylibrary"
  - "gentoo"
  - "kicad"
  - "exim"
  - "cosmic"
  - "icewm"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20402.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Release of fheroes2 1.1.6:](https://github.com/ihhub/fheroes2/releases/tag/1.1.6)

- [Postfix 3.10.0 has been released:](https://www.mail-archive.com/postfix-announce@postfix.org/msg00101.html)

- [OpenSSH 9.9p2 Update to Fix MITM Attack:](https://lists.mindrot.org/pipermail/openssh-unix-dev/2025-February/041810.html)

- [Mesa 25.0 is released:](https://lists.freedesktop.org/archives/mesa-dev/2025-February/226464.html)

- [OBS Studio and Fedora Resolve Conflict:](https://gitlab.com/fedora/sigs/flatpak/fedora-flatpaks/-/issues/39%23note_2354562186)

- [Xenoeye Netflow/IPFIX/sFlow Collector Release 25.02:](https://github.com/vmxdev/xenoeye/releases/tag/v25.02-Novokuznetsk)

- [Ubuntu 24.04.2 LTS Released with Graphics Stack and Linux Kernel Updates:](https://lists.ubuntu.com/archives/ubuntu-announce/2025-February/000308.html)

- [Release of MyLibrary 3.2:](https://github.com/ProfessorNavigator/mylibrary/releases/tag/v3.2)

- [Gentoo Releases Official QCOW2 Boot Images:](https://www-gentoo-org.translate.goog/news/2025/02/20/gentoo-qcow2-images.html?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en-US&_x_tr_pto=wapp)

- [KiCad 9.0 CAD Release:](https://www.kicad.org/blog/2025/02/Version-9.0.0-Released/)

- [Exim 4.98.1 mail server:](https://lists.exim.org/lurker/message/20250221.121401.a509f6c9.en.html)

- [The sixth alpha release of the COSMIC desktop environment:](https://blog.system76.com/post/cosmic-alpha-6-big-leaps-forward)

- [IceWM 3.7.0 Released:](https://github.com/ice-wm/icewm/releases/tag/3.7.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
