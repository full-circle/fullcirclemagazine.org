---
title: "Full Circle Weekly News 156"
date: 2019-12-03
draft: false
tags:
  - "20-04"
  - "anonymous"
  - "antivirus"
  - "canonical"
  - "debian"
  - "defender"
  - "linux"
  - "microsoft"
  - "phone"
  - "python"
  - "sparky"
  - "touch"
  - "ubports"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20156.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Will Get Microsoft Defender ATP Antivirus](https://www.zdnet.com/article/microsoft-defender-atp-is-coming-to-linux-in-2020/)

- [Ubuntu 20.04 and Debian 11 Progress on Python 2 Removal](https://news.softpedia.com/news/ubuntu-20-04-lts-and-debian-gnu-linux-11-bullseye-progress-on-python-2-removal-528138.shtml)

- [Canonical Outs Security Updates for All Supported Ubuntu OSes](https://news.softpedia.com/news/canonical-outs-important-linux-kernel-updates-for-all-supported-ubuntu-releases-528153.shtml)

- [Canonical Donates More Ubuntu Phones to UBports](https://twitter.com/UBports/status/1194673684847480833)

- [A Linux-Based Smartphone Promises to Keep You Anonymous](https://fossbytes.com/linux-based-smartphone-volla-phone-protects-anonymity/)

- [Sparky Linux Releases Special Editions](https://sparkylinux.org/sparky-2019-11-special-editions/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
