---
title: "Full Circle Weekly News 228"
date: 2021-09-21
draft: false
tags:
  - "bsd"
  - "budgie"
  - "cura"
  - "desktop"
  - "discord"
  - "efl"
  - "enlightenment"
  - "europe"
  - "gimp"
  - "hacked"
  - "interface"
  - "jdk"
  - "kde"
  - "libraries"
  - "libre"
  - "libreoffice"
  - "mesa"
  - "midnightbsd"
  - "office"
  - "opencl"
  - "oracle"
  - "plasma"
  - "postgresql"
  - "revolt"
  - "russia"
  - "rust"
  - "server"
  - "tabbed"
  - "terraform"
  - "ubuntu"
  - "ultimaker"
  - "windows"
  - "windowsfx"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20228.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Lakka 3.4 and RetroArch 1.9.9 emulators](https://lakka.tv/articles/2021/09/06/lakka-3.4/)

- [Ultimaker Cura 4.11 released](https://github.com/Ultimaker/Cura/releases/tag/4.11.0)

- [Revolt – alternative to Discord](https://revolt.chat/)

- [Terraform Project news](https://www.hashicorp.com/blog/terraform-community-contributions)

- [MidnightBSD Project Server Hacked](https://twitter.com/midnightbsd/status/1434543206067159041)

- [A third party is trying to trademark PostgreSQL in Europe and the US](https://www.postgresql.org/about/news/trademark-actions-against-the-postgresql-community-2302/)

- [PostgreSQL conference to be held in Nizhny Novgorod, Russia](https://pgconf.ru/202109)

- [Budgie desktop migrates from GTK to EFL libraries from Enlightenment project](https://joshuastrobl.com/2021/09/14/building-an-alternative-ecosystem/)

- [LibreOffice 8.0 new tabbed interface layout available](https://libreofficemaster.blogspot.com/2021/07/libreoffice-80-ui-mockup.html)

- [Oracle removes restriction on commercial use of JDK](https://blogs.oracle.com/java/post/free-java-license)

- [Ubuntu 18.04.6 LTS release](https://lists.ubuntu.com/archives/ubuntu-announce/2021-September/000272.html)

- [GIMP 2.10.28 Released](https://www.gimp.org/news/2021/09/18/gimp-2-10-28-released/)

- [An OpenCL frontend written in Rust is being developed for Mesa](https://gitlab.freedesktop.org/karolherbst/mesa/-/tree/rusticl/src/gallium/frontends/rusticl)

- [The Windowsfx project with Windows11 interface](https://www.windowsfx.org/index.php/release-news/windowsfx-operating-system)

- [Testing the KDE Plasma 5.23 desktop](https://kde.org/announcements/plasma/5/5.22.90/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
