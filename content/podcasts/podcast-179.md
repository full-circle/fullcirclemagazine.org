---
title: "Full Circle Weekly News 179"
date: 2020-08-03
draft: false
tags:
  - "arm"
  - "beowulf"
  - "centos"
  - "defender"
  - "dell"
  - "devuan"
  - "elementary"
  - "floss"
  - "gnu"
  - "hub"
  - "kde"
  - "kernel"
  - "kup"
  - "laptop"
  - "libre"
  - "linux"
  - "microsoft"
  - "mint"
  - "nextcloud"
  - "open-source"
  - "oracle"
  - "plasma"
  - "rescuezilla"
  - "snap"
  - "snaps"
  - "supercomputer"
  - "system76"
  - "tuxedo"
  - "ulyana"
  - "xps"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20179.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Mint Confirms Snap Will Be Forbidden](https://blog.linuxmint.com/?p=3906)

- [TUXEDO Computers and System76 Now Have AMD Laptop Offerings](https://9to5linux.com/tuxedo-computers-unveils-their-first-amd-only-linux-laptop)

- [Kup Is Now an Official KDE App](https://kde.org/announcements/releases/2020-06-apps-update/)

- [Kernel 5.8 Will Be One of the Biggest Releases of All Time](https://www.zdnet.com/article/linus-torvalds-linux-kernel-5-8-is-one-of-our-biggest-releases-of-all-time/)

- [Open Source Contributor Survey Looking To Secure FLOSS](https://www.zdnet.com/article/linux-foundation-and-harvard-announce-linux-and-open-source-contributor-security-survey/)

- [Fastest ARM Supercomputer in the World](https://www.zdnet.com/article/arm-and-linux-take-supercomputer-top500-crown/)

- [Dell XPS 13 Now Shipping with Ubuntu 20.04](https://9to5linux.com/dell-xps-13-developer-edition-linux-laptop-is-now-available-with-ubuntu-20-04-lts)

- [CentOS 8.2 Out](https://lists.centos.org/pipermail/centos-announce/2020-June/035756.html)

- [Oracle Linux 8.2 Out](https://blogs.oracle.com/linux/announcing-the-release-of-oracle-linux-8-update-2)

- [Elementary OS 5.1.5 Out](https://blog.elementary.io/hera-updates-for-may-2020/)

- [Linux Mint 20, Ulyana, Out](https://blog.linuxmint.com/?p=3928)

- [Devuan Beowulf 3.0 Out](https://devuan.org/os/announce/beowulf-stable-announce-060120)

- [Linux For All 200607 Out](http://lfa.exton.net/?p=203)

- [Rescuezilla 1.0.6.1 Out](https://github.com/rescuezilla/rescuezilla/releases/tag/1.0.6.1)

- [KDE Plasma 5.19 Out](https://kde.org/announcements/plasma-5.19.0)

- [Nextcloud Hub 19 Out](https://nextcloud.com/blog/nextcloud-hub-brings-productivity-to-home-office/)

- [GNU Linux-Libre Kernel 5.7 Out](https://lists.gnu.org/archive/html/info-gnu/2020-06/msg00002.html)

- [Microsoft Defender ATP Out](https://www.zdnet.com/article/microsoft-releases-defender-atp-for-linux/)
