---
title: "Full Circle Weekly News 266"
date: 2022-07-19
draft: false
tags:
  - "ubuntu"
  - "api"
  - "atom"
  - "code"
  - "compiler"
  - "desktop"
  - "distribution"
  - "distro"
  - "editor"
  - "enterprise"
  - "environment"
  - "github"
  - "http"
  - "kernel"
  - "language"
  - "leap"
  - "lighttpd"
  - "ntfs3"
  - "opensuse"
  - "paragon"
  - "perl"
  - "raku"
  - "rakudo"
  - "regolith"
  - "server"
  - "suse"
  - "tails"
  - "vulkan"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20266.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Paragon Software has resumed maintenance of the NTFS3 module in the Linux kernel:](https://github.com/Paragon-Software-Group/linux-ntfs3/commits/devel)

- [Release of Tails 5.1:](https://tails.boum.org/news/version_5.1/index.en.html)

- [New driver for the Vulkan graphics API is being developed:](https://gitlab.freedesktop.org/nouveau/mesa/-/commits/nouveau/vk/)

- [Rakudo compiler release 2022.06 for the Raku programming language (former Perl 6):](https://rakudo.org/post/announce-rakudo-release-2022.06)

- [SUSE Linux Enterprise 15 SP4 distribution available:](https://www.suse.com/news/SUSE-Unveils-Comprehensive-Infrastructure-Security-Stack/)

- [Lighttpd http server release 1.4.65:](http://www.lighttpd.net/)

- [Release of the openSUSE Leap 15.4 distribution:](https://news.opensuse.org/2022/06/08/leap-offers-new-features-familiar-stability/)

- [GitHub wraps up development of Atom code editor:](https://github.blog/2022-06-08-sunsetting-atom/)

- [Pale Moon Browser 31.1 Released:](https://forum.palemoon.org/viewtopic.php?t=28454&p=228952)

- [Regolith 2.0 Desktop Environment Released:](https://regolith-desktop.com/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
