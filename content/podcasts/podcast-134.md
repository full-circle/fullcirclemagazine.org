---
title: "Full Circle Weekly News 134"
date: 2019-06-10
draft: false
tags:
  - "5-0"
  - "canonical"
  - "gazelle"
  - "kernel"
  - "laptop"
  - "leap"
  - "linux"
  - "mageia"
  - "opensuse"
  - "pinephone"
  - "sailfish"
  - "security"
  - "system76"
  - "ubuntu"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20134.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Mageia 7 Linux distribution reaches release candidate (RC) status](https://betanews.com/2019/06/02/mageia-7-linux-rc-download/)

- [Zorin OS 15 Officially Released](https://news.softpedia.com/news/zorin-os-15-linux-distro-officially-released-based-on-ubuntu-18-04-2-lts-526309.shtml)

- [Support for OpenSuse Leap 42.3 ends in June](https://www.pro-linux.de/news/1/27122/support-f%C3%83%C2%BCr-opensuse-leap-423-endet-im-juni.html)

- [Linux Kernel 5.0 Reaches End of Life](https://news.softpedia.com/news/linux-kernel-5-0-reaches-end-of-life-users-urged-to-upgrade-to-linux-kernel-5-1-526292.shtml)

- [Canonical Issues Linux Kernel Security Updates for All Supported Ubuntu Releases](https://news.softpedia.com/news/canonical-outs-linux-kernel-security-updates-for-all-supported-ubuntu-releases-526308.shtml)

- [PinePhone could support Ubuntu, Sailfish and more](https://liliputing.com/2019/06/pinephone-149-linux-smartphone-could-support-ubuntu-sailfish-maemo-luneos-and-more.html)

- [System76 Ubuntu Linux-powered 'Gazelle' laptop reborn!](https://betanews.com/2019/05/31/system76-gazelle-linux-laptop-14/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
