---
title: "Full Circle Weekly News 199"
date: 2021-02-07
draft: false
tags:
  - "apple"
  - "buffer"
  - "clonezilla"
  - "driver"
  - "firefox"
  - "gnome"
  - "gparted"
  - "kernel"
  - "libgcrypt"
  - "live"
  - "lmde"
  - "m1"
  - "mac"
  - "mint"
  - "nitrux"
  - "nvidia"
  - "overflow"
  - "ported"
  - "root"
  - "sudo"
  - "support"
  - "system"
  - "system-76"
  - "system76"
  - "tails"
  - "ubuntu"
  - "vulnerability"
  - "wayland"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20199.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Giving Wayland Another Shot](https://discourse.ubuntu.com/t/trying-wayland-by-default-again/20575)

- [Networking and Touchpads Work in Linux on M1 Macs](https://twitter.com/cmwdotme/status/1355660127433535490?s=20)

- [Greg Kroah-Hartman Needs Commercial Buy In For Longer Kernel Support](https://lore.kernel.org/lkml/ef30af4d-2081-305d-cd63-cb74da819a6d@broadcom.com/)

- [Sudo Buffer Overflow Vulnerability Allows Unauthorized Root Access](https://blog.qualys.com/vulnerabilities-research/2021/01/26/cve-2021-3156-heap-based-buffer-overflow-in-sudo-baron-samedit)

- [Libgcrypt 1.9.0 Released with Vulnerability](https://nakedsecurity.sophos.com/2021/01/31/gnupg-crypto-library-can-be-pwned-during-decryption-patch-now/)

- [System 76 Posts Guide to Gaming](https://blog.system76.com/post/641571610853326848/the-system76-guide-to-gaming-on-popos)

- [Linux Mint Ported 20.1 Features to LMDE 4](https://blog.linuxmint.com/?p=4024)

- [Tails 4.15.1 Out](https://tails.boum.org/news/version_4.15.1/index.en.html)

- [Nitrux 1.3.7 Out](https://nxos.org/changelog/changelog-nitrux-1-3-7/)

- [Clonezilla Live 2.7.1-22 Out](https://sourceforge.net/p/clonezilla/news/2021/01/stable-clonezilla-live-271-22-released/)

- [Gparted Live 1.2.0-1 Out](https://gparted.org/news.php?item=237)

- [Gnome 3.38.3 Out](https://ftp-chi.osuosl.org/pub/gnome/core/3.38/3.38.3/NEWS)

- [Firefox 85 Out](https://www.mozilla.org/en-US/firefox/85.0/releasenotes/)

- [Nvidia Linux Driver 460.39 Out](https://www.nvidia.com/Download/driverResults.aspx/170134/en-us)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
