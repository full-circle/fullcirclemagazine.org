---
title: "Full Circle Weekly News 250"
date: 2022-02-28
draft: false
tags:
  - "clutter"
  - "dino"
  - "gnome"
  - "kali"
  - "kaos"
  - "obs"
  - "ota-22"
  - "ota22"
  - "pfsense"
  - "slint"
  - "touch"
  - "ubports"
  - "uchmviewer"
  - "webos"
  - "zabbix"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20250.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Slint 0.2](https://github.com/slint-ui/slint/releases/tag/v0.2.0)

- [New release of uChmViewer](https://github.com/gyunaev/kchmviewer)

- [Dino 0.3 communication client released](https://dino.im/blog/2022/02/dino-0.3-release/)

- [OBS Studio 27.2 Live Streaming Release](https://github.com/obsproject/obs-studio/releases/tag/27.2.0)

- [Release of qxkb5](https://github.com/AndreyBarmaley/qxkb5)

- [AV Linux MX-21](http://www.bandshed.net/2022/02/14/av-linux-mx-21-consciousness-released/)

- [Zabbix 6.0 LTS](https://www.zabbix.com/ru/whats_new_6_0)

- [Kali Linux 2022.1 Released](https://www.kali.org/blog/kali-linux-2022-1-release/)

- [Release of pfSense 2.6.0](https://www.netgate.com/blog/pfsense-plus-software-version-22.01-and-ce-2.6.0-are-now-available)

- [KaOS 2022.02](https://kaosx.us/news/2022/kaos02/)

- [GNOME stops maintaining the Clutter graphics library](https://github.com/GNOME/clutter)

- [22nd Ubuntu Touch Firmware Update](https://ubports.com/)

- [WebOS Open Source Edition 2.15 Platform Released](https://www.webosose.org/blog/2022/02/18/webos-ose-2-15-0-release/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
