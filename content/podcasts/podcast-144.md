---
title: "Full Circle Weekly News 144"
date: 2019-09-11
draft: false
tags:
  - "adwaita"
  - "desktop"
  - "epel"
  - "firefox"
  - "https"
  - "mozilla"
  - "theme"
  - "ubuntu"
  - "xfce"
  - "yaru"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20144.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Default Ubuntu Yaru Theme Rebased on Adwaita 3.32](https://www.linuxuprising.com/2019/08/default-ubuntu-yaru-theme-rebased-on.html)

- [Announcing the EPEL 8.0 Official Release](http://smoogespace.blogspot.com/2019/08/announcing-epel-80-official-release.html)

- [Mozilla Revamps Firefox’s HTTPS Address Bar Information](https://www.ghacks.net/2019/08/13/mozilla-revamps-firefoxs-https-address-bar-information/)

- [XFCE 4.14 Desktop Officially Released](https://www.omgubuntu.co.uk/2019/08/xfce-4-14)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
