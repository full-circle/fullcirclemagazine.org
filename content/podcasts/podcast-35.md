---
title: "Full Circle Weekly News 35"
date: 2016-09-17
draft: false
tags:
  - "3-14"
  - "ai"
  - "beta"
  - "box"
  - "core"
  - "eol"
  - "gpu"
  - "intel"
  - "kernel"
  - "nextcloud"
  - "nvidia"
  - "pi"
  - "rust"
  - "snappy"
  - "trojan"
  - "update"
  - "vi"
  - "vim"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2035.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Classic Unix/Linux editor Vim gets first update in years](http://www.zdnet.com/article/vim-update-classic-unix-linux-editor-gets-first-update-in-years/)

- [Nextcloud Box – a private cloud and IoT solution for home users – from Nextcloud, Canonical and WDLabs](http://nextcloud.com/box)

- [Ubuntu Snappy Core 16 Beta Images Now Available for PC and Raspberry Pi 3](http://news.softpedia.com/news/ubuntu-snappy-core-16-beta-images-are-now-available-for-pc-and-raspberry-pi-3-508219.shtml)

- [Linux Kernel 3.14 Reaches End of Life](http://news.softpedia.com/news/linux-kernel-3-14-reached-end-of-life-users-urged-to-move-to-linux-4-4-lts-508181.shtml)

- [New Linux Trojan Discovered Coded in Mozilla's Rust Language](http://news.softpedia.com/news/new-linux-trojan-discovered-coded-in-mozilla-s-rust-language-508135.shtml)

- [Nvidia Debuts New Graphics Processors For AI To Compete With Intel](http://www.itechpost.com/articles/29459/20160913/nvidia-debuts-two-new-graphic-processors-for-artificial-intelligence-to-compete-with-intel.htm)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
