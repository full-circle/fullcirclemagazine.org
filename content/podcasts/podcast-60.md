---
title: "Full Circle Weekly News 60"
date: 2017-05-06
draft: false
tags:
  - "4-11"
  - "assembly"
  - "docs"
  - "email"
  - "gmail"
  - "gnome"
  - "google"
  - "hacking"
  - "hijack"
  - "jailed"
  - "kernel"
  - "line"
  - "linux"
  - "mint"
  - "robots"
  - "rpcbind"
  - "terrorist"
  - "unity"
  - "unity8"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2060.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux kernel 4.11 released](http://www.infoworld.com/article/3193452/linux/linux-kernel-4-11-released.html)

- [Linux Mint terrorist jailed for 8 years](https://www.theregister.co.uk/2017/05/02/samata_ullah_linux_mint_terrorist_jailed_8_yrs/)

- [Post Unity 8 Ubuntu shock? Relax, Linux has been here before](https://www.theregister.co.uk/2017/05/02/post_unity_8_what_next_for_canonical/)

- [Assembly line robots vulnerable to hacking](https://betanews.com/2017/05/03/industrial-robot-hacking/)

- [Don't click that Google Docs link! Gmail hijack mail spreads like wildfire](https://www.theregister.co.uk/2017/05/03/google_docs_hit_phishing_email_campaign/)

- [You only need 60 bytes to break Linux's rpcbind](https://www.theregister.co.uk/2017/05/04/linux_rpcbind_vulnerability/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
