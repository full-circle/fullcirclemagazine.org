---
title: "Full Circle Weekly News 84"
date: 2018-03-12
draft: false
tags:
  - "18-04"
  - "android"
  - "ddos"
  - "debian"
  - "exim"
  - "extonos"
  - "github"
  - "kotlin"
  - "librem"
  - "linux"
  - "lts"
  - "malware"
  - "microsoft"
  - "phone"
  - "purism"
  - "snap"
  - "windows-10"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2084.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Purism to Bring Hardware Encryption to Its Librem 5 Privacy-Focused Linux Phone](http://news.softpedia.com/news/purism-to-bring-hardware-encryption-to-its-librem-5-privacy-focused-linux-phone-520140.shtml)

- [Android Phones Caught Selling with Pre-Installed Factory Malware](http://news.softpedia.com/news/android-phones-caught-selling-with-pre-installed-factory-malware-520058.shtml)

- [Exton|OS Claims to Be First Distribution Based on Ubuntu 18.04 LTS, Linux 4.16](http://news.softpedia.com/news/exton-os-claims-to-be-first-distribution-based-on-ubuntu-18-04-lts-linux-4-16-520052.shtml)

- [As Debian comes to Windows 10, should we worry Microsoft will 'embrace, extend, and extinguish' Linux?](https://betanews.com/2018/03/06/debian-linux-windows/)

- [Security researchers' warning over Linux feature used in biggest ever DDoS attack on Github](https://www.computing.co.uk/ctg/news/3027797/security-researchers-warning-over-linux-feature-used-in-biggest-ever-ddos-attack-on-github)

- [Open-source Exim remote attack bug: 400,000 servers still vulnerable](http://www.zdnet.com/article/open-source-exim-remote-attack-bug-400000-servers-still-vulnerable-patch-now/)

- [Kotlin programming language snap available on Ubuntu](https://www.neowin.net/news/kotlin-programming-language-snap-available-on-ubuntu)
