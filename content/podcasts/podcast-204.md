---
title: "Full Circle Weekly News 204"
date: 2021-04-06
draft: false
tags:
  - "7zip"
  - "audacity"
  - "beta"
  - "collabora"
  - "conference"
  - "debian"
  - "driver"
  - "fedora"
  - "firefox"
  - "fsf"
  - "gnome"
  - "krita"
  - "libreelec"
  - "linux"
  - "manjaro"
  - "matrix"
  - "nvidia"
  - "opensuse"
  - "ornara"
  - "ota-16"
  - "ota16"
  - "pangolin"
  - "panvk"
  - "port"
  - "rc1"
  - "stallman"
  - "testing"
  - "touch"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20204.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

**Please welcome new host, Moss Bliss**

- [DigiKam 7.2 released](https://www.digikam.org/news/2021-03-22-7.2.0_release_announcement/)

- [4MLinux 36.0 released](https://4mlinux-releases.blogspot.com/2021/03/4mlinux-360-stable-released.html)

- [Malicious changes detected in the PHP project Git repository](https://news-web.php.net/php.internals/113838)

- [New version of Cygwin 3.2.0, the GNU environment for Windows](https://www.mail-archive.com/cygwin-announce@cygwin.com/msg09612.html)

- [SeaMonkey 2.53.7 Released](https://www.seamonkey-project.org/news#2021-03-30)

- [Nitrux 1.3.9 with NX Desktop is Released](https://nxos.org/changelog/changelog-nitrux-1-3-9/)

- [Parrot 4.11 Released with Security Checker Toolkit](https://parrotsec.org/blog/parrot-4.11-release-notes/)

- [Systemd 248 system manager released](https://lists.freedesktop.org/archives/systemd-devel/2021-March/046289.html)

- [GIMP 2.10.24 released](https://www.gimp.org/news/2021/03/29/gimp-2-10-24-released/)

- [Deepin 20.2 ready for download](https://www.deepin.org/en/2021/03/31/deepin-20-2-beautiful-and-wonderful/)

- [Installer added to Arch Linux installation images](https://archlinux.org/news/installation-medium-with-installer/)

- [Ubuntu 21.04 beta released](https://ubuntu.com//blog/announcing-ubuntu-on-windows-community-preview-wsl-2)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
