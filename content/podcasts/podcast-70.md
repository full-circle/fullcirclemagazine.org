---
title: "Full Circle Weekly News 70"
date: 2017-09-30
draft: false
tags:
  - "17-10"
  - "3-2"
  - "32-bit"
  - "election"
  - "foss"
  - "germany"
  - "iss"
  - "microsoft"
  - "rc"
  - "software"
  - "space"
  - "stallman"
  - "supercomputer"
  - "tails"
  - "testing"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2070.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [It’s Official: Ubuntu 17.10 Is Killing 32-Bit Desktop ISO](https://fossbytes.com/ubuntu-17-10-killing-32-bit-desktop-iso/)

- [Richard Stallman says Microsoft’s “Love” For Linux Will Hurt Free And Open Source Software](https://fossbytes.com/richard-stallman-microsoft-linux-love-will-hurt-free-open-source-software/)

- [The ISS just got its own Linux supercomputer](http://www.zdnet.com/article/the-iss-just-got-its-own-linux-supercomputer/)

- [Tails 3.2 release candidate has been released for testing](https://www.neowin.net/news/tails-32-release-candidate-has-been-released-for-testing)

- [Germany's Election Software Is Dangerously Hackable](https://www.wired.com/story/security-roundup-germany-election-software-is-hackable/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
