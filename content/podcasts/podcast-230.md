---
title: "Full Circle Weekly News 230"
date: 2021-10-03
draft: false
tags:
  - "api"
  - "beta"
  - "browser"
  - "bsd"
  - "dragonfly"
  - "electron"
  - "exim"
  - "fedora"
  - "firezone"
  - "foundation"
  - "free"
  - "fsf"
  - "gnu"
  - "google"
  - "identrust"
  - "java"
  - "javascript"
  - "jshelter"
  - "linux"
  - "mail"
  - "obs"
  - "open source"
  - "openbsd"
  - "pash"
  - "postgresql"
  - "root"
  - "script"
  - "server"
  - "shell"
  - "software"
  - "studio"
  - "supertuxkart"
  - "testing"
  - "vpn"
  - "wget"
  - "wireguard"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20230.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The first stable release of GNU wget2](https://lists.gnu.org/archive/html/info-gnu/2021-09/msg00011.html)

- [PaSh shell script parallelization system moved under the wing of Linux Foundation](https://linuxfoundation.org/press-release/linux-foundation-to-host-the-pash-project-accelerating-shell-scripting-with-automated-parallelization-for-industrial-use-cases/)

- [Fedora Linux 35 has entered beta testing](https://fedoramagazine.org/announcing-fedora-35-beta/)

- [OBS Studio 27.1 Released](https://github.com/obsproject/obs-studio/releases/tag/27.1.0)

- [SuperTuxKart 1.3 released](https://blog.supertuxkart.net/2021/09/supertuxkart-13-release.html)

- [Firezone - create VPN servers based on WireGuard](https://github.com/firezone/firezone)

- [New version of Exim mail server](https://lists.exim.org/lurker/message/20200601.152400.f7bce8f6.en.html)

- [PostgreSQL 14 released](https://www.postgresql.org/about/news/postgresql-14-released-2318/)

- [Free Software Foundation Introduces JShelter Browser Add-on to Restrict JavaScript API](https://www.fsf.org/news/fsf-announces-jshelter-browser-add-on-to-combat-threats-from-nonfree-javascript)

- [Crashes in OpenBSD, DragonFly BSD and Electron due to deprecation of the IdenTrust root certificate](https://github.com/libressl-portable/portable/issues/595)

- [Google donates a million dollars to improve the security of open source software](https://security.googleblog.com/2021/10/introducing-secure-open-source-pilot.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
