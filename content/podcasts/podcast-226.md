---
title: "Full Circle Weekly News 226"
date: 2021-09-06
draft: false
tags:
  - "armbian"
  - "beta"
  - "beyond"
  - "desktop"
  - "docs"
  - "editor"
  - "kde"
  - "kernel"
  - "linux"
  - "linux-libre"
  - "mobile"
  - "multitextor"
  - "nitrux"
  - "ntfs-3g"
  - "onlyoffice"
  - "openwrt"
  - "patch"
  - "plasma"
  - "scratch"
  - "session"
  - "smplayer"
  - "vulnerability"
  - "wayland"
  - "yt-dlp"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20226.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux 5.14 kernel released](https://lkml.org/lkml/2021/8/29/382)

- [Beta version of Multitextor](https://github.com/vikonix/multitextor)

- [A completely free version of the Linux-libre 5.14 kernel is available](https://www.fsfla.org/pipermail/linux-libre/2021-August/003439.html)

- [NTFS-3G Releases with Vulnerability Elimination](https://sourceforge.net/p/ntfs-3g/mailman/ntfs-3g-devel/?viewmonth=202108)

- [ONLYOFFICE Docs 6.4 online editor](https://www.onlyoffice.com/blog/2021/08/onlyoffice-docs-v6-4-with-conditional-formatting/)

- [New version of SMPlayer](https://blog.smplayer.info/smplayer-21-8-has-been-released/)

- [Armbian 21.08 Released](https://forum.armbian.com/topic/18874-armbian-2108-has-been-released/)

- [Linux From Scratch 11 and Beyond Linux From Scratch 11 Released](https://lists.linuxfromscratch.org/sympa/arc/lfs-announce/2021-09/msg00000.html)

- [Nitrux 1.6.0 Releases with NX Desktop](https://nxos.org/changelog/release-announcement-nitrux-1-6-0/)

- [Release of KDE Plasma Mobile 21.08](https://www.plasma-mobile.org/2021/08/31/plasma-mobile-gear-21-08/)

- [Release yt-dlp](https://github.com/yt-dlp/yt-dlp/releases/tag/2021.09.02)

- [OpenWrt 21.02.0 Released](https://lists.infradead.org/pipermail/openwrt-devel/2021-September/036260.html)

- [Wayland-based KDE session is stable](https://pointieststick.com/2021/09/03/this-week-in-kde-gazillions-of-bugfixes/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
