---
title: "Full Circle Weekly News 108"
date: 2018-09-23
draft: false
tags:
  - "16-04"
  - "18-04"
  - "5-13"
  - "5-14"
  - "android"
  - "arch"
  - "ascii"
  - "canonical"
  - "gnu"
  - "google"
  - "kde"
  - "kernel"
  - "linus"
  - "linux"
  - "lts"
  - "nano"
  - "nitrux"
  - "plasma"
  - "torvalds"
  - "ubuntu"
  - "unity8"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20108.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [KDE Plasma 5.13 Desktop Reaches End of Life, KDE Plasma 5.14 Arrives October 9](https://news.softpedia.com/news/kde-plasma-5-13-desktop-reaches-end-of-life-kde-plasma-5-14-arrives-october-9-522681.shtml)

- [Unity8 on Arch Source](https://github.com/vanyasem/Unity8-Arch/releases/tag/v0.1)

- [Nitrux 1.0.15 brings Kernel 4.18.5 and Plasma 5.13.4 for Most Secure and Integrated Performance Yet](https://appuals.com/nitrux-1-0-15-brings-kernel-4-18-5-and-plasma-5-13-4-for-most-secure-and-integrated-performance-yet/)

- [Canonical Outs New Linux Kernel Live Patch for Ubuntu 18.04 LTS and 16.04 LTS](https://news.softpedia.com/news/canonical-outs-new-linux-kernel-live-patch-for-ubuntu-18-04-lts-and-16-04-lts-522643.shtml)

- [Linus Torvalds Is Taking A Break From Linux, Here’s Why?](https://fossbytes.com/linus-torvalds-taking-break-from-linux/)

- [Google changed a battery setting on Android phones by mistake](https://www.engadget.com/2018/09/14/google-android-pie-battery-saver-setting-remote-update/)

- [GNU Nano 3.0 claims to read files 70% better with improved ASCII text handling](https://appuals.com/gnu-nano-3-0-claims-to-read-files-70-better-with-improved-ascii-text-handling/)
