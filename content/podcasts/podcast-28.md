---
title: "Full Circle Weekly News 28"
date: 2016-07-30
draft: false
tags:
  - "16-04-1"
  - "4-7"
  - "ai"
  - "archstrike"
  - "art"
  - "baidu"
  - "build"
  - "ethical"
  - "hacking"
  - "iso"
  - "kernel"
  - "music"
  - "openbsd"
  - "ota-12"
  - "ota12"
  - "radeon"
  - "rx480"
  - "security"
  - "touch"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2028.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 16.04.1 is out with improvements to software installation and low-graphics mode](http://www.pcworld.com/article/3098865/linux/ubuntu-16-04-1-is-out-with-improvements-to-software-installation-and-low-graphics-mode.html)

- [Ubuntu Touch OTA-12 Launches with Biometric Authentication for Meizu PRO 5](http://news.softpedia.com/news/ubuntu-touch-ota-12-launches-today-with-biometric-authentication-for-meizu-pro-5-506663.shtml)

- [Linux Kernel 4.7 Officially Released, Introduces Support for Radeon RX480 GPUs](http://news.softpedia.com/news/linux-kernel-4-7-officially-released-introduces-support-for-radeon-rx480-gpus-506568.shtml)

- [Baidu's New AI Creates Music Just by Looking at Art](http://en.yibada.com/articles/144079/20160722/baidus-new-ai-creates-music-looking-art.htm)

- [OpenBSD 6.0 tightens security by losing Linux compatibility](http://www.infoworld.com/article/3099038/open-source-tools/openbsd-60-tightens-security-by-losing-linux-compatibility.html)

- [ArchStrike The Ethical Hacking Linux Gets Its First ISO Builds](http://news.softpedia.com/news/archstrike-ethical-hacking-linux-operating-system-gets-its-first-iso-builds-506612.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
