---
title: "Full Circle Weekly News 159"
date: 2019-12-26
draft: false
tags:
  - "alpha"
  - "aws"
  - "beta"
  - "bullseye"
  - "canonical"
  - "cinnamon"
  - "debian"
  - "elementary"
  - "elementaryos"
  - "hera"
  - "librem"
  - "mint"
  - "mozilla"
  - "pro"
  - "purism"
  - "remix"
  - "tails"
  - "thunderbird"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20159.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [elementaryOS 5.1 Hera Released](https://blog.elementary.io/introducing-elementary-os-5-1-hera/)

- [Linux Mint 19.3 Beta Available](https://blog.linuxmint.com/?p=3816)

- [Debian 11 "Bullseye" Alpha 1 Released](https://lists.debian.org/debian-devel-announce/2019/12/msg00001.html)

- [Ubuntu Cinnamon Remix 19.10 Released](https://discourse.ubuntu.com/t/ubuntu-cinnamon-remix-19-10-eoan-ermine-released/13579)

- [Canonical Introduces Ubuntu AWS Rolling Kernel](https://ubuntu.com/blog/introducing-the-ubuntu-aws-rolling-kernel)

- [Canonical Announces Ubuntu Pro for AWS](https://ubuntu.com/blog/canonical-announces-ubuntu-pro-for-amazon-web-services)

- [Purism Announces the Librem 5 USA](https://puri.sm/posts/librem-5-usa/)

- [Thunderbird 68.3.0 Released](https://www.thunderbird.net/en-US/thunderbird/68.3.0/releasenotes/)

- [Tails 4.1 Released](https://news.softpedia.com/news/tails-4-1-anonymous-os-released-with-latest-tor-browser-linux-kernel-5-3-9-528437.shtml)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
