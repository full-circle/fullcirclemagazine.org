---
title: "Full Circle Weekly News 118"
date: 2019-01-20
draft: false
tags:
  - "18-04"
  - "5-0"
  - "amd"
  - "bluetooth"
  - "canonical"
  - "entroware"
  - "gnome"
  - "hades"
  - "intel"
  - "linux"
  - "ota-7"
  - "ota7"
  - "phone"
  - "pi"
  - "raspberry"
  - "systemd"
  - "touch"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20118.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Raspberry Pi Receives Official Touchscreen Support With Linux 4.21](https://fossbytes.com/raspberry-pi-touchscreen-support-linux-4-21/)

- [Entroware Launches Ubuntu Linux AIO PC With 6-Core Intel CPU](https://fossbytes.com/entroware-ubuntu-linux-pc-with-6-core-intel-cpu/)

- [Entroware Launches Hades, Its First AMD-Powered Workstation with Ubuntu Linux](https://news.softpedia.com/news/entroware-launches-hades-workstation-powered-by-ubuntu-18-04-lts-and-amd-ryzen-2-524526.shtml)

- [Ubuntu Touch Ota Avilable To Ubunu Phone Users](https://news.softpedia.com/news/ubuntu-touch-ota-7-now-available-to-ubuntu-phone-users-with-many-improvements-524472.shtm)

- [Inside Ubuntu's financials](https://www.zdnet.com/article/inside-ubuntus-financials/)

- [Linux reaches the big five (point) oh](https://www.theregister.co.uk/2019/01/07/linux_reaches_the_big_five_point_oh/)

- [Linus Torvalds Says Things Look Pretty Normal for Linux 5.0, Releases Second RC](https://news.softpedia.com/news/linus-torvalds-says-things-look-pretty-normal-for-linux-5-0-releases-second-rc-524541.shtml)

- [Linux systemd Affected by Memory Corruption Vulnerabilities, No Patches Yet](https://www.bleepingcomputer.com/news/security/linux-systemd-affected-by-memory-corruption-vulnerabilities-no-patches-yet/)

- [Canonical Patches GNOME Bluetooth Vulnerability on Ubuntu 18.04 LTS, Update Now](https://news.softpedia.com/news/canonical-patches-gnome-bluetooth-vulnerability-on-ubuntu-18-04-lts-update-now-524542.shtml)
