---
title: "Full Circle Weekly News 42"
date: 2016-11-12
draft: false
tags:
  - "bitcoin"
  - "blockchain"
  - "budgie"
  - "core"
  - "deepmind"
  - "dev"
  - "developer"
  - "flavor"
  - "foundation"
  - "linux"
  - "mythbuntu"
  - "mythtv"
  - "nes"
  - "nintendo"
  - "orange"
  - "pc2"
  - "pi"
  - "starcraft"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2042.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Budgie Is Now an Official Ubuntu Flavor](http://news.softpedia.com/news/ubuntu-budgie-is-now-an-official-ubuntu-flavor-510062.shtml)

- [Former Bitcoin Core Developer Joins Linux Foundation Board](https://cointelegraph.com/news/former-bitcoin-core-developer-jeff-garzik-joins-linux-foundation-board)

- [Orange Pi PC 2 Is A Cheap Quad Core Linux Computer For $20 That Runs Ubuntu](https://fossbytes.com/orange-pi-64-bit-quad-core-linux-computer-price-buy/)

- [Official Ubuntu Flavor Mythbuntu Is Dead.](https://fossbytes.com/official-ubuntu-flavor-mythbuntu-linux-is-dead-what-about-the-dvr/)

- [Nintendo’s NES Classic Edition runs Linux](http://www.infoworld.com/article/3139289/linux/nintendos-nes-classic-edition-runs-linux.html)

- [DeepMind taking artificial intelligence to the next level](http://en.as.com/en/2016/11/07/other_sports/1478529234_449897.html)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
