---
title: "Full Circle Weekly News 113"
date: 2018-10-28
draft: false
tags:
  - "aptoide"
  - "browser"
  - "china"
  - "gnome"
  - "google"
  - "librem"
  - "linux"
  - "morph"
  - "ota-5"
  - "ota5"
  - "phone"
  - "smartphone"
  - "touch"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20113.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Smartphone Librem 5 Will Ship With GNOME 3.32](https://fossbytes.com/linux-smartphone-librem-5-will-ship-with-gnome-3-32/)

- [Ubuntu Touch OTA-5 Is Out for Ubuntu Phones with New Morph Browser, Improvements](https://news.softpedia.com/news/ubuntu-touch-ota-5-is-out-for-ubuntu-phones-with-new-morph-browser-improvements-523228.shtml)

- [Google’s Test For Censored Chinese Search Engine Impressive: Sundar Pichai](https://fossbytes.com/google-test-censored-chinese-search-engine-impressive-sundar-pichai/)

- [Aptoide wins court battle against Google in landmark case](https://www.reuters.com/article/us-google-antitrust-aptoide/aptoide-wins-court-battle-against-google-in-landmark-case-idUSKCN1MW2CL)
