---
title: "Full Circle Weekly News 383"
date: 2024-09-15
draft: false
tags:
  - "armbian"
  - "dietpi"
  - "samba"
  - "ghostbsd"
  - "weston"
  - "composite"
  - "kde"
  - "seamonkey"
  - "syncstar"
  - "qemu"
  - "q4os"
  - "systemd"
  - "nginx"
  - "red hat"
  - "redhat"
  - "system76"
  - "dff"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20383.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Release of Armbian 24.8:](https://www.armbian.com/newsflash/armbian-24-8-yelt/)

- [Release of DietPi 9.7](https://dietpi.com/docs/releases/v9_7/)

- [Samba 4.21.0 Released:](https://lists.samba.org/archive/samba-announce/2024/000674.html)

- [GhostBSD 24.07.1 Released:](https://ghostbsd.org/news/GhostBSD_24.07.1_is_Now_Available)

- [Weston Composite Server 14.0 Released:](https://wayland.freedesktop.org/releases.html)

- [KDE Project Report 2023:](https://ev.kde.org/reports/ev-2023/)

- [SeaMonkey 2.53.19 Released:](https://www.seamonkey-project.org/news%232024-09-04)

- [QEMU 9.1.0 Available:](https://lists.nongnu.org/archive/html/qemu-devel/2024-09/msg00637.html)

- [SyncStar, a service for creating bootable USB drives:](https://fedoramagazine.org/introducing-syncstar/)

- [Release of Q4OS 5.6:](https://www.q4os.org/blog.html)

- [A systemd port for Musl library-based systems:](https://catfox.life/2024/09/05/porting-systemd-to-musl-libc-powered-linux/)

- [The Nginx project has moved to Git and GitHub:](https://mailman.nginx.org/pipermail/nginx-ru/2024-September/JO5YGKLOQA6RZ43PIDOEVFOHV4ON6RCM.html)

- [Red Hat Enterprise Linux AI distribution:](https://www.redhat.com/en/about/press-releases/red-hat-enterprise-linux-ai-now-generally-available-enterprise-ai-innovation-production)

- [System76 Partners with Digital Freedom Foundation for 2024 Celebration](https://digitalfreedoms.org/en/sfd)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
