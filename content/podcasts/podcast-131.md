---
title: "Full Circle Weekly News 131"
date: 2019-05-20
draft: false
tags:
  - "arch"
  - "attack"
  - "chromebooks"
  - "google"
  - "india"
  - "intel"
  - "kerala"
  - "kernel"
  - "linux"
  - "microsoft"
  - "ripper"
  - "whatsapp"
  - "zombieload"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20131.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Kernel Prior to 5.0.8 Vulnerable to Remote Code Execution](https://www.bleepingcomputer.com/news/security/linux-kernel-prior-to-508-vulnerable-to-remote-code-execution/)

- [WhatsApp Vulnerability Allows Hackers to Infect iPhones, Android Phones]([https://news.softpedia.com/news/whatsapp-vulnerability-allows-hackers-to-infect-iphones-android-phones-526019.shtml)

- [Intel CPUs Impacted by New Zombieload Side-channel Attack](https://www.zdnet.com/article/intel-cpus-impacted-by-new-zombieload-side-channel-attack/)

- [Google says All New Chromebooks with be Linux-ready](https://betanews.com/2019/05/12/linux-ready-chromebooks)

- [Microsoft Offers Unauthorized Arch Linux](https://www.pro-linux.de/news/1/27059/microsoft-vertreibt-unautorisiertes-arch-linux.html)

- [Linux-based OS is Saving $430 Million In Indian State of Kerala](https://fossbytes.com/linux-based-os-is-saving-430-million-in-indian-state-of-kerala/)

- [Clear Linux From Intel Brings Best Performance on Intel CPUs](https://fossbytes.com/clear-linux-intel-best-developer-performance/)

- ["John the Ripper" 1.9.0 released](https://www.pro-linux.de/news/1/27068/john-the-ripper-190-freigegeben.html)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
