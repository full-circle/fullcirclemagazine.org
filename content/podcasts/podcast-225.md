---
title: "Full Circle Weekly News 225"
date: 2021-08-29
draft: false
tags:
  - "anonymous"
  - "beta"
  - "chat"
  - "creator"
  - "delta"
  - "doglinux"
  - "editor"
  - "emulator"
  - "gnome"
  - "gnu"
  - "gtk"
  - "i2p"
  - "kernel"
  - "libreelec"
  - "linux"
  - "messenger"
  - "network"
  - "openshot"
  - "project"
  - "qemu"
  - "qt"
  - "seamonkey"
  - "taler"
  - "toolkit"
  - "ubuntu"
  - "video"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20225.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [GTK 4.4 graphical toolkit](https://blog.gtk.org/2021/08/23/gtk-4-4/)

- [GNOME 41 Beta Available](https://www.mail-archive.com/devel-announce-list@gnome.org/msg01058.html)

- [Release of the GNU Taler 0.8](https://taler.net/en/news/2021-09.html)

- Linux kernel turns 30

- [Release of QEMU Emulator 6.1](https://lists.nongnu.org/archive/html/qemu-devel/2021-08/msg04108.html)

- [SeaMonkey 2.53.9](https://blog.seamonkey-project.org/2021/08/26/seamonkey-2-53-9-is-out/)

- [Free OpenShot Video Editor 2.6.0 Released](https://www.openshot.org/blog/2021/08/25/new_openshot_release_260/)

- [The GNOME Project has launched an Application Web Directory](https://blogs.gnome.org/sophieh/2021/08/26/apps-gnome-org-is-online/)

- [Ubuntu 20.04.3 LTS Release with Graphics Stack and Linux Kernel Updates](https://lists.ubuntu.com/archives/ubuntu-announce/2021-August/000271.html)

- [DogLinux Build Update](https://debiandog.github.io/doglinux/)

- [Qt Creator 5.0 Released](https://www.qt.io/blog/qt-creator-5.0-released)

- [LibreELEC 10.0](https://libreelec.tv/2021/08/26/libreelec-matrix-10-0/)

- [New releases of anonymous network I2P 1.5.0](https://geti2p.net/en/blog/post/2021/08/23/1.5.0-Release)

- [Delta Chat 1.22 messenger is available](https://delta.chat/en/2021-08-24-updates)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
