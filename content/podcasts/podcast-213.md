---
title: "Full Circle Weekly News 213"
date: 2021-06-07
draft: false
tags:
  - "apache"
  - "centos"
  - "cinnamon"
  - "clonezilla"
  - "electron"
  - "jami"
  - "jingos"
  - "kali"
  - "netbeans"
  - "nginx"
  - "nix"
  - "nixos"
  - "obsstudio"
  - "openrgb"
  - "opensuse"
  - "peertube"
  - "tor"
  - "unit"
  - "util-linux"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20213.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Apache NetBeans 12.4 Released](https://blogs.apache.org/netbeans/entry/announce-apache-netbeans-12-4)

- [Electron 13.0.0](https://www.electronjs.org/blog/electron-13-0)

- [NGINX Unit 1.24.0 Released](https://mailman.nginx.org/pipermail/unit/2021-May/000265.html)

- [OpenRGB 0.6, RGB Device Control Toolkit](https://gitlab.com/CalcProgrammer1/OpenRGB/-/releases/release_0.6)

- [PeerTube 3.2 released](https://joinpeertube.org/news#release-3.2)

- [JingOS 0.9 available](https://forum.jingos.com/t/jingos-v0-9-release-and-super-early-birds-program/1572)

- [Util-linux 2.37 released](https://www.spinics.net/lists/util-linux-ng/msg16733.html)

- [Cinnamon 5.0 released](https://github.com/linuxmint/Cinnamon/releases/tag/5.0.0)

- [OBS Studio 27.0 Released](https://obsproject.com/blog/obs-studio-27-released)

- [OpenSUSE Leap 15.3 Released](https://www.opensuse.org/)

- [NixOS 21.05 distribution using Nix package manager](https://discourse.nixos.org/t/21-05-has-been-released/13407)

- [Tor Browser 10.0.17 and Tails 4.19 distribution](https://tails.boum.org/news/version_4.19/)

- [Clonezilla Live 2.7.2](https://sourceforge.net/p/clonezilla/news/2021/06/-stable-clonezilla-live-272-38-released-/)

- [Kali Linux 2021.2 Released](https://www.kali.org/blog/kali-linux-2021-2-release/)

- [CentOS Linux 8.4 (2105) Released](https://www.mail-archive.com/centos-announce@centos.org/msg11936.html)

- [Jami “Maloya” is available](https://jami.net/maloya-a-new-version-of-jami/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
