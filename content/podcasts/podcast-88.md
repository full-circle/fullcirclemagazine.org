---
title: "Full Circle Weekly News 88"
date: 2018-04-10
draft: false
tags:
  - "4-16"
  - "4-17"
  - "alterego"
  - "cpus"
  - "data"
  - "facebook"
  - "firefox"
  - "gaming"
  - "hack"
  - "kernel"
  - "leak"
  - "linus"
  - "linux"
  - "mit"
  - "mozilla"
  - "reality"
  - "safespaces"
  - "steam"
  - "torvalds"
  - "valve"
  - "vr"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2088.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [“Safespaces” Is The First Open Source VR Desktop Environment For Linux](https://fossbytes.com/safespaces-first-open-source-vr-desktop-linux/)

- [Mozilla Just Announced An Open Source Virtual Reality Browser: “Firefox Reality”](https://fossbytes.com/mozilla-firefox-reality-vr-ar-browser/)

- [MIT Researchers Create ‘AlterEgo’ Headset That Interprets User’s Thoughts](https://fossbytes.com/mit-researchers-alterego-headset-silent-speech/)

- [Valve insists there’s still life in Steam Machines and Linux gaming](https://www.techradar.com/news/valve-insists-theres-still-life-in-steam-machines-and-linux-gaming)

- [Linus Torvalds Releases Linux Kernel 4.16](https://fossbytes.com/linux-kernel-4-16-released-features-download/)

- [Linux 4.17 Shredding 500,000 Lines Of Code, Killing Support For Older CPUs](https://fossbytes.com/linux-kernel-dropping-support-older-cpus-reducing-size/)

- [Facebook Now Says Data Leak Affected 87 Million Users, Not 50 Million](https://fossbytes.com/facebook-cambridge-analytica-87-million-users-data/)
