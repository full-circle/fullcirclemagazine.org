---
title: "Full Circle Weekly News 382"
date: 2024-09-09
draft: false
tags:
  - "osmc"
  - "openbsd"
  - "calligra"
  - "office"
  - "veracrypt"
  - "mono"
  - "opentofu"
  - "screen"
  - "gnu"
  - "4mlinux"
  - "mediagoblin"
  - "ubuntu"
  - "elasticsearch"
  - "wireshark"
  - "rhino"
  - "scratch"
  - "linux"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20382.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Release of OSMC 2024.08-1:](https://osmc.tv/2024/08/osmcs-august-update-is-here-with-kodi-21-1/)

- [The OpenBSD project has made changes to the entire code base:](https://marc.info/?l%3Dopenbsd-cvs%26m%3D172443408727088%26w%3D2)

- [Release of the Calligra 4.0 office suite:](https://carlschwan.eu/2024/08/27/calligra-office-4.0-is-out/)

- [Release of VeraCrypt 1.26.14:](https://www.veracrypt.fr/en/Release%2520Notes.html)

- [Microsoft hands over Mono project development to Wine community:](https://www.mono-project.com/news/)

- [OpenTofu politics:](https://news.ycombinator.com/item?id=41382437)

- [Release of GNU screen 5.0:](http://savannah.gnu.org/news/?id%3D10668)

- [4MLinux 46.0 release:](https://4mlinux-releases.blogspot.com/2024/08/4mlinux-460-stable-released.html)

- [Release of MediaGoblin 0.14:](https://mediagoblin.org/news/mediagoblin-0.14.0-release.html)

- [Ubuntu 24.04.1 LTS Release:](https://lists.ubuntu.com/archives/ubuntu-announce/2024-August/000304.html)

- [Elasticsearch reverts to open source license:](https://www.elastic.co/blog/elasticsearch-is-open-source-again)

- [Wireshark 4.4.0 Released:](https://www.wireshark.org/news/20240828b.html)

- [Rhino Linux 2024.1 Rolling Release:](https://rhinolinux.org/news-15.html)

- [Linux From Scratch 12.2 and Beyond Linux From Scratch 12.2 Released:](https://www.linuxfromscratch.org/lfs/read.html) and [here](https://www.linuxfromscratch.org/blfs/read.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
