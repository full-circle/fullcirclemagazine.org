---
title: "Full Circle Weekly News 97"
date: 2018-06-23
draft: false
tags:
  - "committee"
  - "copperhead"
  - "eu"
  - "fluffychat"
  - "librem5"
  - "mycrofy"
  - "norman"
  - "touch"
  - "ubuntu"
  - "zombie"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2097.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Chinese Zombie Lanes for Phone Users](https://www.bbc.com/news/blogs-news-from-elsewhere-44383449)

- [Copperhead, Copper Dead](https://player.fm/series/series-2135928/copperheados-takeover-explained-ask-noah-show-71)

- [Where is Ubuntu Touch now?](https://github.com/ubports/ubuntu-touch/milestones)

- [FluffyChat](https://open-store.io/app/fluffychat.christianpauly)

- [Librem5 Propress Report 14](https://puri.sm/posts/librem5-progress-report-14/)

- [MyCroft on Ubuntu Touch](https://github.com/hummlbach/mycroft)

- [Psychopath AI: Norman](https://www.neowin.net/news/norman-is-a-psychopathic-ai-obsessed-with-murder-thanks-to-reddit)

- [Net Neutrality Has Officially Been Repealed](https://www.nytimes.com/2018/06/11/technology/net-neutrality-repeal.html)

- [Delete Article 13](https://ubports.com/blog/ubports-blog-1/post/deleteart13-147)

- [Today, an EU committee voted to destroy the internet. Now what?](https://boingboing.net/2018/06/20/no-surrender.html)

- [South African Linux Conference](https://mybroadband.co.za/news/software/264349-south-african-linux-and-postgres-conferences-planned-for-october.html)
