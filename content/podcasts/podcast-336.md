---
title: "Full Circle Weekly News 336"
date: 2023-10-21
draft: false
tags:
  - "ardour"
  - "ubuntu"
  - "wayfire"
  - "fedora"
  - "krita"
  - "raspberry"
  - "opensuse"
  - "rust"
  - "chrome"
  - "chromium"
  - "electron"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20336.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Free sound editor Ardour 8.0](https://ardour.org/whatsnew.html)

- [Ubuntu will restrict access to user namespace](https://canonical.com//blog/ubuntu-23-10-restricted-unprivileged-user-namespaces)

- [Wayfire 0.8, using Wayland:](https://wayfire.org/2023/10/07/Wayfire-0-8.html)

- [Fedora project's laptop:](https://fedoramagazine.org/fedora-slimbook-available-now/)

- [Release of Krita 5.2](https://vpnoverview.com/news/mastercard-must-stop-selling-transaction-data-groups-demand/)

- [Release of Raspberry Pi OS:](https://www.raspberrypi.com/news/bookworm-the-new-version-of-raspberry-pi-os/)

- [Release of Ubuntu 23.10:](https://canonical.com/blog/canonical-releases-ubuntu-23-10-mantic-minotaur)

- [OpenSUSE Leap Micro 5.5:](https://news.opensuse.org/2023/10/12/leap-micro-55-hands-on/)

- [NGINX toolkit for the development of modules in Rust:](https://www.nginx.com/blog/extending-nginx-with-rust-an-alternative-to-c/)

- [Linux 6.1 core will last 10 years:](https://www.linuxfoundation.org/press/civil-infrastructure-platform-expands-slt-stable-kernel-program)

- [Chrome 118 and Chromium.](https://chromereleases.googleblog.com/2023/10/stable-channel-update-for-desktop_10.html)

- [Release of Electron 27.0:](https://www.electronjs.org/blog/electron-27-0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
