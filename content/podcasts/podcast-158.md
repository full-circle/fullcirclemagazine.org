---
title: "Full Circle Weekly News 158"
date: 2019-12-22
draft: false
tags:
  - "ascii"
  - "cumulus"
  - "devuan"
  - "gnome"
  - "kali"
  - "linux"
  - "network"
  - "networks"
  - "privacy"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20158.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Gnome 3.35.2 Available for Testing](https://news.softpedia.com/news/gnome-3-36-desktop-environment-development-continues-with-the-second-snapshot-528304.shtml)

- [Cumulus Networks Unveils Its 4th-Generation of Modern Network Software](https://cumulusnetworks.com/about/press-releases/fourth-generation-launch/)

- [Devuan ASCII 2.1 released](https://devuan.org/os/debian-fork/ascii-point-release-announce-112119)

- [Zorin OS Responds to the Privacy Concerns](https://itsfoss.com/zorin-os-privacy-concerns/)

- [Kali Linux 2019.4 Released](https://www.kali.org/news/kali-linux-2019-4-release/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
