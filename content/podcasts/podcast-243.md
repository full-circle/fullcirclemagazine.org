---
title: "Full Circle Weekly News 243"
date: 2022-01-09
draft: false
tags:
  - "backup"
  - "busybox"
  - "cad"
  - "desktop"
  - "enlightenment"
  - "firmware"
  - "kernel"
  - "kicad"
  - "lkrg"
  - "lumina"
  - "module"
  - "nitrux"
  - "openrgb"
  - "remote"
  - "rescuezilla"
  - "sound"
  - "yourself"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20243.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Siduction 2021.3](https://siduction.org/2021/12/release-notes-for-siduction-2021-3-0-wintersky/)

- [Release of Rescuezilla 2.3 backup distribution](https://github.com/rescuezilla/rescuezilla/releases/tag/2.3)

- [KiCad 6.0 CAD released](https://www.kicad.org/blog/2021/12/KiCad-6.0.0-Release/)

- [Nitrux 1.8.0 Distribution Release with NX Deskto](https://nxos.org/changelog/release-announcement-nitrux-1-8-0/)

- [Lumina Desktop 1.6.2 Released](https://lumina-desktop.org/post/2021-12-25/)

- [Enlightenment 0.25 Released](https://www.enlightenment.org/news/2021-12-26-enlightenment-0.25.0)

- [Release of BusyBox 1.35](https://busybox.net/news.html)

- [Sound Open Firmware 2.0 is available](https://www.sofproject.org/blog/2021/12/17/sof-2-0-is-here/)

- [Second edition of Linux for Yourself](https://lx4u.ru/rel/stable/)

- [Release of the LKRG 0.9.2 module to protect against exploitation of vulnerabilities in the Linux kerne](https://www.openwall.com/lists/announce/2021/12/29/1)

- [Release of OpenRGB 0.7](https://gitlab.com/CalcProgrammer1/OpenRGB/-/releases/release_0.7)

- [The first stable release of the Linux Remote Desktop project](https://github.com/nubosoftware/linux-remote-desktop/releases/tag/0.9)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
