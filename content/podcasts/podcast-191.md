---
title: "Full Circle Weekly News 191"
date: 2020-11-22
draft: false
tags:
  - "20-04"
  - "cent"
  - "centos"
  - "debian"
  - "feren"
  - "flaw"
  - "framework"
  - "galago"
  - "intel"
  - "kde"
  - "linux"
  - "microcode"
  - "mx"
  - "os"
  - "pinephone"
  - "pro"
  - "proton"
  - "refresh"
  - "root"
  - "snapshot"
  - "system76"
  - "theme"
  - "ubuntu"
  - "update"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20191.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Getting Root on Ubuntu 20.04](https://securitylab.github.com/research/Ubuntu-gdm3-accountsservice-LPE)

- [Intel Vulnerabilities in All Supported Ubuntus](https://9to5linux.com/new-intel-vulnerabilities-now-patched-in-all-supported-ubuntu-releases)

- [Ubuntu Reverts Intel Microcode Flaws](https://9to5linux.com/canonical-reverts-intel-microcode-update-in-ubuntu-due-to-boot-failures-in-tiger-lake-systems)

- [KDE Announces a Pinephone and Framework and an Update](https://kde.org/announcements/kde-frameworks-5.76.0/)

- [Debian Has a New Theme](https://bits.debian.org/2020/11/homeworld-will-be-the-default-theme-for-debian-11.html)

- [Feren OS November Snapshot Out](https://medium.com/feren-os/the-feren-os-november-2020-snapshot-is-now-available-eeabf6806fb)

- [MX Linux 19.3 Out](https://mxlinux.org/blog/mx-19-3-now-available/)

- [CentOS 7.9 Out](https://lists.centos.org/pipermail/centos-announce/2020-November/035820.html)

- [Proton 5.13-2 Out](https://www.phoronix.com/scan.php?page=news_item&px=Proton-5.13-2-Released)

- [System76's Galago Pro Refresh Out](https://9to5linux.com/system76-launches-new-galago-pro-linux-laptop-with-11th-gen-intel-core-cpus)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
