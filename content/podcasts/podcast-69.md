---
title: "Full Circle Weekly News 69"
date: 2017-09-08
draft: false
tags:
  - "4-13"
  - "aardvark"
  - "android"
  - "artful"
  - "beta"
  - "google"
  - "kernel"
  - "linux"
  - "mac"
  - "market"
  - "oreo"
  - "share"
  - "suse"
  - "torvalds"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2069.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 17.10 Artful Aardvark Beta 1 is here](https://betanews.com/2017/09/01/ubuntu-17-10-artful-aardvark-beta-download-linux/)

- [Linux pioneer SUSE marks 25 years in the field](https://www.itwire.com/open-sauce/79739-)

- [With Android Oreo, Google is introducing Linux kernel requirements](https://betanews.com/2017/09/03/android-oreo-linux-kernel/)

- [Linux Kernel 4.13 Released By Linus Torvalds](https://fossbytes.com/linux-kernel-4-13-features-released/)

- [Linux Doubles Its Market Share Since 2015, Windows And Mac Adoption Slows Down](https://fossbytes.com/linux-market-share-double-windows-decrease/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
