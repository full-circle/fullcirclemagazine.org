---
title: "Full Circle Weekly News 02"
date: 2016-01-30
draft: false
tags:
  - "ai"
  - "azure"
  - "debian"
  - "jessie"
  - "kali"
  - "linux"
  - "malware"
  - "marvin-minsky"
  - "microsoft"
  - "ota-9"
  - "ota9"
  - "touch"
  - "trojan"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2002.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Snap-Happy Trojan Targets Linux Servers](http://linuxgizmos.com/emulator-brings-x86-linux-apps-to-arm-devices/)

- [Kali Linux Rolling Release](http://www.zdnet.com/article/hand-on-with-kali-linux-rolling/)

- [Debian 8.3 (Jessie) Officially Released](http://news.softpedia.com/news/debian-8-3-jessie-officially-released-499338.shtml)

- [Artificial intelligence luminary Marvin Minsky has died at 88](http://venturebeat.com/2016/01/25/artificial-intelligence-luminary-marvin-minsky-has-died-at-88/)

- [New Linux malware spotted](http://www.itproportal.com/2016/01/25/new-linux-malware-spotted/)

- [Ubuntu Touch OTA-9 Is Officially Released](http://news.softpedia.com/news/ubuntu-touch-ota-9-is-officially-released-499493.shtml)

- [Ubuntu and open source play a key role in Microsoft Azure Stack](http://betanews.com/2016/01/26/ubuntu-linux-open-source-microsoft-azure-stack-technical-preview/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
