---
title: "Full Circle Weekly News 268"
date: 2022-07-21
draft: false
tags:
  - "ubuntu"
  - "amd"
  - "communist"
  - "editor"
  - "endeavour"
  - "endeavouros"
  - "exim"
  - "fidelityfx"
  - "freebsd"
  - "gecko"
  - "geckolinux"
  - "kaos"
  - "kernel"
  - "linux"
  - "mail"
  - "messenger"
  - "nginx"
  - "p2p"
  - "pitivi"
  - "redbean"
  - "rust"
  - "server"
  - "shotcut"
  - "spiral"
  - "video"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20268.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [nginx 1.23.0 release:](http://nginx.org/#2022-06-21)

- [New Qt project leader appointed:](https://www.qt.io/blog/new-chief-maintainer-for-qt)

- [Rust support in the Linux 5.20 kernel:](https://www.phoronix.com/scan.php?page=news_item&px=Rust-For-Linux-5.20-Possible)

- [The GeckoLinux creator introduced SpiralLinux:](https://spirallinux.github.io/)

- [AMD Opens FidelityFX Super Resolution 2.0 Technology:](https://gpuopen.com/fsr2-source-available/)

- [KaOS 2022.06 distribution released:](https://kaosx.us/news/2022/kaos06/)

- [Communist 1.4 p2p-messenger Released:](https://forum.altlinux.org/index.php?topic=46108.msg371189#msg371189)

- [Pitivi Video Editor Release 2022.06:](https://github.com/pitivi/pitivi/releases/tag/2022.06.0)

- [EndeavourOS 22.6 published:](https://endeavouros.com/news/artemis-is-launched/)

- [Redbean 2.0, a platform for web applications packaged in a universal executable ZIP archive:](https://justine.lol/redbean2/)

- [Exim mail server 4.96:](https://lists.exim.org/lurker/message/20220625.141825.d6de6074.en.html)

- [Shotcut video editor release 22.06:](https://shotcut.org/blog/new-release-220623/)

- [Changes to containerd to allow Linux containers to run on FreeBSD:](https://github.com/containerd/containerd/pull/7000)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
