---
title: "Full Circle Weekly News 79"
date: 2018-02-05
draft: false
tags:
  - "18-04"
  - "4-15"
  - "firefox"
  - "kaos"
  - "linux"
  - "slack"
  - "snap"
  - "ubuntu"
  - "xorg"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2079.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Slack Snap Source](https://www.theinquirer.net/inquirer/news/3024814/slack-has-arrived-on-linux-thanks-to-canonical-snap)

- [Firefox 58 Source](https://www.bleepingcomputer.com/news/software/firefox-58-released-for-linux-mac-and-windows/)

- [Linux 4.15 Source](https://www.theregister.co.uk/2018/01/22/linus_4_15_needs_rc_9/)

- [KaOS Source](https://betanews.com/2018/01/20/kaos-kde-linux-spectre-meltdown/)

- [Xorg Default in Ubuntu 18.04 Source](https://insights.ubuntu.com/2018/01/26/bionic-beaver-18-04-lts-to-use-xorg-by-default/)
