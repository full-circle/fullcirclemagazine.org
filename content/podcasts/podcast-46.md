---
title: "Full Circle Weekly News 46"
date: 2016-12-17
draft: false
tags:
  - "budgie"
  - "canonical"
  - "container"
  - "core"
  - "coreos"
  - "enterprise"
  - "kernel"
  - "live"
  - "minimal"
  - "patch"
  - "torvalds"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2046.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical Outs Live Patch Kernel Update for Ubuntu 16.04 to Patch Security Flaws](http://news.softpedia.com/news/canonical-outs-live-patch-kernel-update-for-ubuntu-16-04-to-patch-security-flaws-510828.shtml)

- [“Ultra Minimal” Ubuntu Budgie Is Coming, Uses “220MB Or Less Of RAM”](https://fossbytes.com/ultra-minimal-ubuntu-budgie-220mb-ram/)

- [CoreOS renames core OS to Container Linux](http://sdtimes.com/coreos-renames-core-os-to-container-linux/)

- [Linus Torvalds says Linux 4.9 is ‘the biggest release’](http://sdtimes.com/linus-torvalds-linux-4-9-biggest-release/)

- [Enterprise Linux 7.1 meets NIST crypto standards](https://gcn.com/articles/2016/12/13/red-hat-linux-fips-140-2.aspx)

- [‘AI will replace 80% of IT helpdesk’](http://timesofindia.indiatimes.com/city/thiruvananthapuram/AI-will-replace-80-of-IT-helpdesk/articleshow/55916576.cms)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
