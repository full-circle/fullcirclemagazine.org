---
title: "Full Circle Weekly News 240"
date: 2021-12-21
draft: false
tags:
  - "apache"
  - "blender"
  - "blueprint"
  - "calculate"
  - "cambalache"
  - "endeavour"
  - "freebsd"
  - "gtk"
  - "kali"
  - "libreoffice"
  - "mesa"
  - "mobile"
  - "netbeans"
  - "nzyme"
  - "plasma"
  - "rhvoice"
  - "toolkit"
  - "tracking"
  - "veracrypt"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20240.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Code removal in Mesa](https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/10153)

- [Blender 3.0 Release](https://www.blender.org/press/blender-3-0-a-new-era-for-content-creation/)

- [Release of EndeavourOS 21.4](https://endeavouros.com/news/the-atlantis-release-is-in-orbit/)

- [Introducing Blueprint, a new UI building language for GTK](https://www.jwestman.net/2021/12/02/introducing-blueprint-a-new-way-to-craft-user-interfaces.html)

- [VeraCrypt 1.25.4 released](https://www.veracrypt.fr/en/Release%20Notes.html)

- [LibreOffice 7.2.4 and 7.1.8 Update with Vulnerability Fix](https://blog.documentfoundation.org/blog/2021/12/06/libreoffice-7-2-4-and-7-1-8-community/)

- [Apache NetBeans IDE 12.6 Released](https://blogs.apache.org/netbeans/entry/announce-apache-netbeans-12-6)

- [Nzyme 1.2.0, Wireless Attack Tracking Toolkit Available](https://www.nzyme.org/blog/nzyme-v1-2-0-peck-slip-has-been-released/)

- [FreeBSD 12.3 Released](https://www.freebsd.org/releases/12.3R/announce/)

- [RHVoice 1.6.0 speech synthesizer released](https://rhvoice.org/post/rhvoice-1.6.0/)

- [Calculate Linux 22 Distribution Released](https://forum.calculate-linux.org/t/calculate-linux-22/10870)

- [MariaDB significantly changes the release schedule](https://mariadb.com/resources/blog/delivering-faster-innovation-to-mariadbs-community)

- [KDE Plasma Mobile 21.12 Released](https://www.plasma-mobile.org/2021/12/07/plasma-mobile-gear-21-12/)

- [Kali Linux 2021.4 Release](https://www.kali.org/blog/kali-linux-2021-4-release/)

- [Cambalache 0.8.0, a GTK development tool released](https://blogs.gnome.org/xjuan/2021/12/09/cambalache-0-8-0-released/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
