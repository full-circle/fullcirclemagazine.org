---
title: "Full Circle Weekly News 245"
date: 2022-01-18
draft: false
tags:
  - "canonical"
  - "clonezilla"
  - "detection"
  - "easyos"
  - "kernel"
  - "library"
  - "libre"
  - "live"
  - "media"
  - "sdl"
  - "slackware"
  - "snapcraft"
  - "system"
  - "toolkit"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20245.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical Announces Snapcraft Toolkit Rework](https://snapcraft.io/blog/the-future-of-snapcraft)

- [Messor, a decentralized intrusion detection system released](https://messor.network/ru/)

- [Release of EasyOS 3.2](https://bkhome.org/news/202201/easyos-version-32-released.html)

- [Clonezilla Live 2.8.1 released](https://sourceforge.net/p/clonezilla/news/2022/01/stable-clonezilla-live-281-12-released-/)

- [A completely free version of the Linux-libre 5.16 kernel is available](https://www.fsfla.org/pipermail/linux-libre/2022-January/003466.html)

- [SDL 2.0.20 Media Library Release](https://libsdl.org/)

- [Third release candidate for Slackware Linux 15.0](http://www.slackware.com/changelog/current.php?cpu=x86_64)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
