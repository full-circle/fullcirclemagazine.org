---
title: "Full Circle Weekly News 224"
date: 2021-08-22
draft: false
tags:
  - "apache"
  - "browser"
  - "busybox"
  - "conferencing"
  - "distro"
  - "ebook"
  - "filter"
  - "filtering"
  - "git"
  - "libre"
  - "libreoffice"
  - "linux"
  - "manjaro"
  - "mate"
  - "nftables"
  - "office"
  - "openmeetings"
  - "openssh"
  - "packet"
  - "palemoon"
  - "pine64"
  - "pinenote"
  - "proton"
  - "rc"
  - "rspamd"
  - "server"
  - "slackware"
  - "source"
  - "spam"
  - "valve"
  - "web"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20226.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Apache OpenMeetings 6.1 web conferencing server released](https://blogs.apache.org/openmeetings/entry/openmeetings-v6-1-0-webrtc)

- [The PINE64 project introduced the PineNote e-book](https://www.pine64.org/2021/08/15/introducing-the-pinenote/)

- [Slackware 15 rc-1 Published](http://www.slackware.com/changelog/current.php?cpu=x86_64)

- [Git 2.33 source control released](https://lkml.org/lkml/2021/8/16/1323)

- [A new Pale Moon Browser version](https://forum.palemoon.org/viewtopic.php?t=27260&p=219173)

- [MATE 1.26 released](https://mate-desktop.org/blog/2021-08-08-mate-1-26-released/)

- [LibreOffice 7.2 Released](https://blog.documentfoundation.org/blog/2021/08/19/libreoffice-7-2-community/)

- [Rspamd 3.0 spam filtering available](https://rspamd.com/announce/2021/08/19/rspamd-3.0.html)

- [Manjaro Linux 21.1.0 Distribution Released](https://forum.manjaro.org/t/manjaro-21-1-0-pahvo-released/78663)

- [Release of BusyBox 1.34](https://busybox.net/news.html)

- [Nftables Packet Filter 1.0.0 Released](https://www.mail-archive.com/netfilter-announce@lists.netfilter.org/msg00231.html)

- [OpennSSH 8.7 released](https://lists.mindrot.org/pipermail/openssh-unix-dev/2021-August/039543.html)

- [Valve releases Proton 6.3-6](https://github.com/ValveSoftware/Proton/releases/tag/proton-6.3-6b)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
