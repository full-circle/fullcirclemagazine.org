---
title: "Full Circle Weekly News 12"
date: 2016-04-09
draft: false
tags:
  - "7-10"
  - "8-4"
  - "android"
  - "bluetooth"
  - "bsd"
  - "canonical"
  - "computer"
  - "conference"
  - "debian"
  - "europe"
  - "freebsd"
  - "germany"
  - "google"
  - "kernel"
  - "kodi"
  - "mini"
  - "patch"
  - "pi"
  - "raspberry"
  - "raspex"
  - "red-hat"
  - "redhat"
  - "rhel"
  - "ubucon"
  - "ubuntubsd"
  - "update"
  - "vocore"
  - "vulnerabilities"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2012.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [UbuCon Europe, the Very First European Ubuntu Conference, Announced for November](http://news.softpedia.com/news/ubucon-europe-the-very-first-european-ubuntu-conference-announced-for-november-502498.shtml)

- [Debian GNU/Linux 8.4 and 7.10 Officially Released](http://news.softpedia.com/news/debian-gnu-linux-8-4-jessie-and-7-10-wheezy-officially-released-502500.shtml)

- [RaspEX Linux for Raspberry Pi 3 Gets Kodi Media Center, and Bluetooth Support](http://news.softpedia.com/news/raspex-linux-for-raspberry-pi-3-gets-kodi-media-center-bluetooth-support-502439.shtml)

- [Red Hat offers free Red Hat Enterprise Linux to developers](http://www.zdnet.com/article/red-hat-offers-free-red-hat-enterprise-linux-to-developers/)

- [ubuntuBSD 15.10 Beta 3 Released, Brings Support for Virtual Text Consoles](http://linux.softpedia.com/blog/ubuntubsd-15-10-beta-3-released-brings-support-for-virtual-text-consoles-502503.shtml)

- [Get the VoCore Mini Linux Computer for $39](http://www.sitepoint.com/get-the-vocore-mini-linux-computer-for-39/)

- [Canonical Patches Six New Linux Kernel Vulnerabilities in Ubuntu 15.10 and 14.04](http://news.softpedia.com/news/canonical-patches-six-new-linux-kernel-vulnerabilities-in-ubuntu-15-10-and-14-04-502653.shtml)

- [Google issues biggest Android security update yet](http://www.infoworld.com/article/3051064/security/google-patches-bugs-mediaserver-in-android.html)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
