---
title: "Full Circle Weekly News 90"
date: 2018-04-23
draft: false
tags:
  - "android"
  - "apps"
  - "azure"
  - "linux"
  - "manjaro"
  - "microsoft"
  - "nextcloud"
  - "sphere"
  - "tracking"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2090.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Android’s trust problem isn’t getting better](https://www.theverge.com/2018/4/13/17233122/android-software-patch-trust-problem)

- [Thousands of Android Apps Are Tracking Kids Without Parental Consent](http://news.softpedia.com/news/thousands-of-android-apps-are-tracking-kids-without-parental-consent-520696.shtml)

- [Federal administration relies on Nextcloud](http://www.linux-magazin.de/news/bundesverwaltung-setzt-auf-nextcloud/)

- [Microsoft Creates Its Own Version of Linux For The First Time, Launches Azure Sphere OS](https://fossbytes.com/microsoft-introduces-azure-sphere-customized-linux-kernel/)

- [Suddenly the # 1: This Linux is even more popular than Ubuntu and Mint](http://www.chip.de/news/Manjaro-Dieses-Linux-ist-sogar-beliebter-als-Ubuntu-und-Mint_137914112.html)
