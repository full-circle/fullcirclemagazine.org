---
title: "Full Circle Weekly News 75"
date: 2017-12-30
draft: false
tags:
  - "17-10"
  - "anbox"
  - "atari"
  - "ataribox"
  - "bios"
  - "dolphins"
  - "exploit"
  - "lenovo"
  - "linux"
  - "monero"
  - "nsa"
  - "python"
  - "repo"
  - "snap"
  - "spotify"
  - "touch"
  - "ubports"
  - "universe"
  - "zealot"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2075.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical temporarily disables Ubuntu 17.10 download](https://www.neowin.net/news/canonical-temporarily-disables-ubuntu-1710-download)

- [Android Apps Will Soon Run On Ubuntu Touch With “Anbox”](https://fossbytes.com/android-apps-on-ubuntu-touch-mobile-os-anbox/)

- [Spotify now available as a Snap for Linux](https://betanews.com/2017/12/20/spotify-snap-linux/)

- [Ubuntu Devs Work on Demoting Python 2 to "Universe" Repo for Ubuntu 18.04](http://news.softpedia.com/news/ubuntu-devs-work-on-demoting-python-2-to-universe-repo-for-ubuntu-18-04-lts-518926.shtml)

- ["Zealot" Campaign Uses NSA Exploits to Mine Monero on Windows and Linux Servers](https://www.bleepingcomputer.com/news/security/-zealot-campaign-uses-nsa-exploits-to-mine-monero-on-windows-and-linux-servers/)

- [Scientists use artificial intelligence to eavesdrop on dolphins](http://www.independent.co.uk/news/science/artificial-intelligence-dolphin-eavesdrop-scientist-ai-gulf-mexico-a8099676.html)

- [With Ataribox, the legend returns -- powered by Linux](http://www.zdnet.com/article/with-ataribox-the-legend-returns-powered-by-linux/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
