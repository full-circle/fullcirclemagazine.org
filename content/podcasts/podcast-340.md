---
title: "Full Circle Weekly News 340"
date: 2023-11-19
draft: false
tags:
  - "libreboot"
  - "sail"
  - "omnios"
  - "openindiana"
  - "mineclonia"
  - "minetest"
  - "minecraft"
  - "fedora"
  - "redhat"
  - "gimp"
  - "touch"
  - "ota3"
  - "clonezilla"
  - "webos"
  - "backbox"
  - "gnome"
  - "vortex"
  - "ffmpeg"
  - "obs studio"

cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20340.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Libreboot 20231106:](https://libreboot.org/docs/hardware/d945gclf.html)

- [Release of SAIL 0.9.0:](https://github.com/HappySeaFox/sail/releases/tag/v0.9.0)

- [OmniOS CE r151048 and OpenIndiana 2023.10:](https://omnios.org/article/r48)

- [Release of Mineclonia 0.91 game created on Minetest engine](https://content.minetest.net/packages/ryvnf/mineclonia/)

- [Release of Fedora Linux 39:](https://fedoramagazine.org/announcing-fedora-linux-39/)

- [GIMP 2.10.36:](https://www.gimp.org/news/2023/11/07/gimp-2-10-36-released/)

- [Ubuntu Touch OTA-3 Focal:](https://ubports.com/en/blog/ubports-news-1/post/ubuntu-touch-ota-3-focal-release-3905)

- [Release of Clonezilla Live 3.1.1:](https://sourceforge.net/p/clonezilla/news/2023/11/stable-clonezilla-live-311-27-released-/)

- [LG has published webOS Open Source Edition 2.24:](https://www.webosose.org/blog/2023/11/08/webos-ose-2-24-0-release/)

- [GNOME project received a million euros for development:](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/)

- [Release of BackBox Linux 8.1:](http://linux.backbox.org/)

- [Available Vortex 2.0:](https://github.com/vortexgpgpu/vortex/releases/tag/v2.x)

- [Release of FFmpeg 6.1:](http://ffmpeg.org/download.html#releases)

- [OBS Studio 30.0:](https://obsproject.com/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
