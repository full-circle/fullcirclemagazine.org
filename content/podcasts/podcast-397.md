---
title: "Full Circle Weekly News 397"
date: 2025-01-18
draft: false
tags:
  - "openzfs"
  - "decibels"
  - "linux"
  - "microsoft"
  - "cudatext"
  - "mint"
  - "cozystack"
  - "haiku"
  - "uutils"
  - "dillo"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20397.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [MX Linux 23.5 Released:](https://mxlinux.org/blog/mx-23-5-now-available/)

- [OpenZFS 2.3.0 Release:](https://github.com/openzfs/zfs/releases/tag/zfs-2.3.0)

- [Linux 6.13 kernel has a bug caused by code from a Microsoft employee:](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id%3D2e45474ab14f0f17c1091c503a13ff2fe2a84486)

- [Decibels Music Player Accepted into Mainstream GNOME:](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/3354)

- [CudaText Code Editor Update 1.220.6:](https://cudatext.github.io/download.html)

- [Release of Linux Mint 22.1:](https://blog.linuxmint.com/?p%3D4793)

- [Cozystack 0.22 Released:](https://github.com/aenix-io/cozystack/releases/tag/v0.22.0)

- [Haiku to restrict UK access over Online Safety Act concerns:](https://russ.garrett.co.uk/2024/12/17/online-safety-act-guide/)

- [Release of uutils 0.0.29, a variant of GNU Coreutils in Rust:](https://github.com/uutils/coreutils/releases/tag/0.0.29)

- [Dillo 3.2.0 released:](https://dillo-browser.github.io/release/3.2.0/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
