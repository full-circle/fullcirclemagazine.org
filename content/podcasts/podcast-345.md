---
title: "Full Circle Weekly News 345"
date: 2023-12-24
draft: false
tags:
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20345.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Debian 12.4 update:](https://www.debian.org/News/2023/20231210)

- [Linux kernel 6.6.6 update:](https://lkml.org/lkml/2023/12/11/226)

- [Release of FreeRDP 3.0:](https://github.com/FreeRDP/FreeRDP/releases/tag/3.0.0)

- [Canonical has transferred the LXD project to the AGPLv3 license:](https://discourse.ubuntu.com/t/lxd-5-20-has-been-released/40865)

- [Not so free Ardour 8.2:](https://ardour.org/whatsnew.html)

- [The openSUSE logo debacle:](https://news.opensuse.org/2023/12/15/insights-from-the-os-logo-contest/)

- [Manjaro Linux 23.1 released:](https://forum.manjaro.org/t/manjaro-23-1-vulcan-released/153458)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
