---
title: "Full Circle Weekly News 22"
date: 2016-06-18
draft: false
tags:
  - "3-5"
  - "ai"
  - "alpha"
  - "app"
  - "black"
  - "bsd"
  - "council"
  - "infect"
  - "kde"
  - "lab"
  - "london"
  - "onyx"
  - "package"
  - "pclinuxos"
  - "public"
  - "ransomware"
  - "sector"
  - "snap"
  - "trinity"
  - "tv"
  - "ubuntubsd"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2022.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu’s snap apps are coming to distros everywhere](http://arstechnica.com/information-technology/2016/06/goodbye-apt-and-yum-ubuntus-snap-apps-are-coming-to-distros-everywhere/)

- [New ubuntuBSD Release Is Coming Soon](http://linux.softpedia.com/blog/new-ubuntubsd-release-is-coming-soon-based-on-ubuntu-16-04-lts-and-freebsd-10-3-505163.shtml)

- [Black Lab Linux 8.0 "Onyx" Alpha 4 Ready for Testing](http://news.softpedia.com/news/black-lab-linux-8-0-onyx-alpha-4-ready-for-testing-based-on-ubuntu-15-10-505165.shtml)

- [Android ransomware infects TVs](http://www.theregister.co.uk/2016/06/13/android_ransomware_infects_tvs/)

- [PCLinuxOS Trinity Brings Back Memories for KDE3.5 Fans](http://news.softpedia.com/news/pclinuxos-64-2016-05-trinity-linux-os-brings-back-old-memories-for-kde3-5-fans-505267.shtml)

- [London Council Deploys UK’s First Public Sector Artificial Intelligence Service](http://www.techweekeurope.co.uk/e-innovation/science/london-council-deploys-artificial-intelligence-193813)

- [Developer to port Ubuntu Touch onto OnePlus 3](http://www.techworm.net/2016/06/developer-port-ubuntu-touch-onto-oneplus-3.html)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
