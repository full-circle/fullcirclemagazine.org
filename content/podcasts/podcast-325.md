---
title: "Full Circle Weekly News 325"
date: 2023-08-06
draft: false
tags:
  - "debian"
  - "mpv"
  - "inkscape"
  - "pascalabc"
  - "network"
  - "toolkit"
  - "ubuntu"
  - "intel"
  - "overlayfs"
  - "wxmedit"
  - "kde"
  - "plasma"
  - "zorin"
  - "gnome"
  - "systemd"
  - "passim"
  - "4mlinux"
  - "touch"
  - "ubports"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20325.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Debian support for RISC-V architecture:](https://lists.debian.org/debian-riscv/2023/07/msg00053.html)

- [Release of MPV 0.36:](https://github.com/mpv-player/mpv/releases/tag/v0.36.0)

- [Inkscape 1.3:](https://inkscape.org/news/2023/07/23/inkscape-launches-version-13-focus-organizing-work/)

- [Release of PascalABC.NET 3.9.0 development environment:](https://pascalabcnet.github.io/mydoc_release_notes_3_9_0.html)

- [MLS proposed standard:](https://datatracker.ietf.org/doc/html/rfc9420)

- [Network Security Toolkit 38:](https://sourceforge.net/p/nst/news/2023/07/nst-version-38-13644-released/)

- [Ubuntu, optimized for Intel Core processors:](https://canonical.com/blog/optimised-real-time-ubuntu-is-now-generally-available-on-intel-socs)

- [OverlayFS in Ubuntu has led to vulnerabilities:](https://www.wiz.io/blog/ubuntu-overlayfs-vulnerability)

- [wxMEdit 3.2:](https://wxmedit.github.io/downloads.html)

- [KDE Plasma 6 plans to remove some features:](https://pointieststick.com/2023/07/26/what-we-plan-to-remove-in-plasma-6/)

- [Release of Zorin OS 16.3:](https://blog.zorin.com/2023/07/27/zorin-os-16.3-is-released/)

- [GNOME plans to change the window management model:](https://blogs.gnome.org/tbernard/2023/07/26/rethinking-window-management/)

- [Release of system manager systemd 254 with soft reboot support:](https://lists.freedesktop.org/archives/systemd-devel/2023-July/049310.html)

- [Passim caching the server:](https://blogs.gnome.org/hughsie/2023/07/28/introducing-passim/)

- [4MLinux 43.0:](https://4mlinux-releases.blogspot.com/2023/07/4mlinux-430-stable-released.html)

- [Ubuntu Touch OTA-2 Focal Firmware:](https://ubports.com/en/blog/ubports-news-1/post/ubuntu-touch-ota-2-focal-release-3894)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
