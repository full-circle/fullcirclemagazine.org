---
title: "Full Circle Weekly News 72"
date: 2017-11-11
draft: false
tags:
  - "ai"
  - "attack"
  - "botnet"
  - "browser"
  - "ddos"
  - "eol"
  - "ethical"
  - "foundation"
  - "hacking"
  - "iotroop"
  - "libreoffice"
  - "linux"
  - "parrot"
  - "patch"
  - "reaper"
  - "tor"
  - "tormoil"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2072.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

Apologies for the inconsistent releases recently. Real-life has been getting in the way. I'll keep the current name, but it will probably be more bi-weekly than weekly at the moment.

- [Tor Browser Users Urged to Patch Critical ‘TorMoil’ Vulnerability](https://threatpost.com/tor-browser-users-urged-to-patch-critical-tormoil-vulnerability/128769/)

- [Parrot 3.9 “Intruder” the Ethical Hacking Linux Distro Released With New Features](https://fossbytes.com/parrot-3-9-intruder-hacking-distro-download-features/)

- [DDoS Attacks Become More Frequent, with Linux Dominating](https://www.infosecurity-magazine.com/news/ddos-attacks-more-frequent/)

- [LibreOffice 5.3.7 Is the Last in the Series, End of Life Set for November 26](http://news.softpedia.com/news/libreoffice-5-3-7-is-the-last-in-the-series-end-of-life-set-for-november-26-518353.shtml)

- [IoTroop/Reaper: A Massive Botnet Cyberstorm Is Coming To Take Down The Internet](https://fossbytes.com/iotroop-reaper-botnet-attack/)

- [New project from The Linux Foundation hopes to make AI tools more accessible](https://www.geekwire.com/2017/new-project-linux-foundation-hopes-make-ai-tools-accessible/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
