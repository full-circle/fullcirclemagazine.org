---
title: "Full Circle Weekly News 49"
date: 2017-01-14
draft: false
tags:
  - "2016"
  - "4-8"
  - "4-9"
  - "ai"
  - "beta"
  - "civ"
  - "civ6"
  - "civilization-6"
  - "ibm"
  - "kernel"
  - "list"
  - "nouveau"
  - "ota-15"
  - "ota15"
  - "patent"
  - "powerpc"
  - "radeon"
  - "red-hat"
  - "redhat"
  - "steam"
  - "steamos"
  - "touch-firmware"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2049.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Firmware updates for Ubuntu phones on hold](http://www.itwire.com/mobility/76372-firmware-updates-for-ubuntu-phones-on-hold.html)

- [Artificial intelligence keeps IBM atop 2016 patent list](https://www.cnet.com/news/ibm-top-2016-us-patent-list-artificial-intelligence-intel-amazon/)

- [Linux Kernel 4.8 Reaches End of Life, Users Urged to Move to Linux 4.9 Series](http://news.softpedia.com/news/linux-kernel-4-8-reaches-end-of-life-users-urged-to-move-to-linux-4-9-series-511678.shtml)

- [‘Civilization 6’ Will Finally Arrive On Linux And SteamOS Very Soon](http://www.mobilenapps.com/articles/31114/20170111/civilization-6-will-finally-arrive-on-linux-and-steamos-very-soon.htm)

- [Red Hat Enterprise Linux 6.9 beta out now](http://www.zdnet.com/article/red-hat-enterprise-linux-6-9-beta-out-now/)

- [Linux Kernel 4.4.41 LTS Introduces Nouveau, Radeon, and PowerPC Improvements](http://news.softpedia.com/news/linux-kernel-4-4-41-lts-introduces-nouveau-radeon-and-powerpc-improvements-511719.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
