---
title: "Full Circle Weekly News 05"
date: 2016-02-20
draft: false
tags:
  - "android"
  - "biometric"
  - "blockapps"
  - "bug"
  - "canonical"
  - "consensys"
  - "debian"
  - "editing"
  - "eol"
  - "february"
  - "gnome"
  - "google"
  - "id"
  - "ios"
  - "kali"
  - "linux"
  - "lts"
  - "maps"
  - "meizu"
  - "openstreetmap"
  - "pro"
  - "pro5"
  - "reality"
  - "smartphone"
  - "tails"
  - "unix"
  - "virtual"
  - "vr"
  - "webkit"
  - "zfs"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2005.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Meizu PRO 5 comes to China and Europe](http://fullcirclemagazine.org/2016/02/17/meizu-pro-5-comes-to-china-and-europe-the-most-powerful-ubuntu-smartphone-launched-to-date/)

- [Debian 6.0 LTS to Reach End-of-Life in February this year](http://news.softpedia.com/news/debian-gnu-linux-6-0-lts-squeeze-to-reach-end-of-life-on-february-29-2016-500361.shtml)

- [Google’s VR headset won’t need a smartphone or PC](http://bgr.com/2016/02/12/google-android-vr-headset/)

- [Tails Will Keep You Anonymous Online](http://news.softpedia.com/news/tails-2-0-debian-based-linux-os-will-keep-you-anonymous-online-500385.shtml)

- [Now You Can Install Kali Linux On Android Smartphones](http://www.techworm.net/2016/02/how-to-install-kali-linux-on-android-smartphone.html)

- [Unix bug puts Linux, Android, and iOS systems at risk](http://www.infoworld.com/article/3033862/security/patch-now-unix-bug-puts-linux-android-and-ios-systems-at-risk.html)

- [GNOME Maps to Allow for OpenStreetMap Editing](http://news.softpedia.com/news/gnome-maps-3-20-to-allow-for-openstreetmap-editing-500387.shtml)

- [ConsenSys, BlockApps, and Canonical partner to deliver bio-ID tools on Ubuntu phones](http://www.biometricupdate.com/201602/consensys-blockapps-canonical-partner-to-deliver-biometric-digital-identity-tools-on-ubuntu-phones)

- [ZFS Will Be Baked Directly into Ubuntu 16.04 LTS and Supported by Canonical](http://news.softpedia.com/news/zfs-will-be-baked-directly-into-ubuntu-16-04-lts-and-supported-by-canonical-500493.shtml)

- [Linux distros aren't updating WebKit, making web browsers and email clients vulnerable](http://www.pcworld.com/article/3034014/linux/linux-distros-arent-updating-webkit-making-web-browsers-and-email-clients-vulnerable.html)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
