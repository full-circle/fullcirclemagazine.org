---
title: "Full Circle Weekly News 94"
date: 2018-05-22
draft: false
tags:
  - "android"
  - "asteroidos"
  - "canonical"
  - "cosmic"
  - "critical"
  - "cuttlefish"
  - "endless"
  - "flaw"
  - "malicious"
  - "package"
  - "pgp"
  - "snap"
  - "store"
  - "wear"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2094.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 18.10 Cosmic Cuttlefish May Include Out-of-the-box Android Integration](https://fossbytes.com/ubuntu-18-10-cosmic-cuttlefish-android-integration-gs-connect/)

- [AsteroidOS, An Open-source Wear OS Alternative, Launches First Stable Version](https://fossbytes.com/asteroidos-open-source-wear-os-alternative/)

- [Endless OS 3.4 Released With New Features, Linux 4.15, And Phone Companion App](https://fossbytes.com/endless-os-3-4-released-new-features-download/)

- [Malicious Package Found on the Ubuntu Snap Store](https://www.bleepingcomputer.com/news/linux/malicious-package-found-on-the-ubuntu-snap-store/)

- [Canonical Says There's No Rules Against Mining Cryptocurrencies through Snaps](https://news.softpedia.com/news/canonical-says-there-s-no-rules-against-mining-cryptocurrencies-through-snaps-521132.shtml)

- [Critical PGP Flaws Can Expose Encrypted Emails In “Plaintext” — Disable It Right Now](https://fossbytes.com/efail-vulnerabilities-openpgp-s-mime-expose-emails-plaintext/)
