---
title: "Full Circle Weekly News 09"
date: 2016-03-18
draft: false
tags:
  - "4-5"
  - "ai"
  - "arch"
  - "artificial"
  - "dell"
  - "developers"
  - "devs"
  - "exploit"
  - "go"
  - "google"
  - "intelligence"
  - "kernel"
  - "lee-sedol"
  - "linux"
  - "minecraft"
  - "ota-10"
  - "ota10"
  - "pi"
  - "pro"
  - "project"
  - "raspberry"
  - "sputnik"
  - "stagefright"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2009.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Torvalds wavers, pauses … then gives the world Linux 4.5](http://www.theregister.co.uk/2016/03/15/linux_4_5_released/)

- [Dell's Ubuntu-powered Precision Sputnik now available worldwide](http://www.theregister.co.uk/2016/03/11/dell_sputnik_precision_world_wide/)

- [Minecraft to run artificial intelligence experiments](http://www.bbc.com/news/technology-35778288)

- [Arch Linux Brought to Raspberry Pi 3 By RasphArch Project](http://www.mobipicker.com/arch-linux-brought-raspberry-pi-3-sbcs-raspharch-project/)

- [Google AI program beats South Korean Go pro](http://www.reuters.com/article/us-science-intelligence-go-idUSKCN0WH0XJ)

- [Ubuntu Touch Devs Have Finalized the New VPN Feature for the OTA-10 Update](http://news.softpedia.com/news/ubuntu-touch-devs-have-finalized-the-new-vpn-feature-for-the-ota-10-update-501834.shtml)

- [Millions of Android devices vulnerable to new Stagefright exploit](http://www.wired.co.uk/news/archive/2016-03/16/stagefright-android-real-world-hack)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
