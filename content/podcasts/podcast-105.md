---
title: "Full Circle Weekly News 105"
date: 2018-08-30
draft: false
tags:
  - "14-04"
  - "18-04"
  - "amdgpu"
  - "beta"
  - "bodhi"
  - "canonical"
  - "centos"
  - "debian"
  - "driver"
  - "flatpak"
  - "games"
  - "gnu"
  - "graphics"
  - "kernel"
  - "linux"
  - "lts"
  - "steam"
  - "stretch"
  - "ubuntu"
  - "valve"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20105.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Bodhi Linux 5.0.0 now available with Ubuntu 18.04 base](https://betanews.com/2018/08/22/bodhi-linux-5/)

- [Valve’s new Steam Play beta boosts Linux as a rival to Windows PCs for gamers](https://www.geekwire.com/2018/valves-new-steam-play-beta-boosts-linux-rival-windows-pcs-gamers/)

- [Flatpak 1.0 released, aims to simplify installation of Linux apps](https://liliputing.com/2018/08/flatpak-1-0-released-aims-to-simplify-installation-of-)

- [Canonical Apologizes for Ubuntu 14.04 LTS Linux Kernel Regression, Releases Fix](https://news.softpedia.com/news/canonical-apologizes-for-ubuntu-14-04-lts-linux-kernel-regression-releases-fix-522360.shtml)

- [Debian GNU/Linux 9 "Stretch" Receives L1 Terminal Fault Mitigations, Update Now](https://news.softpedia.com/news/debian-gnu-linux-9-stretch-receives-l1-terminal-fault-mitigations-update-now-522361.shtml)

- [AMDGPU-PRO 18.30 Linux Graphics Driver Released with Ubuntu 18.04 LTS and RHEL / CentOS Support](https://appuals.com/amdgpu-pro-18-30-linux-graphics-driver-released-with-ubuntu-18-04-lts-and-rhel-centos-support/)
