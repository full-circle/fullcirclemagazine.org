---
title: "Full Circle Weekly News 359"
date: 2024-03-30
draft: false
tags:
  - "gnucobol"
  - "cobol"
  - "microsoft"
  - "garnet"
  - "gnome"
  - "wayland"
  - "redis"
  - "dbms"
  - "red hat"
  - "redhat"
  - "redict"
  - "nintendo"
  - "kde"
  - "theme"
  - "etcd"
  - "palemoon"
  - "wesnoth"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20359.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [GnuCOBOL compiler and the SuperBOL development environment:](https://ftp.heanet.ie/mirrors/fosdem-video/2024/h2215/fosdem-2024-3249-gnucobol-the-free-industrial-ready-alternative-for-cobol-.av1.webm)

- [Microsoft open sourced Garnet storage:](https://www.microsoft.com/en-us/research/blog/introducing-garnet-an-open-source-next-generation-faster-cache-store-for-accelerating-applications-and-services/)

- [GNOME 46 Published:](https://foundation.gnome.org/2024/03/20/introducing-gnome-46/)

- [Release of Wayland-Protocols 1.34:](https://lists.freedesktop.org/archives/wayland-devel/2024-March/043537.html)

- [The Redis DBMS is moving to a proprietary license:](https://redis.com/blog/redis-adopts-dual-source-available-licensing/)

- [Red Hat introduced Nova:](https://lists.freedesktop.org/archives/dri-devel/2024-March/446709.html)

- [The Redict project was founded:](https://codeberg.org/redict/redict)

- [Nintendo being asshats again:](https://twitter.com/MrSujano/status/1770896278165004294)

- [Incident with KDE theme deleting user files:](https://floss.social/@kde/112128243960545659)

- [First alpha release of etcd operator for Kubernetes:](https://github.com/aenix-io/etcd-operator/releases/tag/v0.0.1)

- [Palemoon plans to increase CPU requirements:](https://forum.palemoon.org/viewtopic.php?t%3D30971%26p%3D249944%23p249944)

- [Battle for Wesnoth 1.18:](http://wesnoth.org/start/1.18/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
