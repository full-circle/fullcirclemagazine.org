---
title: "Full Circle Weekly News 327"
date: 2023-08-17
draft: false
tags:
  - "lxd"
  - "linux"
  - "vcmi"
  - "rhino"
  - "networkmanager"
  - "youtube-dl"
  - "outwiker"
  - "rocky"
  - "oracle"
  - "suse"
  - "quake"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20327.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [LXD and the Linux Containers community:](https://linuxcontainers.org/incus/)

- [Release of VCMI 1.3.0:](https://vcmi.eu/news/vcmi-1.3.0-release/)

- [Rhino Linux is available:](https://rhinolinux.org/news-6.html)

- [Release of NetworkManager 1.44.0:](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/tags/1.44.0)

- [Recording companies block Youtube-dl:](https://torrentfreak.com/youtube-dl-site-goes-offline-as-hosting-provider-enforces-court-ordered-ban-230809/)

- [Release of outWiker 3.2 notes:](https://jenyay.net/Soft/Outwiker)

- [Rocky Linux, Oracle and SUSE have created a joint repository:](https://openela.org/news/hello_world/)

- [Quake II re-release open:](https://github.com/id-Software/quake2-rerelease-dll)

- [Release of Ubuntu 22.04.3 LTS:](https://lists.ubuntu.com/archives/ubuntu-announce/2023-August/000294.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
