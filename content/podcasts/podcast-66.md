---
title: "Full Circle Weekly News 66"
date: 2017-07-08
draft: false
tags:
  - "16-10"
  - "bothanspy"
  - "cia"
  - "crypto"
  - "eol"
  - "gnome"
  - "gnupg"
  - "gyrfalcon"
  - "kernel"
  - "library"
  - "linux"
  - "mint"
  - "openssh"
  - "outlawcountry"
  - "shell"
  - "sonya"
  - "ssh"
  - "unity"
  - "updates"
  - "wikileaks"
  - "yak"
  - "yakkety"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2066.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 16.10 (Yakkety Yak) Operating System Reaches End of Life on July 20, 2017](Yakkety Yak)

- [Ubuntu Developer Shares His Thoughts on the Unity to GNOME Shell Transition](http://news.softpedia.com/news/ubuntu-developers-shares-his-thoughts-on-the-unity-to-gnome-shell-transition-516798.shtml)

- [Linux Mint 18.2 ‘Sonya’ Released](https://fossbytes.com/linux-mint-18-2-sonya-released-download-cinnamon-kde-xfce-and-mate-editions-now/)

- [WikiLeaks Exposes CIA Targeting Linux Users With OutlawCountry Network Traffic Re-Routing Tool](https://hothardware.com/news/wikileaks-exposes-cia-targeting-linux-users-with-outlawcountry-network-traffic-re-routing-tool)

- [Linux just got one of its biggest kernel updates yet says Linus Torvalds](http://www.zdnet.com/article/linux-just-got-one-of-its-biggest-kernel-updates-yet-says-linus-torvalds/)

- [GnuPG crypto library cracked, look for patches](https://www.theregister.co.uk/2017/07/04/gnupg_crypto_library_cracked_look_for_patches/)

- [BothanSpy & Gyrfalcon: CIA Malware To Steal SSH Credentials From Windows & Linux PCs](https://fossbytes.com/bothanspy-gyrfalcon-cia-malware-ssh-hacking-windows-linux/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
