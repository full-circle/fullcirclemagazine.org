---
title: "Full Circle Weekly News 273"
date: 2022-08-05
draft: false
tags:
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20273.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [NsCDE 2.2 Released:](https://github.com/NsCDE/NsCDE/releases/tag/2.2)

- [D-Installer 0.4 for SUSE:](https://yast.opensuse.org/blog/2022-07-19/yast-report-2022-5)

- [Fedora intends to ban the supply of software distributed under the CC0 license:](https://lists.fedoraproject.org/archives/list/legal@lists.fedoraproject.org/thread/RRYM3CLYJYW64VSQIXY6IF3TCDZGS6LM/)

- [Release of CDE 2.5.0:](https://sourceforge.net/p/cdesktopenv/mailman/message/37684830/)

- [Debian sued the domain debian.community, which published a critique of the project:](https://suicide.fyi/debian/urgent-domain-stolen-use-new-url-asap-new-debian-private-leaks/)

- [Latte Dock announced the termination of the project:](https://psifidotos.blogspot.com/2022/07/latte-dock-farewell.html)

- [OpenMandriva starts testing the OpenMandriva Lx ROME rolling:](https://www.openmandriva.org/en/news/article/openmandriva-lx-rome-rolling-technical-preview)

- [Updating ClamAV 0.103.7, 0.104.4 and 0.105.1:](https://blog.clamav.net/2022/07/clamav-01037-01041-and-01051-patch.html)

- [Fedora Linux 37 plans to stop supporting robotics, games etc:](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/YXENHLWNVIAOYQQ746KZYRIXYQW6E2FA/)

- [Vulnerability in Samba:](https://www.samba.org/samba/latest_news.html#4.16.4)

- [Ventoy 1.0.79:](https://github.com/ventoy/Ventoy/releases/tag/v1.0.79)

- [Release of OPNsense 22.7:](https://forum.opnsense.org/index.php?topic=29507.0)

- [Release of FreeRDP 2.8.0:](ttps://github.com/FreeRDP/FreeRDP/releases/tag/2.8.0)

- [Release paperless-ngx 1.8.0:](https://github.com/paperless-ngx/paperless-ngx/)

- [4MLinux 40.0:](https://4mlinux-releases.blogspot.com/2022/07/4mlinux-400-stable-released.html)

- [Linux Mint 21:](https://blog.linuxmint.com/?p=4358)

- [Release of Q4OS 4.10:](https://www.q4os.org/blog.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
