---
title: "Full Circle Weekly News 352"
date: 2024-02-11
draft: false
tags:
  - "helios"
  - "shotcut"
  - "touch"
  - "ubports"
  - "browser"
  - "libreoffice"
  - "onlyoffice"
  - "mesa"
  - "dsl"
  - "small"
  - "linux"
  - "easyos"
  - "gentoo"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20352.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Helios distribution based on Illumos:](https://github.com/oxidecomputer/helios/pull/136)

- [Release of Shotcut 24.01:](https://shotcut.org/blog/new-release-240128/)

- [Firmware release for Ubuntu Touch OTA-4 Focal:](https://ubports.com/en/blog/ubports-news-1/post/ubuntu-touch-ota-4-focal-release-3916)

- [Pale Moon Browser 33.0.0 Released:](https://forum.palemoon.org/viewtopic.php?t%3D30803%26p%3D248017%23p248017)

- [Release of LibreOffice 24.2:](https://blog.documentfoundation.org/blog/2024/01/31/libreoffice-24-2/)

- [ONLYOFFICE 8.0 office suite published:](https://www.onlyoffice.com/blog/2024/01/onlyoffice-docs-8-0-released)

- [Release of Mesa 24.0:](https://lists.freedesktop.org/archives/mesa-dev/2024-February/226138.html)

- [Damn Small Linux 2024:](https://www.damnsmalllinux.org/)

- [Release of EasyOS 5.7:](https://bkhome.org/news/202402/easyos-kirkstone-series-version-57-released.html)

- [Gentoo has started creating binary packages for the x86-64-v3 architecture:](https://www.gentoo.org/news/2024/02/04/x86-64-v3.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
