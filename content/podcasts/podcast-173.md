---
title: "Full Circle Weekly News 173"
date: 2020-06-02
draft: false
tags:
  - "beta"
  - "emmabuntus"
  - "fedora"
  - "github"
  - "java"
  - "kernel"
  - "libre"
  - "libreoffice"
  - "linux"
  - "npm"
  - "office"
  - "pinebook"
  - "postfix"
  - "radio"
  - "rc7"
  - "shartwave"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20173.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [March Update on Pinebook Pro](https://www.pine64.org/2020/03/15/march-update-manjaro-on-pinebook-pro-pinephone-software/)

- [GitHub to Acquire NPM](https://www.itprotoday.com/devops-and-software-development/github-acquire-npm-javascript-package-manager)

- [Shortwave Radio 1.0 Out](https://www.linuxuprising.com/2020/03/shortwave-internet-radio-player-sees.html)

- [Emmabuntus 1.01 Out](https://emmabuntus.org/on-march-16th-2020-emma-de3-debian-10-buster-the-foxy-distro/)

- [Fedora 32 Beta Out](https://fedoramagazine.org/announcing-the-release-of-fedora-32-beta/)

- [LibreOffice 6.4.2 Out](https://9to5linux.com/libreoffice-6-4-2-released-with-more-than-90-fixes)

- [Java 14 Out](https://www.itprotoday.com/programming-languages/java-14-improves-runtime-visibility-overall-performance)

- [Linux Kernel 5.6 rc7 Out](https://lkml.org/lkml/2020/3/22/419)

- [Postfix 3.5.0 Out](https://www.mail-archive.com/postfix-announce@postfix.org/msg00063.html)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
