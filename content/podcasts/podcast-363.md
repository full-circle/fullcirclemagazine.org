---
title: "Full Circle Weekly News 363"
date: 2024-04-28
draft: false
tags:
  - "servo"
  - "mozilla"
  - "descent"
  - "gnu"
  - "gentoo"
  - "virtualbox"
  - "lxqt"
  - "miracle"
  - "torvalds"
  - "debian"
  - "thunderbird"
  - "microsoft"
  - "exchange"
  - "niri"
  - "kata"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20363.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The Servo browser engine & Mozilla SpiderMonkey:](https://servo.org/blog/2024/04/15/spidermonkey/)

- [Descent 3 open source:](https://translate.google.com/website?sl=auto&tl=en&hl=en-US&u=https://github.com/kevinbentley/Descent3)

- [Release of GNU Taler 0.10:](https://taler.net/en/news/2024-06.html)

- [The Gentoo Project has banned the adoption of changes prepared using AI tools:](https://www.mail-archive.com/gentoo-dev@lists.gentoo.org/msg99327.html)

- [VirtualBox 7.0.16 released:](https://www.mail-archive.com/vbox-announce@virtualbox.org/msg00230.html)

- [LXQt 2.0.0 desktop environment available:](https://lxqt-project.org/release/2024/04/15/release-lxqt-2-0-0/)

- [Release of video player MPV 0.38:](https://github.com/mpv-player/mpv/releases/tag/v0.38.0)

- [Linus Torvalds spoke out against Kconfig parsers that do not support tabs:](https://lore.kernel.org/lkml/CAHk-%3Dwgw2NW5tar-Xew614JZPKfyTdet5fC0mgwK%2B2sUsZ0Ekw@mail.gmail.com/)

- [miracle-wm 0.2:](https://discourse.ubuntu.com/t/release-v0-2-0-of-miracle-wm-a-wayland-compositor-built-on-mir/44322)

- [New Debian Project Leader Elected:](https://lists.debian.org/debian-vote/2024/04/msg00063.html)

- [Thunderbird & Microsoft Exchange:](https://blog.thunderbird.net/2024/04/adventures-in-rust-bringing-exchange-support-to-thunderbird/)

- [Release of the Niri 0.1.5:](https://github.com/YaLTeR/niri/releases/tag/v0.1.5)

- [Release of Kata Containers 3.4:](https://github.com/kata-containers/kata-containers/releases/tag/3.4.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
