---
title: "Full Circle Weekly News 264"
date: 2022-07-16
draft: false
tags:
  - "ubuntu"
  - "almalinux"
  - "alpine"
  - "budgie"
  - "clonezilla"
  - "deepmind"
  - "desktop"
  - "display"
  - "firefox"
  - "kde"
  - "kernel"
  - "laptop"
  - "linuxfx"
  - "lotus"
  - "mir"
  - "mujoco"
  - "nginx"
  - "perf"
  - "physics"
  - "pipewire"
  - "plasma"
  - "pop!_os"
  - "pulseaudio"
  - "roadmap"
  - "simh"
  - "simulator"
  - "subsystem"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20264.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [SIMH simulator license dispute:](https://groups.io/g/simh/topic/new_license/91108560)

- [Vulnerability in the Linux perf kernel subsystem:](https://www.openwall.com/lists/oss-security/2022/05/20/2)

- [HP has announced a laptop that comes with Pop!_OS:](https://hpdevone.com/)

- [Ubuntu 22.10 will move to audio processing with PipeWire instead of PulseAudio:](https://discourse.ubuntu.com/t/pipewire-as-a-replacement-for-pulseaudio/28489/3)

- [Lotus 1-2-3 ported to Linux:](https://lock.cmpxchg8b.com/linux123.html)

- [KDE Plasma 5.25 desktop testing:](https://kde.org/announcements/plasma/5/5.24.90/)

- [DeepMind Opens Code for MuJoCo Physics Simulator:](https://www.deepmind.com/blog/open-sourcing-mujoco)

- [Alpine Linux 3.16:](https://alpinelinux.org/posts/Alpine-3.16.0-released.html)

- [nginx 1.22.0 released:](http://nginx.org/#2022-05-24)

- [Clonezilla Live 3.0.0 released:](https://sourceforge.net/p/clonezilla/news/2022/05/stable-clonezilla-live-300-26-released/)

- [Mir 2.8 display server released:](https://discourse.ubuntu.com/t/mir-release-2-8-0/28581)

- [Roadmap for Budgie's user environment:](https://blog.buddiesofbudgie.org/state-of-the-budgie-may-2022/)

- [Release of the anonymous network I2P 1.8.0 and the C++ client i2pd 2.42:](https://github.com/PurpleI2P/i2pd/releases/tag/2.42.0)

- [AlmaLinux 9.0 distribution available:](https://almalinux.org/blog/almalinux-9-now-available/)

- [Ubuntu developers begin to solve problems with the slow Firefox snap:](https://ubuntu.com/blog/how-are-we-improving-firefox-snap-performance-part-1)

- [A hardwired password revealed in Linuxfx:](https://kernal.eu/posts/linuxfx/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
