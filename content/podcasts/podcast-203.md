---
title: "Full Circle Weekly News 203"
date: 2021-04-02
draft: false
tags:
  - "7zip"
  - "amd-pangolin"
  - "audacity"
  - "beta"
  - "collabora"
  - "conference"
  - "debian"
  - "driver"
  - "fedora"
  - "firefox"
  - "fsf"
  - "gnome"
  - "gpu"
  - "graphics"
  - "krita"
  - "libreelec"
  - "limnux"
  - "manjaro"
  - "matrix"
  - "nvidia"
  - "opensuse"
  - "ota-16"
  - "ota16"
  - "panvk"
  - "port"
  - "stallman"
  - "system-76"
  - "system76"
  - "testing"
  - "touch"
  - "ubports"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20203.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Testing Week Is Here](https://popey.com/blog/2021/03/ubuntu-21.04-testing-week/)

- [7zip Sees its First Official Linux Port](https://sourceforge.net/p/sevenzip/discussion/45797/thread/cec5e63147/)

- [openSUSE Virtual Conference in June](https://events.opensuse.org/conferences/oSVC21)

- [Richard Stallman Returns to the Free Software Foundation](https://arstechnica.com/tech-policy/2021/03/richard-stallman-returns-to-fsf-18-months-after-controversial-rape-comments/)

- [Collabora Releases PanVk](https://www.collabora.com/news-and-blog/news-and-events/panvk-an-open-source-vulkan-driver-for-arm-mali-midgard-and-bifrost-gpus.html)

- [Ubuntu Touch OTA 16 Out](https://ubports.com/blog/ubport-blogs-news-1/post/ubuntu-touch-ota-16-release-3744)

- [Manjaro 21.0 Ornara RC1 Out](https://forum.manjaro.org/t/manjaro-21-0-rc1-ornara-released/58586)

- [Debian 10.9 Out](https://www.debian.org/News/2021/20210327)

- [Fedora 34 Beta Out](https://fedoramagazine.org/announcing-fedora-34-beta/)

- [Gnome 40 Out](https://foundation.gnome.org/2021/03/24/gnome-40-release/)

- [LibreELEC Matrix 10.0 Beta 1 Out](https://libreelec.tv/2021/03/libreelec-leia-10-0-b1/)

- [Firefox 87 Out](https://www.mozilla.org/en-US/firefox/87.0/releasenotes/?scrolla=5eb6d68b7fedc32c19ef33b4)

- [Audacity 3.0 Out](https://www.audacityteam.org/audacity-3-0-0-released/)

- [Krita 4.4.3 Out](https://krita.org/en/item/krita-4-4-3-released/)

- [All-AMD Pangolin Available from System76](https://system76.com/laptops/pangolin)

- [Nvidia Graphics Driver 460.67 Out](https://www.nvidia.com/Download/driverResults.aspx/171392/en-us)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
