---
title: "Full Circle Weekly News 313"
date: 2023-05-14
draft: false
tags:
  - "vmware"
  - "photon"
  - "cisco"
  - "clamav"
  - "openmoonray"
  - "xorg"
  - "obs"
  - "studio"
  - "pulse"
  - "browser"
  - "engine"
  - "amazon"
  - "doglinux"
  - "sftp"
  - "server"
  - "sftpgo"
  - "sel4"
  
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20313.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [VMware's Photon OS 5.0:](https://github.com/vmware/photon/releases/tag/5.0-GA)

- [Cisco has released ClamAV 1.1.0:](https://blog.clamav.net/2023/05/clamav-110-released.html)

- [Release of the OpenMoonRay 1.1 by Dreamworks studio:](https://github.com/dreamworksanimation/openmoonray/discussions/102)

- [X.Org project to stop supporting 20 outdated libraries and utilities:](https://lists.x.org/archives/xorg-devel/2023-May/059018.html)

- [Release of OBS Studio 29.1:](https://obsproject.com/blog/obs-studio-29-release-notes)

- [Project Pulse Browser:](https://github.com/pulse-browser/browser/releases/tag/1.0.0-a.64)

- [Open 3D Engine 23.05, opened by Amazon:](https://www.o3de.org/blog/posts/23-05-release/)

- [New DogLinux Release:](https://gumanzoy.blogspot.com/2023/05/eng-liveusb-doglinux-debian-12-bookworm.html)

- [SFTP server release SFTPGo 2.5.0:](https://github.com/drakkan/sftpgo/releases/tag/v2.5.0)

- [The seL4 project was awarded the ACM Software System Award:](https://www.acm.org/media-center/2023/may/technical-awards-2022)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
