---
title: "Full Circle Weekly News 137"
date: 2019-07-02
draft: false
tags:
  - "32-bit"
  - "alpha"
  - "bug"
  - "desktop"
  - "fixes"
  - "iot"
  - "kde"
  - "leap"
  - "libreelec"
  - "linux"
  - "malware"
  - "opensuse"
  - "pi"
  - "plasma"
  - "raspberry"
  - "silex"
  - "support"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20137.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Raspberry Pi 4 On Sale Now from $35](https://www.raspberrypi.org/blog/raspberry-pi-4-on-sale-now-from-35/)

- [LibreELEC 9.2 ALPHA1 \[Ships\] with Raspberry Pi 4B Support](https://libreelec.tv/2019/06/libreelec-9-2-alpha1-rpi4b/)

- [Ubuntu Decides to Keep Supporting Selected 32-bit LIbs After Developer Outrage](https://itsfoss.com/ubuntu-19-10-drops-32-bit-support/)

- [openSUSE Leap 42.3 Linux OS Reached End of Life](https://news.softpedia.com/news/opensuse-leap-42-3-linux-os-reaches-end-of-life-upgrade-to-opensuse-leap-15-now-526565.shtml)

- [KDE Plasma 5.16.2 Desktop Environment Released with More Than 30 Bug Fixes](https://news.softpedia.com/news/kde-plasma-5-16-2-desktop-environment-released-with-more-than-30-bug-fixes-526523.shtml)

- [Thousands of IoT Devices Bricked by Silex Malware](https://threatpost.com/thousands-of-iot-devices-bricked-by-silex-malware/146065/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
