---
title: "Full Circle Weekly News 334"
date: 2023-10-14
draft: false
tags:
  - "wolvic"
  - "kaos"
  - "librepcb"
  - "gnu"
  - "porteus"
  - "mint"
  - "debian"
  - "crossover"
  - "redhat"
  - "bugzilla"
  - "jira"
  - "cosmic"
  - "ubuntu"
  - "snap"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20334.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Wolvic 1.5 light:](https://wolvic.com/blog/release_1.5/)

- [KaOS 2023.09:](https://kaosx.us/news/2023/kaos09/)

- [LibrePCB reaches 1.0:](https://librepcb.org/blog/2023-09-24_release_1.0.0/)

- [GNU Project is 40 years old:](https://www.gnu.org/gnu/initial-announcement.html)

- [Release of Porteus 5.01:](https://forum.porteus.org/viewtopic.php?t=11004&p=95911)

- [Linux Mint Debian Edition 6:](https://blog.linuxmint.com/?p=4570)

- [Release of CrossOver 23.5:](https://www.codeweavers.com/support/forums/announce/?t=24;msg=289484)

- [Red Hat moves from Bugzilla Error Tracking System to JIRA:](https://www.redhat.com/en/blog/rhel-tracking-moving-to-jira)

- [COSMIC's replacement of windows:](https://blog.system76.com/post/cosmic-september-new-window-swapping-mode)

- [Ubuntu Snap Store identified malicious packages:](https://forum.snapcraft.io/t/temporary-suspension-of-automatic-snap-registration-following-security-incident/37077)

- [New release of auto-cpufreq 2.0:](https://github.com/AdnanHodzic/auto-cpufreq/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
