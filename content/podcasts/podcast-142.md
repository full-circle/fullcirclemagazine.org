---
title: "Full Circle Weekly News 142"
date: 2019-08-12
draft: false
tags:
  - "16-04"
  - "18-04"
  - "19-2"
  - "5-1"
  - "5-17"
  - "5-2"
  - "canonical"
  - "debconf20"
  - "desktop"
  - "gnome"
  - "kde"
  - "kernel"
  - "linux"
  - "lite"
  - "mint"
  - "patch"
  - "plasma"
  - "rc1"
  - "tina"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20142.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [KDE Plasma 5.17 Desktop to Modernize the Settings and Add Many UI Changes](https://news.softpedia.com/news/kde-plasma-5-17-linux-desktop-to-modernize-the-settings-add-many-ui-changes-526856.shtml)

- [Debconf20 Takes Place August 23-29](https://news.softpedia.com/news/debconf20-conference-takes-place-august-23-29-for-debian-gnu-linux-11-bullseye-526857.shtml)

- [Linux Lite 4.6 RC1 Released](https://www.linuxliteos.com/forums/release-announcements/linux-lite-4-6-rc1-released/)

- [Canonical Releases New Linux Kernel Live Patch for Ubuntu 18.04 and 16.04](https://news.softpedia.com/news/canonical-releases-new-linux-kernel-live-patch-for-ubuntu-18-04-and-16-04-lts-526888.shtml)

- [Linux Mint 19.2 “Tina” Is Now Available for Download](https://blog.linuxmint.com/?p=3786)

- [KDE and GNOME are Joining Hands to Build a New-Age Linux Desktop](https://fossbytes.com/kde-gnome-joining-hands-build-linux-desktop/)

- [Linux Kernel 5.1 Reached End of Life, Users Urged to Upgrade to 5.2](https://news.softpedia.com/news/linux-kernel-5-1-reached-end-of-life-users-urged-to-upgrade-to-linux-kernel-5-2-526905.shtml)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
