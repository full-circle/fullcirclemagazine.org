---
title: "Full Circle Weekly News 195"
date: 2021-01-04
draft: false
tags:
  - "anbox"
  - "arm"
  - "artwork"
  - "bash"
  - "beta"
  - "contest"
  - "darktable"
  - "deepin"
  - "gtk4"
  - "kdenlive"
  - "kernel"
  - "libre"
  - "libreoffice"
  - "linux"
  - "lubuntu"
  - "lzo"
  - "manjaro"
  - "mint"
  - "mozilla"
  - "office"
  - "rc1"
  - "rescuezilla"
  - "rocky"
  - "snap"
  - "snapd"
  - "thunderbird"
  - "ubuntu"
  - "ulyssa"
  - "xfce"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20195.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Lubuntu 21.04 Artwork Contest Open](https://lubuntu.me/lubuntu-hirsute-hippo-21-04-artwork-contest/)

- [Ubuntu's Switch to LZO in Snapd](https://ubuntu.com//blog/why-lzo-was-chosen-as-the-new-compression-method)

- [Ubuntu's Snap Theming Will See Changes for the Better](https://ubuntu.com//blog/snaps-and-themes-on-the-path-to-seamless-desktop-integration)

- [GTK4 Is Available After 4 Years In Development](https://blog.gtk.org/2020/12/16/gtk-4-0/)

- [XFCE 4.16 Out with New Icons](https://xfce.org/about/news/?post=1608595200)

- [A Rocky Linux Update](https://rockylinux.org/posts/community-update-december-2020/)

- [Deepin 20.1, A Big Jump Forward](https://www.deepin.org/en/2020/12/28/deepin-20-1-details-make-perfection/)

- [Linux Mint 20.1 Ulyssa Beta Out](https://blog.linuxmint.com/?p=3989)

- [Rescuezilla 2.1.2 Out](https://github.com/rescuezilla/rescuezilla/releases/tag/2.1.2)

- [Manjaro ARM 20.12 Out](https://forum.manjaro.org/t/manjaro-arm-20-12-released/43709)

- [Linux Kernel 5.11 rc1 Out](https://www.lkml.org/lkml/2020/12/27/180)

- [Bash 5.1 Out](https://lists.gnu.org/archive/html/info-gnu/2020-12/msg00003.html)

- [Darktable 3.4 Out](https://github.com/darktable-org/darktable/releases/tag/release-3.4.0)

- [Thunderbird 78.6.0 Out](https://www.thunderbird.net/en-US/thunderbird/78.6.0/releasenotes/)

- [LibreOffice 7.0.4 Out](https://9to5linux.com/libreoffice-7-0-4-office-suite-released-with-more-than-110-bug-fixes)

- [Kdenlive 20.12 Out](https://news.itsfoss.com/kdenlive-20-12/)

- [Anbox Cloud 1.8.2 Out](https://discourse.ubuntu.com/t/anbox-cloud-1-8-2-has-been-released/19951)
