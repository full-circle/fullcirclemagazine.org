---
title: "Full Circle Weekly News 330"
date: 2023-09-10
draft: false
tags:
  - "antix"
  - "kernel"
  - "knotdns"
  - "toaruos"
  - "regolith"
  - "midnightbsd"
  - "vivaldi"
  - "jetbrains"
  - "wayland"
  - "reiserfs"
  - "lxd"
  - "nginx"
  - "armbian"
  - "nitrux"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20330.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of antiX 23:](https://antixlinux.com/antix-23-released/)

- [Linux 6.5 kernel:](https://lkml.org/lkml/2023/8/27/348)

- [KnotDNS 3.3.0 with DNS support over QUIC:](https://www.knot-dns.cz/2023-08-28-version-330.html)

- [Release of the ToaruOS 2.2:](https://github.com/klange/toaruos/releases/tag/v2.2.0)

- [Regolith 3.0:](https://www.freelists.org/post/regolith-linux/Regolith-Desktop-30-general-availability-announcement)

- [MidnightBSD 3.1:](https://www.justjournal.com/users/mbsd/entry/33943)

- [Vivaldi 6.2 release:](https://vivaldi.com/blog/vivaldi-on-desktop-6-2/)

- [JetBrains announces support for Wayland in IDE IntelliJ:](https://blog.jetbrains.com/platform/2023/08/wayland-support/)

- [ReiserFS declared obsolete in Linux kernel:](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=1500e7e0726e963f64b9785a0cb0a820b2587bad)

- [Release of LXD 5.17:](https://canonical.com//blog/lxd-5-17-is-now-available)

- [Release of NGINX Unit 1.31 application server:](https://unit.nginx.org/news/2023/unit-1.31.0-released/)

- [Linux From Scratch 12.0 and Beyond Linux From Scratch 12.0:](https://lists.linuxfromscratch.org/sympa/arc/lfs-announce/2023-09/msg00000.html)

- [Armbian 23.08:](https://www.armbian.com/newsflash/armbian-23-8/)

- [Release of Nitrux 3.0:](https://nxos.org/changelog/release-announcement-nitrux-3-0-0/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
