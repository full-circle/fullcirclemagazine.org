---
title: "Full Circle Weekly News 187"
date: 2020-10-25
draft: false
tags:
  - "bluetooth"
  - "bugs"
  - "canonical"
  - "chromeos"
  - "core"
  - "driver"
  - "foundation"
  - "free"
  - "fsf"
  - "gnome"
  - "intel"
  - "ipfire"
  - "kde"
  - "kernel"
  - "krita"
  - "libre"
  - "libreoffice"
  - "linux"
  - "microk8s"
  - "nextcloud"
  - "nvidia"
  - "office"
  - "openoffice"
  - "oracle"
  - "plasma"
  - "redo"
  - "rescue"
  - "rescuezilla"
  - "short"
  - "software"
  - "stack"
  - "update"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20187.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonicals MicroK8s Gets High-Availability](https://www.zdnet.com/article/canonical-introduces-high-availability-micro-kubernetes/)

- [A Massive KDE Plasma 5.20 Released](https://kde.org/announcements/plasma-5.20.0)

- [Big Changes in the Nextcloud Hub 20 Release](https://nextcloud.com/blog/nextcloud-hub-20-debuts-dashboard-unifies-search-and-notifications-integrates-with-other-technologies/)

- [The Linux Foundation Announces the Open Governance Network Model](https://www.linux.com/news/introducing-the-open-governance-network-model/)

- [LibreOffice's Mike Saunters Pens Open Letter to OpenOffice](https://blog.documentfoundation.org/blog/2020/10/12/open-letter-to-apache-openoffice/)

- [Intel Reports Bugs In Linux Bluetooth Stack](https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00435.html)

- [Linux Kernel 5.9 Out](https://linuxreviews.org/Linux_5.9_Is_Released_With_New_Drivers,_Improved_AMD_GPU_Support,_And_Support_The_x86-64_FSGSBASE_CPU_Instructions)

- [ChromeOS 86 Out](https://9to5linux.com/chrome-os-86-rolls-out-with-linux-support-for-debian-gnu-linux-10-buster)

- [Oracle Linux 7 Update 9 Out](https://blogs.oracle.com/linux/announcing-the-release-of-oracle-linux-7-update-9)

- [Redo Rescue 3.0 Out](http://redorescue.com/)

- [Rescuezilla 2.0 Out](https://github.com/rescuezilla/rescuezilla/releases/tag/2.0)

- [IPFire 2.25 Core Update 150 Out](https://blog.ipfire.org/post/ipfire-2-25-core-update-150-released)

- [Krita 4.4.0 Out](https://krita.org/en/item/krita-4-4-0-released/)

- [Gnome 3.38.1 Out](https://download.gnome.org/core/3.38/3.38.1/NEWS)

- [Nvidia's Short Live Driver 455.28 Out](https://www.nvidia.com/Download/driverResults.aspx/165689/en-us)

- [The Free Software Foundation is 35 and KDE is 24](https://www.fsf.org/blogs/community/fsf-at-35-join-us-in-celebrating-the-incredible-community)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
