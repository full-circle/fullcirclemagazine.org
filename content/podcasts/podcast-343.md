---
title: "Full Circle Weekly News 343"
date: 2023-12-10
draft: false
tags:
  - "delata"
  - "incus"
  - "comentario"
  - "red hat"
  - "redhat"
  - "tails"
  - "peertube"
  - "armbian"
  - "cinnamon"
  - "mesa"
  - "flowblade"
  - "warzone"
  - "minetest"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20343.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Delta Chat 1.42:](https://delta.chat/en/2023-11-23-jumbo-42)

- [Release of Incus 0.3:](https://discuss.linuxcontainers.org/t/incus-0-3-has-been-released/18351)

- [Comentario 3.0.0:](https://docs.comentario.app/en/about/features/)

- [Red Hat to remove X.org server and related components from RHEL 10:](https://www.redhat.com/en/blog/rhel-10-plans-wayland-and-xorg-server)

- [Tails 5.20:](https://tails.net/news/version_5.20/index.en.html)

- [PeerTube 6.0:](https://framablog.org/2023/11/28/peertube-v6-is-out-and-powered-by-your-ideas/)

- [Release of Armbian 23.11:](https://translate.google.com/website?sl=auto&tl=en&hl=en-US&u=https://www.armbian.com/newsflash/armbian-23-11-topi/)

- [Release of CRIU 3.19:](https://github.com/checkpoint-restore/criu/releases/tag/v3.19)

- [Release of Cinnamon 6.0 with initial Wayland support:](https://github.com/linuxmint/cinnamon/releases/tag/6.0.0)

- [Release of Mesa 23.3:](https://lists.freedesktop.org/archives/mesa-announce/2023-November/000740.html)

- [Flowblade 2.12:](https://github.com/jliljebl/flowblade/releases/tag/v2.12)

- [Repixture 3.11.0:](https://content.minetest.net/packages/Wuzzy/repixture/)

- [Release of Warzone 2100 4.4.2:](https://wz2100.net/news/version-4-4-2/)

- [LDL multimedia library optimized for low-power systems:](https://github.com/JordanCpp/Lib-LDL)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
