---
title: "Full Circle Weekly News 198"
date: 2021-02-04
draft: false
tags:
  - "21-04"
  - "apple"
  - "beta"
  - "chrome"
  - "chromium"
  - "corellium"
  - "diogo"
  - "fedora"
  - "gnome"
  - "gtk3"
  - "krita"
  - "m1"
  - "mac"
  - "mx-linux"
  - "pi"
  - "pico"
  - "ragout"
  - "raspberry"
  - "respin"
  - "snort"
  - "subiquity"
  - "sync"
  - "ubuntu"
  - "virtualbox"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20198.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 21.04 Will Remain on Gnome 3.38 and GTK3](https://discourse.ubuntu.com/t/staying-on-gtk3-and-gnome-3-38-this-cycle/20466)

- [Corellium the First to Boot Linux on M1 Macs](https://twitter.com/cmwdotme/status/1351838924621099008)

- [Chromium Losing Access to Chrome Sync](https://blog.chromium.org/2021/01/limiting-private-api-availability-in.html)

- [Fedora Won't Drop Chromium](https://twitter.com/spotfoss/status/1351624747193409539?s=20)

- [MX Linux Community Respin Beta dubbed Ragout Out](https://mxlinux.org/blog/fluxbox-raspberrypi-respin-ragout-beta/)

- [Subiquity 21.01.1 Out](https://discourse.ubuntu.com/t/subiquity-21-01-1-has-been-released-to-stable/20463)

- [Snort 3 Out](https://blog.snort.org/2021/01/snort-3-officially-released.html)

- [Krita 4.4.2 Out](https://krita.org/en/item/krita-4-4-2-released/)

- [Virtualbox 6.1.18 Out](https://www.virtualbox.org/wiki/Changelog)

- [The Raspberry Pi Pico Out](https://www.raspberrypi.org/blog/raspberry-pi-silicon-pico-now-on-sale/)

- [Diogo Constantino Gains Ubuntu Membership](https://lists.ubuntu.com/archives/ubuntu-news-team/2021-January/002957.html)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
