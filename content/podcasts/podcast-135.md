---
title: "Full Circle Weekly News 135"
date: 2019-06-17
draft: false
tags:
  - "5-16"
  - "atari"
  - "buster"
  - "console"
  - "debian"
  - "endless"
  - "gaming"
  - "gazelle"
  - "kde"
  - "kubuntu"
  - "lenovo"
  - "linux"
  - "matrix"
  - "system76"
  - "terminal"
  - "thinkpad"
  - "ubuntu"
  - "vcs"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20135.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Command Line Editors Vulnerable to High Severity Bug](https://threatpost.com/linux-command-line-editors-high-severity-bug/145569/)

- [KDE 5.16 Is Now Available for Kubuntu](https://news.softpedia.com/news/kde-plasma-5-16-desktop-is-now-available-for-kubuntu-and-ubuntu-19-04-users-526369.shtml)

- [Debian 10 Buster-based Endless OS 3.6.0 Linux Distribution Now Available](https://betanews.com/2019/06/12/debian-10-buster-endless-os-linux/)

- [Introducing Matrix 1.0 and the Matrix.org Foundation](https://www.pro-linux.de/news/1/27145/matrix-10-und-die-matrixorg-foundation-vorgestellt.html)

- [System 76’s Supercharged Gazelle Laptop is Finally Available](https://betanews.com/2019/06/13/system76-linux-gazelle-laptop/)

- [Lenovo Thinkpad P Laptops Are Available with Ubuntu](https://www.omgubuntu.co.uk/2019/06/lenovo-thinkpad-p-series-ubuntu-preinstalled)

- [Atari VCS Linux-powered Gaming Console Is Now Available for Pre-order](https://news.softpedia.com/news/atari-vcs-linux-powered-gaming-console-is-now-available-for-pre-order-for-249-526387.shtml)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
