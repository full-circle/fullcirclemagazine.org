---
title: "Full Circle Weekly News 151"
date: 2019-10-29
draft: false
tags:
  - "android"
  - "cloud"
  - "docker"
  - "gnu"
  - "nvidia"
  - "openstack"
  - "sparky"
  - "sparkylinux"
  - "stallman"
  - "suse"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20151.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [New Android Zero-Day Vulnerability Found](https://fossbytes.com/new-android-zero-day-vulnerability-found-vulnerable-devices/)

- [Docker Has Financial Problems](https://www.linux-magazin.de/news/bericht-docker-hat-finanzprobleme)

- [SparkyLinux 5.9 Released](https://news.softpedia.com/news/sparkylinux-5-9-released-with-latest-updates-from-debian-gnu-linux-10-buster-527728.shtml)

- [GNU Project Developers Object to Richard Stallman's Continued Leadership](https://www.zdnet.com/article/gnu-project-developers-object-to-richard-m-stallmans-continued-leadership/)

- [SUSE Drops OpenStack Cloud](https://www.zdnet.com/article/suse-drops-openstacks/)

- [NVIDIA Still Working On A Generic Allocator](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-Generic-Allocator-2019)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
