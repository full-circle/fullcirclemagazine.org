---
title: "Full Circle Weekly News 269"
date: 2022-07-22
draft: false
tags:
  - "ubuntu"
  - "bittorrent"
  - "browser"
  - "deluge"
  - "email"
  - "epiphany"
  - "firewalld"
  - "freebsd"
  - "git"
  - "java"
  - "javascript"
  - "libraries"
  - "mail"
  - "malicious"
  - "ota-23"
  - "ota23"
  - "packj"
  - "pico"
  - "python"
  - "raspberry"
  - "shell"
  - "thunderbird"
  - "toolkit"
  - "touch"
  - "ubports"
  - "unity"
  - "wayland"
  - "webextension"
  - "webos"
  - "wifi"
  - "wifibox"
  - "xonotic"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20269.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Wifibox 0.10 - environment for using Linux WiFi drivers in FreeBSD:](https://github.com/pgj/freebsd-wifibox/releases/tag/0.10.0)

- [Git 2.37 source control release:](https://lore.kernel.org/git/xmqqy1xinf00.fsf@gitster.g/)

- [Release BitTorrent client Deluge 2.1:](https://github.com/deluge-torrent/deluge/releases/tag/deluge-2.1.0)

- [Thunderbird 102 mail client released:](https://blog.thunderbird.net/2022/06/thunderbird-102-released-a-serious-upgrade-to-your-communication/)

- [23rd Ubuntu Touch Firmware Update:](https://ubports.com/)

- [Packj - a toolkit for detecting malicious libraries in Python and JavaScript:](https://github.com/ossillate-inc/packj)

- [Unity 7.6 Custom Shell Stable Released:](https://unity.ubuntuunity.org/blog/unity-7.6)

- [Wayland 1.21 is available:](https://lists.freedesktop.org/archives/wayland-devel/2022-June/042268.html)

- [WebOS Open Source Edition 2.17 Platform Released:](https://www.webosose.org/blog/2022/07/01/webos-ose-2-17-0-release/)

- [Raspberry Pi Project Unveils Wi-Fi-Enabled Pico W Board:](https://www.raspberrypi.com/news/raspberry-pi-pico-w-your-6-iot-platform/)

- [Release of multiplayer 3D shooter Xonotic 0.8.5:](https://xonotic.org/posts/2022/xonotic-0-8-5-release/)

- [firewalld 1.2 released:](https://firewalld.org/2022/07/firewalld-1-2-0-release)

- [WebExtension support added to Epiphany web browser (GNOME Web):](https://blog.tingping.se/2022/06/29/WebExtensions-Epiphany.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
