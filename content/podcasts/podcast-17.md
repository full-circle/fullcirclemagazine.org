---
title: "Full Circle Weekly News 17"
date: 2016-05-14
draft: false
tags:
  - "16-04"
  - "18"
  - "android"
  - "critical"
  - "flaw"
  - "kernel"
  - "mint"
  - "ota"
  - "ota-11"
  - "ota11"
  - "patch"
  - "qualcomm"
  - "risk"
  - "update"
  - "vulnerabilities"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2017.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Mint 18 Will Be Based on Ubuntu 16.04 LTS, Offers Better Hardware Support](http://news.softpedia.com/news/linux-mint-18-will-be-based-on-ubuntu-16-04-lts-offers-better-hardware-support-503803.shtml)

- [Critical Linux Kernel Update for Ubuntu 16.04 LTS Patches 15 Vulnerabilities](http://news.softpedia.com/news/critical-linux-kernel-update-for-ubuntu-16-04-lts-patches-15-vulnerabilities-503786.shtml)

- [Qualcomm flaw puts millions of Android devices at risk](http://www.infoworld.com/article/3066785/android/qualcomm-flaw-puts-millions-of-android-devices-at-risk.html)

- [Simplicity Linux Digs Deeper Than Puppy Linux](http://www.linuxinsider.com/story/83478.html)

- [Linux is the largest software development project on the planet](http://www.cio.com/article/3069529/linux/linux-is-the-largest-software-development-project-on-the-planet-greg-kroah-hartman.html)

- [Ubuntu Touch OTA-11 Delayed by a Week](http://news.softpedia.com/news/ubuntu-touch-ota-11-delayed-by-a-week-work-on-ota-12-should-start-soon-503970.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
