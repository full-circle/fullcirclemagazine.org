---
title: "Full Circle Weekly News 399"
date: 2025-02-09
draft: false
tags:
  - "hangover"
  - "orbitiny"
  - "hyprland"
  - "pebble"
  - "google"
  - "linux"
  - "kernel"
  - "pidgin"
  - "kaos"
  - "opnsense"
  - "gcompris"
  - "openvox"
  - "0ad"
  - "openvox"
  - "gparted"
  - "scribus"
  - "regolith"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20399.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Release of Hangover 10.0:](https://github.com/AndreRH/hangover/releases/tag/hangover-10.0)

- [Orbitiny DE using Qt Released:](https://www.reddit.com/r/linux/comments/1iayzwm/orbitiny_desktop_environment_released_originally/)

- [Hyprland 0.47 released:](https://hyprland.org/news/update47/)

- [Google Opens Up OS Code for Pebble Smartwatch:](https://opensource.googleblog.com/2025/01/see-code-that-powered-pebble-smartwatches.html)

- [Release of Privoxy 4.0.0:](https://www.privoxy.org/announce.txt)

- [Linux Kernel Reaches 40 Million Lines:](https://translate.google.com/website?sl=auto&tl=en&hl=en-US&client=webapp&u=https://www.heise.de/news/Linux-durchbricht-40-Millionen-Zeilen-Marke-10255488.html)

- [Pidgin 2.14.14 released:](https://discourse.imfreedom.org/t/pidgin-2-14-14-has-been-released/241)

- [KaOS 2025.01 released:](https://kaosx.us/news/2025/kaos01/)

- [Ubuntu Switches to Matrix for Developer Communications:](https://www.mail-archive.com/ubuntu-devel-announce@lists.ubuntu.com/msg01112.html)

- [OPNsense 25.1 released:](https://forum.opnsense.org/index.php?topic%3D45460.0)

- [Release of GCompris 25.0:](https://gcompris.net/news/2025-01-30-en.html)

- [A new 'alpha' of 0 AD:](https://play0ad.com/new-release-0-a-d-alpha-27-agni/)

- [OpenVox, a fork of Puppet:](https://voxpupuli.org/blog/2025/01/21/openvox-release/)

- [Release of GParted 1.7:](https://gparted.org/news.php?item%3D257)

- [Release of Scribus 1.7.0:](https://www.scribus.net/scribus-1-7-0-released/)

- [Regolith 3.2 Released:](https://github.com/regolith-linux/regolith-desktop/releases)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
