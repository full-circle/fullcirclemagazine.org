---
title: "Full Circle Weekly News 129"
date: 2019-05-06
draft: false
tags:
  - "19-10"
  - "debian"
  - "docker"
  - "email"
  - "ethical"
  - "fedora"
  - "hacking"
  - "hub"
  - "librem-one"
  - "parrot"
  - "purism"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20129.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Docker Hub Breached, Impacting 190,000 Accounts](https://www.eweek.com/security/docker-hub-breached-impacting-190-000-accounts)

- [Fedora 30 Released](https://news.softpedia.com/news/fedora-30-released-with-gnome-3-32-and-linux-kernel-5-0-here-s-what-s-new-525820.shtml)

- [Debian GNU/Linux 9.9 Released](https://news.softpedia.com/news/debian-gnu-linux-9-9-released-with-over-120-bug-fixes-and-security-updates-525803.shtml)

- [Parrot Security 4.6 Ethical Hacking OS Officially Released](https://news.softpedia.com/news/parrot-security-4-6-ethical-hacking-os-officially-released-here-s-what-s-new-525805.shtml)

- [Ubuntu 19.10 Now Open for Development](https://news.softpedia.com/news/ubuntu-19-10-to-be-released-on-october-17th-now-open-for-development-525823.shtml)

- [Purism launches Chat, Email and Social Media services with Librem One](https://liliputing.com/2019/04/purisms-librem-one-suite-of-apps-offers-ad-free-privacy-focused-chat-email-social-media-for-a-fee.html)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
