---
title: "Full Circle Weekly News 386"
date: 2024-10-06
draft: false
tags:
  - "wolvic"
  - "browser"
  - "mpv"
  - "video"
  - "openwrt"
  - "centos"
  - "tor"
  - "tails"
  - "freebsd"
  - "cosmic"
  - "busybox"
  - "rtorrent"
  - "torrent"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20386.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [First release of Wolvic browser edition with Chromium engine:](https://wolvic.com/blog/chromium_release_1.0/)

- [MPV Video Player Release 0.39:](https://github.com/mpv-player/mpv/releases/tag/v0.39.0)

- [A console Live build of CentOS Stream MIN:](https://blog.centos.org/2024/09/september-2024-news/)

- [OpenWrt 23.05.5:](https://lists.openwrt.org/pipermail/openwrt-announce/2024-September/000058.html)

- [OpenBSD Bans Null Characters in Shell Scripts:](https://marc.info/?l%3Dopenbsd-cvs%26m%3D172712621620348%26w%3D2)

- [ELKS 0.8:](https://github.com/jbruchon/elks/releases/tag/v0.8.0)

- [PostgreSQL 17 Release:](http://www.postgresql.org/support/versioning/)

- [The Tor Project and the Tails Distribution Merger:](https://www.torproject.org/)

- [Tcl 9.0 release:](http://tcl.tk/)

- [Improving FreeBSD Performance on Laptops:](https://freebsdfoundation.org/blog/why-laptop-support-why-now-freebsds-strategic-move-toward-broader-adoption/)

- [Second alpha release of COSMIC:](https://blog.system76.com/post/cosmic-alpha-2-press-release)

- [Release of BusyBox 1.37:](http://www.busybox.net/)

- [rTorrent 0.10.0 has been released:](https://github.com/rakshasa/rtorrent/releases/tag/v0.10.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
