---
title: "Full Circle Weekly News 06"
date: 2016-02-27
draft: false
tags:
  - "ai"
  - "artificial"
  - "artik"
  - "canonical"
  - "core"
  - "gpl"
  - "intelligence"
  - "kernel"
  - "linus"
  - "partnership"
  - "samsung"
  - "torvalds"
  - "violating"
  - "vulnerabilities"
  - "zfs"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2006.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical and Samsung Forge Partnership, Ubuntu Core to Power Artik Hardware](http://news.softpedia.com/news/canonical-and-samsung-forge-partnership-ubuntu-core-to-power-artik-hardware-500601.shtml)

- [Canonical Patches Five New Linux Kernel Vulnerabilities in Ubuntu 15.10](http://news.softpedia.com/news/canonical-patches-five-new-linux-kernel-vulnerabilities-in-ubuntu-15-10-500800.shtml)

- [Google’s artificial intelligence expert hopes his machines won’t take over the world](http://hotair.com/archives/2016/02/23/googles-artificial-intelligence-expert-hopes-his-machines-wont-take-over-the-world/)

- [Ubuntu 16.04 LTS Is Now Powered by Linux Kernel 4.4.2, Gets LibreOffice 5.1](http://news.softpedia.com/news/ubuntu-16-04-lts-is-now-powered-by-linux-kernel-4-4-2-gets-libreoffice-5-1-500873.shtml)

- [Why Linus Torvalds doesn't really care about open source](http://www.techrepublic.com/article/linux-creator-linus-torvalds-doesnt-really-care-about-open-source/)

- [New platform offers endpoint protection for Linux servers](http://betanews.com/2016/02/25/new-platform-offers-endpoint-protection-for-linux-servers/)

- [Canonical accused of violating GPL with ZFS-in-Ubuntu 16.04 plan](http://www.theregister.co.uk/2016/02/26/canonical_in_zfsonlinux_gpl_violation_spat/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
