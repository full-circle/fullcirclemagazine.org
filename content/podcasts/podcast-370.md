---
title: "Full Circle Weekly News 370"
date: 2024-06-16
draft: false
tags:
  - "mate"
  - "serenityos"
  - "freebsd"
  - "canonical"
  - "ubuntu"
  - "nitrux"
  - "backup"
  - "ffmpeg"
  - "klevernotes"
  - "kde"
  - "openssh"
  - "taler"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20370.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of MATE 1.28:](https://mate-desktop.org/blog/2024-02-27-mate-1-28-released/)

- [SerenityOS loses BDFL: (and Linux gains a browser)](https://awesomekling.substack.com/p/forking-ladybird-and-stepping-down-serenityos)

- [FreeBSD 14.1 release with improved sound stack and cloud-init support:](https://download.freebsd.org/ftp/releases/ISO-IMAGES/14.1/)

- [Canonical has published Ubuntu Core 24:](https://ubuntu.com/blog/canonical-launches-ubuntu-core-24)

- [Nitrux 3.5.0 with custom NX Desktop environment:](https://nxos.org/changelog/release-announcement-nitrux-3-5-0/)

- [Nxs-backup 3.7.0 is available:](https://nxs-backup.io/)

- [FFMpeg presented its own implementation of xHE-AAC decoder:](https://ffmpeg.org/index.html#xheaac)

- [The first release of KleverNotes:](https://blogs.kde.org/2024/06/05/klevernotes-version-1.0-official-release/)

- [OpenSSH added built-in protection against password attacks:](https://marc.info/?l=openbsd-cvs&m=171769392207688&w=2)

- [Release of the GNU Taler 0.11:](https://taler.net/en/news/2024-11.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
