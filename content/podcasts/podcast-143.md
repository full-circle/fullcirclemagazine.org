---
title: "Full Circle Weekly News 143"
date: 2019-08-19
draft: false
tags:
  - "18-04-3"
  - "19-10"
  - "ada"
  - "capsule8"
  - "cloud"
  - "debian"
  - "enterprise"
  - "ffmpeg"
  - "hat"
  - "ibm"
  - "installer"
  - "journal"
  - "linux"
  - "lts"
  - "multimedia"
  - "red"
  - "redhat"
  - "root"
  - "sparky"
  - "sparkylinux"
  - "zfs"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20143.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Journal Ceases Publication: An Awkward Goodbye](https://www.linuxjournal.com/content/linux-journal-ceases-publication-awkward-goodbye)

- [SparkyLinux Gets New Development Cycle Based on Debian 11](https://news.softpedia.com/news/sparkylinux-gets-new-development-cycle-based-on-debian-gnu-linux-11-bullseye-526972.shtml)

- [Capsule8 Adds Significant Funding from Intel Capital](https://www.globenewswire.com/news-release/2019/08/07/1898345/0/en/Capsule8-Adds-Significant-Funding-from-Intel-Capital.html)

- [Three Weeks After Closing Red Hat Deal, IBM Rolls Out New Cloud Offerings](https://www.datacenterknowledge.com/ibm/three-weeks-after-closing-red-hat-deal-ibm-rolls-out-new-cloud-offerings)

- [FFMPEG “Ada” Open Source Multimedia Framework Released](https://news.softpedia.com/news/ffmpeg-4-2-ada-open-source-multimedia-framework-released-here-s-what-s-new-526971.shtml)

- [Ubuntu 19.10 to Support ZFS on Root as an Experimental Option in the Installer](https://news.softpedia.com/news/ubuntu-19-10-to-support-zfs-on-root-as-an-experimental-option-in-the-installer-526985.shtml)

- [Ubuntu 18.04.3 LTS Released](https://lists.ubuntu.com/archives/ubuntu-announce/2019-August/000248.html)

- [Red Hat Enterprise Linux 7.7 Released](https://www.redhat.com/en/about/press-releases/red-hat-drives-cloud-native-flexibility-enhances-operational-security-latest-version-red-hat-enterprise-linux-7)(

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
