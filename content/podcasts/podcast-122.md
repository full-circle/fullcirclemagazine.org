---
title: "Full Circle Weekly News 122"
date: 2019-02-19
draft: false
tags:
  - "16-04"
  - "18-04"
  - "18-10"
  - "access"
  - "arm"
  - "aws"
  - "backbox"
  - "boot"
  - "canonical"
  - "eol"
  - "ethical"
  - "fail"
  - "failure"
  - "fix"
  - "hacking"
  - "kde"
  - "laptop"
  - "linux"
  - "lts"
  - "malware"
  - "neon"
  - "root"
  - "snapd"
  - "ubuntu"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20122.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical Apologizes for Boot Failure in Ubuntu 18.10 & 18.04, Fix Available Now](https://news.softpedia.com/news/canonical-apologizes-for-another-ubuntu-linux-kernel-regression-fix-available-524892.shtml)

- [Open source project aims to make Ubuntu usable on Arm-powered Windows laptops](https://www.techrepublic.com/article/open-source-project-aims-to-make-ubuntu-usable-on-arm-powered-windows-laptops/)

- [KDE neon Systems Based on Ubuntu 16.04 LTS Have Reached End of Life, Upgrade Now](https://news.softpedia.com/news/kde-neon-systems-based-on-ubuntu-16-04-lts-have-reached-end-of-life-upgrade-now-524959.shtml)

- [Good Guy Malware: Linux Virus Removes Other Infections to Mine on Its Own](https://news.softpedia.com/news/good-guy-malware-linux-virus-removes-other-infections-to-mine-on-its-own-524915.shtml)

- [Dirty_Sock vulnerability in Canonical's snapd could give root access on Linux machines](https://betanews.com/2019/02/13/dirty-sock-snapd-linux/)

- [Ethical Hacking, Ubuntu-Based BackBox Linux OS Is Now Available on AWS](https://news.softpedia.com/news/ethical-hacking-ubuntu-based-backbox-linux-is-now-available-on-aws-524960.shtml)
