---
title: "Full Circle Weekly News 319"
date: 2023-06-21
draft: false
tags:
  - "kera"
  - "desktop"
  - "easyos"
  - "debian"
  - "hurd"
  - "gnu"
  - "obs"
  - "studio"
  - "nginx"
  - "nextcloud"
  - "onlyoffice"
  - "office"
  - "nouveau"
  - "driver"
  - "distrobox"
  - "opensmtpd"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20319.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Kera Desktop project:](https://gitlab.com/kerahq/Kera-Desktop/-/tags)

- [Release of EasyOS 5.4:](https://bkhome.org/news/202306/easyos-kirkstone-series-version-54-released.html)

- [Release of Debian GNU/Hurd 2023:](https://lists.debian.org/debian-hurd/2023/06/msg00026.html)

- [OBS Studio added support for WebRTC with P2P broadcasting:](https://github.com/obsproject/obs-studio/pull/7926)

- [Update of nginx 1.25.1:](http://nginx.org/#2023-06-13)

- [Nextcloud Hub 5 collaboration:](https://nextcloud.com/blog/introducing-hub-5-first-to-deliver-self-hosted-ai-powered-digital-workspace/)

- [ONLYOFFICE 7.4.0:](https://www.onlyoffice.com/blog/2023/06/onlyoffice-desktop-editors-v7-4-released)

- [The Nouveau driver error:](https://chaos.social/@karolherbst/110537010739015047)

- [Release of Distrobox 1.5:](https://github.com/89luca89/distrobox/releases/tag/1.5.0)

- [OpenSMTPD 7.3.0p0:](https://www.opensmtpd.org/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
