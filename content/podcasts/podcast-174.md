---
title: "Full Circle Weekly News 174"
date: 2020-06-07
draft: false
tags:
  - "audacious"
  - "bodhi"
  - "git"
  - "kernel"
  - "kubernetes"
  - "linux"
  - "parrot"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20174.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Kernel 5.6 Released](https://lkml.org/lkml/2020/3/29/379)

- [Audacious 4.0 Out](https://audacious-media-player.org/news/45-audacious-4-0-released)

- [Git 2.26.0 Out](https://github.blog/2020-03-22-highlights-from-git-2-26/)

- [Parrot 4.8 Out](https://parrotsec.org/blog/parrot-4.8-release-notes/)

- [Kubernetes 1.18 Out](https://kubernetes.io/blog/2020/03/25/kubernetes-1-18-release-announcement/)

- [Bodhi Linux 5.1.0 Out](https://www.bodhilinux.com/2020/03/25/bodhi-linux-5-1-0-released/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
