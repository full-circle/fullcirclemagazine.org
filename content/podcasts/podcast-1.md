---
title: "Full Circle Weekly News 01"
date: 2016-01-23
draft: false
tags:
  - "app"
  - "atom"
  - "mj"
  - "music"
  - "ocean"
  - "os"
  - "phoenix"
  - "phones"
  - "remix"
  - "server"
  - "ssh"
  - "tablet"
  - "touch"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2001.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

Episode 1! And, yes, I've removed the teletype sound that everyone hated.  :)
RSS feed also added to the side bar of the site.

- [MJ Technology wants to crowdfund Ubuntu tablets with Atom x7 CPU](http://liliputing.com/2016/01/mj-technology-wants-to-crowdfund-ubuntu-tablets-with-atom-x7-cpu.html)

- [Ocean is a phone-size Linux server that runs on batteries](http://www.slashgear.com/ocean-is-a-phone-size-linux-server-that-runs-on-batteries-15423305/)

- [OpenSSH Flaw Exposes Linux to Roaming Risk](http://www.eweek.com/security/openssh-flaw-exposes-linux-servers-to-roaming-risk.html)

- [Phoenix OS, an Alternative to Remix OS](http://news.softpedia.com/news/introducing-phoenix-os-an-alternative-to-remix-os-and-android-x86-499017.shtml)

- [Linux Kernel Vulnerability Patched](https://threatpost.com/serious-linux-kernel-vulnerability-patched/115923/)

- [Linux Foundation causes uproar by quietly removing community representation from its board](http://www.information-age.com/industry/software/123460821/linux-foundation-causes-uproar-quietly-removing-community-representation-its-board)

- [Ubuntu Phones Will Get a New Music App After the OTA-9 Update](http://news.softpedia.com/news/ubuntu-phones-will-get-a-new-music-app-with-lots-of-features-after-the-ota-9-update-499182.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
