---
title: "Full Circle Weekly News 150"
date: 2019-10-22
draft: false
tags:
  - "18-04"
  - "19-04"
  - "book"
  - "calibre"
  - "canonical"
  - "debbie"
  - "debian"
  - "flatpak"
  - "kernel"
  - "linus"
  - "lockdown"
  - "management"
  - "mint"
  - "nextcloud"
  - "security"
  - "torvalds"
  - "update"
  - "wipe"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20150.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Mint Debian Edition 4 to be Dubbed Debbie](https://news.softpedia.com/news/linux-mint-debian-edition-4-to-be-dubbed-debbie-new-linux-mint-logo-unveiled-527651.shtml)

- [The New Nextcloud 17 Brings Remote Wipe](https://nextcloud.com/press/pr20190930/)

- [Flatpak 1.5 Rolls Out With New Features](https://news.softpedia.com/news/flatpak-1-5-linux-app-sandboxing-rolls-out-with-new-features-many-improvements-527689.shtml)

- [Calibre Open Source eBook Management App Gets Major Release After Two Years](https://news.softpedia.com/news/calibre-open-source-ebook-management-app-gets-major-release-after-two-years-527701.shtml)

- [Linus Torvalds To Add "Lockdown" Security Feature in Linux Kernel 5.4](https://fossbytes.com/kernel-lockdown-linux-kernel-5-4/)

- [Canonical Releases Major Kernel Security Update for 19.04 and 18.04 ](https://news.softpedia.com/news/canonical-releases-major-kernel-security-update-for-ubuntu-19-04-and-18-04-lts-527703.shtml)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
