---
title: "Full Circle Weekly News 16"
date: 2016-05-07
draft: false
tags:
  - "16-10"
  - "android"
  - "badge"
  - "blackarch"
  - "chrome"
  - "cpu"
  - "debian"
  - "default"
  - "desktop"
  - "devuan"
  - "entroware"
  - "fork"
  - "foundation"
  - "intel"
  - "laptop"
  - "linux"
  - "malware"
  - "pentest"
  - "pentesting"
  - "processor"
  - "skylake"
  - "systemd"
  - "tool"
  - "tools"
  - "tweak"
  - "unity"
  - "update"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2016.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [UK-based Entroware launches 14 inch Ubuntu laptop with Intel Skylake](http://liliputing.com/2016/04/uk-based-entroware-launches-14-inch-ubuntu-laptop-with-intel-skylake.html)

- [Unity 8 Won't Be the Default Desktop Session for Ubuntu 16.10](http://linux.softpedia.com/blog/unity-8-won-t-be-the-default-desktop-session-for-ubuntu-16-10-yakkety-yak-503631.shtml)

- [Devs release beta of systemd-free Debian fork](http://www.theregister.co.uk/2016/04/29/systemd_free_debian_fork_devuan_reaches_beta/)

- [New Android malware disguises itself as a Chrome update](https://www.yahoo.com/tech/android-malware-disguises-itself-chrome-174145985.html)

- [New BlackArch Linux version released, now provides 1400 pentesting tools](http://www.techworm.net/2016/05/new-blackarch-linux-version-released-now-provides-1400-pentesting-tools.html)

- [Linux Foundation launches badge program to boost open source security](http://www.zdnet.com/article/linux-foundation-launches-badge-program-to-boost-open-source-security/)

- [Ubuntu Tweak Is Now Officially Dead and Buried](http://news.softpedia.com/news/ubuntu-tweak-is-now-officially-dead-and-buried-503672.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
