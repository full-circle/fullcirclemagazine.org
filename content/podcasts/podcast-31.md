---
title: "Full Circle Weekly News 31"
date: 2016-08-20
draft: false
tags:
  - "alpine"
  - "amazon"
  - "autonomous"
  - "bitcoin"
  - "canonical"
  - "corporate"
  - "firefox"
  - "gpl"
  - "kde"
  - "kernel"
  - "malware"
  - "membership"
  - "mining"
  - "netflix"
  - "omega2"
  - "onion"
  - "owncloud"
  - "patron"
  - "replace"
  - "session"
  - "startup"
  - "systemd"
  - "toyota"
  - "upstart"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2031.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical to Replace Upstart with systemd for Ubuntu 16.10's Session Startup](http://news.softpedia.com/news/canonical-to-replace-upstart-with-systemd-for-ubuntu-16-10-s-session-startup-507413.shtml)

- [Canonical Is Now a Patron of KDE, as Part of the Corporate Membership Program](http://news.softpedia.com/news/canonical-is-now-a-patron-of-kde-as-part-of-the-corporate-membership-program-507446.shtml)

- [Firefox for Linux will soon support Netflix and Amazon videos](http://www.pcworld.com/article/3108544/linux/firefox-for-linux-will-soon-support-netflix-and-amazon-videos.html)

- [Server-Oriented Alpine Linux 3.4.3 Lands with Kernel 4.4.17 LTS, ownCloud 9.0.4](http://news.softpedia.com/news/server-oriented-alpine-linux-3-4-3-lands-with-kernel-4-4-17-lts-owncloud-9-0-4-507323.shtml)

- [Linux developer loses GPL suit against VMware](http://www.itwire.com/open-source/74288-linux-developer-loses-gpl-suit-against-vmware.html)

- [New Linux Malware Installs Bitcoin Mining Software on Infected Device](https://www.hackread.com/linux-malware-bitcoin-mining-software/)

- [The Onion Omega2 lets you add Linux to your hardware projects](https://techcrunch.com/2016/08/13/the-onion-omega2-lets-you-add-linux-to-your-hardware-projects/)

- [Toyota Pledges Additional $22 Million For Autonomous Vehicles](http://uk.pcmag.com/consumer-electronics-reviews-ratings/83724/news/toyota-pledges-additional-22-million-for-autonomous-vehicles)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
