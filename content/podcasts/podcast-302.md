---
title: "Full Circle Weekly News 302"
date: 2023-02-26
draft: false
tags:
  - "dino"
  - "wolvic"
  - "plasma"
  - "thunderbird"
  - "crossover"
  - "kernel"
  - "mariadb"
  - "simply"
  - "lapdock"
  - "librem"
  - "webos"
  - "intel"
  - "clonezilla"
  - "parrot"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20302.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Dino 0.4 released:](https://dino.im/blog/2023/02/dino-0.4-release/)

- [Release of Wolvic 1.3:](https://wolvic.com/blog/release_1.3/)

- [Release of KDE Plasma 5.27:](https://kde.org/announcements/plasma/5/5.27.0/)

- [A prototype of the new Thunderbird interface has been published:](https://blog.thunderbird.net/2023/02/thunderbird-115-supernova-preview-the-new-folder-pane/)

- [Release of CrossOver 22.1:](https://www.codeweavers.com/support/forums/announce/?t=24;msg=275240)

- [Real-time kernel in Ubuntu:](https://canonical.com/blog/real-time-ubuntu-is-now-generally-available)

- [Stable release of MariaDB 10.11:](https://mariadb.org/mariadb-10-11-2-ga-now-available/)

- [Simply Linux 10.1 for RISC-V:](https://lists.altlinux.org/pipermail/community/2023-February/688884.html)

- [Lapdock or Librem:](https://puri.sm/posts/announcing-the-lapdock-kit/)

- [Mobile platform /e/OS 1.8:](https://e.foundation/leaving-apple-google-welcome-e-os-1-8-do-not-miss-a-special-offer-for-murena-fairphone-4/)

- [WebOS Open Source Edition 2.20:](https://www.webosose.org/blog/2023/02/17/webos-ose-2-20-0-release/)

- [Intel has opened OpenCL:](https://github.com/intel/llvm/pull/8216)

- [Release of Clonezilla Live 3.0.3:](https://sourceforge.net/p/clonezilla/news/2023/02/-stable-clonezilla-live-303-22-released-/)

- [Release of the Parrot 5.2:](https://parrotsec.org/blog/2023-02-15-parrot-5.2-release-notes/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
