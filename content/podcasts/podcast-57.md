---
title: "Full Circle Weekly News 57"
date: 2017-04-08
draft: false
tags:
  - "android"
  - "canonical"
  - "chip"
  - "community"
  - "cpu"
  - "gnome"
  - "gpu"
  - "learning"
  - "microsoft"
  - "mir"
  - "phones"
  - "spyware"
  - "touch"
  - "tpu"
  - "ubports"
  - "unity"
  - "unity8"
  - "verizon"
  - "wayland"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2057.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical abandons Unity, Mir, and Ubuntu Touch](https://www.bit-tech.net/news/bits/2017/04/06/canonical-abandons-unity/1)

- [Ubuntu Touch and Unity 8 Are Not Dead, UBports Community Will Keep Them Alive](http://news.softpedia.com/news/ubuntu-touch-and-unity-8-are-not-dead-ubports-community-will-keep-them-alive-514620.shtml)

- [Google’s First Machine Learning Chip (TPU) Is 30x Faster Than CPUs And GPUs](TPU)

- [Android Defeats Windows To Become World’s Most Popular OS](https://fossbytes.com/android-defeats-windows-worlds-most-popular-os/)

- [Verizon will install spyware on all its Android phones](https://www.engadget.com/2017/03/31/eff-verizon-will-install-spyware-on-all-its-android-phones/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
