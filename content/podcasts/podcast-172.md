---
title: "Full Circle Weekly News 172"
date: 2020-05-23
draft: false
tags:
  - "4m-linux"
  - "aws"
  - "basilisk"
  - "bottle"
  - "bottle-rocket"
  - "browser"
  - "ceph"
  - "debian"
  - "freenas"
  - "freeze"
  - "gnome"
  - "kde"
  - "kde-plasma"
  - "kernel"
  - "libreelec"
  - "linux"
  - "linux-foundation"
  - "onion"
  - "owl"
  - "plasma"
  - "project-owl"
  - "redhat"
  - "rocket"
  - "sdl"
  - "simple-directmedia-layer"
  - "splice-machine"
  - "storage"
  - "tails"
  - "timeshift"
  - "tor"
  - "truenas"
  - "ubuntu"
  - "vulnerability"
  - "wine"
  - "zorin"
  - "zorin-os"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20172.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Debian Leader Says "One Year Will Do"](https://www.itwire.com/open-source/debian-leader-hartman-says-one-year-at-the-helm-will-do-for-now.html)

- [Debian 8 Adds Longer Support](https://raphaelhertzog.com/2020/03/11/keeping-debian-8-jessie-alive-for-longer-than-5-years/)

- [Debian 11 Package Freeze Scheduled](https://lists.debian.org/debian-devel-announce/2020/03/msg00002.html)

- [Gnome 3.36 "Gresik"](https://help.gnome.org/misc/release-notes/3.36/)

- [The Linux Foundation Open Sources Project OWL](https://www.zdnet.com/article/linux-foundation-open-sources-disaster-relief-iot-firmware-project-owl/)

- [FreeNAS and TrueNAS are Merging](https://www.ixsystems.com/blog/freenas-truenas-unification/)

- [There's a Vulnerability in Timeshift](https://www.openwall.com/lists/oss-security/2020/03/06/3)

- [Linux Kernel 5.6 rc6 Out](https://lkml.org/lkml/2020/3/15/312)

- [Zorin OS 15.2 Out](https://zoringroup.com/blog/2020/03/05/zorin-os-15-2-is-released-harder-better-faster-stronger/)

- [Wine 5.4 Out](https://www.winehq.org/announce/5.4)

- [Red Hat's Ceph Storage 4 Out](https://www.zdnet.com/article/red-hat-ceph-storage-4-arrives/)

- [AWS' Bottle Rocket Out](https://techcrunch.com/2020/03/11/aws-launches-bottlerocket-a-linux-based-os-for-container-hosting/)

- [Tails 4.4 Out](https://tails.boum.org/news/version_4.4/index.en.html)

- [Basilisk Browser Out](https://itsfoss.com/basilisk-browser/)

- [LibreELEC 9.2.1 Out](https://libreelec.tv/2020/03/libreelec-leia-9-2-1/)

- [KDE Plasma 5.18.3 Out](https://kde.org/announcements/plasma-5.18.3.php)

- [SDL (or Simple DirectMedia Layer) 2 Out](or Simple DirectMedia Layer)

- [Splice Machine 3.0 Out](https://searchdatamanagement.techtarget.com/news/252479696/Splice-Machine-30-integrates-machine-learning-capabilities-database)

- [4M Linux 32.0 Out](https://4mlinux-releases.blogspot.com/2020/03/4mlinux-320-stable-released.html)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
