---
title: "Full Circle Weekly News 237"
date: 2021-11-24
draft: false
tags:
  - "almalinux"
  - "architecture"
  - "arm"
  - "cinnamon"
  - "decentralized"
  - "desktop"
  - "distro"
  - "environment"
  - "fedora"
  - "lakka"
  - "linux"
  - "oracle"
  - "ota-20"
  - "ota20"
  - "proxmox"
  - "release"
  - "seamonkey"
  - "storage"
  - "touch"
  - "ubports"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20237.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [AlmaLinux 8.5 is available](https://almalinux.org/blog/almalinux-os-85-stable-now-available/)

- [Release of Lakka 3.6](https://www.lakka.tv/articles/2021/11/14/lakka-3.6/)

- [LF Decentralized Storage Migrated to Open License](https://github.com/zerotier/lf/releases/tag/1.1.0.0)

- [SeaMonkey 2.53.10 Released](https://www.seamonkey-project.org/news#2021-11-16)

- [Fedora Linux 37 intends to end support for 32-bit ARM architecture](https://www.mail-archive.com/devel-announce@lists.fedoraproject.org/msg02684.html)

- [Release of Proxmox VE 7.1](https://forum.proxmox.com/threads/proxmox-ve-7-1-released.99846/)

- [Oracle Linux 8.5 Released](https://blogs.oracle.com/linux/post/announcing-the-release-of-oracle-linux-8-update-5)

- [Cinnamon 5.2 desktop environment released](https://github.com/linuxmint/Cinnamon/releases/tag/5.2.0)

- [Ubuntu Touch 20th Firmware Update](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-20-release-3790)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
