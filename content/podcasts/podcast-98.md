---
title: "Full Circle Weekly News 98"
date: 2018-07-01
draft: false
tags:
  - "beijing"
  - "committee"
  - "copyright"
  - "document"
  - "eu"
  - "facebook"
  - "filters"
  - "flatpak"
  - "foundation"
  - "gtk"
  - "libre"
  - "libreoffice"
  - "metrics"
  - "office"
  - "peppermint"
  - "pulseaudio"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2098.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Peppermint 9 Released](https://peppermintos.com/2018/06/peppermint-9-released/)

- [The Document Foundation announces LibreOffice 6.0.5](https://blog.documentfoundation.org/blog/2018/06/22/libreoffice-6-0-5/)

- [Flatpak reaches '1.0' status (pre-release) - a milestone](https://github.com/flatpak/flatpak/releases/tag/0.99.1)

- [PulseAudio releases version 12](https://www.freedesktop.org/wiki/Software/PulseAudio/Notes/12.0/)

- [GTK+ 3 update](https://blog.gtk.org/2018/06/23/a-gtk-3-update/)

- [Ubuntu Desktop Metrics](https://blog.ubuntu.com/2018/06/22/a-first-look-at-desktop-metrics)

- [EU committee voted to break the Internet: this Sunday, Berliners take to the streets to say NO!](https://boingboing.net/2018/06/21/then-we-take-berlin-3.html#more-613561)

- [EU's new copyright filters will be catastrophic for the web, but they're going to be even worse for your favourite game](https://kotaku.com/proposed-eu-copyright-law-could-cause-problems-for-fan-1827032250)

- [Beijing Wants to Rewrite the Rules of the Internet](https://www.theatlantic.com/international/archive/2018/06/zte-huawei-china-trump-trade-cyber/563033/?single_page=true)

- [Why Don’t You Like Us?](https://slate.com/technology/2018/06/i-left-facebook-for-10-days-and-facebook-did-not-let-me-forget-it.html)
