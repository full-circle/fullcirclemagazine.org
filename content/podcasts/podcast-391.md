---
title: "Full Circle Weekly News 391"
date: 2024-11-10
draft: false
tags:
  - "sway"
  - "raspberry"
  - "flock"
  - "flutter"
  - "fedora"
  - "linux"
  - "shotcut"
  - "audacity"
  - "truenas"
  - "pentester"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20391.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Sway 1.10 User Environment Released:](https://github.com/swaywm/sway/releases/tag/1.10)

- [Raspberry Pi OS switched to Wayland:](https://www.raspberrypi.com/news/a-new-release-of-raspberry-pi-os/)

- [The Flock project:](https://flutterfoundation.dev/blog/posts/we-are-forking-flutter-this-is-why/)

- [Fedora Linux Release 41:](https://fedoramagazine.org/announcing-fedora-linux-41/)

- [X.Org Server 21.1.14 Update with Vulnerability Fix:](https://gitlab.freedesktop.org/xorg/xserver/-/tags/)

- [Shotcut Video Editor 10/24:](https://shotcut.org/blog/new-release-241029/)

- [Release of Audacity 3.7:](https://github.com/audacity/audacity/releases/tag/Audacity-3.7.0)

- [Release of TrueNAS SCALE 24.10:](https://www.truenas.com/blog/truenas-electric-eel-powers-up-your-storage/)

- [Pentesters ready:](https://blog.backbox.org/2024/10/30/backbox-linux-9-released/)
- [and here](https://parrotsec.org/blog/2024-10-23-parrot-6.2-release-notes)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
