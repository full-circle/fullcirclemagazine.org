---
title: "Full Circle Weekly News 373"
date: 2024-07-07
draft: false
tags:
  - "guile"
  - "skudonet"
  - "openshot"
  - "opensuse"
  - "micro"
  - "sway"
  - "remix"
  - "tileos"
  - "pipewire"
  - "shotcut"
  - "google"
  - "kde"
  - "kwin"
  - "wayland"
  - "debian"
  - "theia"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20373.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [GNU Guile 3.0.10:](https://www.mail-archive.com/info-gnu@gnu.org/msg03295.html)

- [Release SKUDONET 7.1:](https://www.skudonet.com/blog/skudonet-7-1-0-community-edition/)

- [Release of OpenShot 3.2.0:](https://www.openshot.org/blog/2024/06/24/new_openshot_release_320/)

- [openSUSE Leap Micro 6.0:](https://news.opensuse.org/2024/06/25/leap-micro-60-availability/)

- [Ubuntu Sway Remix 24.04:](https://github.com/Ubuntu-Sway/Ubuntu-Sway-Remix/releases/tag/24.04)

- [TileOS 1.1 distribution update:](https://gitlab.com/tile-os/tileos/-/tags/v1.1)

- [Release of PipeWire 1.2.0:](https://github.com/PipeWire/pipewire/releases/tag/1.2.0)

- [Release of video editor Shotcut 24.06:](https://shotcut.org/blog/new-release-240626/)

- [Google announced the winners of the Open Source Peer Bonus 2024 award:](https://opensource.googleblog.com/2024/06/google-open-source-peer-bonus-program-first-group-2024-recipients.html)

- [KDE resolves KWin performance issues on older hardware:](https://pointieststick.com/2024/06/28/this-week-in-kde-everything-i-think/)

- [GNOME now supports Wayland-only builds and improves tablet support:](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3362)

- [New versions of Debian 12.6 and 11.10:](https://www.debian.org/News/2024/20240629)

- [Theia IDE:](https://newsroom.eclipse.org/news/announcements/eclipse-foundation-introduces-theia-ide-elevate-modern-developer-experience)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
