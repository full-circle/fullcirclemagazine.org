---
title: "Full Circle Weekly News 18"
date: 2016-05-21
draft: false
tags:
  - "ai"
  - "antivirus"
  - "apache"
  - "apps"
  - "arch"
  - "artificial"
  - "av"
  - "debian"
  - "download-2"
  - "falco"
  - "flaw"
  - "founder"
  - "free"
  - "google"
  - "intelligence"
  - "kodi"
  - "linhes"
  - "mythtv"
  - "openpht"
  - "portable"
  - "symantec"
  - "zfs"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2018.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Google Offers Artificial Intelligence Tool For Free](http://www.itechpost.com/articles/18332/20160514/google-artificial-intelligence-parsey-mcparseface.htm)

- [Portable Apps for Ubuntu 16.04 Now Available for Download](http://news.softpedia.com/news/portable-apps-for-ubuntu-16-04-lts-xenial-xerus-now-available-for-download-504138.shtml)

- [ZFS for Linux Finally Lands in Debian](http://news.softpedia.com/news/after-years-of-hard-work-zfs-for-linux-finally-lands-in-debian-gnu-linux-repos-504047.shtml)

- [Arch Linux-Based LinHES 8.4 OS Launches with Kodi, MythTV, and OpenPHT](http://news.softpedia.com/news/arch-linux-based-linhes-8-4-os-launches-with-kodi-16-1-mythtv-0-28-and-openpht-504076.shtml)

- [Symantec antivirus security flaw exposes Linux, Mac and Windows](http://www.engadget.com/2016/05/17/symantec-antivirus-cross-platform-security-flaw/)

- [Founder of the Apache Software Foundation Joins Linux Foundation to Lead Hyperledger Project](http://www.marketwired.com/press-release/founder-apache-software-foundation-joins-linux-foundation-lead-hyperledger-project-2126672.htm)

- [Open source tool watches Linux systems, containers for suspicious activity](http://www.infoworld.com/article/3072580/security/open-source-tool-watches-linux-systems-containers-for-suspicious-activity.html)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
