---
title: "Full Circle Weekly News 251"
date: 2022-03-08
draft: false
tags:
  - "browser"
  - "dahlia"
  - "debian"
  - "freebsd"
  - "freeciv"
  - "i2p"
  - "kernel"
  - "linutronix"
  - "mythtv"
  - "neptune"
  - "nftables"
  - "openbot"
  - "openssh"
  - "openvi"
  - "otter"
  - "packet"
  - "reiserfs"
  - "robot"
  - "slax"
  - "solaris"
  - "truenas"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20251.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of OpenBot 0.5, a smartphone-based robot platform](https://www.openbot.org/)

- [Solaris 11.4 SRU42 Released](https://blogs.oracle.com/solaris/post/announcing-oracle-solaris-114-sru42)

- [Release of OpenVi 7.0.12](https://github.com/johnsonjh/OpenVi/releases/tag/7.0.12)

- [Freeciv 3.0 game released](https://freeciv.fandom.com/wiki/NEWS-3.0.0)

- [nftables packet filter 1.0.2 released](https://www.mail-archive.com/netfilter-announce@lists.netfilter.org/msg00236.html)

- [Slax 11.2 based on Debian 11](https://www.slax.org/blog/25884-Releasing-final-Slax-11.2.0.html)

- [Release of Otter 1.0.3 web browser](https://otter-browser.org/)

- [New releases of I2P](https://geti2p.net/en/)

- [First release of TrueNAS SCALE distribution using Linux instead of FreeBSD](https://www.ixsystems.com/blog/truenas-scale-launch-press-release/)

- [Linux kernel developers discuss the possibility of removing ReiserFS](https://lkml.org/lkml/2022/2/20/89)

- [Intel acquires Linutronix, which develops the rt branch of the Linux kernel](https://community.intel.com/t5/Blogs/Products-and-Solutions/Software/Intel-Acquires-Linutronix/post/1362692)

- [Release of the media center MythTV 32.0](https://www.mythtv.org/news/172/v32.0%20Released)

- [OpenSSH 8.9 release fixes sshd vulnerability](https://lists.mindrot.org/pipermail/openssh-unix-dev/2022-February/040064.html)

- [NetworkManager 1.36.0 released](https://networkmanager.dev/blog/networkmanager-1-36/)

- [Ubuntu 20.04.4 LTS release with graphics stack and Linux kernel update](https://lists.ubuntu.com/archives/ubuntu-announce/2022-February/000277.html)

- [Mir 2.7 display server release](https://discourse.ubuntu.com/t/mir-release-2-7-0/26850)

- [dahliaOS 220222 operating system available](https://github.com/dahliaOS/releases/releases/tag/220222-x86_64)

- [Neptune OS project develops Windows compatibility layer based on seL4 microkernel](https://github.com/cl91/NeptuneOS/releases/tag/v0.1.0001)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
