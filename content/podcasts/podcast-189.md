---
title: "Full Circle Weekly News 189"
date: 2020-11-08
draft: false
tags:
  - "21-04"
  - "chromium"
  - "driver"
  - "freespire"
  - "hippo"
  - "hirsute"
  - "kde"
  - "linux"
  - "lite"
  - "mint"
  - "nvidia"
  - "plasma"
  - "pressure"
  - "server"
  - "ubuntu"
  - "valve"
  - "vessel"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20189.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Valve's Pressure Vessel Source Code Available](https://www.gamingonlinux.com/2020/10/valve-put-their-pressure-vessel-container-source-for-linux-games-up-on-gitlab)

- [Looking to Abandon the X Server](https://ajaxnwnk.blogspot.com/2020/10/on-abandoning-x-server.html)

- [Linux Mint Now Maintains Their Own Chromium](https://blog.linuxmint.com/?p=3978)

- [Freespire 7.0 Out](https://www.freespire.net/2020/10/freespire-70-released.html)

- [Linux Lite 5.2 Out](https://www.linuxliteos.com/forums/release-announcements/linux-lite-5-2-final-released/)

- [Ubuntu 21.04 Release Date Set](https://discourse.ubuntu.com/t/hirsute-hippo-release-schedule/18539)

- [KDE Plasma 5.20.2 Out](https://kde.org/announcements/plasma-5.20.2/)

- [Nvidia Short Lived Driver 455.38 Out](https://www.nvidia.com/Download/driverResults.aspx/166177/en-us)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
