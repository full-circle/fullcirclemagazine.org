---
title: "Full Circle Weekly News 344"
date: 2023-12-17
draft: false
tags:
  - "godot"
  - "alpine"
  - "endless"
  - "minetest"
  - "librepgp"
  - "gnupg"
  - "debian"
  - "lubuntu"
  - "wayland"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20344.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of Godot 4.2:](https://godotengine.org/article/godot-4-2-arrives-in-style/)

- [Release of Alpine Linux 3.19:](https://alpinelinux.org/posts/Alpine-3.19.0-released.html)

- [Release of Endless OS 5.1:](https://community.endlessos.com/t/release-endless-os-5-1-0/20855)

- [Release of Minetest 5.8.0:](https://blog.minetest.net/2023/12/04/5.8.0-released/)

- [Author GnuPG founded LibrePGP:](https://datatracker.ietf.org/doc/draft-koch-openpgp-2015-rfc4880bis/)

- [Debian 12.3 release delayed due to a problem causing damage to Ext4 FS:](https://www.debian.org/News/2023/2023120902)

- [Lubuntu will switch to Qt 6 and Wayland:](https://lubuntu.me/noble-alpha-featureset/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
