---
title: "Full Circle Weekly News 210"
date: 2021-05-16
draft: false
tags:
  - "antivirus"
  - "armbian"
  - "boot"
  - "bootdisk"
  - "bsd"
  - "client"
  - "code"
  - "codenet"
  - "coreboot"
  - "disk"
  - "distro"
  - "dragonfly"
  - "gnustep"
  - "hubzilla"
  - "ibm"
  - "learning"
  - "machine"
  - "media"
  - "player"
  - "postmarketos"
  - "putty"
  - "rescue"
  - "rescuepack"
  - "smartwatch"
  - "ssh"
  - "translate"
  - "ubuntu"
  - "vlc"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20210.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Interface for smartwatches added to postmarketOS](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2124)

- [New Releases of GNUstep Components](https://www.mail-archive.com/info-gnu@gnu.org/msg02888.html)

- [Armbian Distribution Release 21.05](https://forum.armbian.com/topic/18081-armbian-2105-jerboa/)

- [SSH client PuTTY 0.75 released](https://lists.tartarus.org/pipermail/putty-announce/2021/000031.html)

- [Ubuntu RescuePack 21.05 Antivirus Boot Disk Available](https://ualinux.com/ru/news/obnovlen-ubuntu-rescuepack-21-05)

- [DragonFly BSD 6.0 released](https://www.dragonflydigest.com/2021/05/10/25731.html)

- [VLC 3.0.14 media player update with vulnerability fixes](https://www.videolan.org/news.html#news-2021-05-10)

- [Coreboot 4.14 Released](https://blogs.coreboot.org/blog/2021/05/10/announcing-coreboot-4-14/)

- [Hubzilla 5.6 Released](https://hub.somaton.com/item/28fd7b30-7770-404f-995e-af97ad154187)

- [IBM opens CodeNet for machine learning systems that translate and validate code](https://research.ibm.com/blog/codenet-ai-for-code)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
