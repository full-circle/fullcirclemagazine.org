---
title: "Full Circle Weekly News 326"
date: 2023-08-13
draft: false
tags:
  - "alpine"
  - "gnu"
  - "boot"
  - "mxlinux"
  - "emacs"
  - "wayland"
  - "pixar"
  - "adobe"
  - "apple"
  - "autodesk"
  - "nvidia"
  - "ubuntu"
  - "kernel"
  - "xfs"
  - "freecad"
  - "thunderbird"
  - "lxd"
  - "vim"
  - "kde"
  - "window"
  - "maker"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20326.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The most active dev leaves Alpine Linux:](https://github.com/alpinelinux/aports/commit/9e88698cc296a8492a8f70938c58f5755f39609d)

- [Unofficial GNU Boot renamed:](https://savannah.gnu.org/projects/gnuboot/)

- [Release of MX Linux 23:](https://mxlinux.org/blog/mx-23-libretto-now-available/)

- [Release of GNU Emacs 29.1 with Wayland support:](https://www.mail-archive.com/info-gnu@gnu.org/msg03207.html)

- [Pixar, Adobe, Apple, Autodesk and NVIDIA began joint promotion of the OpenUSD platform:](https://www.linuxfoundation.org/press/announcing-alliance-for-open-usd-aousd)

- [Ubuntu will reduce the time to eliminate vulnerabilities in packages with the Linux kernel:](https://discourse.ubuntu.com/t/ubuntu-kernel-4-2-sru-cycle-announcement/37478)

- [Resignation of the XFS file system maintainer:](https://lore.kernel.org/linux-xfs/169091989589.112530.11294854598557805230.stgit@frogsfrogsfrogs/T/#m3890eb55a7ce45afa1830ed98decd98d0c8772c6)

- [Free CAD FreeCAD 0.21:](https://blog.freecad.org/2023/08/02/freecad-0-21-released/)

- [Update of Thunderbird 115.1:](https://www.thunderbird.net/en-US/thunderbird/115.1.0/releasenotes/)

- [LXD container control system fork:](https://news.ycombinator.com/item?id=36985384)

- [Key developer of Vim has died:](https://groups.google.com/g/vim_announce/c/tWahca9zkt4)

- [Experimental builds of KDE Neon with KDE 6:](https://blog.neon.kde.org/2023/08/04/announcing-kde-neon-experimental/)

- [GTK 4.12 is available:](https://gitlab.gnome.org/GNOME/gtk/-/tags/4.12.0)

- [Release of Window Maker 0.96.0:](http://windowmaker.org/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
