---
title: "Full Circle Weekly News 193"
date: 2020-12-12
draft: false
tags:
  - "apple"
  - "blackarch"
  - "cinnamon"
  - "kde"
  - "kernel"
  - "linux"
  - "m1"
  - "mac"
  - "manjaro"
  - "members"
  - "membership"
  - "nibia"
  - "nitrux"
  - "pangolin"
  - "plasma"
  - "system"
  - "system76"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20193.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Coming to Apple M1 Macs](https://www.patreon.com/marcan)

- [New Ubuntu Members](https://lists.ubuntu.com/archives/ubuntu-news-team/2020-December/002944.html)

- [Manjaro 20.2 Nibia Out](https://manjaro.org/news/)

- [BlackArch 2020.12.1 Out](https://blackarch.org/blog.html)

- [Nitrux 1.3.5 Out](https://blackarch.org/blog.html)

- [KDE Plasma 5.20.4 Out](https://kde.org/announcements/plasma-5.20.3-5.20.4-changelog/)

- [Cinnamon 4.8 Out](https://9to5linux.com/cinnamon-4-8-desktop-environment-released-new-features)

- [OpenZFS 2.0 Out](https://github.com/openzfs/zfs/releases/tag/zfs-2.0.0)

- [Linux Kernel 5.10 rc6 Out](https://www.lkml.org/lkml/2020/11/29/239)

- [System76 Pangolin Announced](https://system76.com/laptops/pangolin)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
