---
title: "Full Circle Weekly News 44"
date: 2016-11-26
draft: false
tags:
  - "ai"
  - "aliens"
  - "antivirus"
  - "apt"
  - "hackproof"
  - "kaspersky"
  - "microkernel"
  - "microsoft"
  - "os"
  - "parrot"
  - "reject"
  - "security"
  - "sha-1"
  - "sha1"
  - "slam"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2044.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu to Reject SHA-1-Signed Repos by Default in APT Starting January 1, 2017](http://news.softpedia.com/news/ubuntu-to-reject-sha-1-signed-repos-by-default-in-apt-starting-january-1-2017-510462.shtml)

- [Kaspersky OS: Antivirus Firm Launches Its Own “Hackproof” OS, Based On Microkernel](https://fossbytes.com/kaspersky-os-hackproof-microkernel/)

- [Parrot S.L.A.M.dunk](http://www.linuxjournal.com/content/parrot-slamdunk)

- [Microsoft Warns Bash on Windows Users About Linux File Changes](https://redmondmag.com/articles/2016/11/18/microsoft-warns-bash-on-windows.aspx)

- [How scientists will use artificial intelligence to find aliens](http://www.popsci.com/how-scientists-will-use-artificial-intelligence-to-find-aliens)

- [Elegant 0-day underscores “serious concerns” about Linux security](http://arstechnica.com/security/2016/11/elegant-0day-unicorn-underscores-serious-concerns-about-linux-security/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
