---
title: "Full Circle Weekly News 249"
date: 2022-02-20
draft: false
tags:
  - "absolute"
  - "binutils"
  - "cassowary"
  - "commander"
  - "desktop"
  - "gnome"
  - "inkscape"
  - "kasper"
  - "kde"
  - "kernel"
  - "mandriva"
  - "mariadb"
  - "openmandriva"
  - "plasma"
  - "postfix"
  - "postgresql"
  - "qubes"
  - "scanner"
  - "server"
  - "tails"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20249.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Cassowary project](https://github.com/casualsnek/cassowary)

- [Release of OS Qubes 4.1](https://www.qubes-os.org/news/2022/02/04/qubes-4-1-0/)

- [GNOME Commander 1.14 released](http://gcmd.github.io/)

- [Kasper, a speculative code execution problem scanner](https://www.vusec.net/projects/kasper/)

- [Absolute Linux 15.0 released](https://www.absolutelinux.org/)

- [Release of OpenMandriva Lx 4.3](https://www.openmandriva.org/en/news/article/openmandriva-lx-4-3-released)

- [Postfix 3.7.0 mail server](https://www.postfix.org/)

- [Alpha-Omega initiative for open source projects](https://openssf.org/press-release/2022/02/01/openssf-announces-the-alpha-omega-project-to-improve-software-supply-chain-security-for-10000-oss-projects/)

- [Inkscape 1.1.2 release and the start of testing of Inkscape 1.2](https://inkscape.org/news/2022/02/05/inkscape-112/)

- [KDE Plasma 5.24 Desktop Release](https://kde.org/announcements/plasma/5/5.24.0/)

- [Tails 4.27 released](https://tails.boum.org/)

- [Release of GNU Binutils 2.38](https://sourceware.org/pipermail/binutils/2022-February/119721.html)

- [MariaDB 10.7 stable release](https://mariadb.com/resources/blog/announcing-mariadb-community-server-10-7-2-ga-and-10-8-1-rc/)

- [PostgreSQL update](https://www.postgresql.org/about/news/postgresql-142-136-1210-1115-and-1020-released-2402/)

- [Remote vulnerability in the Linux kernel via the TIPC protocol](https://www.openwall.com/lists/oss-security/2022/02/10/1)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
