---
title: "Full Circle Weekly News 307"
date: 2023-04-02
draft: false
tags:
  - "libreboot"
  - "dragonflydb"
  - "coreutils"
  - "curl"
  - "apache"
  - "cloudstack"
  - "trisquel"
  - "pale moon"
  - "peertube"
  - "mylibrary"
  - "gnome"
  - "mozilla"
  - "tex"
  - "ubuntu"
  - "midnightbsd"
  - "slackel"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20307.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The release of Libreboot 20230319.](https://libreboot.org/news/libreboot20230319.html)

- [Release of Dragonflydb 1.0:](https://github.com/dragonflydb/dragonfly/releases/tag/v1.0.0)

- [Release of GNU Coreutils 9.2:](https://www.mail-archive.com/info-gnu@gnu.org/msg03156.html)

- [Release of cURL 8.0:](https://daniel.haxx.se/blog/2023/03/20/curl-8-0-1-because-i-jinxed-it/)

- [Release of Apache CloudStack 4.18:](https://blogs.apache.org/cloudstack/entry/apache-cloudstack-4-18-0)

- [Trisquel 11.0:](https://trisquel.info/en/forum/trisquel-11-released)

- [Pale Moon 32.1:](https://forum.palemoon.org/viewtopic.php?t=29594&p=237830#p237830)

- [Release of PeerTube 5.1:](https://joinpeertube.org/news/release-5.1)

- [Release of MyLibrary 2.1:](https://github.com/ProfessorNavigator/mylibrary/releases/tag/v2.1)

- [GNOME 44:](https://foundation.gnome.org/2023/03/22/introducing-gnome-44/)

- [Mozilla launches Mozilla.ai project for open machine learning systems:](https://blog.mozilla.org/en/mozilla/introducing-mozilla-ai-investing-in-trustworthy-ai/)

- [Release of TeX Live 2023:](https://tug.org/texlive/)

- [Ubuntu 20.04.6 LTS distribution update:](https://lists.ubuntu.com/archives/ubuntu-announce/2023-March/000287.html)

- [Release of Proxmox VE 7.4:](https://forum.proxmox.com/threads/proxmox-ve-7-4-released.124614/)

- [Release of the MidnightBSD 3.0:](https://www.justjournal.com/users/mbsd/entry/33919)

- [Slackel 7.6:](http://www.slackel.gr/forum/viewtopic.php?f=3&p=1902#p1902)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
