---
title: "Full Circle Weekly News 177"
date: 2020-07-11
draft: false
tags:
  - "20-04"
  - "bug"
  - "core"
  - "darktable"
  - "enterprise"
  - "eol"
  - "fedora"
  - "git"
  - "ipfire"
  - "kde"
  - "kernel"
  - "leak"
  - "lenovo"
  - "libre"
  - "libreoffice"
  - "linux"
  - "manjaro"
  - "office"
  - "opensuse"
  - "oracle"
  - "parrot"
  - "plasma"
  - "proton"
  - "red-hat"
  - "redhat"
  - "survey"
  - "suse"
  - "thinkpad"
  - "tumbleweed"
  - "ubuntu"
  - "virtualbox"
  - "vlc"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20177.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 20.04 Released](https://www.omgubuntu.co.uk/2020/04/download-ubuntu-20-04)

- [Ubuntu Survey Results](https://ubuntu.com/blog/ubuntu-20-04-survey-results)

- [Fedora 32 Released](https://fedoramagazine.org/announcing-fedora-32/)

- [Lenovo Now Shipping Fedora on Thinkpads](https://fedoramagazine.org/coming-soon-fedora-on-lenovo-laptops/)

- [Manjaro 20 Released](https://forum.manjaro.org/t/manjaro-20-0-lysia-released/138633)

- [Bug In Git May Leak Credentials](https://www.phoronix.com/scan.php?page=news_item&px=Git-Newline-Leak-Vulnerability)

- [Linux Kernel 5.7 rc4 Out](https://lkml.org/lkml/2020/5/3/306)

- [Linux Kernel 5.5 Is Now End of Life](http://lkml.iu.edu/hypermail/linux/kernel/2004.2/07196.html)

- [Red Hat Enterprise Linux 8.2 Out](https://www.redhat.com/archives/rhelv6-list/2020-April/msg00000.html)

- [Parrot 4.9 Out](https://parrotsec.org/blog/parrot-4.9-release-notes/)

- [IPFire 2.25 Core Update 143 Out](https://blog.ipfire.org/post/ipfire-2-25-core-update-143-released)

- [Oracle Virtualbox 6.1.6 Out](https://www.virtualbox.org/wiki/Changelog-6.1)

- [LibreOffice 6.4.3 Out](https://blog.documentfoundation.org/blog/2020/04/16/libreoffice-6-4-3/)

- [Proton 5.0-6 Out](https://www.gamingonlinux.com/articles/steam-play-proton-50-6-is-out-to-help-doom-eternal-rockstar-launcher-and-more-on-linux.16442)

- [VLC 3.0.10 Out](https://www.videolan.org/vlc/releases/3.0.10.html)

- [Darktable 3.0.2 Out](https://www.darktable.org/2020/04/darktable-302-released/)

- [OpenSUSE Tumbleweed for AWS Marketplace Out](https://9to5linux.com/opensuse-tumbleweed-is-now-available-on-aws-marketplace)

- [KDE 20.04 Applications Out](https://9to5linux.com/kde-applications-20-04-officially-released-this-is-whats-new)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
