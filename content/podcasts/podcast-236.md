---
title: "Full Circle Weekly News 236"
date: 2021-11-18
draft: false
tags:
  - "avi"
  - "chm"
  - "cryptographic"
  - "encoder"
  - "godot"
  - "kchmviewer"
  - "nebula"
  - "network"
  - "p2p"
  - "pi"
  - "raspberry"
  - "ravle"
  - "snoop"
  - "system76"
  - "viewer"
  - "wolfssl"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20236.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Cryptographic library wolfSSL 5.0.0 released](https://github.com/wolfSSL/wolfssl/releases/tag/v5.0.0-stable)

- [Release of Snoop 1.3.1](https://github.com/snooppr/snoop/releases)

- [System76 is working to create a new user environment](https://old.reddit.com/r/pop_os/comments/qnvrou/will_pop_os_ever_do_an_officially_kde_flavor_or/hjji8hh/)

- [Rav1e 0.5, AV1 encoder, released](https://github.com/xiph/rav1e/releases/tag/v0.5.0)

- [Godot 3.4 released](http://www.godotengine.org/)

- [New release of Raspberry Pi OS distribution](https://www.raspberrypi.com/news/raspberry-pi-os-debian-bullseye/)

 -[Release of an alternative build of KchmViewer](https://github.com/u-235/kchmviewer/releases/tag/v8.1-rc)

- [Release of the turn-based game Rusted Ruins 0.11](https://github.com/garkimasera/rusted-ruins/releases)

- [Release of Nebula 1.5, a system for creating overlay P2P networks](https://github.com/slackhq/nebula/releases/tag/v1.5.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
