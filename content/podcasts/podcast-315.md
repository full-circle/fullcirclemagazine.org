---
title: "Full Circle Weekly News 315"
date: 2023-05-28
draft: false
tags:
  - "amazon"
  - "google"
  - "snapchange"
  - "buzzer"
  - "cosmic"
  - "dietpi"
  - "rocky"
  - "tor"
  - "tails"
  - "lutris"
  - "weston"
  - "sqlite"
  - "coreboot"
  - "nyxt"
  - "redhat"
  - "almalinux"
  - "junodb"
  - "paypal"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20315.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Amazon and Google have opened Snapchange and Buzzer:](https://aws.amazon.com/blogs/opensource/announcing-snapchange-an-open-source-kvm-backed-snapshot-fuzzing-framework/)

- [A new panel written in Rust for COSMIC:](https://blog.system76.com/post/may-flowers-spring-cosmic-showers)

- [Release of DietPi 8.17:](https://dietpi.com/docs/releases/v8_17/)

- [Release of Rocky Linux 9.2:](https://rockylinux.org/news/rocky-linux-9-2-ga-release/)

- [Release of Tor 12.0.6 and Tails 5.13:](https://tails.boum.org/news/version_5.13/index.en.html)

- [Release of Lutris 0.5.13:](https://github.com/lutris/lutris/releases/tag/v0.5.13)

- [Release of Pale Moon 32.2:](https://forum.palemoon.org/viewtopic.php?t=29817&p=239377#p239377)

- [Release of Weston 12.0 composite server:](https://lists.freedesktop.org/archives/wayland-devel/2023-May/042720.html)

- [Release of SQLite 3.42:](https://www.sqlite.org/changes.html)

- [Release of Coreboot 4.20:](https://github.com/coreboot/coreboot/releases/tag/4.20)

- [Release of Nyxt 3.0.0:](https://nyxt.atlas.engineer/article/release-3.0.0.org)

- [Release of Red Hat Enterprise Linux 8.8:](https://access.redhat.com/announcements/7014081)

- [Available distribution AlmaLinux 8.8:](https://almalinux.org/blog/almalinux-88-now-available/)

- [PayPal opened JunoDB code:](https://medium.com/paypal-tech/unlocking-the-power-of-junodb-paypals-key-value-store-goes-open-source-ee85f935bdc1)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
