---
title: "Full Circle Weekly News 223"
date: 2021-08-17
draft: false
tags:
  - "0ad"
  - "alpha"
  - "alt"
  - "ardor"
  - "bottlerocket"
  - "browser"
  - "bullseye"
  - "client"
  - "debian"
  - "distro"
  - "dock"
  - "editor"
  - "elementary"
  - "elementaryos"
  - "email"
  - "fix"
  - "gear"
  - "gnu"
  - "gnunet"
  - "hurd"
  - "kde"
  - "latte"
  - "mail"
  - "nmap"
  - "p2p"
  - "postgresql"
  - "sound"
  - "tails"
  - "thunderbird"
  - "tor"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20223.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Latte Dock 0.10 released](https://psifidotos.blogspot.com/2021/08/latte-dock-v0100-official-stable-release.html)

- [Nmap 7.92 Released](https://seclists.org/nmap-announce/2021/3)

- [Bottlerocket 1.2, isolated container distribution released](https://github.com/bottlerocket-os/bottlerocket/releases/tag/v1.2.0)

- [Release of the P2P platform GNUnet 0.15.0](https://gnunet.org/en/news/2021-08-0.15.0.html)

- [25th Alpha release available 0 AD](https://play0ad.com/new-release-0-a-d-alpha-25-yauna/)

- [Tor Browser 10.5.4 and Tails 4.21 distribution](https://tails.boum.org/news/version_4.21/)

- [Release of Elementary OS 6](https://blog.elementary.io/elementary-os-6-odin-released/)

- [The tenth ALT platform](http://basealt.ru/about/news/archive/view/desjataja-platforma-proekta-sizif-novyi-vitok-ehvolju/)

- [Thunderbird 91 Mail Client Released](https://www.thunderbird.net/en-US/thunderbird/91.0/releasenotes/)

- [PostgreSQL update with vulnerability fix](https://www.postgresql.org/about/news/postgresql-134-128-1113-1018-9623-and-14-beta-3-released-2277/)

- [Release of KDE Gear 21.08](https://kde.org/announcements/gear/21.08.0/)

- [Debian GNU / Hurd 2021 Available](https://lists.debian.org/debian-hurd/2021/08/msg00040.html)

- [Ardor 6.9 Free Sound Editor Released](https://discourse.ardour.org/t/ardour-6-9-released/106273)

- [Debian 11 “Bullseye” Released](https://www.debian.org/News/2021/20210814)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
