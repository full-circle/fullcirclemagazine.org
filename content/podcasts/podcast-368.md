---
title: "Full Circle Weekly News 368"
date: 2024-06-02
draft: false
tags:
  - "mx linux"
  - "ubuntu"
  - "nintendo"
  - "switch"
  - "icewm"
  - "cadzinho"
  - "geary"
  - "ghostbsd"
  - "alpine"
  - "qualcomm"
  - "kde"
  - "gcompris"
  - "sane"
  - "gnome"
  - "ravynos"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20368.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of MX Linux 23.3:](https://mxlinux.org/blog/mx-23-3-libretto-released/)

- [Ubuntu 24.04 builds for the Nintendo Switch:](https://twitter.com/switchroot_org/status/1785702619509346306)

- [IceWM 3.5.0 released:](https://github.com/ice-wm/icewm/releases/tag/3.5.0)

- [Release of free 2D CAD software CadZinho 0.6:](https://github.com/zecruel/CadZinho/releases/tag/0.6.0)

- [Release of Geary 46.0:](https://gitlab.gnome.org/GNOME/geary/-/tags/46.0)

- [GhostBSD 24.04.1:](https://www.ghostbsd.org/news/GhostBSD_24.04.1_Is_Now_Available)

- [Release of Alpine Linux 3.20:](https://alpinelinux.org/posts/Alpine-3.20.0-released.html)

- [Qualcomm will provide support for Snapdragon X Elite chips in the Linux kernel:](https://www.qualcomm.com/developer/blog/2024/05/upstreaming-linux-kernel-support-for-the-snapdragon-x-elite)

- [Release of KDE Gear 24.05:](https://kde.org/announcements/gear/24.05.0/)

- [GCompris 4.1 update:](https://gcompris.net/news/2024-05-23-en.html)

- [SANE 1.3 published with support for new scanner models:](https://alioth-lists.debian.net/pipermail/sane-announce/2024/000047.html)

- [Initial prototype of the new installer for GNOME OS:](https://thisweek.gnome.org/posts/2024/05/twig-149/)

- [Release of Red Hat Enterprise Linux 8.10:](https://www.redhat.com/en/blog/optimize-application-life-cycles-red-hat-enterprise-linux-810)

- [The ravynOS project is developing an edition of FreeBSD aimed at compatibility with macOS:](https://github.com/ravynsoft/ravynos/releases/tag/v0.5.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
