---
title: "Full Circle Weekly News 216"
date: 2021-06-28
draft: false
tags:
  - "antivirus"
  - "bcm"
  - "brave"
  - "browser"
  - "bypass"
  - "can"
  - "centos"
  - "clamav"
  - "ebpf"
  - "enterprise"
  - "image"
  - "iso"
  - "kernel"
  - "library"
  - "linux"
  - "mini"
  - "network"
  - "nixos"
  - "oasis"
  - "opendocument"
  - "plotly.py"
  - "protocol"
  - "rocky"
  - "search"
  - "search engine"
  - "security"
  - "sp3"
  - "specter"
  - "subsystem"
  - "suse"
  - "toolkit"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20216.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Network Security Toolkit 34](https://sourceforge.net/p/nst/news/2021/06/nst-version-34-12743-released/)

- [Min 1.20 Published](https://minbrowser.org/releases/index.html?n=5)

- [Linux kernel vulnerability affecting CAN BCM network protocol](https://lore.kernel.org/netdev/20210619161813.2098382-1-cascardo@canonical.com/T/#u)

- [Vulnerabilities in eBPF Subsystem to Bypass Specter Protection](https://www.openwall.com/lists/oss-security/2021/06/21/1)

- [NixOS provides repeatable build support for the iso image](https://discourse.nixos.org/t/nixos-unstable-s-iso-minimal-x86-64-linux-is-100-reproducible/13723)

- [Rocky Linux 8.4 Released, Replacing CentOS](https://rockylinux.org/news/rocky-linux-8-4-ga-release/)

- [Release of the plotly.py visualization library 5.0](https://community.plotly.com/t/introducing-plotly-py-5-0-0-a-new-federated-jupyter-extension-icicle-charts-and-bar-chart-patterns/54039)

- [Updating the free antivirus package ClamAV 0.103.3](https://blog.clamav.net/2021/06/clamav-01033-patch-release.html)

- [The Brave project has begun testing its own search engine](https://brave.com/brave-search-beta/)

- [OASIS Consortium Approves OpenDocument 1.3 As Standard](https://blog.documentfoundation.org/blog/2021/06/23/odf-1-3-is-an-oasis-standard/)

- [SUSE Linux Enterprise 15 SP3 available](https://www.suse.com/c/introducing-suse-linux-enterprise-15-sp3/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
