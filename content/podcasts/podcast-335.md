---
title: "Full Circle Weekly News 335"
date: 2023-10-20
draft: false
tags:
  - "veracrypt"
  - "budgie"
  - "thunderbird"
  - "email"
  - "mint"
  - "kernel"
  - "elementary"
  - "openssh"
  - "slax"
  - "valve"
  - "ferrocene"
  - "openpubkey"
  - "incus"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20335.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [VeraCrypt 1.26:](https://www.veracrypt.fr/en/Release%20Notes.html)

- [Budgie 10.8.1:](https://buddiesofbudgie.org/blog/budgie-10-8-1-released)

- [Advertising ransomware as Thunderbird mail client:](https://blog.thunderbird.net/2023/10/ransomware-alert-are-you-using-a-trusted-version-of-thunderbird/)

- [Mint Edge 21.2 with new Linux kernel:](https://blog.linuxmint.com/?p=4584)

- [Release of Elementary OS 7.1:](https://blog.elementary.io/os-7-1-available-now/)

- [Release of OpenSSH 9.5:](https://lists.mindrot.org/pipermail/openssh-unix-dev/2023-October/040967.html)

- [Slax 15.0.3 and 12.1.0:](https://www.slax.org/blog/27935-New-Slax-Key-Features-and-Enhancements.html)

- [Valve has released Proton 8.0-4:](https://github.com/ValveSoftware/Proton/releases/tag/proton-8.0-4)

- [Opening the code of Ferrocene:](https://ferrous-systems.com/blog/ferrocene-open-source/)

- [Release of jsii 1.90:](https://github.com/aws/jsii/releases/tag/v1.90.0)

- [Presenting OpenPubKey:](https://www.linuxfoundation.org/press/announcing-openpubkey-project)

- [First release of Incus:](https://groups.google.com/a/lists.linuxcontainers.org/g/lxc-users/c/5G6jN--29uU)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
