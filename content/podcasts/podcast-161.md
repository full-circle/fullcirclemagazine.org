---
title: "Full Circle Weekly News 161"
date: 2020-01-08
draft: false
tags:
  - "19-3"
  - "alpine"
  - "binary"
  - "calculate"
  - "cinnamon"
  - "dxvk"
  - "gentoo"
  - "irc"
  - "kernel"
  - "krita"
  - "librem"
  - "linux"
  - "mate"
  - "matrix"
  - "mint"
  - "mozilla"
  - "peppermint"
  - "purism"
  - "respin"
  - "riot"
  - "server"
  - "xfce"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20161.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [DXVK 1.5 Released](https://github.com/doitsujin/dxvk/releases/tag/v1.5)

- [Calculate Linux 20 Released](https://forum.calculate-linux.org/t/calculate-linux-20/9805)

- [Purism Announces the Librem Server](https://puri.sm/products/librem-server/)

- [Linux Mint 19.3 Cinnamon, MATE and XFCE Released](https://blog.linuxmint.com/?p=3832)

- [Peppermint 10 Respin Released](https://peppermintos.com/2019/12/peppermint-10-respin-released/)

- [Alpine Linux 3.11.0 Released](https://alpinelinux.org/posts/Alpine-3.11.0-released.html)

- [Krita Recieves Epic MegaGrant](https://krita.org/en/item/krita-receives-epic-megagrant/)

- [Gentoo Adds an Experimental Binary Kernel](https://blogs.gentoo.org/mgorny/2019/12/19/a-distribution-kernel-for-gentoo/)

- [Mozilla Migrating from IRC to Riot and Matrix](https://discourse.mozilla.org/t/synchronous-messaging-at-mozilla-the-decision/50620)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
