---
title: "Full Circle Weekly News 40"
date: 2016-10-22
draft: false
tags:
  - "16-10"
  - "17-04"
  - "android"
  - "backdoor"
  - "bootloader"
  - "entroware"
  - "gtk"
  - "iot"
  - "lubuntu"
  - "lxqt"
  - "nyadrop"
  - "xubuntu"
  - "zapus"
  - "zesty"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2040.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 17.04 codename revealed. Will Launch in April 2017](http://news.softpedia.com/news/ubuntu-17-04-to-be-dubbed-zesty-zapus-will-launch-on-april-2017-509371.shtml)

- [Entroware Is Now Shipping PCs with Ubuntu 16.10 and Ubuntu MATE 16.10](http://news.softpedia.com/news/entroware-is-now-shipping-pcs-with-ubuntu-16-10-and-ubuntu-mate-16-10-509349.shtml)

- [Xubuntu 16.10 Released, Includes Xfce Packages Built with GTK+ 3 Technologies](http://news.softpedia.com/news/xubuntu-16-10-released-includes-xfce-packages-built-with-gtk-plus-3-technologies-509273.shtml)

- [Lubuntu 16.10 Lands as a Bugfix Release That Prepares the Distro for LXQt](http://news.softpedia.com/news/lubuntu-16-10-lands-as-a-bugfix-release-that-prepares-the-distro-for-lxqt-509282.shtml)

- [Pork Explosion Backdoor Found in Some Android Bootloaders](https://www.onthewire.io/pork-explosion-backdoor-found-in-some-android-bootloaders/)

- [Linux-run IoT devices under attack by NyaDrop](http://www.scmagazine.com/linux-run-iot-devices-under-attack-by-nyadrop/article/561801/)

- [“Most serious” Linux privilege-escalation bug ever is under active exploit](http://arstechnica.com/security/2016/10/most-serious-linux-privilege-escalation-bug-ever-is-under-active-exploit/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
