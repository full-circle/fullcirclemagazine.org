---
title: "Full Circle Weekly News 101"
date: 2018-07-30
draft: false
tags:
  - "atari"
  - "atarios"
  - "debian"
  - "enterprise"
  - "forbes"
  - "gnome"
  - "hat"
  - "pinguy"
  - "red"
  - "redhat"
  - "slackware"
  - "stretch"
  - "system76"
  - "ubuntu"
  - "vcs"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20101.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Atari VCS RAM Doubled To 8GB; Will Ship With Linux-based Distro “AtariOS”](https://fossbytes.com/atari-vcs-ram-atarios-linux-distro/)

- [Forbes writer falls in love with Ubuntu after two decades of Windows updates](https://www.forbes.com/sites/jasonevangelho/2018/07/19/ditching-windows-2-weeks-with-ubuntu-linux-on-the-dell-xps-13/#2733a0e81836)

- [Pinguy OS Puts On a Happier GNOME 3 Face](https://www.linuxinsider.com/story/Pinguy-OS-Puts-On-a-Happier-GNOME-3-Face-85439.html)

- [Debian 'Stretch' 9.5 Linux distribution available for download](https://betanews.com/2018/07/14/debian-linux-stretch-95-download/)

- [Slackware, The Oldest Active Linux Distro, Turns 25](https://fossbytes.com/slackware-birthday-25-oldest-active-linux-distro/)

- [Red Hat Enterprise Linux 6 & CentOS 6 Patched Against Spectre V4, Lazy FPU Flaws](https://news.softpedia.com/news/red-hat-enterprise-linux-6-centos-6-patched-against-spectre-v4-lazy-fpu-flaws-521965.shtml)

- [System76 Linux computer maker offers a sneak peek into its new manufacturing facility](https://betanews.com/2018/07/17/system76-linux-manufacturing-facility/)
