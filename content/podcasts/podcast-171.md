---
title: "Full Circle Weekly News 171"
date: 2020-05-15
draft: false
tags:
  - "android"
  - "apt"
  - "bsd"
  - "caa"
  - "collabora"
  - "core"
  - "dragonfly"
  - "driver"
  - "encrypt"
  - "encryption"
  - "glimpse"
  - "gnu"
  - "home"
  - "iphone"
  - "kaos"
  - "kernel"
  - "kiosk"
  - "libre"
  - "libreoffice"
  - "linspire"
  - "nvidia"
  - "office"
  - "porteus"
  - "rc5"
  - "rechecking"
  - "sandcastle"
  - "systemd"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20171.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Let's Encrypt Moves Forward with the CAA Rechecking Bug](https://community.letsencrypt.org/t/2020-02-29-caa-rechecking-bug/114591/2)

- [Project Sandcastle runs Android on iPhone 7](https://www.xda-developers.com/install-android-10-apple-iphone-7-plus-project-sandcastle-checkra1n-jailbreak/)

- [Glimpse 0.1.2 Is More than a Name Change](https://glimpse-editor.org/posts/glimpse-0-1-2-release-notes/)

- [systemd 245 Now Supports Encrypted Home Directories](https://lists.freedesktop.org/archives/systemd-devel/2020-March/044119.html)

- [Linux Kernel 5.6 rc5 Out](https://lkml.org/lkml/2020/3/8/398)

- [Nvidia Driver 440.64 Out; with better support for kernel 5.6](https://www.nvidia.com/Download/driverResults.aspx/157462/en-us?cjevent=7051b3725c4811ea8305029f0a18050e)

- [APT 2.0 Out; and will now wait indefinitely for dpkg locks](https://blog.jak-linux.org/2020/03/07/apt-2.0/)

- [Collabora Online 4.2.0 Out](https://www.collaboraoffice.com/press-releases/collabora-online-4-2-0-released/)

- [LibreOffice 6.4.1 Out](https://blog.documentfoundation.org/blog/2020/02/27/libreoffice-641/)

- [GNU Core Utilities 8.32 Out](http://savannah.gnu.org/forum/forum.php?forum_id=9693)

- [KaOS 2020.02 Out](https://kaosx.us/news/2020/kaos02/)

- [Linspire 8.7 Out](https://www.linspirelinux.com/2020/03/linspire-87-release.html)

- [Kubeflow 1.0 Out](https://medium.com/kubeflow/kubeflow-1-0-cloud-native-ml-for-everyone-a3950202751)

- [Porteus Kiosk 5.0.0 Out](https://porteus-kiosk.org/news.html#200302)

- [DragonFly BSD 5.8 Out](http://lists.dragonflybsd.org/pipermail/users/2020-March/358432.html)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
