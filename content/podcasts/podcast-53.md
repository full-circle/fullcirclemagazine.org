---
title: "Full Circle Weekly News 53"
date: 2017-02-18
draft: false
tags:
  - "16-04-2"
  - "16-10"
  - "17-04"
  - "4-8"
  - "aggresive"
  - "ai"
  - "beta"
  - "deft"
  - "forensics"
  - "freeze"
  - "google"
  - "kernel"
  - "munich"
  - "zero"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2053.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 16.04.2 LTS Officially Released with Linux Kernel 4.8 from Ubuntu 16.10](http://news.softpedia.com/news/ubuntu-16-04-2-lts-officially-released-with-linux-kernel-4-8-from-ubuntu-16-10-512758.shtml)

- [Ubuntu 17.04 (Zesty Zapus) Has Entered Feature Freeze, Beta Lands February 23](Zesty Zapus)

- [Linux pioneer Munich poised to ditch open source and return to Windows](http://www.techrepublic.com/article/linux-pioneer-munich-poised-to-ditch-open-source-and-return-to-windows/)

- [Terrifyingly, Google's Artificial Intelligence acts aggressively when cornered](http://www.chron.com/news/science-environment/article/Google-s-Artificial-Intelligence-acts-10931151.php)

- [DEFT “Zero” Linux 2017.1 Lightweight Digital Forensics Distro Available For Download](https://fossbytes.com/deft-zero-linux-2017-features-download/)

- [Open source smart home platform gains Ubuntu snap packages](http://linuxgizmos.com/open-source-smart-home-platform-gains-ubuntu-snap-packages/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
