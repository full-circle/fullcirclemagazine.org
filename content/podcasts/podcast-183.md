---
title: "Full Circle Weekly News 183"
date: 2020-09-30
draft: false
tags:
  - "btrfs"
  - "disk"
  - "fedora"
  - "filesystem"
  - "kde"
  - "linux"
  - "manjaro"
  - "mikah"
  - "mint"
  - "red-hat"
  - "redhat"
  - "virtualbox"
  - "wayland"
  - "webapp"
  - "wsl2"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20183.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [WSL2 Now Mounts Linux Disks](https://www.omgubuntu.co.uk/2020/09/wsl-2-access-ext4-from-windows-10)

- [Linux Mint Planning a WebApp Manager](https://blog.linuxmint.com/?p=3960)

- [Fedora to Use BTRFS as Default Filesystem in Fedora 33](https://fedoramagazine.org/btrfs-coming-to-fedora-33/)

- [Fedora Adding Wayland Support for KDE in 34](https://fedoraproject.org/wiki/Changes/WaylandByDefaultForPlasma)

- [Manjaro 20.1, Mikah Out](https://forum.manjaro.org/t/manjaro-20-1-mikah-got-released/24173)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
