---
title: "Full Circle Weekly News 119"
date: 2019-02-01
draft: false
tags:
  - "19-04"
  - "android"
  - "core"
  - "iot"
  - "lts"
  - "multipass"
  - "orange"
  - "pi"
  - "pureos"
  - "purism"
  - "runtime"
  - "studio"
  - "support"
  - "ubuntu"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20119.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Purism 'PureOS Store' will be for both desktop and mobile apps, and that's a mistake](https://betanews.com/2019/01/18/purism-pureos-store-desktop-mobile/)

- [Ubuntu Core doubles down on Internet of Things](https://www.zdnet.com/article/ubuntu-core-doubles-down-on-internet-of-things/)

- [Ubuntu Core 18 Released With 10-Year LTS Support](https://fossbytes.com/ubuntu-core-18-released-with-10-year-lts-support/)

- [Ubuntu Studio 19.04 Has New Tricks Up Its Sleeve For Linux Creatives](https://www.forbes.com/sites/jasonevangelho/2019/01/24/ubuntu-studio-19-04-has-new-tricks-up-its-sleeve-for-linux-creatives/#7a3a077f3dc5)

- [Want to spin up Ubuntu VMs from Windows 10's command line, eh? We'll need to see a Multipass](https://www.theregister.co.uk/2019/01/22/multipass)

- [Orange Pi 3 Single Board Computer Is Here; Runs Linux And Android](https://fossbytes.com/orange-pi-3-single-board-computer-is-here-runs-linux-and-android/)

- [Runtime security agent tailors itself to each Linux-based IoT device](http://linuxgizmos.com/runtime-security-agent-tailors-itself-to-each-linux-based-iot-device/)
