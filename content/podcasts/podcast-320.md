---
title: "Full Circle Weekly News 320"
date: 2023-07-02
draft: false
tags:
  - "freebsd"
  - "bcachefs"
  - "kernel"
  - "syslinuxos"
  - "sdl"
  - "suse"
  - "enterprise"
  - "centos"
  - "peertube"
  - "intel"
  - "tor"
  - "proxmox"
  - "flowblade"
  - "opensnitch"
  - "browserbox"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20320.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The FreeBSD project is 30 years old:](https://www.freebsdfoundation.org/national-freebsd-day/)

- [Bcachefs in the Linux kernel:](https://news.ycombinator.com/item?id=36366002)

- [SysLinuxOS 12:](https://syslinuxos.com/syslinuxos-12-for-system-integrators/)

- [Release of the SDL 2.28.0:](https://discourse.libsdl.org/t/announcing-sdl-2-28-0/44341)

- [SUSE Linux Enterprise 15 SP5:](https://www.suse.com/news/SUSE-Advances-Capabilities-to-its-Comprehensive-Software-Stack-to-Help-Customers-Accelerate-Digital-Transformation/)

- [CentOS Stream will be the only public source of RHEL packages:](https://www.redhat.com/en/blog/furthering-evolution-centos-stream)

- [PeerTube 5.2:](https://joinpeertube.org/news/release-5.2)

- [Intel and Blockade Labs have released a model for the synthesis of 3D images:](https://www.intc.com/news-events/press-releases/detail/1630/intel-labs-introduces-ai-diffusion-model-generates)

- [Release Tor Browser 12.5:](https://blog.torproject.org/new-release-tor-browser-125/)

- [Release of Proxmox VE 8.0:](https://forum.proxmox.com/threads/proxmox-ve-8-0-released.129320/)

- [Release of Flowblade 2.10:](http://jliljebl.github.io/flowblade/)

- [OpenSnitch 1.6.0:](https://github.com/evilsocket/opensnitch/releases/tag/v1.6.0)

- [BrowserBox Pro:](https://github.com/dosyago/BrowserBoxPro)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
