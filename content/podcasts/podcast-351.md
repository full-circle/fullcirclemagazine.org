---
title: "Full Circle Weekly News 351"
date: 2024-02-04
draft: false
tags:
  - "dietpi"
  - "linux"
  - "rubywm"
  - "cryptsetup"
  - "radix"
  - "zed"
  - "parrot"
  - "gnoppix"
  - "redcore"
  - "budgie"
  - "slackel"
  - "niri"
  - "wayland"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20351.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of DietPi 9.0:](https://dietpi.com/docs/releases/v9_0/)

- [MX Linux 23.2:](https://mxlinux.org/blog/mx-23-2-libretto-released/)

- [AV Linux 23.1:](http://www.bandshed.net/2024/01/14/av-linux-23-1-enlightened-released/)

- [RubyWM, a window manager written entirely in Ruby:](https://rubyflow.com/p/pb1tu2-rubywm-an-x11-window-manager-in-pure-ruby)

- [Cryptsetup 2.7, with support for OPAL hardware disk encryption:](https://gitlab.com/cryptsetup/cryptsetup)

- [Release of the Radix cross 1.9.367:](https://radix.pro/platform/install/)

- [Zed editor opens to support collaborative coding:](https://zed.dev/blog/zed-is-now-open-source)

- [Release of Parrot 6.0:](https://parrotsec.org/blog/2024-01-24-parrot-6.0-release-notes/)

- [Gnoppix 24:](https://www.gnoppix.com/blog/gnoppix-24-1/)

- [Release of Redcore Linux 2401:](https://redcorelinux.org/news/redcore-linux-hardened-2401-tarazed-stable)

- [Final performance optimization and interface polish in KDE 6:](https://pointieststick.com/2024/01/26/this-week-in-kde-everything-everywhere-all-at-once-edition/)

- [Budgie Desktop Shell Roadmap in 2024:](https://buddiesofbudgie.org/blog/state-of-the-budgie-2023)

- [Release of the Slackel 7.7 with the MATE desktop:](https://slackel.sourceforge.io/forum/viewtopic.php?t=778)

- [First release of Niri composite server using Wayland:](https://github.com/YaLTeR/niri/releases/tag/v0.1.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
