---
title: "Full Circle Weekly News 360"
date: 2024-04-07
draft: false
tags:
  - "sysvinit"
  - "emacs"
  - "truenas"
  - "ubuntu"
  - "fedora"
  - "ext2"
  - "bubblewrap"
  - "qubes"
  - "tails"
  - "samba"
  - "redis"
  - "furygpu"
  - "debian"
  - "backdoor"
  - "netbsd"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20360.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of SysVinit 3.09:](https://github.com/slicer69/sysvinit/releases/tag/3.09)

- [Release of GNU Emacs 29.3:](https://www.mail-archive.com/info-gnu@gnu.org/msg03269.html)

- [After TrueNAS CORE 13.3, the FreeBSD-based branch will be put into maintenance mode:](https://www.truenas.com/blog/truenas-core-13-3-plans/)

- [Starting with Ubuntu 14.04, support for LTS releases will be increased to 12 years:](https://canonical.com/blog/canonical-expands-long-term-support-to-12-years-starting-with-ubuntu-14-04-lts)

- [Fedora 41 plans to move to the DNF5 package manager:](https://www.mail-archive.com/devel-announce@lists.fedoraproject.org/msg03249.html)

- [Ext2 changes:](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id%3Db960e8093e7a57de98724931d17b2fa86ff1105f)

- [Fedora Linux 40 distribution enters beta testing:](https://fedoramagazine.org/announcing-fedora-linux-40-beta/)

- [Release of Bubblewrap 0.9:](https://github.com/containers/bubblewrap/releases/tag/v0.9.0)

- [Update to Qubes OS 4.2.1:](https://www.qubes-os.org/news/2024/03/26/qubes-os-4-2-1-has-been-released/)

- [Release of Tails 6.1:](https://tails.net/)

- [Samba 4.20.0 release:](https://lists.samba.org/archive/samba-announce/2024/000663.html)

- [VPN Lanemu 0.11.6 Released:](https://gitlab.com/Monsterovich/lanemu/-/releases/0.11.6)

- [A fork of the Redis:](https://www.linuxfoundation.org/press/linux-foundation-launches-open-source-valkey-community)

- [Release of GNU Coreutils 9.5:](https://www.mail-archive.com/info-gnu@gnu.org/msg03270.html)

- [The FuryGpu project develops FPGA-based GPUs:](https://www.furygpu.com/blog/hello)

- [A backdoor was discovered in the xz/liblzma library that allows entry via sshd:](https://www.openwall.com/lists/oss-security/2024/03/29/4)

- [Debian 10 "Buster" moved to archive:](https://lists.debian.org/debian-devel-announce/2024/03/msg00003.html)

- [Release of NetBSD 10.0:](https://blog.netbsd.org/tnf/entry/netbsd_10_0_available)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
