---
title: "Full Circle Weekly News 169"
date: 2020-04-29
draft: false
tags:
  - "census"
  - "foundation"
  - "gnome"
  - "gtk"
  - "i3"
  - "linux"
  - "mangohud"
  - "mesa"
  - "mx-linux"
  - "mxlinux"
  - "virtualbox"
  - "wine"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20169.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Foundation's Core Infrastructure Initiative Releases Census II Analysis](https://www.zdnet.com/article/the-linux-foundation-identifies-the-most-important-open-source-software-components-and-their-problems/)

- [Refreshed GTK Website Goes Live](https://www.omgubuntu.co.uk/2020/02/gtk-website-redesign)

- [MX Linux 19.1 Now Available](https://mxlinux.org/blog/mx-19-1-now-available/)

- [MangoHud 0.2.0 Is Out](https://www.gamingonlinux.com/articles/vulkan-overlay-layer-mangohud-continues-advancing-quickly-with-a-big-new-release.15994)

- [Wine 5.2 Is Out](https://www.winehq.org/announce/5.2)

- [Mesa 20 Is Out](https://lists.freedesktop.org/archives/mesa-dev/2020-February/224132.html)

- [Virtualbox 6.1.4 Is Out](https://www.virtualbox.org/wiki/Changelog-6.1#v4)

- [Gnome 3.35.2 Is Out](https://mail.gnome.org/archives/devel-announce-list/2019-November/msg00001.html)

- [Gnome 3.34.4 Is Out](https://mail.gnome.org/archives/devel-announce-list/2020-February/msg00003.html)

- [i3 Window Manager 4.18 Is Out](https://i3wm.org/downloads/RELEASE-NOTES-4.18.txt)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
