---
title: "Full Circle Weekly News 106"
date: 2018-09-06
draft: false
tags:
  - "16-04"
  - "2018-3"
  - "3-9"
  - "anonymous"
  - "arm"
  - "browser"
  - "centos"
  - "debian"
  - "ethical"
  - "firefox"
  - "gnome"
  - "hacking"
  - "kali"
  - "linux"
  - "lts"
  - "ota-4"
  - "ota4"
  - "patch"
  - "security"
  - "stretch"
  - "tails"
  - "truecrypt"
  - "ubports"
  - "ubuntu"
  - "veracrypt"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20106.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Firefox Browser Will Soon Block All Web Trackers By Default](https://fossbytes.com/firefox-blocking-trackers/)

- [UBports Releases Ubuntu Touch OTA-4 for Ubuntu Phones, Based on Ubuntu 16.04 LTS](https://news.softpedia.com/news/ubuntu-touch-ota-4-released-for-ubuntu-phones-finally-based-on-ubuntu-16-04-lts-522382.shtml)

- [Ubuntu and CentOS Are Undoing a GNOME Security Feature](https://www.bleepingcomputer.com/news/security/ubuntu-and-centos-are-undoing-a-gnome-security-feature/)

- [Debian Stretch Gets Patch for Regression Causing Boot Failures on ARM Systems](https://news.softpedia.com/news/debian-project-fixes-regression-causing-arm-boot-failures-on-debian-stretch-522418.shtml)

- [Kali Linux 2018.3 Ethical Hacking OS Adds iOS Research, Penetration Testing Tool](https://news.softpedia.com/news/kali-linux-2018-3-ethical-hacking-os-adds-ios-research-penetration-testing-tool-522401.shtml)

- [Tails 3.9 Anonymous OS Is Coming September 5 with TrueCrypt & VeraCrypt Support](https://news.softpedia.com/news/tails-3-9-anonymous-os-is-coming-september-5-with-truecrypt-veracrypt-support-522397.shtml)
