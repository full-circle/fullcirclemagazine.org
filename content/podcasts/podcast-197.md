---
title: "Full Circle Weekly News 197"
date: 2021-01-23
draft: false
tags:
  - "2021"
  - "21-04"
  - "alpine"
  - "cloudlinux"
  - "defender"
  - "fedora"
  - "flatpak"
  - "folder"
  - "home"
  - "kaos"
  - "kinote"
  - "lenix"
  - "linux"
  - "microsoft"
  - "mobian"
  - "os"
  - "pi"
  - "pinephone"
  - "project"
  - "proton"
  - "raspberry"
  - "servers"
  - "tails"
  - "ubuntu"
  - "valve"
  - "wine"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20197.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Full Circle Weekly News is now on Spotify:](https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK)

- [Ubuntu Making Home Folders Private in 21.04](https://discourse.ubuntu.com/t/private-home-directories-for-ubuntu-21-04-onwards/19533/2)

- [Ubuntu 21.04 Makes Phased Updates a Reality](https://discourse.ubuntu.com/t/phased-updates-in-apt-in-21-04/20345/3)

- [Tails Has a Focus in 2021](https://tails.boum.org/news/plans_for_2021/index.en.html)

- [Project Lenix from CloudLinux Gets a Name](https://almalinux.org/)

- [Valve Will Continue Their Linux Investment](https://store.steampowered.com/news/group/4145017/view/2961646623386540826)

- [Fedora Kinoite, a New Immutable OS](https://fedoramagazine.org/discover-fedora-kinoite/)

- [Microsoft Defender for Linux Servers Now Generally Available](https://techcommunity.microsoft.com/t5/microsoft-defender-for-endpoint/edr-for-linux-is-now-generally-available/ba-p/2048539)

- [Alpine Linux 3.13.0 Out](https://www.alpinelinux.org/posts/Alpine-3.13.0-released.html)

- [KaOS 2021.01 Out](https://kaosx.us/news/2021/kaos01/)

- [Raspberry Pi OS 2011-01-11 Out](https://downloads.raspberrypi.org/raspios_full_armhf/release_notes.txt)

- [Flatpak 1.10.0 Out](https://github.com/flatpak/flatpak/releases/tag/1.10.0)

- [Wine 6.0 Out](https://www.winehq.org/announce/6.0)

- [Proton 5.13-5 Out](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.13-5)

- [Mobian Community Edition PinePhone Out](https://blog.mobian-project.org/posts/2021/01/15/mobian-community-edition/)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
