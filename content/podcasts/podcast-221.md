---
title: "Full Circle Weekly News 221"
date: 2021-08-03
draft: false
tags:
  - "apache"
  - "cassandra"
  - "deep"
  - "flight"
  - "haiku"
  - "ndpi"
  - "opnsense"
  - "orbiter"
  - "packet"
  - "pulseaudio"
  - "server"
  - "siduction"
  - "sim"
  - "simulator"
  - "sound"
  - "space"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20221.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Haiku R1 beta 3](https://www.haiku-os.org/news/2021-07-26_media_release_the_haiku_project_celebrates_the_release_of_beta_3/)

- [Release of nDPI 4.0 deep packet inspection system](https://www.ntop.org/ndpi/introducing-ndpi-4-0-dpi-for-cybersecurity-and-traffic-analysis/)

- [PulseAudio 15.0 sound server released](https://lists.freedesktop.org/archives/pulseaudio-discuss/2021-July/032099.html)

- [Release of OPNsense 21.7](https://forum.opnsense.org/index.php?topic=24112.0)

- [Apache Cassandra 4.0 is available](https://blogs.apache.org/foundation/entry/the-apache-cassandra-project-releases)

- [SEE](https://www.youtube.com/watch?v=kOf29M8qFts)

- [Release of Siduction 2021.2](https://siduction.org/2021/07/release-notes-for-siduction-2021-2-0/)

- [Open source Orbiter space flight simulator](https://www.orbiter-forum.com/threads/orbiter-is-now-open-source.40023/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
