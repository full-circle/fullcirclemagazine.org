---
title: "Full Circle Weekly News 103"
date: 2018-08-19
draft: false
tags:
  - "18-04-1"
  - "beaver"
  - "beta"
  - "bionic"
  - "bug"
  - "cindy"
  - "debian"
  - "dos"
  - "flaw"
  - "kernel"
  - "lmde"
  - "lubuntu"
  - "mint"
  - "mysql"
  - "opera"
  - "segmentsmack"
  - "snap"
  - "tcp"
  - "vpn"
  - "wireguard"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20103.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Linux 18.04.1 LTS Bionic Beaver available for download](https://betanews.com/2018/07/26/ubuntu-linux-bionic-beaver-point/)

- [Future Lubuntu Releases Won't Focus on Old PCs, Will Offer a Modular Linux OS](https://news.softpedia.com/news/future-lubuntu-releases-won-t-focus-on-old-pcs-will-offer-a-modular-linux-os-522141.shtml)

- [Linux Mint Debian Edition (LMDE) 3 'Cindy' BETA available for download](https://betanews.com/2018/07/31/linux-mint-debian-lmde-cindy-beta/)

- [Opera is available in a Snap on Linux](https://www.zdnet.com/article/opera-is-available-in-a-snap-on-linux/)

- [UK cyber security boffins dispense Ubuntu 18.04 wisdom]([https://www.theregister.co.uk/2018/08/01/ncsc_ubuntu/)

- [MySQL Updates for Ubuntu Resolve Server Data Manipulation and DoS Vulnerabilities](https://appuals.com/mysql-updates-for-ubuntu-resolve-server-data-manipulation-and-dos-vulnerabilities/)

- [SegmentSmack: TCP Flaw In Linux Kernel Could Trigger A Remote Denial Of Service](https://fossbytes.com/segmentsmack-tcp-flaw-linux-kernel-remote-denial-of-service/)

- [Drink this potion, Linux kernel, and tomorrow you'll wake up with a WireGuard VPN driver](https://www.theregister.co.uk/2018/08/02/linux_kernel_wireguard/)

- [Linux kernel 4.18 delayed: Bug ate my rc7, says Linus Torvalds](https://www.theregister.co.uk/2018/08/01/linux_kernel_418_delayed_bug_eats_rc7/)
