---
title: "Full Circle Weekly News 148"
date: 2019-10-08
draft: false
tags:
  - "bsd"
  - "canonical"
  - "deepin"
  - "firefox"
  - "ghostbsd"
  - "graphics"
  - "huawei"
  - "ibm"
  - "kernel"
  - "linux"
  - "linuxone"
  - "microsoft"
  - "mozilla"
  - "navi"
  - "pinetime"
  - "security"
  - "smartwatch"
  - "ubuntu"
  - "update"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20148.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Huawei Linux Laptops Running Deepin Linux Now Available](https://fossbytes.com/huawei-linux-laptops-running-deepin-distr)

- [PineTime, a Smartwatch Companion for Linux Smartphones](https://liliputing.com/2019/09/pinetime-is-a-25-smartwatch-companion-for-linux-smartphones-work-in-progress-from-pine64.html)

- [First-Ever Microsoft Linux Conference Announced](https://news.softpedia.com/news/first-ever-microsoft-linux-conference-announced-for-march-10-11-2020-527424.shtml)

- [Linux 5.3 Kernel Bundles New Navi Graphics Support](https://www.theregister.co.uk/2019/09/16/linux_5_3_kernel_arrives/)

- [Canonical Outs New Linux Kernel Security Update for Supported Ubuntu OSes](https://news.softpedia.com/news/canonical-outs-new-linux-kernel-security-update-for-all-supported-ubuntu-oses-527454.shtml)

- [Mozilla Is Transitioning Firefox to a Monthly Release Cycle](https://www.techspot.com/news/81979-mozilla-transitioning-firefox-monthly-release-cycle.html)

- [Announcing the new IBM LinuxONE III with Ubuntu](https://ubuntu.com/blog/announcing-the-new-ibm-linuxone-iii-with-ubuntu)

- [GhostBSD 19.09 Now Available](https://www.ghostbsd.org/19.09_release_announcement)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
