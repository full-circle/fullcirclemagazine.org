---
title: "Full Circle Weekly News 63"
date: 2017-06-10
draft: false
tags:
  - "7-0"
  - "9-0"
  - "debian"
  - "desktop"
  - "fireball"
  - "gnu"
  - "graphics"
  - "irix"
  - "linux"
  - "malware"
  - "mintbox"
  - "phone"
  - "ransomware"
  - "samba"
  - "sgi"
  - "silicon"
  - "stretch"
  - "tablet"
  - "tor"
  - "touch"
  - "ubports"
  - "ubuntu"
  - "wannacry"
  - "windows"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2063.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [UBports is keeping the Ubuntu Phone dream alive](https://liliputing.com/2017/06/ubports-keeping-ubuntu-phone-dream-alive-third-party-development.html)

- [Debian GNU/Linux 9 “Stretch” Will Be Released On June 17](https://fossbytes.com/debian-linux-9-stretch-released-date-download/)

- [Samba vulnerability brings WannaCry fears to Linux/Unix](http://searchsecurity.techtarget.com/news/450419712/Samba-vulnerability-brings-WannaCry-fears-to-Linux-Unix)

- [Silicon Graphics' IRIX and Magic Desktop return as Linux desktop](https://www.theregister.co.uk/2017/06/05/maxx_interactive_desktop_revives_sgi_irix_and_magic_desktop/)

- [Tor 7.0 Released With Multiprocess Mode And Content Sandbox, Now Available For Download](https://fossbytes.com/tor-browser-7-released-available-for-download/)

- [After WannaCry, Fireball Malware Infects 250 Million Computers; India Worst Affected](https://fossbytes.com/fireball-malware/)

- [Linux Mint-powered MintBox 2 has security vulnerability -- needs Windows to fix it](https://betanews.com/2017/06/07/linux-mint-mintbox-vulnerability/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
