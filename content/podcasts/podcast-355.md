---
title: "Full Circle Weekly News 355"
date: 2024-03-03
draft: false
tags:
  - "miracle"
  - "webkit"
  - "skia"
  - "valve"
  - "steam"
  - "rawtherapee"
  - "lighttp"
  - "kubuntu"
  - "ardor"
  - "gcompris"
  - "gimp"
  - "mylibrary"
  - "ubuntu"
  - "antix"
  - "armbian"
  - "dietpi"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20355.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Miracle-wm, a composite manager based on Wayland and Mir:](https://discourse.ubuntu.com/t/introducting-miracle-wm-a-wayland-compositor-built-on-mir/42583)

- [WebKit adds support for the Skia library for rendering 2D graphics:](https://blogs.igalia.com/carlosgc/2024/02/20/a-clarification-about-webkit-switching-to-skia/)

- [Valve open sourced Steam Audio toolkit:](https://steamcommunity.com/games/596420/announcements/detail/7745698166044243233)

- [RawTherapee 5.10 released:](https://discuss.pixls.us/t/rawtherapee-5-10-released/42149)

- [Release of Lighttpd 1.4.74:](https://blog.lighttpd.net/articles/2024/02/19/lighttpd-1.4.74-released/)

- [Kubuntu has announced a competition to create a logo and branding elements:](https://kubuntu.org/news/kubuntu-graphic-design-contest/)

- [The Ardor 8.4 GTK2 fork:](https://ardour.org/whatsnew.html)

- [Release of GCompris 4.0:](https://gcompris.net/news/2024-02-21-en.html)

- [Release of GIMP 2.99.18. Freeze before GIMP 3.0 release:](https://www.gimp.org/news/2024/02/21/gimp-2-99-18-released/)

- [Release of MyLibrary 3.0:](https://github.com/ProfessorNavigator/mylibrary/releases/tag/v3.0)

- [Release of Ubuntu 22.04.4 LTS:](https://lists.ubuntu.com/archives/ubuntu-announce/2024-February/000299.html)

- [Release of antiX 23.1:](https://antixlinux.com/antix-23-1-released/)

- [Armbian 24.2 is available:](https://www.armbian.com/newsflash/armbian-24-2-kereru/)

- [DietPi 9.1:](https://dietpi.com/docs/releases/v9_1/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
