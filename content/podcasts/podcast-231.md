---
title: "Full Circle Weekly News 231"
date: 2021-10-10
draft: false
tags:
  - "apache"
  - "canonical"
  - "compiler"
  - "debian"
  - "desktop"
  - "firewall"
  - "flatpak"
  - "frame"
  - "http"
  - "icewm"
  - "ipfire"
  - "lakka"
  - "llvm"
  - "lumina"
  - "mir"
  - "nitrux"
  - "nx"
  - "onlyoffice"
  - "opensilver"
  - "scummvm"
  - "server"
  - "shell"
  - "silverlight"
  - "ubuntu"
  - "vulnerability"
  - "window"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20231.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical introduced Ubuntu Frame shell](https://discourse.ubuntu.com/t/release-2-5-0/24489)

- [Release of Lakka 3.5](https://www.lakka.tv/articles/2021/10/03/lakka-3.5/)

- [Lumina Desktop 1.6.1 Release](https://github.com/lumina-desktop/lumina/releases)

- [Nitrux 1.6.1 with NX Desktop is Released](https://nxos.org/changelog/release-announcement-nitrux-1-6-1/)

- [Release of OnlyOffice Desktop 6.4](https://www.onlyoffice.com/blog/2021/10/onlyoffice-desktop-editors-v6-4-with-improved-arm-support-and-conditional-formatting/)

- [LLVM Compiler 13.0 Released](https://lists.llvm.org/pipermail/llvm-announce/2021-October/000095.html)

- [Vulnerability in Apache http server 2.4.49](https://httpd.apache.org/security/vulnerabilities_24.html)

- [IceWM 2.8 window manager released](https://github.com/ice-wm/icewm/releases/tag/2.8.0)

- [IPFire 2.27 firewall](https://blog.ipfire.org/post/ipfire-2-27-core-update-160-released)

- [Mir Display Server 2.5 released](https://discourse.ubuntu.com/t/release-2-5-0/24489)

- [Canonical introduced Ubuntu Frame shell](https://ubuntu.com/blog/canonical-launches-ubuntu-frame-the-foundation-for-embedded-displays)

- [Bottlerocket 1.3, an isolated container distribution released](https://github.com/bottlerocket-os/bottlerocket/releases/tag/v1.3.0)

- [OpenSilver 1.0, an open source implementation of Silverlight](https://github.com/OpenSilver/OpenSilver/releases/tag/OpenSilver-1.0.0)

- [Debian 11.1](https://www.debian.org/News/2021/20211009)

- [Release of Flatpak 1.12.0](https://lists.freedesktop.org/archives/flatpak/2021-October/002186.html)

- [Release of ScummVM 2.5.0](https://www.scummvm.org/news/20211009/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
