---
title: "Full Circle Weekly News 34"
date: 2016-09-10
draft: false
tags:
  - "africa"
  - "baidu"
  - "bashlite"
  - "brain"
  - "cloud"
  - "ddos"
  - "devices"
  - "drone"
  - "ibm"
  - "iot"
  - "kernel"
  - "linux"
  - "malware"
  - "mirai"
  - "rc"
  - "release"
  - "server"
  - "torvalds"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2034.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [This drone development kit can also be an Ubuntu PC](http://www.pcworld.com/article/3117562/this-drone-development-kit-can-also-be-an-ubuntu-pc.html)

- [IBM to set up new Linux cloud for Africa](http://www.itwire.com/cloud/74663-ibm-to-set-up-new-linux-cloud-for-africa.html)

- [BASHLITE malware turning millions of Linux Based IoT Devices into DDoS botnet](https://www.hackread.com/bashlite-malware-linux-iot-ddos-botnet/)

- [Torvalds Announces a Noticeably Bigger Linux 4.8 RC5 Kernel Release](http://news.softpedia.com/news/linus-torvalds-announces-the-a-noticeably-bigger-linux-kernel-4-8-rc5-release-507939.shtml)

- [Baidu offers brainy solutions](http://www.chinapost.com.tw/china/china-business/2016/09/03/477361/Baidu-offers.htm)

- [Mirai DDoS Trojan Is the Next Big Threat to IoT Devices and Linux Servers](http://news.softpedia.com/news/mirai-ddos-trojan-is-the-next-big-threat-for-iot-devices-and-linux-servers-507964.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
