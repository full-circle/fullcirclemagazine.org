---
title: "Full Circle Weekly News 168"
date: 2020-04-19
draft: false
tags:
  - "blender"
  - "epoch"
  - "fix"
  - "flatpak"
  - "gnome"
  - "guake"
  - "kde"
  - "kernel"
  - "linux"
  - "lts"
  - "mate"
  - "netbsd"
  - "openshot"
  - "perens"
  - "plasma"
  - "project"
  - "project-trident"
  - "rc2"
  - "security"
  - "tails"
  - "times"
  - "trident"
  - "void"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20168.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 18.04.4 Released with Updated Kernel 5.3](https://www.omgubuntu.co.uk/2020/02/ubuntu-18-04-4-lts-released)

- [Devs Push for Unicode 13 Support in Ubuntu 20.04 LTS](https://www.omgubuntu.co.uk/2020/02/unicode-13-in-ubuntu-20-04)

- [Perens Versus Open Source Security; Perens Wins in Court](https://www.itwire.com/open-source/linux-kernel-patch-maker-says-court-case-was-only-way-out.html)

- [New Long Term Support KDE Plamsa 5.18 Is Out](https://kde.org/announcements/plasma-5.18.0)

- [Kernel Updates to Fix 32-Bit Epoch Time in the Works](https://lkml.org/lkml/2020/1/29/355?anz=web)

- [Project Trident Now Officially Based on Void Linux](https://project-trident.org/post/void-20.02-available/)

- [Linux Kernel 5.6 RC2 Is Out](https://lkml.org/lkml/2020/2/16/241)

- [MATE 1.24 Is Out](https://mate-desktop.org/blog/2020-02-10-mate-1-24-released/)

- [Gnome 3.35.90 Is Out](https://mail.gnome.org/archives/devel-announce-list/2020-February/msg00000.html)

- [OpenShot 2.5.0 Is Out](https://www.openshot.org/blog/2020/02/08/openshot-250-released-video-editing-hardware-acceleration/)

- [Tails 4.3 Is Out](https://tails.boum.org/news/version_4.3/index.en.html)

- [Flatpak 1.6.2 Is Out](https://www.phoronix.com/scan.php?page=news_item&px=Flatpak-1.6.2-Released)

- [Guake 3.7.0 Is Out](https://www.linuxuprising.com/2020/02/guake-370-drop-down-terminal-released.html)

- [NetBSD 9.0 Is Out](http://blog.netbsd.org/tnf/entry/netbsd_9_0_available)

- [Blender 2.82 Is Out](https://www.blender.org/download/releases/2-82/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
