---
title: "Full Circle Weekly News 347"
date: 2024-01-07
draft: false
tags:
  - "libreelec"
  - "nobara"
  - "exim"
  - "gentoo"
  - "inetutils"
  - "netsurf"
  - "raspberry"
  - "wattos"
  - "scummvm"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20347.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of LibreELEC 11.0.4:](https://libreelec.tv/2023/12/23/libreelec-nexus-11-0-4/)

- [Release of Nobara 39:](https://nobaraproject.org/2023/12/26/december-26-2023)

- [Exim 4.97.1 update:](https://github.com/Exim/exim/releases/tag/exim-4.97.1)

- [Gentoo has announced the availability of binary packages:](https://www.gentoo.org/news/2023/12/29/Gentoo-binary.html)

- [Release of GNU inetutils 2.5:](https://www.mail-archive.com/info-gnu@gnu.org/msg03239.html)

- [NetSurf browser 3.11:](https://www.netsurf-browser.org/)

- [MX Linux for Raspberry Pi:](https://mxlinux.org/blog/mx-23-1-raspberry-pi-os-respin/)

- [Release of wattOS 13:](https://www.planetwatt.com/R13-details/)

- [Release of ScummVM 2.8.0:](https://www.scummvm.org/news/20231230/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
