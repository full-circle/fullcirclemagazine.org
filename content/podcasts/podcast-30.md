---
title: "Full Circle Weekly News 30"
date: 2016-08-13
draft: false
tags:
  - "android"
  - "bedrock"
  - "beta"
  - "boot"
  - "bsp"
  - "chinese"
  - "italian"
  - "keys"
  - "leak"
  - "malware"
  - "microsoft"
  - "ota-13"
  - "ota13"
  - "secure"
  - "snapcraft"
  - "snapd"
  - "ubuntubsd"
  - "uefi"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2030.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Touch OTA-13 Delayed to Bring Android 6.0 BSP Support](http://news.softpedia.com/news/ubuntu-touch-ota-13-delayed-for-september-7-to-bring-android-6-0-bsp-support-507029.shtml)

- [Oops! Microsoft Accidentally Leaks Backdoor Keys to Bypass UEFI Secure Boot](http://thehackernews.com/2016/08/uefi-secure-boot-hack.html?m=1)

- [UbuntuBSD 16.04 "A New Hope" Beta 1 Now Available](http://news.softpedia.com/news/ubuntubsd-16-04-a-new-hope-beta-1-now-available-based-on-ubuntu-16-04-lts-507128.shtml)

- [Bedrock Linux gathers distros under one umbrella](http://www.infoworld.com/article/3105017/linux/bedrock-linux-gathers-disparate-distros-under-one-umbrella.html)

- [Snapcraft 2.13 and Snapd 2.11 Land with Support for Downgrading Installed Snaps](http://news.softpedia.com/news/snapcraft-2-13-and-snapd-2-11-land-with-support-for-downgrading-installed-snaps-507087.shtml)

- [Italian malware is spying on Chinese Android users: But why?](http://www.zdnet.com/article/italian-malware-is-spying-on-chinese-android-users-but-why/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
