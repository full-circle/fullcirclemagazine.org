---
title: "Full Circle Weekly News 14"
date: 2016-04-23
draft: false
tags:
  - "16-04"
  - "aquaris"
  - "bq"
  - "bsd"
  - "canonical"
  - "freebsd"
  - "lts"
  - "m10"
  - "malware"
  - "microsoft"
  - "pwobot"
  - "python"
  - "snaps"
  - "tablet"
  - "ubuntubsd"
  - "xenial"
  - "xerus"
  - "zfs"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2014.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 16.04 LTS arrives complete with ZFS](http://www.theregister.co.uk/2016/04/21/ubuntu_16_04_lts_launched/)

- [Canonical and BQ's Aquaris M10 Ubuntu Edition Tablet](http://www.linuxjournal.com/content/canonical-and-bqs-aquaris-m10-ubuntu-edition-tablet)

- [With Snaps, Ubuntu 16.04 LTS will facilitate application management](http://sivertimes.com/with-snaps-ubuntu-16-04-lts-will-facilitate-the-application-management/23832)

- [ubuntuBSD, now Has a homepage](http://linux.softpedia.com/blog/ubuntubsd-the-os-that-brings-ubuntu-and-freebsd-together-now-has-a-homepage-503063.shtml)

- [PWOBot: Python malware family can eat Windows, Linux and OS X](http://www.theinquirer.net/inquirer/news/2455133/pwobot-python-malware-family-can-eat-windows-linux-and-os-x)

- [Microsoft headhunters seek Linux folk for secret open source unit](http://www.theregister.co.uk/2016/04/21/microsoft_open_source_practice/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
