---
title: "Full Circle Weekly News 300"
date: 2023-02-12
draft: false
tags:
  - "budgie"
  - "coreboot"
  - "onlyoffice"
  - "elementary"
  - "xfce"
  - "wayland"
  - "cosmic"
  - "rust"
  - "glibc"
  - "hermes"
  - "libreoffice"
  - "postgres"
  - "ndpi"
  - "grease"
  - "pencil"

cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20300.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

**PLEASE NOTE:** NEW RSS FEED URL: [https://fullcirclemagazine.org/podcasts/index.xml](https://fullcirclemagazine.org/podcasts/index.xml)

- [Release of Budgie 10.7:](https://blog.buddiesofbudgie.org/budgie-10-7-released/)

- [Release of Coreboot 4.19:](https://blogs.coreboot.org/blog/2023/01/28/announcing-coreboot-release-4-19/)

- [Release of ONLYOFFICE Docs 7.3.0:](https://www.onlyoffice.com/blog/2023/01/onlyoffice-docs-7-3-released/)

- [Release of Elementary OS 7:](https://blog.elementary.io/os-7-available-now/)

- [Xfce developing support Wayland:](https://mail.xfce.org/pipermail/xfce-announce/2023-January/001220.html)

- [Progress of COSMIC written in Rust:](https://blog.system76.com/post/more-on-cosmic-de-to-kick-off-2023)

- [Release of Glibc 2.37:](https://sourceware.org/pipermail/libc-alpha/2023-February/145190.html)

- [HashiCorp unveils open document management system for Hermes:](https://www.hashicorp.com/blog/introducing-hermes-an-open-source-document-management-system)

- [Release of LibreOffice 7.5:](https://blog.documentfoundation.org/blog/2023/02/02/tdf-announces-libreoffice-75-community/)

- [Release of Postgres Pro Enterprise 15.1.1:](https://postgrespro.ru/blog/news/5969951)

- [Release of deep packet inspection system, nDPI 4.6:](https://www.ntop.org/ndpi/welcome-to-ndpi-4-6-code-fuzzing-new-protocol-and-flow-risks/)

- [SPA Studios opened Grease Pencil:](https://www.youtube.com/watch?v=0HNmJebYY8M)

- [Foundation for Sustainable Open and Free Software:](https://en.wikipedia.org/wiki/Open_Technology_Fund)

- [The Open-Assistant project AI bot:](https://open-assistant.io/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
