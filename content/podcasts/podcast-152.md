---
title: "Full Circle Weekly News 152"
date: 2019-11-06
draft: false
tags:
  - "20-04"
  - "bsd"
  - "bug"
  - "gnu"
  - "hyperbola"
  - "libre"
  - "linux"
  - "project"
  - "root"
  - "sudo"
  - "trident"
  - "ubuntu"
  - "zabbix"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20152.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Project Trident Ditches BSD for Linux](https://itsfoss.com/bsd-project-trident-linux/)

- [Zabbix 4.4 released](https://www.zabbix.com/whats_new_4_4)

- [Hyperbola GNU / Linux-libre releases "Milky Way" v0.3](https://www.hyperbola.info/news/milky-way-v03-release/)

- [Linux sudo Bug Allows Commands To Be Run As Root](https://www.bleepingcomputer.com/news/linux/linux-sudo-bug-lets-you-run-commands-as-root-most-installs-unaffected/)

- [Ubuntu 19.10 Has Arrived](https://itsfoss.com/ubuntu-19-10-released/)

- [Ubuntu 20.04 Codename Revealed](https://www.omgubuntu.co.uk/2019/10/the-ubuntu-20-04-lts-codename-has-been-revealed)

- [Happy Birthday, Ubuntu](https://www.omgubuntu.co.uk/2019/10/happy-birthday-ubuntu-2019)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
