---
title: "Full Circle Weekly News 267"
date: 2022-07-20
draft: false
tags:
  - "ubuntu"
  - "alpha"
  - "android"
  - "bottlerocket"
  - "browser"
  - "cambalache"
  - "cinnamon"
  - "debian"
  - "desktop"
  - "easyos"
  - "eurolinux"
  - "freecad"
  - "ghostbsd"
  - "gimp"
  - "k9"
  - "k-9"
  - "kde"
  - "lxc"
  - "mail"
  - "manjaro"
  - "midnightbsd"
  - "oracle"
  - "php"
  - "plasma"
  - "puppy"
  - "qt6"
  - "thunderbird"
  - "tor"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20267.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Qt6 packages prepared for Debian 11:](https://perezmeyer.com.ar/blog/2022/06/10/qt6-in-debian-bullseye/)

- [Release of the MidnightBSD 2.2:](https://www.midnightbsd.org/notes/)

- [Cinnamon 5.4 desktop environment released:](https://github.com/linuxmint/Cinnamon/releases/tag/5.4.0)

- [Release of EasyOS 4.0, a custom distribution from the creator of Puppy Linux:](https://bkhome.org/news/202206/easyos-dunfell-series-version-40-released.html)

- [Bottlerocket 1.8 is available:](https://github.com/bottlerocket-os/bottlerocket/releases/tag/v1.8.0)

- [Alpha testing of PHP 8.2 has begun:](https://stitcher.io/blog/new-in-php-82)

- [The K-9 Mail project will become the basis of Thunderbird for Android:](https://blog.thunderbird.net/2022/06/revealed-thunderbird-on-android-plans-k9/)

- [The browser-linux - Linux distribution to run in a web browser:](https://github.com/Darin755/browser-linux)

- [Oracle Linux 9 Preview:](https://blogs.oracle.com/linux/post/oracle-linux-9-developer-preview-now-available-for-download)

- [KDE Plasma 5.25 user environment:](https://kde.org/ru/announcements/plasma/5/5.25.0/)

- [Free CAD release FreeCAD 0.20:](https://github.com/FreeCAD/FreeCAD/releases/tag/0.20)

- [EuroLinux 9.0:](https://en.euro-linux.com/blog/eurolinux-9-0-released/)

- [Ubuntu Core 22 monolithic distribution available:](https://ubuntu.com/blog/canonical-ubuntu-core-22-is-now-available-optimised-for-iot-and-embedded-devices)

- [GIMP 2.10.32 graphics editor released:](https://www.gimp.org/news/2022/06/14/gimp-2-10-32-released/)

- [Cambalache 0.10, a tool for developing GTK interfaces:](https://blogs.gnome.org/xjuan/2022/06/15/cambalache-0-10-0-is-out/)

- [Release of LXC 5.0 container management system:](https://discuss.linuxcontainers.org/t/lxc-5-0-lts-has-been-released/14381)

- [Tor 0.4.7.8 update fixes vulnerability:](https://forum.torproject.net/t/stable-release-0-4-7-8/3679)

- [GhostBSD Release 6/22/15:](http://ghostbsd.org/22.06.15_iso_is_now_available)

- [Manjaro Linux 21.3 distribution released:](https://forum.manjaro.org/t/manjaro-21-3-0-ruah-released/114220)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
