---
title: "Full Circle Weekly News 390"
date: 2024-11-03
draft: false
tags:
  - "iwd"
  - "minetest"
  - "sysvinit"
  - "kitten"
  - "openssl"
  - "libressl"
  - "botan"
  - "rustls"
  - "steam"
  - "cozystack"
  - "vivaldi"
  - "bitwarden"
  - "sway"
  - "ubuntu"
  - "tileos"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20390.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [Release of iwd 3.0:](https://iwd.wiki.kernel.org/)

- [Minetest gets a name:](https://blog.minetest.net/2024/10/13/Introducing-Our-New-Name/)

- [Release of SysVinit 3.11:](https://lists.nongnu.org/archive/html/sysvinit-devel/2024-10/msg00000.html)

- [Kitten 10:](https://almalinux.org/blog/2024-10-22-introducing-almalinux-os-kitten/)

- [Release of OpenSSL 3.4, LibreSSL 4.0, Botan 3.6 and Rustls 0.23.15 cryptographic libraries:](https://openssl-library.org/post/2024-10-22-openssl-3-4-final/)

- [Steam OS 3.6.19:](https://steamcommunity.com/games/1675200/announcements/detail/4676514574283544995)

- [Release of Cozystack 0.17.0:](https://github.com/aenix-io/cozystack/releases/tag/v0.17.0)

- [Vivaldi 7.0 Browser with New Interface Design:](https://vivaldi.com/blog/read-all-about-vivaldi-7-0/)

- [Bitwarden SDK Moved from Proprietary License to GPLv3:](https://news.ycombinator.com/item?id=41940580)

- [Ubuntu Sway Remix 24.10:](https://github.com/Ubuntu-Sway/Ubuntu-Sway-Remix/releases/tag/24.10)

- [TileOS 1.2:](https://gitlab.com/tile-os/tileos/-/releases/v1.2)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
