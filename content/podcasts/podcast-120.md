---
title: "Full Circle Weekly News 120"
date: 2019-02-08
draft: false
tags:
  - "18-04"
  - "18-10"
  - "canonical"
  - "japan"
  - "kernel"
  - "lxqt"
  - "patch"
  - "snap"
  - "snapcraft"
  - "system76"
  - "tails"
  - "ubuntu"
  - "update"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20120.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [System76 unveils 'Darter Pro' Linux laptop with choice of Ubuntu or Pop!\_OS](https://betanews.com/2019/01/29/system76-darter-pro-linux-laptop/)

- [Ubuntu 18.04.2 LTS to Arrive on February 7 with New Components from Ubuntu 18.10 Source](https://news.softpedia.com/news/ubuntu-18-04-2-lts-to-arrive-on-february-7-with-updated-components-524785.shtml)

- [Canonical Releases Snapcraft 3.1 Snap Creator Tool with Various Improvements](https://news.softpedia.com/news/canonical-releases-snapcraft-3-1-snap-creator-tool-with-various-improvements-524761.shtml)

- [LXQt 0.14 Desktop Adds Split View in File Manager, LXQt 1.0 Still in Development](https://news.softpedia.com/news/lxqt-0-14-desktop-adds-split-view-in-file-manager-lxqt-1-0-still-in-development-524700.shtml)

-[Japan Will Hack Its Citizens’ IoT Devices To ‘Make Them Secure’](https://fossbytes.com/japanese-will-hack-its-citizens-iot-devices-secure/)

- [Canonical Outs Major Linux Kernel Update for Ubuntu 18.04 LTS to Patch 11 Flaws](https://news.softpedia.com/news/canonical-outs-major-linux-kernel-update-for-ubuntu-18-04-lts-to-patch-11-flaws-524740.shtml)

- [Tails 3.12 with new installation method](https://www.pro-linux.de/news/1/26725/tails-312-mit-neuer-installationsmethode.html)
