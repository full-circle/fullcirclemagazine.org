---
title: "Full Circle Weekly News 194"
date: 2020-12-23
draft: false
tags:
  - "apps"
  - "cent"
  - "centos"
  - "cpufreq"
  - "debian"
  - "elementary"
  - "kde"
  - "kernel"
  - "os"
  - "pi"
  - "raspberry"
  - "souk"
  - "stream"
  - "zareason"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20194.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [The End of ZaReason](https://news.itsfoss.com/zareason-shutdown/)

- [CentOS Linux 8 End of Life Moved from 2029 to 2021, Stream Takes Over](https://lists.centos.org/pipermail/centos-announce/2020-December/048208.html)

- [New CentOS Replacement Lineups Spring Up](https://github.com/rocky-linux/rocky)

- [Raspberry Pi OS Updates Out](https://9to5linux.com/raspberry-pi-os-has-a-new-release-with-improved-audio-and-printing-support)

- [Experimental Elementary OS on the Raspberry Pi Out](https://news.itsfoss.com/elementary-os-raspberry-pi-release/)

- [Debian 10.7 Out](https://www.debian.org/News/2020/20201205)

- [Kernel 5.10 Out](https://www.lkml.org/lkml/2020/12/13/290)

- [KDE Apps 20.12 Out](https://kde.org/announcements/fulllog_releases-20.12.0/)

- [CPUfreq 1.5.1 Out](https://www.linuxuprising.com/2020/12/linux-cpu-speed-and-power-optimizer.html)

- [Souk Out](https://www.debugpoint.com/2020/12/souk/)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
