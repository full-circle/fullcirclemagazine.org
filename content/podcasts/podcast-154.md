---
title: "Full Circle Weekly News 154"
date: 2019-11-19
draft: false
tags:
  - "automated"
  - "code"
  - "fedora"
  - "flaw"
  - "gimp"
  - "gnome"
  - "kernel"
  - "linspire"
  - "linux"
  - "mint"
  - "nginx"
  - "oneplus3"
  - "phones"
  - "php"
  - "remote"
  - "servers"
  - "sony"
  - "touch"
  - "ubports"
  - "xperia"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20154.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [PHP Remote Code Execution Flaw Actively Exploited Against NGINX Servers](https://www.trendmicro.com/vinfo/us/security/news/vulnerabilities-and-exploits/php-fpm-vulnerability-cve-2019-11043-can-lead-to-remote-code-execution-in-nginx-web-servers)

- [Fedora 31 Released with GNOME 3.34 & Linux 5.3](https://news.softpedia.com/news/fedora-31-officially-released-with-gnome-3-34-linux-5-3-drops-32-bit-support-528031.shtml)

- [Automated testing comes to the Linux kernel](https://www.zdnet.com/article/automated-testing-comes-to-the-linux-kernel-kernelci/)

- [GIMP 2.10.14 Released](https://www.linuxuprising.com/2019/10/gimp-21014-released-with-new-show-all.html)

- [Linux Mint 19.3 Announced](https://blog.linuxmint.com/?p=3811)

- [Linspire 8.5 Released](https://www.linspirelinux.com/2019/10/linspire-85-released.html)

- [Ubuntu Touch Installer Now Supports OnePlus3 and Sony Xperia X Phones](https://news.softpedia.com/news/ubuntu-touch-installer-now-supports-oneplus-3-and-sony-xperia-x-ubuntu-phones-528097.shtml)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
