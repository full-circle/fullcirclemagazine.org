---
title: "Full Circle Weekly News 317"
date: 2023-06-11
draft: false
tags:
  - "libmicro"
  - "libraryhttpd"
  - "goldendict"
  - "canonical"
  - "snap"
  - "armbian"
  - "angie"
  - "truenas"
  - "kali"
  - "apache"
  - "sendmail"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20317.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of GNU libmicro libraryhttpd 0.9.77:](https://www.mail-archive.com/info-gnu@gnu.org/msg03188.html)

- [GoldenDict 1.5.0:](https://github.com/goldendict/goldendict/releases/tag/1.5.0)

- [Canonical prepares Ubuntu Desktop version containing only Snap packages:](https://www.omgubuntu.co.uk/2023/05/immutable-all-snap-ubuntu-desktop)

- [Release of Armbian 23.05:](https://www.armbian.com/newsflash/armbian-23-05-suni/)

- [Release Angie 1.2.0:](https://github.com/webserver-llc/angie/releases/tag/1.2.0)

- [Release of TrueNAS CORE 13.0-U5:](https://www.truenas.com/blog/truenas-13-0-u5-maximizes-quality-and-your-storage-experience/)

- [Release of Kali Linux 2023.2:](https://www.kali.org/blog/kali-linux-2023-2-release/)

- [Release of Apache NetBeans 18:](https://blogs.apache.org/netbeans/entry/announce-apache-netbeans-18-released)

- [Updating Sendmail 8.17.2:](https://marc.info/?l=sendmail-announce&m=168578185221545&w=2)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
