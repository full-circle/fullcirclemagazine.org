---
title: "Full Circle Weekly News 13"
date: 2016-04-16
draft: false
tags:
  - "ai"
  - "android"
  - "authentication"
  - "backdoor"
  - "ddos"
  - "dgx-1"
  - "everywhere"
  - "google"
  - "hardware"
  - "infographic"
  - "intel"
  - "kits"
  - "nvidia"
  - "ota-10"
  - "ota10"
  - "release"
  - "trojan"
  - "two-factor"
  - "wine"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2013.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Academics claim Google Android two-factor authentication is breakable](http://www.theregister.co.uk/2016/04/08/google_android_2fa_breakable/)

- [Nvidia Bets Big On Artificial Intelligence Deep Learning Supercomputer DGX-1](http://www.techtimes.com/articles/147589/20160407/nvidia-bets-big-on-artificial-intelligence-deep-learning-supercomputer-dgx-1.htm)

- [New Wine Release](http://www.linuxjournal.com/content/new-wine-release)

- [Ubuntu Touch OTA-10 Officially Released](http://news.softpedia.com/news/ubuntu-touch-ota-10-officially-released-502633.shtml)

- [Infographic: Ubuntu Linux Is Everywhere](http://news.softpedia.com/news/infographic-ubuntu-linux-is-everywhere-502722.shtml)

- [Intel's new hardware kits make it easier to build robots and drones](http://www.pcworld.com/article/3055321/hardware/intels-new-hardware-kits-make-it-easier-to-build-robots-and-drones.html)

- [Linux Computers Targeted by New Backdoor and DDoS Trojan](http://news.softpedia.com/news/linux-machines-targeted-by-new-backdoor-and-ddos-trojan-502915.shtml)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
