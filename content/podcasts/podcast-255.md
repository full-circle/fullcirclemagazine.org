---
title: "Full Circle Weekly News 255"
date: 2022-04-02
draft: true
tags:
  - "apple"
  - "asahi"
  - "backup"
  - "creator"
  - "crossover"
  - "debian"
  - "fsf"
  - "keepassxc"
  - "lakka"
  - "m1"
  - "mint"
  - "nscde"
  - "parrot"
  - "password"
  - "rclone"
  - "samba"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20255.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [rclone 1.58 backup utility released](https://forum.rclone.org/t/rclone-1-58-0-release/29784)

- [First test release of Asahi Linux, a distribution for Apple devices with the M1 chip](https://asahilinux.org/2022/03/asahi-linux-alpha-release/)

- [Free Software Foundation announces the winners of the annual award for contribution to the development of free software](https://www.fsf.org/news/free-software-awards-winners-announced-securepairs-protesilaos-stavrou-paul-eggert)

- [Release of Lakka 4.0](https://www.lakka.tv/articles/2022/03/19/lakka-4.0/)

- [Linux Mint Debian Edition 5 distribution released](https://linuxmint.com/rel_elsie.php)

- [Release of Samba 4.16.0](https://lists.samba.org/archive/samba-announce/2022/000601.html)

- [KeePassXC 2.7 password manager released](https://keepassxc.org/blog/2022-03-21-2.7.0-released/)

- [CrossOver 21.2 release for Linux, Chrome OS and macOS](https://www.codeweavers.com/support/forums/announce/?t=24;msg=257259)

- [NsCDE 2.1 user environment available](https://github.com/NsCDE/NsCDE/releases/tag/2.1)

- [Release of the Qt Creator 7 development environment](https://www.qt.io/blog/qt-creator-7-released)

- [Release of the Parrot 5.0](https://parrotsec.org/blog/2022-03-24-parrot-5.0-press-release)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
