---
title: "Full Circle Weekly News 71"
date: 2017-10-20
draft: false
tags:
  - "17-10"
  - "aardvark"
  - "ai"
  - "artful"
  - "attack"
  - "endless"
  - "flathub"
  - "flatpak"
  - "kernel"
  - "krack"
  - "learning"
  - "linux"
  - "lts"
  - "machine"
  - "patched"
  - "wifi"
  - "wpa2"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2071.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 17.10 Artful Aardvark is ready for download](https://www.neowin.net/news/ubuntu-1710-artful-aardvark-is-ready-for-download)

- [Linux Kernel LTS Releases Will Now Get 6 Years Support](https://fossbytes.com/linux-kernel-lts-releases-6-years-support/)

- [Severe WiFi Hack: WPA2 “KRACK Attack” Threatens WiFi Users Around The World](https://fossbytes.com/wpa-2-vulnerability-krack-attack-eavesdropping/)

- [Ubuntu, Debian, Fedora and elementary OS All Patched Against WPA2 KRACK Bug](http://news.softpedia.com/news/ubuntu-debian-fedora-and-elementary-os-all-patched-against-wpa2-krack-bug-518075.shtml)

- [Endless OS Is First Linux Distro to Support Flatpak Apps from Flathub by Default](http://news.softpedia.com/news/endless-os-is-first-linux-distro-to-enable-support-for-flatpak-apps-from-flathub-518002.shtml)

- [Google’s Machine Learning Software Can Create Code Better Than The Researchers Who Made It](https://fossbytes.com/google-automl-defeats-human-wrote-better-code/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
