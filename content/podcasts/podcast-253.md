---
title: "Full Circle Weekly News 253"
date: 2022-03-20
draft: false
tags:
  - "budgie"
  - "chrome"
  - "fedora"
  - "i686"
  - "libreelec"
  - "lwqt"
  - "lxqt"
  - "player"
  - "release"
  - "video"
  - "wayland"
  - "xine"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20253.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Chrome OS 99 released](https://chromereleases.googleblog.com/2022/03/stable-channel-update-for-chrome-os.html)

- [Budgie 10.6 desktop](https://github.com/solus-project/budgie-desktop)

- [First release of LWQt, a Wayland-based variant of the LXQt wrapper](https://www.reddit.com/r/linux/comments/t8aslk/lwqt_10013_released/)

- [Fedora Linux 37 intends to stop building optional packages for the i686 architecture](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/FPWU6UGCYO5YHLLWCFKWWYYGJZYKUR2A/)

- [xine 1.2.12 released](https://sourceforge.net/projects/xine/files/xine-lib/1.2.12/README.txt/view)

- [LibreELEC 10.0.2 released](https://libreelec.tv/2022/03/09/libreelec-matrix-10-0-2/)

- [Zorin OS 16.1 released](https://blog.zorin.com/2022/03/10/zorin-os-16-1-released-support-for-ukraine/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
