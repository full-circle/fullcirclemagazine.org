---
title: "Full Circle Weekly News 256"
date: 2022-04-08
draft: true
tags:
  - "4mlinux"
  - "beta"
  - "brutalos"
  - "capyloon"
  - "commander"
  - "debian"
  - "deepin"
  - "drawing"
  - "elementary"
  - "file"
  - "firefox"
  - "kiosk"
  - "midnight"
  - "mirage"
  - "mirageos"
  - "opensuse"
  - "opentoonz"
  - "porteus"
  - "remix"
  - "rhino"
  - "rolling"
  - "suse"
  - "sysinit"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20256.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Capyloon operating system based on Firefox OS](https://www.reddit.com/r/pinephone/comments/tnq22v/capyloon_now_available_for_the_pinephone_pro/)

- [Debian 11.3 and 10.12 update](https://www.debian.org/News/2022/20220326)

- [The Rolling Rhino Remix Project develops a continuously updated build of Ubuntu](https://rollingrhinoremix.github.io/blog/)

- [4MLinux distribution release 39.0](https://4mlinux-releases.blogspot.com/2022/03/4mlinux-390-stable-released.html)

- [Drawing 1.0.0 rleased](https://maoschanz.github.io/drawing/)

- [Release of the SDL_sound 2.0 library](https://github.com/icculus/SDL_sound/releases/tag/v2.0.1)

- [Fourth experimental release of Brutal OS](https://brutal.smnx.sh/articles/milestone-4)

- [sysvinit 3.02 init system release](https://www.altusintel.com/public-yy89mr/)

- [Midnight Commander 4.8.28 file manager release](https://mail.gnome.org/archives/mc-devel/2022-March/msg00006.html)

- [Release of Porteus Kiosk 5.4.0](https://porteus-kiosk.org/news.html#220328)

- [Release of Finnix 124](https://www.finnix.org/)

- [Release of MirageOS 4.0](https://mirage.io/blog/announcing-mirage-40)

- [First release of D-Installer, a new installer for openSUSE and SUSE](https://yast.opensuse.org/blog/2022-03-31/d-installer-first-public-release)

- [Release of OpenToonz 1.6](https://github.com/opentoonz/opentoonz/releases/tag/v1.6.0)

- [Release of the Deepin 20.5 distribution](https://www.deepin.org/en/2022/03/31/deepin-20-5/)

- [Ubuntu 22.04 beta release](https://lists.ubuntu.com/archives/ubuntu-announce/2022-March/000278.html)

- [One of the founders left the elementary OS project](https://cassidyjames.com/blog/farewell-elementary/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
