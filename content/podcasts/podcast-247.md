---
title: "Full Circle Weekly News 247"
date: 2022-02-06
draft: false
tags:
  - "bind"
  - "dns"
  - "framework"
  - "https"
  - "laptop"
  - "retroarch"
  - "sane"
  - "scanner"
  - "scribus"
  - "server"
  - "sway"
  - "vulcan"
  - "wayland"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20247.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Framework Computer has opened its firmware code for laptops](https://en.wikipedia.org/wiki/Framework_Computer)

- [Sway 1.7 release using Wayland](https://github.com/swaywm/sway/releases/tag/1.7)

- [JingOS 1.2, Tablet PC Distribution Released](https://en.jingos.com/)

- [Release of SANE 1.1 with support for new scanner models](https://alioth-lists.debian.net/pipermail/sane-devel/2022-January/039340.html)

- [Vulkan 1.3 graphics standard published](https://www.khronos.org/news/press/vulkan-reduces-fragmentation-and-provides-roadmap-visibility-for-developers)

- [RetroArch 1.10.0 released](https://www.libretro.com/index.php/retroarch-1-10-0-release/)

- [Release of Scribus 1.5.8](https://www.scribus.net/scribus-1-5-8-released/)

- [SDL media library moves to use Wayland by default](https://github.com/libsdl-org/SDL/commit/8ceba27d6291f1195e13608033ec439aec621fc6)

- [BIND DNS Server 9.18.0 released with DNS-over-TLS and DNS-over-HTTPS support](https://www.mail-archive.com/bind-announce@lists.isc.org/msg00622.html)

- [Release of OPNsense 22.1](https://forum.opnsense.org/index.php?topic=26536.0)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
