---
title: "Full Circle Weekly News 121"
date: 2019-02-15
draft: false
tags:
  - "android"
  - "bareos"
  - "canonical"
  - "darter-pro"
  - "distro"
  - "endless"
  - "gpu"
  - "hackers"
  - "kernel"
  - "kodi"
  - "laptop"
  - "libreelec"
  - "linux"
  - "mali"
  - "patch"
  - "phone"
  - "spectre"
  - "system76"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20121.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Endless OS Functionality Controls Simplify Computing](https://www.linuxinsider.com/story/Endless-OS-Functionality-Controls-Simplify-Computing-85819.html)

- [System76 'Darter Pro' laptop finally here](https://betanews.com/2019/02/06/system76-darter-pro-linux-notebook/)

- [LibreELEC 9.0 released: Linux distro built around Kodi media center](https://liliputing.com/2019/02/libreelec-9-0-released-linux-distro-built-around-kodi-media-center.html)

- [Bareos 18.2 released](https://www.pro-linux.de/news/1/26733/bareos-182-freigegeben.html)

- [Linux kernel gets another option to disable Spectre mitigations](https://www.zdnet.com/article/linux-kernel-gets-another-option-to-disable-spectre-mitigations/)

- [Canonical Releases Important Ubuntu Linux Kernel Security Patches, Update Now](https://news.softpedia.com/news/canonical-releases-important-ubuntu-kernel-security-patches-update-now-524834.shtml)

- [Linux driver for old Mali GPUs should be maintained](https://www.golem.de/news/lima-projekt-linux-treiber-fuer-alte-mali-gpus-soll-eingepflegt-werden-1902-139251.html)

- [Hackers can compromise your Android phone with a single image file](https://bgr.com/2019/02/07/android-security-update-one-png-image-file-can-compromise-your-phone/)
