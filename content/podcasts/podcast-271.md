---
title: "Full Circle Weekly News 271"
date: 2022-07-24
draft: false
tags:
  - "ubuntu"
  - "arcan"
  - "audacious"
  - "calibre"
  - "chrome"
  - "doglinux"
  - "gcc"
  - "homm2"
  - "kernel"
  - "lubuntu"
  - "memchr"
  - "might and magic"
  - "network"
  - "red hat"
  - "redhat"
  - "rust"
  - "scale19x"
  - "seamonkey"
  - "security"
  - "tails"
  - "toolkit"
  - "tor"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20271.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

**SCALE19x** returns to the Los Angeles area July 28-31. Join the Ubuntu and Linux community for 100+ sessions on all things opensource. Keynotes include Vint Cerf, Aeva Black, Demetris Cheatham, and more.
https://www.socallinuxexpo.org

- [SeaMonkey 2.53.13 Released:](https://blog.seamonkey-project.org/2022/07/11/seamonkey-2-53-13-is-out/)

- [Audacious 4.2 Released:](https://audacious-media-player.org/news/53-audacious-4-2-released)

- [Release of Caliber 6.0:](https://calibre-ebook.com/new-in/fifteen)

- [GCC approves inclusion of Rust language support:](https://gcc.gnu.org/pipermail/gcc/2022-July/239057.html)

- [Network Security Toolkit 36 ​​ released:](https://sourceforge.net/p/nst/news/2022/07/nst-version-36-13232-released/)

- [Up to 4 times faster implementation of memchr function proposed for Linux kernel:](https://www.phoronix.com/scan.php?page=news_item&px=Linux-Kernel-Faster-memchr)

- [Heroes of Might and Magic 2 open engine release - fheroes2 - 0.9.17:](https://github.com/ihhub/fheroes2/releases/tag/0.9.16)

- [X.Org Server 21.1.4 update with security fixes:](https://lists.x.org/archives/xorg/2022-July/061036.html)

- [Release of Tails 5.2:](https://forum.torproject.net/t/new-release-tails-5-2/3944)

- [Red Hat appoints new CEO:](https://www.redhat.com/en/about/press-releases/red-hat-names-matt-hicks-president-and-chief-executive-officer)

- [Updating a DogLinux Build to Check Hardware:](https://gumanzoy.blogspot.com/2021/05/liveusb-1100mb-doglinux-debian-11.html)

- [Builds with LXQt 1.1 user environment prepared for Lubuntu 22.04:](https://lubuntu.me/jammy-backports-22-04-1-cft/)

- [Tor Browser 11.5:](https://blog.torproject.org/new-release-tor-browser-115/)

- [Chrome OS Flex operating system ready for installation on any hardware:](https://cloud.google.com/blog/products/chrome-enterprise/chromeos-flex-ready-to-scale-to-pcs-and-macs)

- [Arcan desktop engine release 0.6.2:](https://arcan-fe.com/2022/07/15/arcan-0-6-2-its-all-connected/)

- [T2 SDE 22.6 Meta Distribution Released:](https://www.mail-archive.com/t2@t2-project.org/msg04039.html)

New teeth coming soon, I promise!

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
