---
title: "Full Circle Weekly News 192"
date: 2020-12-03
draft: false
tags:
  - "2020"
  - "ardour"
  - "blender"
  - "computers"
  - "core"
  - "defender"
  - "drivers"
  - "fingerprint"
  - "firefox"
  - "gimp"
  - "ipfire"
  - "kali"
  - "kaos"
  - "kde"
  - "librem"
  - "linux"
  - "microsoft"
  - "mozilla"
  - "purism"
  - "remix"
  - "tails"
  - "thunderbird"
  - "tuxedo"
  - "ubuntu"
  - "vulkan"
  - "web"
  - "wine"
  - "xfce"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20192.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [KDE Will Support Fingerprints](https://www.debugpoint.com/2020/11/kde-plasma-5-21-fingerprint-manager/)

- [XFCE Shaping Up For a Strong Version 4.16](https://www.debugpoint.com/2020/11/xfce-4-16-release-highlights-2/)

- [Microsoft Defender Previewing New Features for Linux Only](https://www.zdnet.com/article/microsoft-defender-for-linux-adds-new-security-feature/#ftag=RSSbaffb68)

- [Tuxedo Computers Reverse Engineers Drivers](https://9to5linux.com/tuxedo-computers-enables-full-linux-support-on-the-intel-tongfang-qc7-gaming-laptop)

- [Tails 4.13 Out](https://tails.boum.org/news/version_4.13/)

- [Kali 2020.4 Out](https://www.kali.org/news/kali-linux-2020-4-release/)

- [IPFire 2.25 Core Update 152 Out](https://blog.ipfire.org/post/ipfire-2-25-core-update-152-released)

- [Kaos 2020.11 Out](https://kaosx.us/news/2020/kaos11/)

- [Ubuntu Web Remix Out](https://discourse.ubuntu.com/t/ubuntu-web-remix/19394)

- [Firefox 83 Out](https://www.mozilla.org/en-US/firefox/83.0/releasenotes/)

- [Firefox 84 Soon to be out](https://www.mozilla.org/en-US/firefox/84.0beta/releasenotes/)

- [Thunderbird 78.5.0 Out](https://www.thunderbird.net/en-US/thunderbird/78.5.0/releasenotes/)

- [Wine 5.22 Out](https://www.winehq.org/announce/5.22)

- [Ardour 6.5 Out](https://ardour.org/whatsnew.html)

- [Blender 2.91 Out](https://www.blender.org/download/releases/2-91/)

- [Vulkan Ray Tracing Support Out](https://www.gamingonlinux.com/2020/11/vulkan-ray-tracing-becomes-official-with-vulkan-12162)

- [Purism's Librem 5 Out](https://puri.sm/posts/the-librem-5-mass-production-shipping-faq/)

- [GIMP turns 25](https://www.gimp.org/news/2020/11/21/25-years-of-gimp/)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
