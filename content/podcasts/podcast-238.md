---
title: "Full Circle Weekly News 238"
date: 2021-12-04
draft: false
tags:
  - "airyx"
  - "alpine"
  - "archinstall"
  - "cad"
  - "cadzinho"
  - "clonezilla"
  - "deepin"
  - "desktop"
  - "endless"
  - "engine"
  - "filter"
  - "freebsd"
  - "germany"
  - "libreoffice"
  - "linux"
  - "macos"
  - "nftables"
  - "packet"
  - "php"
  - "pureos"
  - "wireshark"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20238.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [nftables Packet Filter 1.0.1](https://www.mail-archive.com/netfilter-announce@lists.netfilter.org/msg00234.html)

- [Release of PureOS 10](https://puri.sm/posts/true-convergence-is-here-pureos-10-is-released-for-all-librem-products/)

- [Arcan 0.6.1 Desktop Engine Released](https://arcan-fe.com/)

- [Germany plans for Linux and LibreOffice](https://blog.documentfoundation.org/blog/2021/11/18/german-state-planning-to-switch-25000-pcs-to-libreoffice/)

- [Q4OS 4.7 Distribution Released](https://www.q4os.org/blog.html#news211122)

- [First test release of free 2D CAD CadZinho](https://github.com/zecruel/CadZinho/)

- [PHP Foundation announced](https://blog.jetbrains.com/phpstorm/2021/11/the-php-foundation/)

- [Release of Deepin 20.3](https://www.deepin.org/en/2021/11/23/deepin-20-3/)

- [Wireshark 3.6](https://www.wireshark.org/news/20211122.html)

- [Release of the minimalistic distribution kit Alpine Linux 3.15](https://alpinelinux.org/posts/Alpine-3.15.0-released.html)

- [Release of Endless OS 4.0](https://community.endlessos.com/t/release-endless-os-4-0-0/18014)

- [Airyx project develops a FreeBSD edition compatible with macOS applications](https://github.com/mszoek/airyx/releases/tag/airyx-0.3.0)

- [Release of the Archinstall 2.3.0 installer](https://github.com/archlinux/archinstall/releases/tag/v2.3.0)

- [Release of the Clonezilla Live 2.8.0](https://sourceforge.net/p/clonezilla/news/2021/11/stable-clonezilla-live-280-27-released-/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
