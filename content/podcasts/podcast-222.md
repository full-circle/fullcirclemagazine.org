---
title: "Full Circle Weekly News 222"
date: 2021-08-09
draft: false
tags:
  - "bullseye"
  - "crossover"
  - "deadbeef"
  - "debian"
  - "desktop"
  - "edgex"
  - "glibc"
  - "installer"
  - "iot"
  - "l0phtcrack"
  - "library"
  - "linux"
  - "media"
  - "music"
  - "nightly"
  - "pipewire"
  - "platform"
  - "player"
  - "rc"
  - "server"
  - "steam"
  - "system"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20222.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Glibc 2.34 system library released](https://sourceware.org/pipermail/libc-alpha/2021-August/129718.html)

- [Third candidate for Debian 11 “Bullseye” installer](https://lists.debian.org/debian-devel-announce/2021/08/msg00000.html)

- [The share of Linux users on Steam was 1% in July](https://store.steampowered.com/hwsurvey)

- [The opening of the source code for L0phtCrack was announced](https://twitter.com/dildog/status/1421830165911556099)

- [CrossOver 21.0 released](https://www.codeweavers.com/support/forums/announce/?t=24;msg=243826)

- [Nightly builds of Ubuntu Desktop have a new installer](https://github.com/canonical/ubuntu-desktop-installer)

- [New version of DeaDBeeF music player](https://deadbeef.sourceforge.io/posts/deadbeef_1.8.8_is_out.html)

- [PipeWire Media Server 0.3.33 released](https://github.com/PipeWire/pipewire/releases/tag/0.3.33)

- [EdgeX 2.0 IoT Platform Released](https://www.linuxfoundation.org/press-release/edgex-foundry-releases-the-most-modern-secure-and-production-ready-open-source-iot-framework/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
