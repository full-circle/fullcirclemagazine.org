---
title: "Full Circle Weekly News 261"
date: 2022-05-17
draft: false
tags:
  - "apache"
  - "canonical"
  - "cisco"
  - "clamav"
  - "firmware"
  - "fwupd"
  - "github"
  - "kaos"
  - "microsoft"
  - "nextcloud"
  - "openmediavault"
  - "openoffice"
  - "oracle"
  - "proxmox"
  - "seamonkey"
  - "shell"
  - "solaris"
  - "steamos"
  - "tails"
  - "thunderbird"
  - "toolkit"
  - "tor"
  - "trinity"
  - "unity"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20261.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Canonical introduces Steam Snap for easier access to games on Ubuntu:](https://ubuntu.com//blog/level-up-linux-gaming-new-steam-snap)

- [Unity 7.6.0 Custom Shell Release:](https://ubuntuunity.org/)

- [fwupd 1.8.0, firmware download toolkit available:](https://blogs.gnome.org/hughsie/2022/04/28/fwupd-1-8-0-and-50-million-updates/)

- [Release of Trinity R14.0.12 DE:](https://www.trinitydesktop.org/)

- [KaOS 2022.04 distribution released:](https://kaosx.us/news/2022/kaos04/)

- [Microsoft has joined the work on the Open 3D Engine:](https://www.linuxfoundation.org/press-release/the-open-3d-foundation-welcomes-microsoft-as-a-premier-member-to-advance-the-future-of-open-source-3d-development/)

- [Debugger Release GDB 12](https://sourceware.org/pipermail/gdb/2022-May/050077.html)

- [Oracle Publishes Solaris 10 to Solaris 11.4 Application Migration Tool](https://blogs.oracle.com/solaris/post/sysdiff-moving-legacy-oracle-solaris-10-3rd-party-apps-to-114)

- [T.A.I.L.S. 5.0 released:](https://tails.boum.org/news/version_5.0/index.en.html)

- [Release of SeaMonkey 2.53.12, Tor Browser 11.0.11 and Thunderbird 91.9.0:](https://www.seamonkey-project.org/)
(https://blog.torproject.org/new-release-tor-browser-11011/)
(https://www.thunderbird.net/en-US/thunderbird/91.9.0/releasenotes/)

- [Enthusiasts have prepared a Steam OS 3 build for regular PCs:](https://github.com/bhaiest/holoiso/releases/tag/beta2)

- [Microsoft Open Sources the basically obsolete 3D Movie Maker:](https://github.com/microsoft/Microsoft-3D-Movie-Maker)(https://3dmm.com/)

- [Cisco has released ClamAV 0.105:](https://blog.clamav.net/2022/05/clamav-01050-01043-01036-released.html)

- [Release of Proxmox VE 7.2:](https://forum.proxmox.com/threads/proxmox-ve-7-2-released.108969/)

- [OpenMediaVault 6 is available:](https://www.openmediavault.org/?p=3201)

- [Apache OpenOffice 4.1.12 released:](https://cwiki.apache.org/confluence/display/OOOUSERS/AOO+4.1.12+Release+Notes)

- [GitHub will switch to mandatory two-factor authentication:](https://github.blog/2022-05-04-software-security-starts-with-the-developer-securing-developer-accounts-with-2fa/)

- [Kubernetes 1.24 Released:](https://kubernetes.io/blog/2022/05/03/kubernetes-1-24-release-announcement/)

- [Nextcloud Hub 24 Collaboration Platform Available:](https://nextcloud.com/blog/nextcloud-hub-24-is-here/)

- [Apple has published the code for the kernel and system components of macOS 12.3:](https://opensource.apple.com/releases/)

- [Release of the GCC 12 compiler suite:](https://gcc.gnu.org/pipermail/gcc-announce/2022/000171.html)

- [The deb-get utility, offering apt-get-like functionality for third-party packages:](https://twitter.com/m_wimpress/status/1521806830707560448)

- [China intends to transfer state institutions and state-owned enterprises to Linux:](https://www.bloomberg.com/news/articles/2022-05-06/china-orders-government-state-firms-to-dump-foreign-pcs)

- [Release of new stable branch Tor 0.4.7](https://blog.torproject.org/congestion-contrl-047/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
