---
title: "Full Circle Weekly News 25"
date: 2016-07-09
draft: false
tags:
  - "6-8"
  - "bleachbit"
  - "calamares"
  - "enterprise"
  - "fedora"
  - "hat"
  - "installer"
  - "nexus"
  - "phone"
  - "rc1"
  - "red"
  - "redhat"
  - "scientific"
  - "universale"
  - "unofficial"
  - "vmware"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2025.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Nexus 6 Is Now an Unofficial Ubuntu Phone](http://news.softpedia.com/news/nexus-6-is-now-an-unofficial-ubuntu-phone-wi-fi-bluetooth-support-coming-soon-506050.shtml)

- [BleachBit 1.12 Brings Support for Ubuntu 16.04, and Fedora 24](http://news.softpedia.com/news/bleachbit-1-12-free-system-cleaner-brings-support-for-ubuntu-16-04-fedora-24-505988.shtml)

- [Scientific Linux 6.8 to Be Based on Red Hat Enterprise Linux 6.8, RC1 Is Out Now](http://news.softpedia.com/news/scientific-linux-6-8-to-be-based-on-red-hat-enterprise-linux-6-8-rc1-is-out-now-505869.shtml)

- [Calamares 2.3 Universal Linux OS Installer Released with Full-Disk Encryption](http://news.softpedia.com/news/calamares-2-3-universal-linux-os-installer-released-with-full-disk-encryption-505866.shtml)

- [Millions of Android devices have flawed full disk encryption](https://www.engadget.com/2016/07/01/android-qualcomm-security-flaw-encryption/)

- [VMware Hires Longtime Intel Linux Exec As Its First-Ever Chief Open Source Officer](http://www.crn.com/news/cloud/300081237/vmware-hires-longtime-intel-linux-exec-as-its-first-ever-chief-open-source-officer.htm)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
