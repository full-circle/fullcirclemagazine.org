---
title: "Full Circle Weekly News 372"
date: 2024-06-30
draft: false
tags:
  - "suse"
  - "enterprise"
  - "kde"
  - "plasma"
  - "pale moon"
  - "tinygo"
  - "easyos"
  - "libgcrypt"
  - "syslinuxos"
  - "vivaldi"
  - "exectos"
  - "darktable"
  - "amelia"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20372.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [SUSE Linux Enterprise 15 SP6:](https://www.suse.com/c/announcing-suse-linux-enterprise-15-sp6-the-linux-choice-for-security-and-compliance-in-a-reliable-it/)

- [Release of the KDE Plasma 6.1:](https://kde.org/announcements/plasma/6/6.1.0/)

- [Pale Moon Browser 33.2.0:](https://forum.palemoon.org/viewtopic.php?t%3D31260%26p%3D252591%23p252591)

- [Tinygo 0.32:](https://github.com/tinygo-org/tinygo/releases/tag/v0.32.0)

- [Release of EasyOS 6.0:](https://bkhome.org/news/202406/easyos-scarthgap-series-version-60-released.html)

- [A new release of Libgcrypt 1.11.0:](https://lists.gnupg.org/pipermail/gnupg-devel/2024-June/035585.html)

- [Release of SysLinuxOS 12.4:](https://syslinuxos.com/syslinuxos-12-4-released/)

- [The X Window System is 40 years old:](https://www.talisman.org/x-debut.shtml)

- [Release of Vivaldi 6.8:](https://vivaldi.com/blog/desktop/desktop-releases/vivaldi-on-desktop-6-8/)

- [The ExectOS open OS:](https://exectos.eu.org/)

- [Release of Darktable 4.8.0:](https://www.darktable.org/2024/06/darktable-4.8.0-released/)

- [Amelia 5.8:](https://www.reddit.com/r/archlinux/comments/1dm6y7s/amelia_installer_updated/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
