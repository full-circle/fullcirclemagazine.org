---
title: "Full Circle Weekly News 130"
date: 2019-05-13
draft: false
tags:
  - "4-8"
  - "5-1"
  - "alpine"
  - "bug"
  - "computing"
  - "docker"
  - "firefox"
  - "foundation"
  - "freespire"
  - "gnu"
  - "kernel"
  - "linux"
  - "linux-libre"
  - "mozilla"
  - "ota-9"
  - "ota9"
  - "phone"
  - "problem"
  - "root"
  - "touch"
  - "unlocked"
  - "urban"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20130.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Linux Kernel 5.1 Released With Lots Of New Features](https://fossbytes.com/linux-5-1-kernel-features-download-linus/)

- [GNU Linux-libre 5.1 Released](https://www.pro-linux.de/news/1/27036/gnu-linux-libre-51-freigegeben.html)

- [Mozilla Issues New Update to Fix Add-ons Problem](https://betanews.com/2019/05/06/firefox-update-add-ons-fix/)

- [Freespire 4.8 Officially Released](https://news.softpedia.com/news/freespire-4-8-officially-released-based-on-ubuntu-18-04-2-lts-525902.shtml)

- [Ubuntu Touch OTA-9 Released for Ubuntu Phones](https://news.softpedia.com/news/ubuntu-touch-ota-9-released-for-ubuntu-phones-with-refreshed-look-improvements-525949.shtml)

- [Linux Foundation forms the Urban Computing Foundation](https://www.pro-linux.de/news/1/27039/urban-computing-foundation-gegr%C3%83%C2%BCndet.html)

- [Bug in Alpine Linux Docker Image Leaves Root Account Unlocked](https://www.bleepingcomputer.com/news/security/bug-in-alpine-linux-docker-image-leaves-root-account-unlocked/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
