---
title: "Full Circle Weekly News 116"
date: 2018-12-04
draft: false
tags:
  - "alert"
  - "breach"
  - "firefox"
  - "flatpak"
  - "foundation"
  - "hacked"
  - "ibm"
  - "linux"
  - "patch"
  - "red-hat"
  - "redhat"
  - "sandbox"
  - "spectre"
  - "torvalds"
  - "touch"
  - "uber"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20116.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Flatpak Linux App Sandboxing Format Now Lets You Kill Running Flatpak Instances](https://news.softpedia.com/news/flatpak-linux-app-sandboxing-format-now-lets-you-kill-running-flatpak-instances-523928.shtml)

- [Red Hat Exec Says IBM Must Keep the Open-Source Culture Untouched](https://news.softpedia.com/news/red-hat-exec-says-ibm-must-keep-the-open-source-culture-untouched-523900.shtml)

- [Uber Joins Linux Foundation As A Gold Member](https://fossbytes.com/uber-joins-linux-foundation-as-a-gold-member/)

- [Uber Webapp for Ubuntu Touch:](https://open-store.io/app/uberwebapp.totalsonic)

- [Firefox Will Now Show You Data Breach Alert If You Visit Hacked Sites](https://fossbytes.com/firefox-will-now-show-you-data-breach-alert-if-you-visit-hacked-sites/)

- [Almost a quarter of reported vulnerabilities have no known solution Source](https://betanews.com/2018/11/19/vulnerabilities-no-solution/)

- [Linus Torvalds: After big Linux performance hit, Spectre v2 patch needs curbs](https://www.zdnet.com/article/linus-torvalds-after-big-linux-performance-hit-spectre-v2-patch-needs-curbs/)
