---
title: "Full Circle Weekly News 235"
date: 2021-11-15
draft: false
tags:
  - "animated"
  - "antix"
  - "apache"
  - "asterisk"
  - "blender"
  - "canonical"
  - "clamav"
  - "enterprise"
  - "fedora"
  - "freepbx"
  - "intel"
  - "leap"
  - "lua"
  - "lxqt"
  - "movie"
  - "mpv"
  - "openmeetings"
  - "opensuse"
  - "redhat"
  - "red hat"
  - "tails"
  - "vaultwarden"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20235.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Apache OpenMeetings 6.2 available](https://blogs.apache.org/openmeetings/entry/openmeetings-v6-2-0-openapi)

- [Vaultwarden 1.23, released](https://github.com/dani-garcia/vaultwarden)

- [Blender Community Releases Sprite Fright Animated Movie](https://www.blender.org/)

- [New release of antiX 21](https://antixlinux.com/antix-21-grup-yorum-released/)

- [Release of MPV 0.34](http://mpv.io/)

- [Fedora Linux 35 Distribution Released](https://fedoramagazine.org/announcing-fedora-35/)

- [OpenSUSE Leap 15.3-2 First Quarterly Update Available](https://news.opensuse.org/2021/11/02/leaps-first-quarterly-update-is-released/)

- [Canonical unveils Intel-optimized Ubuntu builds](https://ubuntu.com//blog/ubuntu-optimised-for-intel-processors-accelerates-adoption-of-iot-innovations)

- [Release of Asterisk 19 communication platform and FreePBX 16 distribution](https://www.asterisk.org/asterisk-news/asterisk-19-0-0-now-available/)

- [Red Hat Enterprise Linux 9 beta testing begins](https://www.redhat.com/en/blog/red-hat-enterprise-linux-85-beta-now-available)

- [ClamAV 0.104.1 update](https://blog.clamav.net/2021/11/clamav-01034-and-01041-patch-releases.html)

- [Open source Luau, a type-checking variant of Lua](https://luau-lang.org/2021/11/03/luau-goes-open-source.html)

- [LXQt 1.0 Graphics Environment Released](https://github.com/lxqt/lxqt/releases/tag/1.0.0)

- [Tails 4.24 distribution released](https://tails.boum.org/news/version_4.24/index.en.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
