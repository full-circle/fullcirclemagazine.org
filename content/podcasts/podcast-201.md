---
title: "Full Circle Weekly News 201"
date: 2021-03-05
draft: false
tags:
  - "20-04-1"
  - "backport"
  - "blender"
  - "debian"
  - "devuan"
  - "firefox"
  - "frameworks"
  - "hippo"
  - "hirstute-hippo"
  - "kali"
  - "kde"
  - "kernel"
  - "kodi"
  - "linspire"
  - "linux"
  - "mageia"
  - "manjaro"
  - "mesa"
  - "mir"
  - "netrunner"
  - "openmandriva"
  - "pinephone"
  - "plasma"
  - "pop_os"
  - "project"
  - "proton"
  - "red-hat"
  - "redhat"
  - "regression"
  - "rhel"
  - "system76"
  - "tails"
  - "thunderbird"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20201.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Releases 20.04.2.0 After Regression](https://lists.ubuntu.com/archives/ubuntu-announce/2021-February/000265.html)

- [Hirsute Hippo Feature Freeze](https://lists.ubuntu.com/archives/ubuntu-devel-announce/2021-February/001288.html)

- [Thunderbird Backported to Ubuntu 20.04](https://discourse.ubuntu.com/t/thunderbird-lts-update/20819?u=d0od)

- [Mesa Project Looking for Help](https://www.phoronix.com/scan.php?page=article&item=radv-aco-cft&num=1)

- [System76 Milling Their Own Open Source Keyboards](https://blog.system76.com/post/612874398967513088/making-a-keyboard-the-system76-approach)

- [Red Hat Announces More Free RHEL](https://www.phoronix.com/scan.php?page=news_item&px=RHEL-For-Open-Infrastructure)

- [Kernel 5.11 Out](http://lkml.iu.edu/hypermail/linux/kernel/2102.1/08310.html)

- [Debian 10.8 Out](https://www.debian.org/News/2021/20210206)

- [Devuan 3.1 Out](https://www.devuan.org/os/announce/beowulf-point-release-announce-021421)

- [Linspire 10 Out](https://www.linspirelinux.com/2021/02/linspire-10-released.html)

- [OpenMandriva 4.2 Out](https://www.openmandriva.org/en/news/article/openmandriva-lx-4-2-is-out-now)

- [Mageia 8 Out](https://blog.mageia.org/en/2021/02/26/made-it-to-a-byte-announcing-the-release-of-mageia-8/)

- [Kali Linux 2021.1 Out](https://www.kali.org/blog/kali-linux-2021-1-release/)

- [Tails 4.16 Out](https://tails.boum.org/news/version_4.16/index.en.html)

- [Netrunner 21.01, XOXO, Out](https://www.netrunner.com/netrunner-21-01-xoxo-released/)

- [Firefox 86 Out](https://www.mozilla.org/en-US/firefox/86.0/releasenotes/)

- [Mir 2.3.2 Out](https://discourse.ubuntu.com/t/mir-release-2-3-2/20891)

- [Pop!\_OS Mouse Driven Tiling Out](https://twitter.com/system76/status/1359906476353982466?s=20)

- [Proton 5.13-6 Out](https://github.com/ValveSoftware/Proton/releases/tag/proton-5.13-6)

- [Kodi 19 Out](https://kodi.tv/article/kodi-190-matrix-release)

- [KDE Plasma 5.21 Out](https://kde.org/announcements/plasma/5/5.21.0/)

- [KDE Frameworks 5.79.0 Out](https://kde.org/announcements/frameworks/5/5.79.0/)

- [KDE Plasma 5.21 for Pinephone Powered by Manjaro Out](https://twitter.com/ManjaroLinux/status/1360618565959876608?s=20)

- [Blender 2.92 Out](https://www.blender.org/download/releases/2-92/)

- [0 A.D. Alpha 24 Out](https://play0ad.com/new-release-0-a-d-alpha-24-xsayarsa/)

**Credits**

- Host: [@leochavez](https://twitter.com/leochavez)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
