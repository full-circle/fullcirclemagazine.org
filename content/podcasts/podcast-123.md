---
title: "Full Circle Weekly News 123"
date: 2019-02-27
draft: false
tags:
  - "18-04"
  - "android"
  - "crypto"
  - "debian"
  - "distro"
  - "elisa"
  - "fixes"
  - "foundation"
  - "gnu"
  - "kali"
  - "kernel"
  - "linux"
  - "metasploit"
  - "slax"
  - "tor"
  - "traffic"
  - "ubuntu"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20123.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu 18.04.2 LTS Is Here With New Hardware Enablement Stack Source](https://fossbytes.com/ubuntu-18-04-2-lts-release-download-features/)

- [Debian GNU/Linux 9.8 Released with over 180 Security Updates and Bug Fixes Source](https://news.softpedia.com/news/debian-gnu-linux-9-8-released-with-over-180-security-updates-and-bug-fixes-524979.shtml)

- [Slax 9.8 Linux Distro Released with Various Updates from Debian GNU/Linux 9.8 Source](https://news.softpedia.com/news/slax-9-8-linux-distro-released-with-various-updates-from-debian-gnu-linux-9-8-524996.shtml)

- [Linux Foundation launches ELISA, an open source project for building safety-critical systems](https://venturebeat.com/2019/02/21/linux-foundation-elisa/)

- [Tor traffic from individual Android apps detected with 97 percent accuracy](https://www.zdnet.com/article/tor-traffic-from-individual-android-apps-detected-with-97-percent-accuracy/)

- [10-year-old critical gap discovered in Linux kernel crypto function](https://www.heise.de/security/meldung/10-Jahre-alte-kritische-Luecke-in-Linux-Kernel-Kryptofunktion-entdeckt-4315290.html)

- [Kali Linux 2019.1 Launched With Metasploit 5.0 Source](https://fossbytes.com/kali-linux-2019-1-launched-with-metasploit-5-0/)
