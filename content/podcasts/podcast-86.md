---
title: "Full Circle Weekly News 86"
date: 2018-03-25
draft: false
tags:
  - "18-10"
  - "amd"
  - "cinnamon"
  - "cpu"
  - "devices"
  - "distro"
  - "facebook"
  - "flaw"
  - "hypervisor"
  - "initramfs"
  - "inkscape"
  - "lz4"
  - "mint"
  - "processors"
  - "tara"
  - "zorin"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%2086.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Zorin OS 12.3 Linux Distro Released: Download The Perfect Windows Replacement](https://fossbytes.com/zorin-os-12-3-linux-download-features/)

- [Ubuntu 18.10 Will Boot Faster, Thanks to LZ4 Initramfs Compression](http://news.softpedia.com/news/ubuntu-18-10-will-boot-faster-thanks-to-lz4-initramfs-compression-520317.shtml)

- [Linux Mint 19 'Tara' Cinnamon will be faster](https://betanews.com/2018/03/18/mint-linux-slow-fast/)

- [Announcing the 0.92.3 Release of Inkscape](https://inkscape.org/en/news/2018/03/22/announcing-0923-release-inkscape/)

- [Linux Foundation Announces ACRN —Open Source Hypervisor for IoT Devices](https://www.bleepingcomputer.com/news/technology/linux-foundation-announces-acrn-open-source-hypervisor-for-iot-devices/)

- [Firefox’s Weak Master Password Encryption Can Be Cracked In Just 1 Minute](https://fossbytes.com/firefox-master-password-weak-encryption-brute-force-one-minute/)

- [Zuckerberg Facing Heat After Multiple Investigations Into Facebook Data Breach](https://fossbytes.com/facebook-cambridge-analytica-scandal-investigation/)

- [AMD Processors Flaws: Firmware Patches Coming Soon, Won’t Affect Performance](https://fossbytes.com/amd-processors-flaws-firmware-patches-wont-affect-performance/)
