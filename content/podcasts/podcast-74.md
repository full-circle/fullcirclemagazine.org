---
title: "Full Circle Weekly News 74"
date: 2017-12-09
draft: false
tags:
  - "2016"
  - "ai"
  - "chip"
  - "crossover"
  - "engine"
  - "god-mode"
  - "hackers"
  - "imagination"
  - "intel"
  - "linux"
  - "management"
  - "me"
  - "microsoft"
  - "nvidia"
  - "office"
  - "omg"
  - "puppy"
  - "remix"
  - "ubuntu"
  - "unity"
  - "xenialpup"
cover: "covers/podcasts/fallback.webp"
mp3: "https://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20-%20episode%2074.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Ubuntu Unity Remix? Are We Going To Get A New Ubuntu “Unity” Flavor?](https://fossbytes.com/ubuntu-unity-7-remix-new-ubuntu-flavor-possible/)

- [Nvidia looks to reduce AI training material through 'imagination'](http://www.zdnet.com/article/nvidia-looks-to-reduce-ai-training-material-through-imagination/)

- [CrossOver 17 Lets You Install Microsoft Office 2016 on Your Linux Computer](http://news.softpedia.com/news/crossover-17-lets-you-install-microsoft-office-2016-on-your-linux-computer-518843.shtml)

- [Computer vendors start disabling Intel Management Engine](http://www.zdnet.com/article/computer-vendors-start-disabling-intel-management-engine/)

- [Hackers Turn On “GOD MODE” To Hack Intel ME Chip Like A Boss](https://fossbytes.com/intel-me-chip-god-mode-hack-black-hat-europe/)

- [Lightweight Distro Puppy Linux 7.5 “Xenialpup” Released](https://fossbytes.com/lightweight-puppy-linux-7-5-download-iso/)

**CREDITS**

- Intro: ["Weapons" - Cory Gray](http://freemusicarchive.org/music/Cory_Gray/Music_For_Film__TV/Weapons_1703)
- [News Beeps - johnnytal](http://freesound.org/people/johnnytal/sounds/88517/)
- Ubuntu drum - Canonical
