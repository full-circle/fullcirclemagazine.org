---
title: "Full Circle Weekly News 257"
date: 2022-04-15
draft: false
tags:
  - "browser"
  - "builds"
  - "carbon"
  - "carbonos"
  - "claws"
  - "desktop"
  - "emacs"
  - "email"
  - "fedora"
  - "ferretdb"
  - "gentoo"
  - "lutris"
  - "maui"
  - "mongodb"
  - "network"
  - "nitrux"
  - "openssh"
  - "postgresql"
  - "qute"
  - "sftp"
  - "shell"
  - "shepherd"
  - "speek"
  - "terminal"
  - "tor"
  - "uefi"
  - "xfce"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20257.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Release of the Lutris 0.5.10 platform](https://github.com/lutris/lutris/releases/tag/v0.5.10)

- [Release of Nitrux 2.1 with the NX Desktop](https://nxos.org/changelog/release-announcement-nitrux-2-1-0/)

- [Release of xfce4-terminal 1.0.0](http://users.uoa.gr/~sdi1800073/sources/xfce_blog12.html)

- [qutebrowser 2.5 and Min 1.24 web browsers available](https://github.com/minbrowser/min/releases/tag/v1.24.0)(https://github.com/qutebrowser/qutebrowser/releases/tag/v2.5.0)

- [New versions of Claws Mail 3.19.0 and 4.1.0](https://lists.claws-mail.org/pipermail/users/2022-April/029774.html)

- [Speek 1.6 messenger available, using Tor network for privacy](https://github.com/Speek-App/Speek/releases/tag/v1.6.0-release)

- [Gentoo Starts Publishing Weekly Live Builds](https://www.gentoo.org/news/2022/04/03/livegui-artwork-contest.html)

- [First alpha release of Maui Shell userspace](https://nxos.org/maui/maui-shell-alpha-release/)

- [FerretDB 0.1, MongoDB implementation based on PostgreSQL DBMS](https://github.com/FerretDB/FerretDB/releases/tag/v0.1.0)

- [Fedora 37 intends to only support UEFI](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/K5YKCQU3YVCTMSBHLP4AOQWIE3AHWCKC/)

- [GNU Emacs 28.1 released](https://www.mail-archive.com/info-gnu@gnu.org/msg03030.html)

- [Release of the GNU Shepherd 0.9 init system](https://www.mail-archive.com/info-gnu@gnu.org/msg03031.html)

- [First release of the atomically upgradable carbonOS distribution](https://carbon.sh/blog/2022-03-31-release.html)

- [Release of the SELKS 7.0, aimed at creating intrusion detection systems](https://www.stamus-networks.com/pr/06-april-2022)

- [Release of gzip utility 1.12](https://www.mail-archive.com/info-gnu@gnu.org/msg03032.html)

- [Release of OpenSSH 9.0 with transfer of scp to the SFTP protocol](https://lists.mindrot.org/pipermail/openssh-unix-dev/2022-April/040174.html)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
