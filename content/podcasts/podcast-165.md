---
title: "Full Circle Weekly News 165"
date: 2020-03-18
draft: false
tags:
  - "fortitude"
  - "fsf"
  - "gnu"
  - "gparted"
  - "kernel"
  - "kubuntu"
  - "laptop"
  - "linux"
  - "make"
  - "protonvpn"
  - "red-hat"
  - "redhat"
  - "solus"
  - "upcycle"
  - "windows"
  - "wine"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20165.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Kubuntu Focus Laptop to Ship in Early February](https://www.zdnet.com/article/kubuntu-focus-a-new-top-of-the-line-linux-laptop-arrives/)

- [ProtonVPN Open Sources All of Its Apps](https://protonvpn.com/blog/open-source/)

- [Wine 5.0 Released](https://www.winehq.org/news/2020012101)

- [Kernel 5.5 Released](https://lkml.org/lkml/2020/1/26/232)

- [FSF Wants to Upcycle Windows 7](https://www.fsf.org/windows/upcycle-windows-7)

- [Gnu Make 4.3 Released](https://savannah.gnu.org/forum/forum.php?forum_id=9654)

- [Red Hat Enterprise Linux 8.2 Beta Released](https://www.zdnet.com/article/red-hat-enterprise-linux-8-2-beta-arrives/)

- [KDE Plasma 5.18 LTS Default Wallpaper Has Been Selected](https://dot.kde.org/2020/01/24/volna-wins-plasma-518-wallpaper-contest)

- [Solus 4.1 Fortitude Released](https://getsol.us/2020/01/25/solus-4-1-released/)

- [Gparted 1.1.0 Released](https://gparted.org/news.php?item=231)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
