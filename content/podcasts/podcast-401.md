---
title: "Full Circle Weekly News 401"
date: 2025-02-23
draft: false
tags:
  - "endeavour"
  - "plasma"
  - "flac"
  - "systemd"
  - "duckdb"
  - "opensuse"
  - "kernel"
  - "chimera"
  - "serpent"
  - "kde"
  - "luanti"
  - "cadbase"
  - "bios"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20401.mp3"
spotify: "https://open.spotify.com/show/0AYBF3gfbHpYvhW0pnjPrK"
---

- [EndeavourOS 25.02 released:](https://endeavouros.com/)

- [Plasma 6.3 Released](https://kde.org/announcements/plasma/6/6.3.0/)
    
- [Free audio codec FLAC 1.5 released:](https://xiph.org/flac/2025/02/11/flac-1-5-0-released.html)

- [Systemd wants to download system images via HTTP:](https://github.com/systemd/systemd/pull/36314)

- [DuckDB 1.2.0 Released:](https://duckdb.org/2025/02/05/announcing-duckdb-120)

- [openSUSE Tumbleweed switches to SELinux:](https://news.opensuse.org/2025/02/13/tw-plans-to-adopt-selinux-as-default/)

- [Increasing the Linux kernel timer frequency to 1000Hz:](https://lore.kernel.org/lkml/20250210001915.123424-1-qyousef@layalina.io/)

- [Chimera 20250214 released:](https://chimera-linux.org/news/2025/02/new-images.html)

- [Serpent OS distribution renamed to AerynOS:](https://serpentos.com/blog/2025/02/14/evolve-this-os/)

- [KDE 6.4 development begins:](https://blogs.kde.org/2025/02/15/this-week-in-plasma-post-release-polishing/)

- [Release of Luanti 5.11.0:](https://blog.luanti.org/2025/02/14/5.11.0-released/)

- [Update to CADBase, a platform for exchanging 3D models and drawings:](https://gitlab.com/cadbase/cdbs-app)

- [Freezing disk format changes in Bcachefs:](https://lore.kernel.org/lkml/hodakekojuga62jmvqimb63dyyavx6jqdy7t67cltmha55fl5n@jl2guh3xzh4s/)

 -[SUSE and openSUSE consider dropping boot support on BIOS systems:](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/H4JPQKOEKV6ECTNRE764NJFYSLLAWQLL/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
