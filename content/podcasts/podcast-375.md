---
title: "Full Circle Weekly News 375"
date: 2024-07-21
draft: false
tags:
  - "arch"
  - "gnome"
  - "box64"
  - "shotstars"
  - "debian"
  - "zed"
  - "clonezilla"
  - "exim"
  - "firewalld"
  - "freebsd"
  - "thunderbird"
  - "whonix"
  - "obs"
  - "qubes"
cover: "covers/podcasts/fallback.webp"
mp3: "http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20375.mp3"
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Booting Arch Linux from Google Drive:](https://ersei.net/en/blog/fuse-root)

- [GNOME switching font?](https://gitlab.gnome.org/GNOME/gsettings-desktop-schemas/-/merge_requests/85)

- [GDB Debugger 15:](https://www.mail-archive.com/info-gnu@gnu.org/msg03302.html)

- [Release of Box64 0.3.0:](https://box86.org/2024/07/new-box64-v0-3-0-released/)

- [Shotstars 0.2:](https://github.com/snooppr/shotstars/)

- [Debian GNU/Hurd builds 71% of Debian packages:](https://www.gnu.org/software/hurd/news/2024-q2.html)

- [Multi-user code editor Zed now supports Linux:](https://zed.dev/blog/zed-on-linux)

- [Clonezilla Live 3.1.3:](https://sourceforge.net/p/clonezilla/news/2024/07/stable-clonezilla-live-313-11-released/)

- [New version of Exim mail server 4.98:](https://lists.exim.org/lurker/message/20240710.155945.8823670d.en.html)

- [Release of firewalld 2.2.0:](https://github.com/firewalld/firewalld/releases/tag/v2.2.0)

- [FreeBSD switches to a shorter release cycle:](https://lists.freebsd.org/archives/freebsd-announce/2024-July/000143.html)

- [Thunderbird email client 128:](https://blog.thunderbird.net/2024/07/welcome-to-thunderbird-128-nebula/)

- [Release of the OBS Studio 30.2:](https://obsproject.com/blog/obs-studio-hybrid-mp4)

- [Release of Whonix 17.2:](https://forums.whonix.org/t/whonix-17-2-0-1-all-platforms-point-release/20078)

- [Update to Qubes OS 4.2.2:](https://www.qubes-os.org/news/2024/07/13/qubes-os-4-2-2-has-been-released/)

**Credits**

- Host: [@bardictriad](https://twitter.com/bardictriad), [@zaivala](mailto:zaivala@hostux.social)
- Bumper: [Canonical](https://canonical.com/)
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

fdDLNqbR7DYYQo8cLyry