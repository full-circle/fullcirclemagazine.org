---
title: "Full Circle Weekly News 155"
date: 2019-11-26
draft: false
tags:
  - "14-04"
  - "apache"
  - "browser"
  - "bug"
  - "canonical"
  - "client"
  - "edge"
  - "enterprise"
  - "fedora"
  - "foundation"
  - "freebsd"
  - "libarchive"
  - "livepatch"
  - "microsoft"
  - "netbsd"
  - "redhat"
  - "singa"
  - "sparky"
  - "sparkylinux"
  - "ubuntu"
  - "vulnerability"
cover: "covers/podcasts/fallback.webp"
mp3: http://dl.fullcirclemagazine.org/podcast/Full%20Circle%20Weekly%20News%20155.mp3
spotify: "https://open.spotify.com/show/6JhPBfSm6cLEhGSbYsGarP"
---

- [Red Hat Enterprise Linux 8.1 Officially Released](https://www.redhat.com/en/about/press-releases/red-hat-ups-iq-intelligent-operating-system-latest-release-red-hat-enterprise-linux-8)

- [SparkyLinux's November ISO Brings Debian "Bullseye" Updates](https://news.softpedia.com/news/sparkylinux-s-november-iso-brings-latest-debian-gnu-linux-11-bullseye-updates-528100.shtml)

- [Microsoft Edge Will be Available on Linux](https://itsfoss.com/microsoft-edge-linux/)

- [SINGA becomes top-level project of the Apache Software Foundation](https://blogs.apache.org/foundation/entry/the-apache-software-foundation-announces57)

- [Canonical Will Fully Support Ubuntu Linux on All Raspberry Pi Boards](https://ubuntu.com/blog/roadmap-for-official-support-for-the-raspberry-pi-4)

- [Canonical's Kernel Livepatch Ubuntu Advantage Client Is Out for Ubuntu 14.04 ESM](https://news.softpedia.com/news/canonical-s-kernel-livepatch-ubuntu-advantage-client-is-out-for-ubuntu-14-04-lts-528118.shtml)

- [Ubuntu Bug Reveals Your Media Files To Others Without Warning](https://fossbytes.com/ubuntu-bug-media-files-no-warning/)

- [Libarchive vulnerability can lead to code execution on Linux, FreeBSD, NetBSD](https://www.zdnet.com/article/libarchive-vulnerability-can-lead-to-code-execution-on-linux-freebsd-netbsd/)

**Credits**

- Ubuntu "Complete" sound: Canonical
- Theme Music: From The Dust - Stardust
- [https://soundcloud.com/ftdmusic](https://soundcloud.com/ftdmusic)
- [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
