---
title: "Full Circle Magazine 144"
date: 2019-04-26
draft: false
tags:
  - "birthday"
  - "bsd"
  - "certification"
  - "certified"
  - "editing"
  - "freeplane"
  - "ghostbsd"
  - "inkscape"
  - "kdenlive"
  - "libreoffice"
  - "netcat"
  - "python"
  - "video"
  - "wesnoth"
cover: covers/magazines/issue-144.webp
pdf: https://dl.fullcirclemagazine.org/issue144_en.pdf
epub: https://dl.fullcirclemagazine.org/issue144_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Freeplane, and Using Netcat
- Graphics : Inkscape
- My Opinion: Video Editing
- Linux Loopback: BSD
- Everyday Ubuntu: Writing An Article For FCM
- Book Review: Linux Command Line 2nd Ed.
- [**NEW!**] Linux Certified
- Ubuntu Games: Help Battle for Wesnoth

Plus: News, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue144_fr.pdf)
