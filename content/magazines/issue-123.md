---
title: "Full Circle Magazine 123"
date: 2017-07-28
draft: false
tags:
  - "arduino"
  - "eeepc"
  - "freecad"
  - "inkscape"
  - "kdenlive"
  - "latex"
  - "python"
  - "rising world"
  - "veracrypt"
cover: covers/magazines/issue-123.webp
pdf: https://dl.fullcirclemagazine.org/issue123_en.pdf
epub: https://dl.fullcirclemagazine.org/issue123_en.epub
---

**This month**

- Command & Conquer
- How-To : Python (Arduino), Intro To FreeCAD, and Convert to LaTeX
- Graphics : Inkscape & Kdenlive
- [**NEW!**] Researching With Linux
- Linux Labs: EEE PC Music Player
- Review: Veracrypt
- Ubuntu Games: Rising World

Plus: News, Q&A, My Desktop, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue123_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue123_hu.pdf)
