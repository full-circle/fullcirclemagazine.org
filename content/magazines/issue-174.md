---
title: "Full Circle Magazine 174"
date: 2021-10-29
draft: false
tags:
  - "clone"
  - "21.10"
  - "clone"
  - "comic"
  - "death trash"
  - "galaga"
  - "inkscape"
  - "laptop"
  - "latex"
  - "python"
  - "review"
  - "system 76"
  - "system76"
  - "ubuntu"
  - "waddle"
cover: covers/magazines/issue-174.webp
pdf: https://dl.fullcirclemagazine.org/issue174_en.pdf
epub: https://dl.fullcirclemagazine.org/issue174_en.epub
---

**This month**

- ~~Command & Conquer : LMMS~~
- How-To : Python, Latex and Clone To New PC
- Graphics : Inkscape
- Everyday Ubuntu
- Micro This Micro That
- Review : Ubuntu 21.10
- Review : System76 Galaga Pro Laptop
- Ubuntu Games : Death Trash

Plus: News, The Daily Waddle, Q&A, and more.
