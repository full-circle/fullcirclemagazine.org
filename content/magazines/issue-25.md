---
title: "Full Circle Magazine 25"
date: 2009-05-30
draft: false
tags:
  - "box"
  - "game"
  - "games"
  - "guillaume martes"
  - "history"
  - "increase"
  - "inkscape"
  - "interview"
  - "kubuntu"
  - "motu"
  - "office"
  - "open"
  - "openoffice"
  - "shell"
  - "speed"
  - "virtual"
  - "virtualbox"
cover: covers/magazines/issue-25.webp
pdf: https://dl.fullcirclemagazine.org/issue25_en.pdf
epub: https://dl.fullcirclemagazine.org/issue25_en.epub
---

**This month, we've got some good stuff for you. Coming your way is all the usual, including**

- Command and Conquer - Shell History.
- How To: Test Drive VirtualBox, Increase Game Speed In X, and Inkscape - Part 2.
- My Story - Why I Converted To Linux.
- My Opinion - First Experience Of Kubuntu.
- Book Review - Beginning OpenOffice 3, one copy up for grabs!
- MOTU Interview - Guillaume Martres.
- Top 5 - Games You've Never Heard Of.

Plus : all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue25_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue25_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue25_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue25_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue25_ru.pdf)
