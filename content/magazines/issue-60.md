---
title: "Full Circle Magazine 60"
date: 2012-04-26
draft: false
tags:
  - "12.04"
  - "format"
  - "gimp"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "news"
  - "office"
  - "port"
  - "prey"
  - "python"
  - "serial"
  - "settings"
  - "sound"
  - "svg"
  - "upgrade"
cover: covers/magazines/issue-60.webp
pdf: https://dl.fullcirclemagazine.org/issue60_en.pdf
epub: https://dl.fullcirclemagazine.org/issue60_en.epub
---

**This month**

- Command and Conquer.
- How-To : Beginning Python - Part 32, LibreOffice - Part 14, and Prey.
- [**NEW!**] Graphics : GIMP - The Beanstalk Part 1, and Inkscape - An Introduction to the SVG file format.
- Linux Lab - Serial Port.
- Review - Desura.
- I Think - Will You Upgrade To 12.04?
- Closing Windows - Sound Settings

Plus: Ubuntu Games, My Desktop, My Opinion, My Story, and much much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAownNvjAQ/full_circle_magazine_60_lite](http://www.google.com/producer/editions/CAownNvjAQ/full_circle_magazine_60_lite)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue60_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue60_hu.pdf)
- [Indonesian PDF](https://dl.fullcirclemagazine.org/issue60_id.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue60_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue60_it.pdf)
