---
title: "Full Circle Magazine 17"
date: 2008-09-26
draft: false
tags:
  - "c"
  - "convert"
  - "email"
  - "gimp"
  - "harald sitter"
  - "interview"
  - "irc"
  - "motu"
  - "nano"
  - "notifiers"
  - "pdf"
  - "scan"
  - "vim"
cover: covers/magazines/issue-17.webp
pdf: https://dl.fullcirclemagazine.org/issue17_en.pdf
epub: https://dl.fullcirclemagazine.org/issue17_en.epub
---

**This month**

- Command and Conquer - Nano & Vim.
- How-To : Program in C - Part 1, Connect to IRC, Using GIMP - Part 6 and Scan & Convert to PDF.
- My Story - ...When I Was Two
- My Opinion - Is This The Year?
- MOTU Interview - Harald Sitter
- Top 5 - Email Notifiers

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue17_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue17_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue17_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue17_it.pdf)
