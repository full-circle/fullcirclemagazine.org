---
title: "Full Circle Magazine 40"
date: 2010-08-29
draft: false
tags:
  - "adsl"
  - "loco"
  - "modem"
  - "motu"
  - "open"
  - "opensolaris"
  - "python"
  - "sofa"
  - "solaris"
  - "statistics"
  - "switch"
  - "virtualise"
  - "virtualize"
cover: covers/magazines/issue-40.webp
pdf: https://dl.fullcirclemagazine.org/issue40_en.pdf
epub: https://dl.fullcirclemagazine.org/issue40_en.epub
---

**This month we begin using the new Ubuntu font and a new FCM logo created by Thorsten Wilms!**

- Command and Conquer.
- How-To : Program in Python - Part 14, Virtualize Part 3 - OpenSolaris, and ADSL Modem As A Switch.
- Review - SOFA Statistics.
- Top 5 - Favourite Apps.

Plus: MOTU Interview, Ubuntu Games, My Opinion, My Story, and now with all new LoCo and Translation Team interviews!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue40_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue40_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue40_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue40_ru.pdf)
