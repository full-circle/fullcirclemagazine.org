---
title: "Full Circle Magazine 58"
date: 2012-02-24
draft: false
tags:
  - "backup"
  - "device"
  - "format"
  - "formatting"
  - "investigative"
  - "libre"
  - "libreoffice"
  - "mint"
  - "mobile"
  - "news"
  - "office"
  - "os"
  - "paperless"
  - "python"
  - "screencast"
  - "strategy"
  - "tools"
  - "usb"
cover: covers/magazines/issue-58.webp
pdf: https://dl.fullcirclemagazine.org/issue58_en.pdf
epub: https://dl.fullcirclemagazine.org/issue58_en.epub
---

**This month**

- Command and Conquer.
- How-To : Beginning Python - Part 30, LibreOffice - Part 12, Backup Strategy - Part 6, Screencast Your Desktop, and The Paperless Office.
- Linux Lab - Useful Investigative Tools.
- Review - Linux Mint 12
- I Think - Which Mobile OS.
- Closing Windows - Formatting a USB Device

Plus: more Ubuntu Games, My Desktop, My Opinion, My Story, and much much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAowgc1a/full_circle_magazine_58_lite](http://www.google.com/producer/editions/CAowgc1a/full_circle_magazine_58_lite)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue58_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue58_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue58_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue58_it.pdf)
