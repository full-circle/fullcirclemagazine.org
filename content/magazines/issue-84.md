---
title: "Full Circle Magazine 84"
date: 2014-04-25
draft: false
tags:
  - "apple"
  - "arduino"
  - "bitcoin"
  - "blender"
  - "crypto"
  - "currency"
  - "design"
  - "dogecoin"
  - "inkscape"
  - "mac"
  - "open"
  - "openvpn"
  - "prize"
  - "python"
  - "source"
  - "Ubuntu"
  - "vpn"
  - "win"
cover: covers/magazines/issue-84.webp
pdf: https://dl.fullcirclemagazine.org/issue84_en.pdf
epub: https://dl.fullcirclemagazine.org/issue84_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Establish An OpenVPN Connection, and Put Ubuntu On A Mac.
- Graphics : Blender and Inkscape.
- Review: Arduino Starter Kit
- Security Q&A
- What Is: CryptoCurrency
- [**NEW!**] Open Source Design

Plus: Q&A, Linux Labs, Ask The New Guy, Ubuntu Games, and another competition!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue84_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue84_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue84_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue84_it.pdf)
