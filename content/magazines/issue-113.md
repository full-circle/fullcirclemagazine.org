---
title: "Full Circle Magazine 113"
date: 2016-09-30
draft: false
tags:
  - "freepascal"
  - "inkscape"
  - "museum"
  - "network"
  - "networking"
  - "pascal"
  - "python"
  - "stardew valley"
  - "syd bolton"
cover: covers/magazines/issue-113.webp
pdf: https://dl.fullcirclemagazine.org/issue113_en.pdf
epub: https://dl.fullcirclemagazine.org/issue113_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Connecting Linux PCs, and Program With FreePascal
- Graphics : Inkscape
- Linux Labs: Syd Bolton's PC Museum
- Review: GNU Cash
- Ubuntu Games: Stardew Valley

Plus: News, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue113_fr.pdf)
