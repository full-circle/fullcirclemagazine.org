---
title: "Full Circle Magazine 2"
date: 2007-06-28
draft: false
tags:
  - "76"
  - "darter"
  - "gramps"
  - "grandma"
  - "granny"
  - "intel"
  - "interview"
  - "kubuntu"
  - "laptop"
  - "mac"
  - "mini"
  - "network"
  - "networking"
  - "private"
  - "scribus"
  - "system"
  - "system76"
  - "virtual"
  - "vpn"
cover: covers/magazines/issue-2.webp
pdf: https://dl.fullcirclemagazine.org/issue2_en.pdf
epub: https://dl.fullcirclemagazine.org/issue2_en.epub
---

**This issue includes**

- Flavour of the Month - Kubuntu
- How-To - Ubuntu on the Intel Mac Mini, Virtual Private Networking, Learning Scribus Part 2 and Ubuntu for your Grandma!
- Review - System 76 Darter
- Top 5 - Widgets
- MyDesktop, MyPC and more!

**Other languages**

- [Dutch PDF](https://dl.fullcirclemagazine.org/issue2_nl.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue2_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue2_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue2_it.pdf)
