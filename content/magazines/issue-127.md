---
title: "Full Circle Magazine 127"
date: 2017-11-24
draft: false
tags:
  - "base"
  - "fixmestick"
  - "freecad"
  - "gcb"
  - "great cow basic"
  - "greatcowbasic"
  - "humble bundle"
  - "inkscape"
  - "kde"
  - "kodi"
  - "plasma 4"
  - "plasma 5"
  - "Ubuntu"
cover: covers/magazines/issue-127.webp
pdf: https://dl.fullcirclemagazine.org/issue127_en.pdf
epub: https://dl.fullcirclemagazine.org/issue127_en.epub
---

**This month**

- Command & Conquer
- How-To : Ubuntu Base, Intro To FreeCAD, and Great Cow Basic [**NEW!**]
- Graphics : Inkscape
- Researching With Linux
- My Opinion: Plasma 5, or Plasma 4?
- KODI Room
- Review: FixMeStick
- Ubuntu Games: Humble Bundles

Plus: News, Q&A, and much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue127_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue127_hu.pdf)
