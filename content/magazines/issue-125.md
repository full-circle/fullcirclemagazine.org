---
title: "Full Circle Magazine 125"
date: 2017-09-29
draft: false
tags:
  - "budgie"
  - "freecad"
  - "hdr"
  - "inkscape"
  - "kodi"
  - "luminance"
  - "photography"
  - "tmux"
  - "trello"
cover: covers/magazines/issue-125.webp
pdf: https://dl.fullcirclemagazine.org/issue125_en.pdf
epub: https://dl.fullcirclemagazine.org/issue125_en.epub
---

**This month**

- Command & Conquer
- How-To : LuminanceHDR, Intro To FreeCAD, and tmux
- Graphics : Inkscape
- Researching With Linux
- My Opinion: Ubuntu Budgie
- KODI Room: Our KODI Setup Pt.2
- Review: Trello

Plus: News, Q&A, My Desktop, and much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue125_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue125_hu.pdf)
