---
title: "Full Circle Magazine 161"
date: 2020-09-25
draft: false
tags:
  - "audacity"
  - "inkscape"
  - "krita"
  - "netrunner"
  - "ort"
  - "photography"
  - "photos"
  - "podcast"
  - "python"
  - "rawtherapee"
cover: covers/magazines/issue-161.webp
pdf: https://dl.fullcirclemagazine.org/issue161_en.pdf
epub: https://dl.fullcirclemagazine.org/issue161_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Podcast Production, and Rawtherapee
- Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback
- Everyday Ubuntu
- Review : Netrunner
- Ubuntu Games : Fates of Ort

Plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue161_fr.pdf)
