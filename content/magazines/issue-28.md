---
title: "Full Circle Magazine 28"
date: 2009-08-30
draft: false
tags:
  - "allmyapps"
  - "apps"
  - "clients"
  - "interview"
  - "lamp"
  - "motu"
  - "networking"
  - "pytohn"
  - "server"
  - "sip"
  - "squid"
  - "sshfs"
  - "stephane graber"
  - "tellico"
cover: covers/magazines/issue-28.webp
pdf: https://dl.fullcirclemagazine.org/issue28_en.pdf
epub: https://dl.fullcirclemagazine.org/issue28_en.epub
---

**This month**

- Command and Conquer
- How-To : Program in Python - Part 2, LAMP Server - Part 1, Networking with SSHFSand Fast Internet With Squid.
- My Story - My Linux Experience I and II.
- My Opinion - AllMyApps
- Review - Tellico.
- MOTU Interview - Stephane Graber.
- Top 5 - SIP Clients.

Plus : Ubuntu Women Interview, Ubuntu Games and all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue28_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue28_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue28_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue28_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue28_ru.pdf)
