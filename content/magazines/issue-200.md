---
title: "Full Circle Magazine 200"
date: 2023-12-29
draft: false
tags:
  - "python"
  - "stable"
  - "diffusion"
  - "latex"
  - "inkscape"
  - "ubports"
  - "touch"
  - "kubuntu"
  - "lite"
  - "tiny life"
cover: "covers/magazines/issue-200.webp"
pdf: "https://dl.fullcirclemagazine.org/issue200_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue200_en.epub"
---

**Happy Holidays!**

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion and Latex
- Graphics : Inkscape
- Micro This Micro That
- Ubports Touch : OTA-3
- Review : Kubuntu 23.10 and Linux Lite 6.6
- Ubuntu Games - Tiny Life

plus: News, My Opinion, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue200\_fr.pdf)
