---
title: "Full Circle Magazine 209"
date: 2024-09-27
draft: false
tags:
  - "ubuntu"
cover: "covers/magazines/issue-209.webp"
pdf: "https://dl.fullcirclemagazine.org/issue209_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue209_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion and Latex
- Graphics : Inkscape
- Micro This Micro That
- Review : Ubuntu Cinnamon vs Mint Cinnamon, and Ubuntu MATE vs Mint MATE
- Book Review : Efficient Linux at the Command Line
- UbPorts Touch
- Ubuntu Games - Minami Lane

plus: News, My Opinion, Q&A, The Daily Waddle, and more.


**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue209\_fr.pdf)
