---
title: "Full Circle Magazine 64"
date: 2012-08-31
draft: false
tags:
  - "davmail"
  - "flirc"
  - "inkscape"
  - "kdenlive"
  - "lamp"
  - "libre"
  - "libreoffice"
  - "news"
  - "office"
  - "python"
  - "themes"
  - "tweet screen"
  - "wallpaper"
cover: covers/magazines/issue-64.webp
pdf: https://dl.fullcirclemagazine.org/issue64_en.pdf
epub: https://dl.fullcirclemagazine.org/issue64_en.epub
---

**This month**

- **Linux News - Security Alert!**
- Command and Conquer.
- How-To : Beginning Python - Part 36, LibreOffice Part 17, and DavMail.
- Graphics : Kdenlive Part 1, and Inkscape Part 4.
- Web Dev : LAMP
- Linux Lab - Tweet Screen The Finale.
- Review - Flirc.
- Closing Windows - Wallpaper and Themes.

Plus: Ask The New Guy, Ubuntu Games, My Desktop, My Opinion, My Story, and much much more!

**Google Currents Edition: ** [http://www.google.com/producer/editions/CAow5ITZAg/full_circle_magazine_64_lite](http://www.google.com/producer/editions/CAow5ITZAg/full_circle_magazine_64_lite "Full Circle #64")

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue64_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue64_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue64_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue64_it.pdf)
