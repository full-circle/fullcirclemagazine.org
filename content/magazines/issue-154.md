---
title: "Full Circle Magazine 154"
date: 2020-02-28
draft: false
tags:
  - "19.10"
  - "catmaze"
  - "darktable"
  - "disk"
  - "file"
  - "game"
  - "inkscape"
  - "jmp"
  - "krita"
  - "lubuntu"
  - "manager"
  - "photography"
  - "pim"
  - "python"
  - "usage"
cover: covers/magazines/issue-154.webp
pdf: https://dl.fullcirclemagazine.org/issue154_en.pdf
epub: https://dl.fullcirclemagazine.org/issue154_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Check Disk Usage, and Darktable
- Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback
- Everyday Ubuntu
- Review : Lubuntu 19.10, and Jump File Manager
- Ubuntu Games : Catmaze

Plus: News, Letters, My Story, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue154_fr.pdf)
