---
title: "Full Circle Magazine 78"
date: 2013-10-25
draft: false
tags:
  - "capture"
  - "gnome"
  - "install"
  - "kde"
  - "kubuntu"
  - "libre"
  - "libreoffice"
  - "Linux"
  - "Mir"
  - "office"
  - "pxe"
  - "python"
  - "screen"
  - "Ubuntu"
cover: covers/magazines/issue-78.webp
pdf: https://dl.fullcirclemagazine.org/issue78_en.pdf
epub: https://dl.fullcirclemagazine.org/issue78_en.epub
---

**This month**

- Ubuntu News
- Command & Conquer
- How-To : Python, LibreOffice, and Install Linux via PXE.
- Graphics : Blender, and Inkscape.
- Review: Google Music All Access

Plus: Q&A, Linux Labs, Ask The New Guy, My Story, and soooo much more!

Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue78_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue78_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue78_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue78_it.pdf)
