---
title: "Full Circle Magazine 176"
date: 2021-12-31
draft: false
tags:
  - "terminal"
  - "python"
  - "blender"
  - "libreoffice"
  - "writer"
  - "inkscape"
  - "micro"
  - "xencelabs"
  - "tablet"
  - "slipstream"

cover: "covers/magazines/issue-176.webp"
pdf: "https://dl.fullcirclemagazine.org/issue176_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue176_en.epub"
---

**This month:**

- Command & Conquer : Terminal
- How-To : Python, Blender [NEW!] and LibreOffice Writer Tips
- Graphics : Inkscape
- ~~Everyday Ubuntu~~
- Micro This Micro That
- Review : Lubuntu 21.10
- Review : Xencelabs Pen Tablet Medium Bundle
- Book Review : Object Orientated Python
- ~~Ubports Touch~~
- Ubuntu Games : Slipstream

plus: News, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue176_fr.pdf)
