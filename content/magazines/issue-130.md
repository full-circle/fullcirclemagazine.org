---
title: "Full Circle Magazine 130"
date: 2018-02-23
draft: false
tags:
  - "able2extract"
  - "basic"
  - "cow"
  - "freeplane"
  - "gcb"
  - "great"
  - "inkscape"
  - "latex"
  - "sphinx"
cover: covers/magazines/issue-130.webp
pdf: https://dl.fullcirclemagazine.org/issue130_en.pdf
epub: https://dl.fullcirclemagazine.org/issue130_en.epub
---

**This month**

- Command & Conquer
- How-To : Sphinx, Freeplane [**NEW!**], and Great Cow Basic
- Graphics : Inkscape
- Everyday Linux [**NEW!**]
- Researching With Linux
- My Opinion
- Review: Able2Extract 12

Plus: News, Q&A, and much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue130_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue130_hu.pdf)
