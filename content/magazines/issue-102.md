---
title: "Full Circle Magazine 102"
date: 2015-10-30
draft: false
tags:
  - "arduino"
  - "chrome"
  - "drives"
  - "erasing"
  - "games"
  - "hard"
  - "hdd"
  - "inkscape"
  - "install"
  - "minimal"
  - "multiboot"
  - "ota-7"
  - "ota7"
  - "python"
  - "streaming"
  - "touch"
  - "tron-club"
  - "tronclub"
  - "uefi"
  - "wiping"
cover: covers/magazines/issue-102.webp
pdf: https://dl.fullcirclemagazine.org/issue102_en.pdf
epub: https://dl.fullcirclemagazine.org/issue102_en.epub
---

**This month**

- Command & Conquer
- How-To : Python in the Real World, MultiBoot with UEFI, Website With Infrastructure and
- Ubuntu Minimal Install
- Graphics : Inkscape.
- Chrome Cult
- Linux Labs: Wiping Hard Drives
- Ubuntu Phones: OTA-7
- Ubuntu Games: Streaming Games

Plus: News, Arduino, Book Review, Q&A, Security, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue102_fr.pdf)
- [Spanish PDF](https://dl.fullcirclemagazine.org/issue102_es.pdf)
