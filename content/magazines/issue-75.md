---
title: "Full Circle Magazine 75"
date: 2013-07-27
draft: false
tags:
  - "command"
  - "conky"
  - "console"
  - "gimp"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "line"
  - "news"
  - "office"
  - "ouya"
  - "photographs"
  - "photos"
  - "python"
  - "restore"
  - "tasks"
cover: covers/magazines/issue-75.webp
pdf: https://dl.fullcirclemagazine.org/issue75_en.pdf
epub: https://dl.fullcirclemagazine.org/issue75_en.epub
---

**This month**

- Ubuntu News
- Command & Conquer
- How-To : Python, LibreOffice, and Tasks, Command Line & Conky.
- Graphics : Repairing Photos in GIMP, and Inkscape.
- Review: OUYA Console

Plus: Q&A, Linux Labs, Ask The New Guy, My Story, and soooo much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAownt21Bw/full_circle_magazine_75_lite](http://www.google.com/producer/editions/CAownt21Bw/full_circle_magazine_75_lite)Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue75_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue75_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue75_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue75_it.pdf)
