---
title: "Full Circle Magazine 181"
date: 2022-05-27
draft: false
tags:
  - "ubuntu"
  - "22.04"
  - "blender"
  - "darkside"
  - "detective"
  - "game"
  - "inkscape"
  - "kde"
  - "latex"
  - "linux"
  - "micro"
  - "puppy"
  - "python"
  - "science"
  - "slacko"
cover: "covers/magazines/issue-181.webp"
pdf: "https://dl.fullcirclemagazine.org/issue181_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue181_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Blender and Latex
- Graphics : Inkscape
- Everyday Ubuntu : KDE Science Pt.2
- Micro This Micro That
- Review : Ubuntu 22.04
- Review : Puppy Linux Slacko 7
- My Story : My Journey To Ubuntu 22.04
- ~~Ubports Touch~~
- Ubuntu Games : The Darkside Detective

plus: News, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue181_fr.pdf)
