---
title: "Full Circle Magazine 140"
date: 2018-12-27
draft: false
tags:
  - "games"
  - "gdpr"
  - "hypervisor"
  - "inkscape"
  - "milky"
  - "ota-6"
  - "ota6"
  - "python"
  - "retro"
  - "sendemail"
  - "survey"
  - "tracker"
  - "ubports"
  - "unvisible"
cover: covers/magazines/issue-140.webp
pdf: https://dl.fullcirclemagazine.org/issue140_en.pdf
epub: https://dl.fullcirclemagazine.org/issue140_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LXD Hypervisor, and Send Email using Sendemail
- Graphics : Inkscape
- Everyday Ubuntu: Retro Games
- My Opinion: GDPR Pt.2
- Review: MilkyTracker
- UBports Devices: OTA-6
- Ubuntu Games: Unvisible
- **The 2018 Survey Results**

Plus: News, Linux Loopback, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue140_fr.pdf)
