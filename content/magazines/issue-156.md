---
title: "Full Circle Magazine 156"
date: 2020-04-24
draft: false
tags:
  - "exapinks"
  - "fashion"
  - "game"
  - "gui"
  - "inkscape"
  - "krita"
  - "Linux"
  - "patterns"
  - "penguin"
  - "photograph"
  - "photography"
  - "photos"
  - "pim"
  - "python"
  - "rawtherapee"
  - "stacer"
  - "Ubuntu"
  - "valentina"
  - "waddle"
cover: covers/magazines/issue-156.webp
pdf: https://dl.fullcirclemagazine.org/issue156_en.pdf
epub: https://dl.fullcirclemagazine.org/issue156_en.epub
---

Being occupied with a family health issue, I completely forget to mention in this issue that it marks our 13th year!
**Happy 13th birthday everyone!**
**This month**

- Command & Conquer
- How-To : Python, Valentina, and Rawtherapee**[1]**
  - Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback
- Everyday Ubuntu : PIM
- Review : Stacer
- Ubuntu Games : Exapunks

Plus: News, My Story, The Daily Waddle, Q&A, and more.

**[1]** New link for downloading the Rawtherapee example image (now hosted here): [https://bit.ly/2WNVfPf](https://bit.ly/2WNVfPf)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue156_fr.pdf)
