---
title: "Full Circle Magazine 121"
date: 2017-05-26
draft: false
tags:
  - "arduino"
  - "editing"
  - "freecad"
  - "inkscape"
  - "kdenlive"
  - "Linux"
  - "mint"
  - "porteus"
  - "python"
  - "subsystem"
  - "video"
  - "warhammer"
  - "windows"
cover: covers/magazines/issue-121.webp
pdf: https://dl.fullcirclemagazine.org/issue121_en.pdf
epub: https://dl.fullcirclemagazine.org/issue121_en.epub
---

**This month**

- Command & Conquer
- How-To : Python (Arduino), Intro To FreeCAD Pt.1 and Installing Ubuntu with No External Media
- Graphics : Inkscape & Kdenlive
- ChromeCult
- Linux Labs: Windows Subsystem Linux
- Review: Porteus Kiosk
- Book Review: Mint 18
- Ubuntu Games: Total War: Warhammer

Plus: News, Q&A, My Desktop, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue121_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue121_hu.pdf)
