---
title: "Full Circle Magazine 12"
date: 2008-04-25
draft: false
tags:
  - "backup"
  - "create"
  - "creating"
  - "deb"
  - "disk"
  - "distributing"
  - "fcm"
  - "files"
  - "gimp"
  - "history"
  - "kubuntu"
  - "partimage"
  - "server"
  - "stick"
  - "usage"
  - "usb"
  - "wiki"
cover: covers/magazines/issue-12.webp
pdf: https://dl.fullcirclemagazine.org/issue12_en.pdf
epub: https://dl.fullcirclemagazine.org/issue12_en.epub
---

**This month**

- Flavor of the Year\*\* - How Ubuntu and Kubuntu have matured in twelve months.
- How-To - Creating & Distributing Deb files, Ubuntu Disk Usage, Backup with Partimage, Using GIMP Part 1 and Create Your Own Server Part 4
- Review of [Wiki on a Stick](http://sourceforge.net/projects/stickwiki/ "project page").
- Letters, Q & A, My Desktop, Top 5, and more!

Plus : a a bit on the history of Full Circle.

**Other languages**

- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue12_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue12_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue12_it.pdf)
