---
title: "Full Circle Magazine 163"
date: 2020-11-27
draft: false
tags:
  - "20.10"
  - "400"
  - "freeorion"
  - "game"
  - "inkscape"
  - "kit"
  - "micropad"
  - "nmap"
  - "notes"
  - "notetaking"
  - "pi"
  - "podcast"
  - "podcasting"
  - "python"
  - "raspberry"
  - "rhythmbox"
  - "xubuntu"
cover: covers/magazines/issue-163.webp
pdf: https://dl.fullcirclemagazine.org/issue163_en.pdf
epub: https://dl.fullcirclemagazine.org/issue163_en.epub
---

**This month**

- Command & Conquer : Nmap
- How-To : Python, Podcast Production, and Micropad
- Graphics :Inkscape
- Linux Loopback
- Everyday Ubuntu
- Review : Xubuntu 20.10 and Raspberry Pi 400 Kit
- Ubuntu Games : FreeOrion

Plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue163_fr.pdf)
