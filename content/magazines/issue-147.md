---
title: "Full Circle Magazine 147"
date: 2019-07-27
draft: false
tags:
  - "basilisk"
  - "chess"
  - "darktable"
  - "freeplane"
  - "inkscape"
  - "lubuntu"
  - "python"
  - "xubuntu"
cover: covers/magazines/issue-147.webp
pdf: https://dl.fullcirclemagazine.org/issue147_en.pdf
epub: https://dl.fullcirclemagazine.org/issue147_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Freeplane, and Darktable
- Graphics : Inkscape
- My Opinion: Lubuntu Changes
- Linux Loopback
- Everyday Ubuntu: Retrogaming
- Review: Basilisk Browser
- Review: Xubuntu 19.04
- Linux Certified
- Ubuntu Games: Auto-Chess Pt2

Plus: News, The Daily Waddle, Q&A, Crossword and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue147_fr.pdf)
