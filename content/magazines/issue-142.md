---
title: "Full Circle Magazine 142"
date: 2019-02-22
draft: false
tags:
  - "alsa"
  - "assembly"
  - "darktable"
  - "freeplane"
  - "games"
  - "gaming"
  - "hatari"
  - "inkscape"
  - "Linux"
  - "python"
  - "retro"
  - "Ubuntu"
cover: covers/magazines/issue-142.webp
pdf: https://dl.fullcirclemagazine.org/issue142_en.pdf
epub: https://dl.fullcirclemagazine.org/issue142_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Freeplane, and Darktable
- Graphics : Inkscape
- Everyday Ubuntu: Retro Games
- My Opinion: Linux Devices
- My Story: ALSA Settings
- Book Review: Assemble Language
- Ubuntu Games: Hatari Pt.2

Plus: News, Linux Loopback, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue142_fr.pdf)
