---
title: "Full Circle Magazine 172"
date: 2021-08-27
draft: false
tags:
  - "don't forget me"
  - "gaming"
  - "inkscape"
  - "lmms"
  - "micro"
  - "micropython"
  - "news"
  - "pi"
  - "print"
  - "python"
  - "retro"
  - "retrogaming"
  - "server"
  - "terminal"
  - "ubuntu"
  - "unity"
  - "waddle"
cover: covers/magazines/issue-172.webp
pdf: https://dl.fullcirclemagazine.org/issue172_en.pdf
epub: https://dl.fullcirclemagazine.org/issue172_en.epub
---

**This month**

- Command & Conquer : LMMS
- How-To : Python, Eternal Terminal and Making A Network Print Server
- Graphics : Inkscape
- Everyday Ubuntu : Retrogaming Revised
- Micro This Micro That
- Review : Ubuntu Unity 21.04
- Ubuntu Games : Don't Forget Me

Plus: News, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue172_fr.pdf)
