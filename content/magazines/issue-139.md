---
title: "Full Circle Magazine 139"
date: 2018-11-30
draft: false
tags:
  - "appimage"
  - "basic"
  - "cow"
  - "freeplane"
  - "game"
  - "gcb"
  - "graveyeard keeper"
  - "great"
  - "inkscape"
  - "Linux"
  - "philosophy"
  - "portable"
  - "sysadmin"
cover: covers/magazines/issue-139.webp
pdf: https://dl.fullcirclemagazine.org/issue139_en.pdf
epub: https://dl.fullcirclemagazine.org/issue139_en.epub
---

**This month**

- Command & Conquer
- How-To : Portable Apps with AppImage, Freeplane, and Great Cow BASIC
- Graphics : Inkscape
- My Opinion: GDPR Pt.1
- Book Review: Linux Philosophy for SysAdmins
- Ubuntu Games: Graveyard Keeper

Plus: News, The Daily Waddle [**NEW!**], Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue139_fr.pdf)
