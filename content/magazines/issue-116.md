---
title: "Full Circle Magazine 116"
date: 2016-12-30
draft: false
tags:
  - "alert"
  - "android"
  - "arduino"
  - "box"
  - "command"
  - "conquer"
  - "docs"
  - "freepascal"
  - "inkscape"
  - "kodi"
  - "latex"
  - "minitube"
  - "openra"
  - "pascal"
  - "python"
  - "red"
  - "tv"
  - "zim"
  - "zoho"
  - "zoomtak"
cover: covers/magazines/issue-116.webp
pdf: https://dl.fullcirclemagazine.org/issue116_en.pdf
epub: https://dl.fullcirclemagazine.org/issue116_en.epub
---

**This month**

- Command & Conquer
- How-To : Python (Arduino), LaTeX With Zim, and Program With FreePascal
- Graphics : Inkscape
- ChromeCult: Zoho Docs
- Linux Labs: Zoomtak T8H V8 Android Box
- [**NEW!**] KODI Room: Tips & Tricks
- Review: miniTube
- Ubuntu Games: OpenRA

Plus: News, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue116_fr.pdf)
