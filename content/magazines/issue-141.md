---
title: "Full Circle Magazine 141"
date: 2019-01-25
draft: false
tags:
  - "5c"
  - "course"
  - "darktable"
  - "Debian"
  - "games"
  - "hatari"
  - "honor"
  - "inkscape"
  - "ota-7"
  - "ota7"
  - "photography"
  - "python"
  - "retro"
  - "ruby"
  - "touch"
  - "ubports"
  - "Ubuntu"
cover: covers/magazines/issue-141.webp
pdf: https://dl.fullcirclemagazine.org/issue141_en.pdf
epub: https://dl.fullcirclemagazine.org/issue141_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Ubuntu on an Honor 5C, and Darktable for Photographers
- Graphics : Inkscape
- Everyday Ubuntu: Retro Games
- Course Review: Ruby
- UBports Devices: OTA-7
- Ubuntu Games: Hatari

Plus: News, Linux Loopback, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue141_fr.pdf)
