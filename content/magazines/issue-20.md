---
title: "Full Circle Magazine 20"
date: 2008-12-29
draft: false
tags:
  - "andrea colangelo"
  - "backup"
  - "book"
  - "c"
  - "dev"
  - "development"
  - "foss"
  - "interview"
  - "kung fu"
  - "motu"
  - "music"
  - "review"
  - "sync"
  - "terminal"
  - "web"
cover: covers/magazines/issue-20.webp
pdf: https://dl.fullcirclemagazine.org/issue20_en.pdf
epub: https://dl.fullcirclemagazine.org/issue20_en.epub
---

**This month**

- Command and Conquer - The Daunting Terminal.
- How-To : Program in C - Part 4, Web Development - Part 1, Backup & Sync Your Music.
- My Story - Making Money With FOSS
- Book Review - Ubuntu Kung Fu
- My Opinion - Italy Speaks OSS
- MOTU Interview - Andrea Colangelo
- Top 5 - Backup Solutions

**Don't forget to take our reader survey** http://url.fullcirclemagazine.org/e78bdf
It'll help us to improve FCM

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue20_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue20_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue20_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue20_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue20_ru.pdf)
