---
title: "Full Circle Magazine 164"
date: 2020-12-25
draft: false
tags:
  - "20.10"
  - "baking"
  - "cookery"
  - "cookies"
  - "cooking"
  - "erase"
  - "gmm"
  - "goggles"
  - "inkscape"
  - "kookbook"
  - "kubuntu"
  - "lubuntu"
  - "manager"
  - "music"
  - "nmap"
  - "nwipe"
  - "ota-15"
  - "ota15"
  - "podcast"
  - "podcasting"
  - "python"
  - "review"
  - "touch"
  - "ubports"
  - "wipe"
cover: covers/magazines/issue-164.webp
pdf: https://dl.fullcirclemagazine.org/issue164_en.pdf
epub: https://dl.fullcirclemagazine.org/issue164_en.epub
---

**This month**

- Command & Conquer : Nmap
- How-To : Python, Podcast Production, and Erase With nwipe
- Graphics : Inkscape
- Linux Loopback
- Everyday Ubuntu : Kookbook
- Ubports Touch : OTA-15
- Review : Kubuntu 20.10 and Lubuntu 20.10
- Review & Interview : Goggles Music Manager
- Ubuntu Games plus: News, My Opinion, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue164_fr.pdf)
