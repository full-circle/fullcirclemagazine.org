---
title: "Full Circle Magazine 186"
date: 2022-10-28
draft: false
tags:
  - "python"
  - "blender"
  - "latex"
  - "inkscape"
  - "morrowind"
  - "micro"
  - "ubuntu budgie"
  - "ubuntu"
  - "budgie"
  - "nixos"
  - "tabletop"
  - "waddle"
cover: "covers/magazines/issue-186.webp"
pdf: "https://dl.fullcirclemagazine.org/issue186_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue186_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Blender and Latex
- Graphics : Inkscape
- Everyday Ubuntu : Morrowind
- Micro This Micro That
- Review : Ubuntu Budgie 22.04
- Review : NixOS
- Book Review : Dead Simple Python
- Tabletop Ubuntu [NEW!] : Eight Minute Empire
  plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue186_fr.pdf)
