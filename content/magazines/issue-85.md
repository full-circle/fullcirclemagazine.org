---
title: "Full Circle Magazine 85"
date: 2014-05-30
draft: false
tags:
  - "14.04"
  - "arduino"
  - "blender"
  - "boot"
  - "grub"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "loader"
  - "office"
  - "open"
  - "python"
  - "security"
  - "source"
  - "Ubuntu"
cover: covers/magazines/issue-85.webp
pdf: https://dl.fullcirclemagazine.org/issue85_en.pdf
epub: https://dl.fullcirclemagazine.org/issue85_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, and GRUB2 Pt.1.
- Graphics : Blender and Inkscape.
- Review: Ubuntu 14.04
- Security Q&A
- What Is: CryptoCurrency
- Open Source Design
- [**NEW!**] Arduino

Plus: Q&A, Linux Labs, Ask The New Guy, Ubuntu Games, and a competition to win a Humble Bundle!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue85_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue85_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue85_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue85_it.pdf)
