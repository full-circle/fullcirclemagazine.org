---
title: "Full Circle Magazine 119"
date: 2017-03-31
draft: false
tags:
  - "32-bit"
  - "accounting"
  - "accounts"
  - "arduino"
  - "chrome"
  - "hitman"
  - "inkscape"
  - "kdenlive"
  - "kodi"
  - "latex"
  - "nolapro"
  - "pi"
  - "python"
  - "touch"
  - "zim"
cover: covers/magazines/issue-119.webp
pdf: https://dl.fullcirclemagazine.org/issue119_en.pdf
epub: https://dl.fullcirclemagazine.org/issue119_en.epub
---

**This month**

- Command & Conquer
- How-To : Python (Arduino), LaTeX With Zim, and Program For Ubuntu Touch
- Graphics : Inkscape & Kdenlive
- ChromeCult: No Options
- Linux Labs: Should Linux Drop 32-bit Support?
- Review: NolaPro Accounting
- Ubuntu Games: Hitman: Complete First Season

Plus: News, Q&A, Kodi, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue119_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue119_hu.pdf)
