---
title: "Full Circle Magazine 109"
date: 2016-05-27
draft: false
tags:
  - "aquaris"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "ltsp"
  - "m10"
  - "mate"
  - "netbook"
  - "office"
  - "python"
  - "skullgirls"
  - "tablet"
cover: covers/magazines/issue-109.webp
pdf: https://dl.fullcirclemagazine.org/issue109_en.pdf
epub: https://dl.fullcirclemagazine.org/issue109_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, Migrating From VAX, and The Basics of Audacity
- Graphics : Inkscape
- Linux Labs: Netbook Revival and Small LTSP Installation
- Review: BQ Aquaris M10 Ubuntu Tablet
- Book Review: Python Crash Course
- Ubuntu Games: Skullgirls

Plus: News, Arduino, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue109_fr.pdf)
