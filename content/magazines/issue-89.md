---
title: "Full Circle Magazine 89"
date: 2014-09-26
draft: false
tags:
  - "arduino"
  - "clone"
  - "dmc4che"
  - "gimp"
  - "inkscape"
  - "kernel"
  - "kodi"
  - "libreoffice"
  - "Linux"
  - "oracle"
  - "perspective"
  - "PET"
  - "scanner"
  - "xbmc"
cover: covers/magazines/issue-89.webp
pdf: https://dl.fullcirclemagazine.org/issue89_en.pdf
epub: https://dl.fullcirclemagazine.org/issue89_en.epub
---

**This month**

- Command & Conquer
- How-To : Install Oracle, LibreOffice, and dmc4che.
- Graphics : GIMP Perspective Clone Tool and Inkscape.
- Linux Labs: Kodi/XBMC, and Compiling a Kernel Pt.2
- Arduino

Plus: News, Q&A, Ubuntu Games, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue89_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue89_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue89_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue89_it.pdf)
