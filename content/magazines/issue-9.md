---
title: "Full Circle Magazine 9"
date: 2008-01-25
draft: false
tags:
  - "4.0"
  - "create"
  - "deluxe"
  - "directory"
  - "ibook"
  - "kde"
  - "kde4"
  - "open"
  - "opengeu"
  - "openttd"
  - "server"
  - "transport"
  - "ttd"
  - "tycoon"
cover: covers/magazines/issue-9.webp
pdf: https://dl.fullcirclemagazine.org/issue9_en.pdf
epub: https://dl.fullcirclemagazine.org/issue9_en.epub
---

**This month**

- OpenGEU - A step-by-step install from OpenGEU creator Luca De Marini.
- How-tos: Directory Server, Ubuntu on an iBook, Installing OpenTTD (Transport Tycoon Deluxe), and part one of the new "Create Your Own Server" series.
- The new "Create Your Own Server" series (mentioned above).
- Review of the KDE 4.0 release
- Letters, the Q&A, My Desktop, the Ubuntu Women column, the Top 5, and much, much more!

**Other languages**

- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue9_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue9_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue9_it.pdf)
