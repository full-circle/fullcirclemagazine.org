---
title: "Full Circle Magazine 180"
date: 2022-04-29
draft: false
tags:
  - "22.04"
  - "blender"
  - "cutefish"
  - "cutefishos"
  - "freeoffice"
  - "growbot"
  - "inkscape"
  - "kde"
  - "latex"
  - "office"
  - "python"
  - "science"
  - "ubuntu"
cover: "covers/magazines/issue-180.webp"
pdf: "https://dl.fullcirclemagazine.org/issue180_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue180_en.epub"
---

**This month:**

- ~~Command & Conquer~~
- How-To : Python, Blender and Latex
- Graphics : Inkscape
- Everyday Ubuntu : KDE Science
- ~~Micro This Micro That~~
- Review : CutefishOS
- Review : FreeOffice 2021
- My Opinion : First Look At Ubuntu 22.04
- ~~Ubports Touch~~
- Ubuntu Games : Growbot

plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue180_fr.pdf)
