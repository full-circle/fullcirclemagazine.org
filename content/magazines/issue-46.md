---
title: "Full Circle Magazine 46"
date: 2011-02-25
draft: false
tags:
  - "apps"
  - "boxee"
  - "guitar"
  - "libre"
  - "libreoffice"
  - "mythbuntu"
  - "office"
  - "python"
  - "tools"
cover: covers/magazines/issue-46.webp
pdf: https://dl.fullcirclemagazine.org/issue46_en.pdf
epub: https://dl.fullcirclemagazine.org/issue46_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 20, NEW SERIES! LibreOffice - Part 1 and Installing Mythbuntu.
- Linux Lab - File Formats Part One of Two.
- Review - Boxee Box.
- Top 5 - Guitar Apps.

Plus: Ubuntu Women, Ubuntu Games, My Opinion, My Story, and much much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue46_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue46_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue46_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue46_it.pdf)
