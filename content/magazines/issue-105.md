---
title: "Full Circle Magazine 105"
date: 2016-01-29
draft: false
tags:
  - "3d"
  - "arduino"
  - "chrome"
  - "cloud"
  - "dirt showdown"
  - "distro"
  - "gnucash"
  - "inkscape"
  - "latex"
  - "libre"
  - "libreoffice"
  - "migrating"
  - "mygica"
  - "office"
  - "ota-9"
  - "ota9"
  - "printer"
  - "python"
  - "valentines"
  - "vax"
cover: covers/magazines/issue-105.webp
pdf: https://dl.fullcirclemagazine.org/issue105_en.pdf
epub: https://dl.fullcirclemagazine.org/issue105_en.epub
---

**This month**

- Command & Conquer
- How-To : Python in the Real World, LibreOffice, Migrating From VAX and Using GnuCash
- Graphics : Inkscape and Creating A Valentines Card In LaTeX.
- Chrome Cult: Cloud Distros
- Linux Labs: Building A 3D Printer
- Ubuntu Phones: OTA-9
- Review: MyGica ATV582 TV Box
- Book Review: Python Playground
- Ubuntu Games: DIRT Showdown

Plus: News, Arduino, Q&A, Security, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue105_fr.pdf)
