---
title: "Full Circle Magazine 82"
date: 2014-02-28
draft: false
tags:
  - "apple"
  - "bitcoin"
  - "blender"
  - "cash"
  - "crypto"
  - "cryptocurrency"
  - "currency"
  - "device"
  - "digital"
  - "dogecoin"
  - "inkscape"
  - "ios"
  - "libreoffice"
  - "litecoin"
  - "money"
  - "python"
  - "QA"
  - "security"
cover: covers/magazines/issue-82.webp
pdf: https://dl.fullcirclemagazine.org/issue82_en.pdf
epub: https://dl.fullcirclemagazine.org/issue82_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, and Connecting iOS Devices
- Graphics : Blender and Inkscape
- Review: NOD32 Anti-virus
- [**NEW!**] - Security Q&A
- [**NEW!**] - What Is: CryptoCurrency

Plus: Q&A, Linux Labs, Ask The New Guy, Ubuntu Games, and even some competitions!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue82_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue82_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue82_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue82_it.pdf)
