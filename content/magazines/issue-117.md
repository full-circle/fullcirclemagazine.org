---
title: "Full Circle Magazine 117"
date: 2017-01-27
draft: false
tags:
  - "arduino"
  - "book"
  - "dictating"
  - "dictation"
  - "dropbox"
  - "eee pc"
  - "freepascal"
  - "kodi"
  - "life is strange"
  - "paper"
  - "pascal"
  - "python"
  - "recognition"
  - "snappy"
  - "speech"
cover: covers/magazines/issue-117.webp
pdf: https://dl.fullcirclemagazine.org/issue117_en.pdf
epub: https://dl.fullcirclemagazine.org/issue117_en.epub
---

**This month**

- Command & Conquer
- How-To : Python (Arduino), Snappy on an EEE PC, and Program With FreePascal
- Graphics : Inkscape
- [**NEW!**] Kdenlive
- ChromeCult: Dropbox Paper
- Linux Labs: Dictating a Novel
- KODI Room
- Book Review: Invent Your Own Computer Games With Python
- Ubuntu Games: Life Is Strange

Plus: News, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue117_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue117_hu.pdf)
