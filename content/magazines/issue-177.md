---
title: "Full Circle Magazine 177"
date: 2022-01-28
draft: false
tags:
  - "21.10"
  - "blender"
  - "drox"
  - "inkscape"
  - "micro"
  - "python"
  - "terminal"
  - "touch"
  - "ubports"
  - "ubuntu"
  - "xubuntu"
cover: "covers/magazines/issue-177.webp"
pdf: "https://dl.fullcirclemagazine.org/issue177_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue177_en.epub"
---

**This month:**

- Command & Conquer : Terminal
- How-To : Python, Blender and A Plea For Articles
- Graphics : Inkscape
- ~~Everyday Ubuntu~~
- Micro This Micro That
- Review : Xubuntu 21.10
- My Opinion : The Origins Of The GUI [NEW!]
- Ubports Touch
- Ubuntu Games : Drox Operative

plus: News, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue177_fr.pdf)
