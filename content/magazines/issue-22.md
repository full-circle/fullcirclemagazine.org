---
title: "Full Circle Magazine 22"
date: 2009-02-28
draft: false
tags:
  - "book"
  - "c"
  - "dvd"
  - "eee"
  - "emanuele gentili"
  - "ffmpeg"
  - "images"
  - "interview"
  - "motu"
  - "non-geeks"
  - "pc"
  - "resize"
  - "resizing"
  - "review"
  - "rippers"
  - "ripping"
cover: covers/magazines/issue-22.webp
pdf: https://dl.fullcirclemagazine.org/issue22_en.pdf
epub: https://dl.fullcirclemagazine.org/issue22_en.epub
---

**This month**

- Command and Conquer - Resizing Images With FFMPEG.
- How-To : Program in C - Part 6, Web Development - Part 3,
- Installing CrunchEEE To The EEE PC, and Spreading Ubuntu.
- My Story - Making The Switch
- Book Review - Ubuntu For Non-Geeks 3rd Edition
- MOTU Interview - Emanuele Gentili
- Top 5 - DVD Rippers

Plus : all the usual goodness...

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue22_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue22_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue22_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue22_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue22_ru.pdf)
