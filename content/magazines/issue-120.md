---
title: "Full Circle Magazine 120"
date: 2017-04-28
draft: false
tags:
  - "arduino"
  - "firefox"
  - "freecad"
  - "geeky"
  - "gui"
  - "inkscape"
  - "kdenlive"
  - "lxle"
  - "pi"
  - "projects"
  - "python"
  - "scratch"
  - "steam"
  - "survey"
cover: covers/magazines/issue-120.webp
pdf: https://dl.fullcirclemagazine.org/issue120_en.pdf
epub: https://dl.fullcirclemagazine.org/issue120_en.epub
---

**This issue marks TEN YEARS of Full Circle Magazine.**
**This month**

- Command & Conquer
- How-To : Python (Arduino), A Week Without a GUI, and [**NEW!**] Intro To FreeCAD Pt.1
- Graphics : Inkscape & Kdenlive
- ChromeCult: Firefox Add-ons
- Linux Labs: Old Hardware Benchmarks & Linux From Scratch
- Review: LXLE
- Book Review: Geeky Projects For The Experienced Maker
- Ubuntu Games: Steam Hardware Survey
- The return of **My Desktop**
- Survey Results

Plus: News, Q&A, Kodi, and soooo much more in **90** pages.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue120_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue120_hu.pdf)
