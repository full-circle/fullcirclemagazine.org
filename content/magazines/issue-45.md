---
title: "Full Circle Magazine 45"
date: 2011-01-27
draft: false
tags:
  - "annotation"
  - "debian"
  - "kde"
  - "multiboot"
  - "music"
  - "python"
  - "ubuntu"
  - "virtualization"
cover: covers/magazines/issue-45.webp
pdf: https://dl.fullcirclemagazine.org/issue45_en.pdf
epub: https://dl.fullcirclemagazine.org/issue45_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 19, Virtualization : Debian Xen Part 2 and Installing Ubuntu with m23.
- Linux Lab - Creating a Multiboot USB stick.
- Review - KDE 4.5.
- Top 5 - Music Annotation Apps.

Plus: Ubuntu Women, Ubuntu Games, My Opinion, My Story, and much much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue45_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue45_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue45_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue45_it.pdf)
