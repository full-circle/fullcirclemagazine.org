---
title: "Full Circle Magazine 132"
date: 2018-04-27
draft: false
tags:
  - "dwarf fortress"
  - "freeplane"
  - "inkscape"
  - "python"
  - "touch"
  - "ubports"
cover: covers/magazines/issue-132.webp
pdf: https://dl.fullcirclemagazine.org/issue132_en.pdf
epub: https://dl.fullcirclemagazine.org/issue132_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Freeplane, and Ubuntu Touch
- Graphics : Inkscape
- Everyday Linux
- Researching With Linux
- My Opinion
- My Story
- Book Review: Cracking Codes With Python
- Ubuntu Games: Dwarf Fortress

Plus: News, Q&A, and much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue132_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue132_hu.pdf)
