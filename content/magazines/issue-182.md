---
title: "Full Circle Magazine 182"
date: 2022-06-24
draft: false
tags:
  - "22.04"
  - "blender"
  - "calibre"
  - "catie"
  - "ebook"
  - "esp32"
  - "fedora"
  - "inkscape"
  - "kubuntu"
  - "latex"
  - "meowmeowland"
  - "micro"
  - "python"
  - "readers"
  - "software"
  - "waddle"
  - "ubuntu"
cover: "covers/magazines/issue-182.webp"
pdf: "https://dl.fullcirclemagazine.org/issue182_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue182_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Blender and Latex
- Graphics : Inkscape
- Everyday Ubuntu : Ubuntu Software Centre
- Micro This Micro That
- Review : Kubuntu 22.04
- Review : Fedora 35
- Review : Ebook Readers
- My Story : Calibre
- Ubports Touch
- Ubuntu Games : Catie in Meowmeowland

plus: News, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue182_fr.pdf)
