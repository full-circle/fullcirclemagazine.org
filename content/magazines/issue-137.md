---
title: "Full Circle Magazine 137"
date: 2018-09-28
draft: false
tags:
  - "action"
  - "book"
  - "deco"
  - "dnscrypt"
  - "freeplane"
  - "gaming"
  - "graphics"
  - "inkscape"
  - "Linux"
  - "python"
  - "retro"
  - "steam"
  - "tabet"
  - "xp-pen"
cover: covers/magazines/issue-137.webp
pdf: https://dl.fullcirclemagazine.org/issue137_en.pdf
epub: https://dl.fullcirclemagazine.org/issue137_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Freeplane, and DNScrypt
- Graphics : Inkscape
- Everyday Ubuntu: Retro Gaming Pt.1
- My Story - Book Review: Linux In Action
- Review: XP-Pen Deco 2 graphics tablet
- Ubuntu Games: Steam Play

Plus: News, Q&A, and much more.

**Other languages**
