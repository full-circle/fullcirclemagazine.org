---
title: "Full Circle Magazine 178"
date: 2022-02-25
draft: false
tags:
  - "ubuntu"
  - "21.10"
  - "blender"
  - "distro"
  - "gui"
  - "inkscape"
  - "micro"
  - "pingus"
  - "python"
  - "quickemu"
  - "review"
  - "terminal"
  - "touch"
  - "ubports"
  - "unity"
  - "waddle"
cover: "covers/magazines/issue-178.webp"
pdf: "https://dl.fullcirclemagazine.org/issue178_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue178_en.epub"
---

**This month:**

- Command & Conquer : Terminal
- How-To : Python, Blender and Quickemu
- Graphics : Inkscape
- Everyday Ubuntu
- Micro This Micro That
- Review : Ubuntu Unity 21.10
- My Opinion : The Origins Of The GUI
- Ubports Touch
- Ubuntu Games : Making Levels For Pingus

plus: News, Letters, My Story, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue178_fr.pdf)
