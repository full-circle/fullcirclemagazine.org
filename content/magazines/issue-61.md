---
title: "Full Circle Magazine 61"
date: 2012-05-25
draft: false
tags:
  - "add"
  - "apps"
  - "cc"
  - "command"
  - "dvd"
  - "encoding"
  - "gimp"
  - "inkscape"
  - "kickstarter"
  - "libre"
  - "music"
  - "newbies"
  - "news"
  - "noob"
  - "noobs"
  - "office"
  - "python"
  - "remove"
  - "repos"
  - "repositories"
  - "rip"
  - "ripping"
  - "sites"
  - "top"
cover: covers/magazines/issue-61.webp
pdf: https://dl.fullcirclemagazine.org/issue61_en.pdf
epub: https://dl.fullcirclemagazine.org/issue61_en.epub
---

**This month**

- Command and Conquer.
- How-To : Beginning Python - Part 33, Use The TOP Command, and VirtualBox Networking.
- Graphics : GIMP - The Beanstalk Part 2, and Inkscape Part 1.
- Linux Lab - DVD-Ripping and Encoding.
- Book Review & Competition - Linux For Newbies.
- I Think - Have You Helped a Kickstarter?
- Closing Windows - Add/Remove Software and Repos.
- [**NEW!**] Audio Flux - CC music, sites and apps.

Plus: Ubuntu Games, My Desktop, My Opinion, My Story, and much much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAowlYCXCQ/full_circle_magazine_61_lite](http://www.google.com/producer/editions/CAowlYCXCQ/full_circle_magazine_61_lite)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue61_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue61_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue61_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue61_it.pdf)
