---
title: "Full Circle Magazine 146"
date: 2019-06-28
draft: false
tags:
  - "chess"
  - "darktable"
  - "freeplane"
  - "help"
  - "inkscape"
  - "lubuntu"
  - "python"
  - "Ubuntu"
cover: covers/magazines/issue-146.webp
pdf: https://dl.fullcirclemagazine.org/issue146_en.pdf
epub: https://dl.fullcirclemagazine.org/issue146_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Freeplane, and Darktable pt4
- Graphics : Inkscape
- My Story: Installing Ubuntu
- Linux Loopback
- Everyday Ubuntu: Getting Help
- Book Review: Math Adventures With Python
- Review: Lubuntu 19.04
- Linux Certified
- Ubuntu Games: Auto-Chess Pt1

Plus: News, The Daily Waddle, Q&A, Crossword and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue146_fr.pdf)
