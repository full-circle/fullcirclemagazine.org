---
title: "Full Circle Magazine 193"
date: 2023-05-26
draft: false
tags:
  - "python"
  - "latex"
  - "stable"
  - "diffusion"
  - "inkscape"
  - "freecad"
  - "ubuntu"
  - "blendos"
  - "game"
cover: "covers/magazines/issue-193.webp"
pdf: "https://dl.fullcirclemagazine.org/issue193_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue193_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion and Latex
- Graphics : Inkscape
- Graphics : FreeCAD
- Review : Ubuntu 23.04
- Review : blendOS
- Book Review : Python Crash Course
- Ubuntu Games : Deep Sky Derelicts

plus: News, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue193_fr.pdf)
