---
title: "Full Circle Magazine 62"
date: 2012-06-29
draft: false
tags:
  - "cc"
  - "equivalents"
  - "gimp"
  - "inkscape"
  - "installing"
  - "libre"
  - "libreoffice"
  - "music"
  - "news"
  - "office"
  - "python"
  - "software"
  - "tracks"
  - "tweet screen"
cover: covers/magazines/issue-62.webp
pdf: https://dl.fullcirclemagazine.org/issue62_en.pdf
epub: https://dl.fullcirclemagazine.org/issue62_en.epub
---

**This month**

- Command and Conquer.
- How-To : Beginning Python - Part 34, LibreOffice Part 15, and Linux Astronomy Apps.
- Graphics : GIMP - The Beanstalk Part 3, and Inkscape Part 2.
- Linux Lab - Tweet Screen Part 1.
- Review - Installing Software.
- Closing Windows - Linux Equivalents.
- Audio Flux - some CC music tracks. plus: Ubuntu Games, My Desktop, My Opinion, My Story, and much much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAowntbsAQ/full_circle_magazine_62_lite](http://www.google.com/producer/editions/CAowntbsAQ/full_circle_magazine_62_lite)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue62_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue62_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue62_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue62_it.pdf)
