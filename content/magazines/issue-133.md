---
title: "Full Circle Magazine 133"
date: 2018-05-25
draft: false
tags:
  - "aptfast"
  - "deus ex"
  - "distro"
  - "freeplane"
  - "inkscape"
  - "kde"
  - "neon"
  - "python"
  - "upgrade"
cover: covers/magazines/issue-133.webp
pdf: https://dl.fullcirclemagazine.org/issue133_en.pdf
epub: https://dl.fullcirclemagazine.org/issue133_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Freeplane, and Upgrade With Aptfast
- Graphics : Inkscape
- Everyday Linux
- Researching With Linux
- My Opinion
- My Story
- Review: Neon (KDE distro)
- Ubuntu Games: Deus Ex

Plus: News, Q&A, and much more.
