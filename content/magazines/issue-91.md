---
title: "Full Circle Magazine 91"
date: 2014-11-28
draft: false
tags:
  - "arduino"
  - "bash"
  - "borderlands"
  - "inkscape"
  - "kernel"
  - "kodi"
  - "libre"
  - "libreoffice"
  - "mongodb"
  - "node.js"
  - "nodejs"
  - "office"
  - "pushbullet"
  - "python"
  - "script"
  - "scripting"
  - "xbmc"
cover: covers/magazines/issue-91.webp
pdf: https://dl.fullcirclemagazine.org/issue91_en.pdf
epub: https://dl.fullcirclemagazine.org/issue91_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, and Managing Multiple Passwords With A Script
- Graphics : Inkscape.
- Linux Labs: Compiling a Kernel Pt 4 and Kodi Pt 2
- Review: Elementary OS
- Book Review: Web Development with MongoDB and Node.js
- Ubuntu Games: Borderlands 2

Plus: News, Arduino, Q&A, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.
**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue91_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue91_hu.pdf)
