---
title: "Full Circle Magazine 184"
date: 2022-08-26
draft: false
tags:
  - "bash"
  - "python"
  - "vax"
  - "vms"
  - "latex"
  - "inkscape"
  - "dia"
  - "xubuntu"
  - "22.04"
  - "void"
  - "linux"
  - "waddle"
  - "crystal caves"
cover: "covers/magazines/issue-184.webp"
pdf: "https://dl.fullcirclemagazine.org/issue184_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue184_en.epub"
---

This month:

- Command & Conquer
- How-To : Bash to Python, Migrating from VAX/VMS and Latex
- Graphics : Inkscape
- Everyday Ubuntu: Diagramming with Dia
- Review : Xubuntu 22.04
- Review : Void Linux
- Ubuntu Games : Crystal Caves HD
  plus: News, My Opinion, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue184_fr.pdf)
