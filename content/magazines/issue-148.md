---
title: "Full Circle Magazine 148"
date: 2019-08-30
draft: false
tags:
  - "baba"
  - "bsd"
  - "certification"
  - "certified"
  - "crossword"
  - "darktable"
  - "featherpad"
  - "help"
  - "inkscape"
  - "kde"
  - "kde neon"
  - "neon"
  - "project trident"
  - "python"
  - "serial"
  - "server"
  - "terminal"
cover: covers/magazines/issue-148.webp
pdf: https://dl.fullcirclemagazine.org/issue148_en.pdf
epub: https://dl.fullcirclemagazine.org/issue148_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Serial Terminal Server, and Darktable
- Graphics : Inkscape
- My Opinion: Building Software
- Linux Loopback: Project Trident
- Interview: Project Trident Dev
- Everyday Ubuntu: Help
- Review: FeatherPad
- Review: KDE Neon
- Linux Certified
- Ubuntu Games: Baba Is You

Plus: News, The Daily Waddle, Q&A, Crossword and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue148_fr.pdf)
