---
title: "Full Circle Magazine 190"
date: 2023-02-24
draft: false
tags:
  - "python"
  - "rescuezilla"
  - "backup"
  - "latex"
  - "inkscape"
  - "freecad"
  - "xubuntu"
  - "mxlinux"
  - "microsoft"
  - "surface"
cover: "covers/magazines/issue-190.webp"
pdf: "https://dl.fullcirclemagazine.org/issue190_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue190_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Rescuezilla for Backups and Latex
- Graphics : Inkscape
- Graphics : FreeCAD [NEW!]
- Review : Xubuntu 22.10
- Review : MX Linux

plus: News, My Opinion, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue190\_fr.pdf)
