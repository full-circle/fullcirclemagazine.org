---
title: "Full Circle Magazine 196"
date: 2023-08-25
draft: false
tags:
  - "python"
  - "stable"
  - "diffusion"
  - "inkscape"
  - "micro"
  - "ubports"
  - "touch"
  - "xubuntu"
  - "notable"
  - "modelling"
  - "simulation"
  - "garden"
  - "steam"
cover: "covers/magazines/issue-196.webp"
pdf: "https://dl.fullcirclemagazine.org/issue196_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue196_en.epub"
---

This month:

- --Command & Conquer--
- How-To : Python, Stable Diffusion and --Latex--
- Graphics : Inkscape
- Micro This Micro That
- Ubports Touch : OTA-2
- Review : Xubuntu 23.04 and Notable
- Book Review : Modeling and Simulation in Python
- Ubuntu Games : Garden Galaxy

plus: News, My Opinion, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue196\_fr.pdf)
