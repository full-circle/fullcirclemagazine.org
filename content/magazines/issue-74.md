---
title: "Full Circle Magazine 74"
date: 2013-06-28
draft: false
tags:
  - "blender"
  - "clementine"
  - "gpodder"
  - "inkscape"
  - "ipv6"
  - "libre"
  - "libreoffice"
  - "news"
  - "office"
cover: covers/magazines/issue-74.webp
pdf: https://dl.fullcirclemagazine.org/issue74_en.pdf
epub: https://dl.fullcirclemagazine.org/issue74_en.epub
---

**This month**

- Ubuntu News
- Command & Conquer
- How-To : Gpodder, LibreOffice, and Connecting With IPV6.
- Graphics : Blender, and Inkscape
- Review: Clementine

Plus: Q&A (NEW & Improved!), Linux Labs, Ask The New Guy, My Story, and soooo much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAowv4-oBw/full_circle_magazine_74_lite](http://www.google.com/producer/editions/CAowv4-oBw/full_circle_magazine_74_lite)
Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue74_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue74_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue74_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue74_it.pdf)
