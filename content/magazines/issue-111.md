---
title: "Full Circle Magazine 111"
date: 2016-07-29
draft: false
tags:
  - "Debian"
  - "freepascal"
  - "inkscape"
  - "pascal"
  - "python"
  - "raider"
  - "refurb"
  - "refurbish"
  - "security"
  - "server"
  - "tomb"
cover: covers/magazines/issue-111.webp
pdf: https://dl.fullcirclemagazine.org/issue111_en.pdf
epub: https://dl.fullcirclemagazine.org/issue111_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Install Software In Debian/Ubuntu, and Program With FreePascal
- Graphics : Inkscape
- Linux Labs: Refurbish An Old Computer
- Book Review: Linux Server Security
- Ubuntu Games: Tomb Raider

Plus: News, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue111_fr.pdf)
