---
title: "Full Circle Magazine 131"
date: 2018-03-30
draft: false
tags:
  - "basic"
  - "cow"
  - "deb"
  - "encryption"
  - "file"
  - "freeplane"
  - "gcb"
  - "great"
  - "inkscape"
  - "install"
  - "opinion"
  - "Q&amp;A"
  - "software"
  - "story"
  - "veracrypt"
cover: covers/magazines/issue-131.webp
pdf: https://dl.fullcirclemagazine.org/issue131_en.pdf
epub: https://dl.fullcirclemagazine.org/issue131_en.epub
---

**This month**

- Command & Conquer
- How-To : Encryption with Veracrypt, Freeplane, and Great Cow Basic
- Graphics : Inkscape
- Everyday Linux
- My Story
- My Opinion
- Review: Linux Lite

Plus: News, Q&A, and much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue131_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue131_hu.pdf)
