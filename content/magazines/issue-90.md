---
title: "Full Circle Magazine 90"
date: 2014-10-31
draft: false
tags:
  - "architect"
  - "arduino"
  - "cisco"
  - "inkscape"
  - "kernel"
  - "libreoffice"
  - "mega"
  - "megasync"
  - "openconnect"
  - "prison"
  - "wcs"
  - "webcamstudio"
  - "x-plane"
  - "xplane"
cover: covers/magazines/issue-90.webp
pdf: https://dl.fullcirclemagazine.org/issue90_en.pdf
epub: https://dl.fullcirclemagazine.org/issue90_en.epub
---

**This month**

- Command & Conquer
- How-To : OpenConnect to Cisco, LibreOffice, and Broadcasting With WebcamStudio
- Graphics : Inkscape.
- Linux Labs: Compiling a Kernel Pt.3
- Review: MEGAsync
- Ubuntu Games: Prison Architect, and X-Plane Plugins

Plus: News, Arduino, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue90_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue90_hu.pdf)
