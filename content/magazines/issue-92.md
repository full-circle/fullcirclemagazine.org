---
title: "Full Circle Magazine 92"
date: 2014-12-26
draft: false
tags:
  - "book"
  - "build"
  - "bulk"
  - "inkscape"
  - "kernel"
  - "libre"
  - "libreoffice"
  - "nautilus"
  - "office"
  - "print"
  - "printing"
  - "scilab"
  - "ssh"
  - "website"
  - "x-plane"
  - "xp10"
  - "xplane"
cover: covers/magazines/issue-92.webp
pdf: https://dl.fullcirclemagazine.org/issue92_en.pdf
epub: https://dl.fullcirclemagazine.org/issue92_en.epub
---

**This month**

- Command & Conquer
- How-To : Make a Special Edition, LibreOffice, and Bulk Print with Nautilus
- Graphics : Inkscape.
- Linux Labs: Compiling a Kernel Pt 5 and Graphically Renaming Files Over SSH
- Review: Scilabs
- Book Review: Build Your Own Web Site
- Ubuntu Games: X-Plane Flight Plans

Plus: News, Arduino, Q&A, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.
**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue92_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue92_hu.pdf)
