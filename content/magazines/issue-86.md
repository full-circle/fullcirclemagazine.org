---
title: "Full Circle Magazine 86"
date: 2014-06-27
draft: false
tags:
  - "arduino"
  - "blender"
  - "boot"
  - "dogecoin"
  - "drive"
  - "games"
  - "git"
  - "grub"
  - "heartbleed"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "menu"
  - "office"
  - "python"
  - "reddcoin"
  - "security"
  - "ssd"
  - "toshiba"
cover: covers/magazines/issue-86.webp
pdf: https://dl.fullcirclemagazine.org/issue86_en.pdf
epub: https://dl.fullcirclemagazine.org/issue86_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, and GRUB2
- Graphics : Blender and Inkscape.
- Review: Toshiba SSD
- Security and Q&A
- CryptoCurrency: Compiling an Alt-Coin Wallet
- [**NEW!**] Arduino

Plus: Q&A, Linux Labs, Ubuntu Games, and another competition to win Humble Bundles!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue86_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue86_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue86_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue86_it.pdf)
