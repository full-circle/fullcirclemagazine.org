---
title: "Full Circle Magazine 99"
date: 2015-07-31
draft: false
tags:
  - "aquaris"
  - "book"
  - "BQ"
  - "doom"
  - "dreamfall"
  - "e5"
  - "grub"
  - "grub2"
  - "inkscape"
  - "javascript"
  - "latex"
  - "libre"
  - "libreoffice"
  - "meizu"
  - "mx4"
  - "office"
cover: covers/magazines/issue-99.webp
pdf: https://dl.fullcirclemagazine.org/issue99_en.pdf
epub: https://dl.fullcirclemagazine.org/issue99_en.epub
---

**This month**

- Command & Conquer
- How-To : LaTeX, LibreOffice, and Programming JavaScript
- Graphics : Inkscape.
- Chrome Cult
- Linux Labs: Customizing GRUB
- Ubuntu Phones
- Review: Meizu MX4 and BQ Aquaris E5
- Book Review: How Linux Works
- Ubuntu Games: Brutal Doom, and Dreamfall Chapters

Plus: News, Arduino, Q&A, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.

**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue99_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue99_hu.pdf)
