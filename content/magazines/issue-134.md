---
title: "Full Circle Magazine 134"
date: 2018-06-29
draft: false
tags:
  - "basic"
  - "cow"
  - "freeplane"
  - "great"
  - "greatcowbasic"
  - "inkscape"
  - "os"
  - "python"
  - "zorin"
cover: covers/magazines/issue-134.webp
pdf: https://dl.fullcirclemagazine.org/issue134_en.pdf
epub: https://dl.fullcirclemagazine.org/issue134_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Freeplane, and Great Cow Basic
- Graphics : Inkscape
- Everyday Linux
- Researching With Linux
- My Story
- Review: ZorinOS
- Ubuntu Games: Warcraft III

Plus: News, and much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue134_fr.pdf)
