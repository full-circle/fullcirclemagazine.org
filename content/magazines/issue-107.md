---
title: "Full Circle Magazine 107"
date: 2016-03-25
draft: false
tags:
  - "able2extract"
  - "apricity"
  - "deepin"
  - "inkscape"
  - "kindred"
  - "libre"
  - "libreoffice"
  - "migrating"
  - "nexus"
  - "office"
  - "pocket"
  - "printer"
  - "python"
  - "saints row"
  - "server"
  - "vax"
cover: covers/magazines/issue-107.webp
pdf: https://dl.fullcirclemagazine.org/issue107_en.pdf
epub: https://dl.fullcirclemagazine.org/issue107_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, Migrating From VAX, and The Pocket Server
- Graphics : Inkscape
- Chrome Cult: Apricity OS
- Linux Labs: Building A 3D Printer
- Ubuntu Devices
- Review: Able2Extract 10, and Deepin OS
- Ubuntu Games: Saints Row IV, and The Kindred

Plus: News, Arduino, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue107_fr.pdf)
