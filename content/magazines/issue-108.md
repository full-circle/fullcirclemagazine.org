---
title: "Full Circle Magazine 108"
date: 2016-04-29
draft: false
tags:
  - "Debian"
  - "drive"
  - "gopanda"
  - "hard"
  - "inkscape"
  - "latex"
  - "libre"
  - "libreoffice"
  - "linux lite"
  - "noroot"
  - "office"
  - "python"
  - "testing"
  - "vax"
cover: covers/magazines/issue-108.webp
pdf: https://dl.fullcirclemagazine.org/issue108_en.pdf
epub: https://dl.fullcirclemagazine.org/issue108_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, Migrating From VAX, and LaTeX
- Graphics : Inkscape
- Chrome Cult: Options
- Linux Labs: Debian (Noroot) in Android & Testing and Troubleshooting Hard Drives
- Ubuntu Devices
- Review: Linux Lite
- Ubuntu Games: GoPanda

Plus: News, Arduino, Q&A, and soooo much more.

**NOTE**
Many thanks to _Ian Pawson_ for stepping up to take over from the late _Brian Donovan_ and to make this, and hopefully future, EPUB files

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue108_fr.pdf)
