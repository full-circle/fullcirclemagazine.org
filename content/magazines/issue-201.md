---
title: "Full Circle Magazine 201"
date: 2024-01-26
draft: false
tags:
  - "lubuntu"
  - "python"
  - "duplicati"
  - "latex"
  - "inkscape"
  - "micro"
  - "linux"
  - "games"
  - "book"
  - "review"
  - "backup"
cover: "covers/magazines/issue-201.webp"
pdf: "https://dl.fullcirclemagazine.org/issue201_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue201_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Backup With Duplicati and Latex
- Graphics : Inkscape
- Micro This Micro That
- Review : Lubuntu 23.10 and Other 'buntu's
- Book Review : Linux In Easy Steps
- Ubuntu Games - Please Fix The Road

plus: News, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue201\_fr.pdf)

