---
title: "Full Circle Magazine 68"
date: 2012-12-27
draft: false
tags:
  - "12.10"
  - "blender"
  - "crud"
  - "Debian"
  - "didier"
  - "eee"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "mint"
  - "news"
  - "office"
  - "pc"
  - "pi"
  - "programming"
  - "python"
  - "raspberry"
  - "roche"
  - "scratch"
  - "super"
  - "windows"
cover: covers/magazines/issue-68.webp
pdf: https://dl.fullcirclemagazine.org/issue68_en.pdf
epub: https://dl.fullcirclemagazine.org/issue68_en.epub
---

**This month**

- Ubuntu News.
- How-To : Programming in Python, LibreOffice, and Install 12.10 on an EEE PC.
- Graphics : [**NEW!**] Blender, and Inkscape.
- Web Dev : CRUD Part 4.
- Book Review - Super Scratch Programming.
- Review: Linux Mint Debian Edition KDE.
- Closing Windows - Run An App On Startup.
- What Is - Raspberry Pi.
- **SPECIAL Q&A** with Didier Roche - How Ubuntu Is Made.

Plus: Q&A, Command & Conquer, Linux Labs, Ask The New Guy, My Story, and so much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAow-fuBBA/full_circle_magazine_68_lite](http://www.google.com/producer/editions/CAow-fuBBA/full_circle_magazine_68_lite)
Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)
**Happy Holidays to everyone!**

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue68_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue68_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue68_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue68_it.pdf)
