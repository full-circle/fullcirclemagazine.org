---
title: "Full Circle Magazine 13"
date: 2008-05-30
draft: false
tags:
  - "2.0"
  - "8.04"
  - "automatic"
  - "backup"
  - "create"
  - "day"
  - "demo"
  - "email"
  - "gimp"
  - "hardy"
  - "heron"
  - "interview"
  - "luca falavigna"
  - "motu"
  - "server"
  - "tv"
  - "web"
cover: covers/magazines/issue-13.webp
pdf: https://dl.fullcirclemagazine.org/issue13_en.pdf
epub: https://dl.fullcirclemagazine.org/issue13_en.epub
---

Please enjoy the 13th edition of Full Circle Magazine!

**Other languages**

- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue13_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue13_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue13_it.pdf)
