---
title: "Full Circle Magazine 81"
date: 2014-01-31
draft: false
tags:
  - "book"
  - "convert"
  - "games"
  - "inkscape"
  - .webp to pdf"
  - "libre"
  - "libreoffice"
  - "lynis"
  - "office"
  - "python"
  - "security"
  - "super scratch"
cover: covers/magazines/issue-81.webp
pdf: https://dl.fullcirclemagazine.org/issue81_en.pdf
epub: https://dl.fullcirclemagazine.org/issue81_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, and Improve Security with Lynis
- Graphics : JPG>PDF, and Inkscape
- Review: LXLE Linux
- Book Review: Super Scratch (Updated Edition)
- [**NEW!**] Security Q&A

Plus: Linux Labs, Ask The New Guy, My Story, Ubuntu Games, and soooo much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue81_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue81_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue81_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue81_it.pdf)
