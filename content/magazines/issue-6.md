---
title: "Full Circle Magazine 6"
date: 2007-10-25
draft: false
tags:
  - "feisty"
  - "font"
  - "games"
  - "gimp"
  - "gutsy"
  - "interview"
  - "john philips"
  - "library"
  - "open"
  - "photoshop"
  - "plugin"
  - "racing"
  - "samba"
  - "scribus"
  - "upgrade"
cover: covers/magazines/issue-6.webp
pdf: https://dl.fullcirclemagazine.org/issue6_en.pdf
epub: https://dl.fullcirclemagazine.org/issue6_en.epub
---

Please enjoy the 6th edition of Full Circle Magazine!

**Other languages**

- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue6_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue6_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue6_it.pdf)
- [Spanish PDF](https://dl.fullcirclemagazine.org/issue6_es.pdf)
