---
title: "Full Circle Magazine 33"
date: 2010-01-30
draft: false
tags:
  - "apps"
  - "boxee"
  - "center"
  - "centre"
  - "clients"
  - "create"
  - "didier roche"
  - "education"
  - "exaile"
  - "interview"
  - "media"
  - "motu"
  - "perfect"
  - "public"
  - "python"
  - "revo"
  - "server"
  - "sync"
  - "synchronization"
  - "tools"
cover: covers/magazines/issue-33.webp
pdf: https://dl.fullcirclemagazine.org/issue33_en.pdf
epub: https://dl.fullcirclemagazine.org/issue33_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 7, Create A Media Center with a Revo, Ubuntu and Boxee, and The Perfect Server - Part 3.
- My Story - Ubuntu in Public Education, and Why I Use Linux.
- Review - Exaile.
- MOTU Interview - Didier Roche.
- Top 5 - Synchronization Clients.

Plus : Ubuntu Women, Ubuntu Games and all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue33_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue33_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue33_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue33_ru.pdf)
