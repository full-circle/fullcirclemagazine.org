---
title: "Full Circle Magazine 198"
date: 2023-10-27
draft: false
tags:
  - "ubuntu"
  - "budgie"
  - "python"
  - "stable"
  - "diffusion"
  - "latex"
  - "inkscape"
  - "book"
  - "moonstone"
cover: "covers/magazines/issue-198.webp"
pdf: "https://dl.fullcirclemagazine.org/issue198_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue198_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion and Latex
- Graphics : Inkscape
- Micro This Micro That
- Review : Ubuntu Budgie 23.04 and Pop!_OS 22.04 LTS
- Book Review : Python Playground
- Ubuntu Games : Moonstone Island

plus: News, My Opinion, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue198\_fr.pdf)
