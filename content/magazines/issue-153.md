---
title: "Full Circle Magazine 153"
date: 2020-01-31
draft: false
tags:
  - "bsd"
  - "darktable"
  - "furybsd"
  - "inkscape"
  - "krita"
  - "Linux"
  - "mtpaint"
  - "photography"
  - "python"
  - "stygian"
  - "trident"
  - "Ubuntu"
  - "virtualbox"
cover: covers/magazines/issue-153.webp
pdf: https://dl.fullcirclemagazine.org/issue153_en.pdf
epub: https://dl.fullcirclemagazine.org/issue153_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Test Linux in VirtualBox, and Darktable
- Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback: Project Trident and other BSD options
- Everyday Ubuntu
- Interview : FuryBSD Developer
- Review : mtPaint
- Ubuntu Games : Stygian

Plus: News, My Opinion, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue153_fr.pdf)
