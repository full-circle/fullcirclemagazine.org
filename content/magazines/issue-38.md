---
title: "Full Circle Magazine 38"
date: 2010-06-26
draft: false
tags:
  - "10.04"
  - "blog"
  - "blogger"
  - "blogging"
  - "browser"
  - "interview"
  - "motu"
  - "python"
  - "top 5"
  - "virtualisation"
  - "virtualization"
cover: covers/magazines/issue-38.webp
pdf: https://dl.fullcirclemagazine.org/issue38_en.pdf
epub: https://dl.fullcirclemagazine.org/issue38_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 12, a NEW SERIES: Virtualization, and Browser Blogging.
- Review - Ubuntu 10.04.
- Top 5 - Favourite Applications.

Plus: MOTU Interview, Ubuntu Games, My Opinion, My Story, and all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue38_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue38_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue38_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue38_ru.pdf)
