---
title: "Full Circle Magazine 98"
date: 2015-06-26
draft: false
tags:
  - "arduino"
  - "automate boring stuff with python"
  - "chicken feeder"
  - "chrome"
  - "conky"
  - "controls"
  - "flight"
  - "free to play"
  - "games"
  - "inkscape"
  - "javascript"
  - "libre"
  - "libreoffice"
  - "midnight commander"
  - "minetest"
  - "office"
  - "reminder"
  - "saitek"
  - "teach your kids to code"
cover: covers/magazines/issue-98.webp
pdf: https://dl.fullcirclemagazine.org/issue98_en.pdf
epub: https://dl.fullcirclemagazine.org/issue98_en.epub
---

**This month**

- Command & Conquer
- How-To : Conky Reminder, LibreOffice, and Programming JavaScript
- Graphics : Inkscape.
- Chrome Cult
- Linux Labs: Midnight Commander
- Ubuntu Phones
- Review: Saitek Pro Flight System
- Book Reviews: Automate Boring Stuff With Python, and Teach Your Kids To Code
- Ubuntu Games: Minetest, and Free to Play Games

Plus: News, Arduino, Q&A, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.
**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue98_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue98_hu.pdf)
