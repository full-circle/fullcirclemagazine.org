---
title: "Full Circle Magazine 94"
date: 2015-02-27
draft: false
tags:
  - "arduino"
  - "block"
  - "blocking"
  - "book"
  - "call"
  - "cookbook"
  - "data"
  - "flame"
  - "i2p"
  - "inkscape"
  - "led"
  - "libre"
  - "libreoffice"
  - "lo"
  - "office"
  - "practical"
  - "review"
  - "science"
  - "thunder"
  - "tor"
  - "war"
cover: covers/magazines/issue-94.webp
pdf: https://dl.fullcirclemagazine.org/issue94_en.pdf
epub: https://dl.fullcirclemagazine.org/issue94_en.epub
---

**This month**

- Command & Conquer
- How-To : Block Calls, LibreOffice, and Using i2P
- Graphics : Inkscape
- Linux Labs: BTRFS
- Book Review: Practical Data Science Cookbook
- Ubuntu Games: War Thunder

Plus: News, Arduino, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue94_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue94_hu.pdf)
