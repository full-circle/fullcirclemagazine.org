---
title: "Full Circle Magazine 50"
date: 2011-06-24
draft: false
tags:
  - "dev"
  - "development"
  - "facial"
  - "gnome"
  - "installer"
  - "installers"
  - "kde"
  - "libre"
  - "libreoffice"
  - "office"
  - "pam"
  - "python"
  - "recognition"
  - "release"
  - "rolling"
  - "shell"
  - "Unity"
  - "usb"
cover: covers/magazines/issue-50.webp
pdf: https://dl.fullcirclemagazine.org/issue50_en.pdf
epub: https://dl.fullcirclemagazine.org/issue50_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 24, LibreOffice - Part 5, Ubuntu Development - Part 2, and Use KDE (4.6).
- Linux Lab - Gnome Shell -vs- Unity.
- Review - PAM Facial Recognition.
- Top 5 - USB Installers.
- I Think - Should Ubuntu keep it's current schedule, or switch to a rolling release?

Plus: Ubuntu Games, My Story, and much much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue50_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue50_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue50_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue50_it.pdf)
