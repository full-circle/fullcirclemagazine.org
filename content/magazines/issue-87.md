---
title: "Full Circle Magazine 87"
date: 2014-07-25
draft: false
tags:
  - "cgminer"
  - "dualminer"
  - "grub"
  - "inkscape"
  - "libre"
  - "office"
  - "puppet"
  - "python"
  - "truecrypt"
cover: covers/magazines/issue-87.webp
pdf: https://dl.fullcirclemagazine.org/issue87_en.pdf
epub: https://dl.fullcirclemagazine.org/issue87_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, and GRUB2
- Graphics : Inkscape
- Book Review: Puppet
- Security - TrueCrypt Alternatives
- CryptoCurrency: Dualminer and dual-cgminer
- Arduino

Plus: Q&A, Linux Labs, Ubuntu Games, and Ubuntu Women.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue87_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue87_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue87_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue87_it.pdf)
