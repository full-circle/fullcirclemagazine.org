---
title: "Full Circle Magazine 93"
date: 2015-01-30
draft: false
tags:
  - "arduino"
  - "book"
  - "bsd"
  - "freebsd"
  - "fs economy"
  - "fseconomy"
  - "heaven"
  - "inkscape"
  - "kernel"
  - "kiosk"
  - "libre"
  - "libreoffice"
  - "mate"
  - "office"
  - "radio"
  - "rtl"
  - "rtl-sdr"
  - "sdr"
  - "software"
  - "unigine"
  - "x-plane"
  - "xp10"
cover: covers/magazines/issue-93.webp
pdf: https://dl.fullcirclemagazine.org/issue93_en.pdf
epub: https://dl.fullcirclemagazine.org/issue93_en.epub
---

**This month**

- Command & Conquer
- How-To : RTL-SDR Radio, LibreOffice, and Ubuntu Kiosk
- Graphics : Inkscape.
- Linux Labs: Compiling a Kernel Pt 6 and Trying FreeBSD
- Review: Ubuntu Mate 14.10
- Book Review: Official Ubuntu Book 8th Edition
- Ubuntu Games: X-Plane & FSEconomy, and Unigine Heaven Benchmark

Plus: News, Arduino, Q&A, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.
**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue93_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue93_hu.pdf)
