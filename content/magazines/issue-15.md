---
title: "Full Circle Magazine 15"
date: 2008-07-25
draft: false
tags:
  - "c3180"
  - "create"
  - "directories"
  - "folders"
  - "gimp"
  - "grub"
  - "home"
  - "hp"
  - "interview"
  - "kubuntu"
  - "mathias gug"
  - "motu"
  - "partition"
  - "photosmart"
  - "printer"
  - "scanner"
  - "server"
cover: covers/magazines/issue-15.webp
pdf: https://dl.fullcirclemagazine.org/issue15_en.pdf
epub: https://dl.fullcirclemagazine.org/issue15_en.epub
---

**This month**

- Command and Conquer - The Ins and Outs of Directories
- How-To: Create a Separate Home Partition, Create Your Own Server (Part 7), Using GIMP (Part 4), and GRUB 101.
- My Story - Ubuntu Saves the Day and From Mickey's ABCs to Kubuntu
- Review - HP Photosmart C3180 Printer
- MOTU Interview - Mathais Gug
- Our usual letters, Q & A, My Desktop, and much more!

**Other languages**

- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue15_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue15_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue15_it.pdf)
