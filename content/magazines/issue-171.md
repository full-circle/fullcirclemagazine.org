---
title: "Full Circle Magazine 171"
date: 2021-07-30
draft: false
tags:
  - "clone"
  - "codelite"
  - "dolly"
  - "esp32"
  - "game"
  - "gaming"
  - "huntdown"
  - "hybrid"
  - "inkscape"
  - "kubuntu"
  - "latex"
  - "lmms"
  - "micro"
  - "ota-18"
  - "ota18"
  - "python"
  - "retrograming"
  - "touch"
  - "uborts"
cover: covers/magazines/issue-171.webp
pdf: https://dl.fullcirclemagazine.org/issue171_en.pdf
epub: https://dl.fullcirclemagazine.org/issue171_en.epub
---

**This month**

- Command & Conquer : LMMS
- How-To : Python, Latex and Creating Hybrid Portable Clone
- Graphics : Inkscape
- Everyday Ubuntu : Retrogaming
- Micro This Micro That
- Ubports Devices : OTA-18
- Review : Kubuntu 21.04
- Review : Codelite
- Ubuntu Games : Huntdown

Plus: News, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue171_fr.pdf)
