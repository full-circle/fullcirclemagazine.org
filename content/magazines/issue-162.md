---
title: "Full Circle Magazine 162"
date: 2020-10-30
draft: false
tags:
  - "20.10"
  - "carrion"
  - "game"
  - "hungary"
  - "inkscape"
  - "krita"
  - "Linux"
  - "music"
  - "network"
  - "networking"
  - "nmap"
  - "pardus"
  - "photography"
  - "podcast"
  - "podcasting"
  - "production"
  - "python"
  - "rawtherapee"
  - "task warrior"
  - "ubuntu"
cover: covers/magazines/issue-162.webp
pdf: https://dl.fullcirclemagazine.org/issue162_en.pdf
epub: https://dl.fullcirclemagazine.org/issue162_en.epub
---

**This month**

- Command & Conquer : Nmap
- How-To : Python, Podcast Production, and Rawtherapee
- Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback
- Everyday Ubuntu
- Review : Ubuntu 20.10 and Task Warrior
- Ubuntu Games : Carrion

Plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue162_fr.pdf)
