---
title: "Full Circle Magazine 23"
date: 2009-03-28
draft: false
tags:
  - "c"
  - "dev"
  - "development"
  - "geek goddess"
  - "interview"
  - "managers"
  - "motu"
  - "steve stalcup"
  - "task"
  - "web"
cover: covers/magazines/issue-23.webp
pdf: https://dl.fullcirclemagazine.org/issue23_en.pdf
epub: https://dl.fullcirclemagazine.org/issue23_en.epub
---

**This month**

- Command and Conquer - Troubleshooting.
- How-To : Program in C - Part 7, Web Development - Part 4, and Spreading Ubuntu - Part 2.
- My Story - Becoming An Ubuntu User
- Book Review - How To Be A Geek Goddess
- MOTU Interview - Steve Stalcup
- Top 5 - Task Managers

Plus : all the usual goodness...

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue23_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue23_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue23_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue23_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue23_ru.pdf)
