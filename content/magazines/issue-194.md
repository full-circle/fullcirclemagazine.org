---
title: "Full Circle Magazine 194"
date: 2023-06-30
draft: false
tags:
  - "kubuntu"
  - "python"
  - "stable"
  - "diffusion"
  - "latex"
  - "inkscape"
  - "freecad"
  - "apico"
  - "game"
  - "blender"
  - "graphics"
  - "micro"
cover: "covers/magazines/issue-194.webp"
pdf: "https://dl.fullcirclemagazine.org/issue194_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue194_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion and Latex
- Graphics : Inkscape
- Graphics : FreeCAD
- Micro This Micro That
- Review : Kubuntu 23.04
- Book Review : Complete Guide to Blender Graphics
- Ubuntu Games : APICO

plus: News, My Story, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue194\_fr.pdf)
