---
title: "Full Circle Magazine 8"
date: 2007-12-21
draft: false
tags:
  - "boot"
  - "desktop"
  - "Linux"
  - "multi"
  - "multi-book"
  - "multiboot"
  - "mythbuntu"
  - "scribus"
  - "tomboy"
  - "wubi"
cover: covers/magazines/issue-8.webp
pdf: https://dl.fullcirclemagazine.org/issue8_en.pdf
epub: https://dl.fullcirclemagazine.org/issue8_en.epub
---

**This month**

- Mythbuntu - Step-by-step Install
- How-To : Install Wubi, Get a Christmas Desktop, Multi Boot Linux and Learning Scribus Pt.8.
- Review of TomBoy.
- New Column for Ubuntu Women
- Letters, Q&A, My Desktop, Top 5 and more!

**Other languages**

- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue8_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue8_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue8_it.pdf)
