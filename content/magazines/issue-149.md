---
title: "Full Circle Magazine 149"
date: 2019-09-27
draft: false
tags:
  - "automation"
  - "darktable"
  - "devuan"
  - "inkscape"
  - "Linux"
  - "photography"
  - "python"
  - "Ubuntu"
cover: covers/magazines/issue-149.webp
pdf: https://dl.fullcirclemagazine.org/issue149_en.pdf
epub: https://dl.fullcirclemagazine.org/issue149_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Automation, and Darktable
- Graphics : Inkscape
- My Opinion: Ubuntu 18.04
- Linux Loopback
- Everyday Ubuntu
- Book Review: Linux Basics for Hackers
- Review: DeVuan Linux
- Linux Certified
- Ubuntu Games: Irony Curtain

Plus: News, The Daily Waddle, Q&A, Crossword and more.

**NOTE**
EPUB's will be delayed for a month/two as Ian is recovering from a broken hip.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue149_fr.pdf)
