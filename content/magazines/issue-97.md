---
title: "Full Circle Magazine 97"
date: 2015-05-29
draft: false
tags:
  - "android"
  - "arc"
  - "camera"
  - "chrome"
  - "inkscape"
  - "javascript"
  - "kde"
  - "latex"
  - "libre"
  - "office"
  - "plasma"
  - "powerline"
  - "this war of mine"
  - "welder"
cover: covers/magazines/issue-97.webp
pdf: https://dl.fullcirclemagazine.org/issue97_en.pdf
epub: https://dl.fullcirclemagazine.org/issue97_en.epub
---

**This month**

- Command & Conquer
- How-To : Run Android Apps in Ubuntu, LibreOffice, Using LaTeX, and Programming JavaScript
- Graphics : Inkscape.
- Chrome Cult
- Linux Labs: IP Camera with Powerline Adapter
- Ubuntu Phones
- Review: KDE Plasma 5
- Ubuntu Games: This War of Mine

Plus: News, Arduino, Q&A, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.
**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue97_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue97_hu.pdf)
