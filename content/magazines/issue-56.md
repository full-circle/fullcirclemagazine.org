---
title: "Full Circle Magazine 56"
date: 2011-12-25
draft: false
tags:
  - "11.10"
  - "backup"
  - "centre"
  - "classic"
  - "copying"
  - "files"
  - "irc"
  - "libre"
  - "libreoffice"
  - "Linux"
  - "look"
  - "media"
  - "moving"
  - "office"
  - "persistent"
  - "puppy"
  - "stick"
  - "usb"
  - "use"
  - "xbmc"
  - "xbox"
cover: covers/magazines/issue-56.webp
pdf: https://dl.fullcirclemagazine.org/issue56_en.pdf
epub: https://dl.fullcirclemagazine.org/issue56_en.epub
---

**This month**

- Command and Conquer.
- How-To : Make 11.10 Look 'Classic', LibreOffice - Part 10, Backup Strategy - Part 4, Persistent USB Stick, and Connect To IRC.
- Linux Lab - Xbox Media Centre.
- Review - Puppy Linux.
- I Think - Would you attend a monthly FCM meeting on IRC?
- Closing Windows - Moving, copying & deleting files

Plus: more Ubuntu Games, My Desktop (and an extra!), My Opinion, My Story, and much **much** more!

[**NEW!**] The English edition is also available through _**Google Currents**_: [http://www.google.com/producer/editions/CAowkLxE/full_circle_magazine_56_lite](http://www.google.com/producer/editions/CAowkLxE/full_circle_magazine_56_lite)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue56_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue56_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue56_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue56_it.pdf)
