---
title: "Full Circle Magazine 71"
date: 2013-03-29
draft: false
tags:
  - "blender"
  - "chrome"
  - "chrome os"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "news"
  - "office"
  - "PAF"
  - "python"
  - "python for kids"
cover: covers/magazines/issue-71.webp
pdf: https://dl.fullcirclemagazine.org/issue71_en.pdf
epub: https://dl.fullcirclemagazine.org/issue71_en.epub
---

**This month**

- Ubuntu News.
- How-To : Programming in Python, LibreOffice, and Using PAF.
- Graphics : Blender, and Inkscape.
- Book Review: Python for Kids.
- Review: Chrome OS plus: Q&A, Command & Conquer, Linux Labs, Ask The New Guy, My Story, *and soooo much more!*

**Google Currents Edition** [http://www.google.com/producer/editions/CAow0YSQBw/full_circle_magazine_71_lite](http://www.google.com/producer/editions/CAow0YSQBw/full_circle_magazine_71_lite)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue71_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue71_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue71_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue71_it.pdf)
