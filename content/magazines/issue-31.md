---
title: "Full Circle Magazine 31"
date: 2009-11-28
draft: false
tags:
  - "andreas wenning"
  - "clients"
  - "conversion"
  - "interview"
  - "mint"
  - "motu"
  - "perfect"
  - "python"
  - "server"
  - "subversion"
  - "tools"
  - "windows 7"
cover: covers/magazines/issue-31.webp
pdf: https://dl.fullcirclemagazine.org/issue31_en.pdf
epub: https://dl.fullcirclemagazine.org/issue31_en.epub
---

**This month**

- Command and Conquer.
- How-To: Program in Python - Part 5, The Perfect Server - Part 1, and Universe of Sound.
- My Story - The Conversion.
- My Opinion - Windows 7.
- Review - Linux Mint 7.
- MOTU Interview - Andreas Wenning.
- Top 5 - Subversion Clients.

Plus : Ubuntu Women, Ubuntu Games and all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue31_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue31_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue31_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue31_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue31_ru.pdf)
