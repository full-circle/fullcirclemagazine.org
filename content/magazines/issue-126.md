---
title: "Full Circle Magazine 126"
date: 2017-10-27
draft: false
tags:
  - "freecad"
  - "inkscape"
  - "kodi"
  - "latex"
  - "office"
  - "pac-man"
  - "pacman"
  - "pop!_os"
  - "rebuilding"
  - "tmux"
  - "writing"
cover: covers/magazines/issue-126.webp
pdf: https://dl.fullcirclemagazine.org/issue126_en.pdf
epub: https://dl.fullcirclemagazine.org/issue126_en.epub
---

**This month**

- Command & Conquer
- How-To : LateX, Intro To FreeCAD, and tmux
- Graphics : Inkscape
- Researching With Linux
- My Opinion: Writing With Ubuntu
- KODI Room: Rebuilding
- Review: Pop!\_OS - Ubuntu Games: Pac-Man 256

Plus: News, Q&A, My Desktop, and much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue126_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue126_hu.pdf)
