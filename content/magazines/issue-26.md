---
title: "Full Circle Magazine 26"
date: 2009-06-28
draft: false
tags:
  - "account"
  - "apt-cacher"
  - "conversion"
  - "converted"
  - "devices"
  - "guest"
  - "inkscape"
  - "interview"
  - "irssi"
  - "Linux"
  - "moc"
  - "motu"
  - "powered"
  - "stefan ebner"
  - "webhttrack"
cover: covers/magazines/issue-26.webp
pdf: https://dl.fullcirclemagazine.org/issue26_en.pdf
epub: https://dl.fullcirclemagazine.org/issue26_en.epub
---

**This month**

- Command and Conquer - MOC & IRSSI
- How To: Ubuntu As A Guest, Apt-Cacher, and Inkscape - Part 3.
- My Story - Why I Converted To Linux.
- Review - WebHTTrack
- MOTU Interview - Stefan Ebner.
- Top 5 - Linux-powered Devices.

Plus : Ubuntu Women, Ubuntu Games, and all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue26_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue26_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue26_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue26_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue26_ru.pdf)
