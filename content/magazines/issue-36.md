---
title: "Full Circle Magazine 36"
date: 2010-04-29
draft: false
tags:
  - "admin"
  - "administration"
  - "applications"
  - "automating"
  - "gimp"
  - "google"
  - "interview"
  - "jo shields"
  - "Linux"
  - "motu"
  - "python"
  - "retouch"
  - "retouching"
  - "scanning"
  - "system"
  - "tools"
  - "top 5"
  - "unix"
  - "using"
cover: covers/magazines/issue-36.webp
pdf: https://dl.fullcirclemagazine.org/issue36_en.pdf
epub: https://dl.fullcirclemagazine.org/issue36_en.epub
---

**It's our 3rd anniversary!**
**This month**

- Some thoughts on year three of FCM.
- Command and Conquer.
- How To: Program in Python - Part 10, Retouching in GIMP - Part 3, and Use Google Effectively.
- Book Review - Automating Linux and Unix System Administration.
- MOTU Interview - Jo Shields.
- Top 5 - Scanning Applications.

Plus: Ubuntu Women, Ubuntu Games, My Opinion, My Story, and all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue36_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue36_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue36_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue36_ru.pdf)
