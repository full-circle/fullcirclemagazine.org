---
title: "Full Circle Magazine 112"
date: 2016-08-26
draft: false
tags:
  - "apps"
  - "arduino"
  - "book"
  - "chrome"
  - "device"
  - "file"
  - "FREE"
  - "freepascal"
  - "graphics"
  - "inkscape"
  - "install"
  - "iso"
  - "latex"
  - "optimise"
  - "optimize"
  - "pascal"
  - "project"
  - "python"
  - "spreadsheet"
cover: covers/magazines/issue-112.webp
pdf: https://dl.fullcirclemagazine.org/issue112_en.pdf
epub: https://dl.fullcirclemagazine.org/issue112_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Spreadsheets in LaTeX, and Program With FreePascal
- Graphics : Inkscape
- My Opinion: Ubuntu Download ISOs
- Book Review: Arduino Project Book
- Ubuntu Devices: Installing X Apps On Ubuntu Devices
- Ubuntu Games: Optimizing Gaming Graphics

Plus: News, Chrome Cult, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue112_fr.pdf)
