---
title: "Full Circle Magazine 168"
date: 2021-05-01
draft: false
tags:
  - "bibletime"
  - "game"
  - "inkscape"
  - "latex"
  - "lmms"
  - "micro"
  - "micropython"
  - "pi"
  - "pico"
  - "pygtk"
  - "python"
  - "shells.com"
  - "urtuk"
cover: covers/magazines/issue-168.webp
pdf: https://dl.fullcirclemagazine.org/issue168_en.pdf
epub: https://dl.fullcirclemagazine.org/issue168_en.epub
---

**This month**

- Command & Conquer : LMMS
- How To : Python, Latex [**NEW!**] and Compiling an FCM Special Edition
- Graphics : Inkscape
- My Story : How I Found Full Circle Magazine
- Everyday Ubuntu : BibleTime Pt1
- Micro This Micro That [**NEW!**]
- Review : Shells.com
- Book Review: Foundations of PyGTK Development
- Ubuntu Games : Urtuk The Desolation

Plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue168_fr.pdf)
