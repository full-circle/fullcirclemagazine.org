---
title: "Full Circle Magazine 145"
date: 2019-05-31
draft: false
tags:
  - "bsd"
  - "crossword"
  - "draw"
  - "ghostbsd"
  - "gramps"
  - "help"
  - "inkscape"
  - "libreoffice"
  - "netcat"
  - "perl"
  - "python"
  - "steam"
cover: covers/magazines/issue-145.webp
pdf: https://dl.fullcirclemagazine.org/issue145_en.pdf
epub: https://dl.fullcirclemagazine.org/issue145_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, GRAMPS, and Using Netcat pt2
- Graphics : Inkscape
- My Opinion: LibreOffice Draw
- Linux Loopback: GhostBSD & Interview
- Everyday Ubuntu: Getting Help
- Book Review: Learning Perl 6
- [**NEW!**] Linux Certified
- Ubuntu Games: Steam Survey Results

Plus: News, The Daily Waddle, Q&A, Crossword and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue145_fr.pdf)
