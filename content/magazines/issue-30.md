---
title: "Full Circle Magazine 30"
date: 2009-10-30
draft: false
tags:
  - "acer"
  - "applications"
  - "aspire"
  - "base"
  - "bookworms"
  - "distro"
  - "distros"
  - "ebook"
  - "interview"
  - "laptop"
  - "motu"
  - "oap"
  - "office"
  - "one"
  - "open"
  - "openoffice"
  - "pensioner"
  - "prs-505"
  - "python"
  - "reader"
  - "recording"
  - "sony"
  - "thierry carrez"
  - "writing"
cover: covers/magazines/issue-30.webp
pdf: https://dl.fullcirclemagazine.org/issue30_en.pdf
epub: https://dl.fullcirclemagazine.org/issue30_en.epub
---

**This month**

- How-To: Program in Python - Part 4, Applications for Bookworms, Installing OpenOffice.org Base.
- My Story - The Doctor Is In, Recording Porgy & Bess, Ubuntu Reincarnates Pensioners Laptop
- Command and Conquer
- My Opinion - Acer Aspire One Distros
- Review - Sony PRS-505 Ebook Reader
- MOTU Interview - Thierry Carrez.
- Top 5 - Writing Applications.

Plus : Ubuntu Women, Ubuntu Games and all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue30_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue30_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue30_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue30_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue30_ru.pdf)
