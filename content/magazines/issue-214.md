---
title: "Full Circle Magazine 214"
date: 2025-02-28
draft: false
tags:
  - "latex"
  - "inkscape"
  - "budgie"
  - "unity"
  - "cardbob"
  - "dell"
  - "latitude"
  - "ubuntu"
cover: "covers/magazines/issue-214.webp"
pdf: "https://dl.fullcirclemagazine.org/issue214_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue214_en.epub"
---

*This month:

- How-To : Learn About, Trading Up To Linux and Latex
- Graphics : Inkscape
- Review : Ubuntu 24.10 Budgie and Unity
- UbPorts Touch
- Ubuntu Games - Cardbob

plus: News, Command and Conquer, My Opinion, The Daily Waddle, Q&A and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue214\_fr.pdf)
