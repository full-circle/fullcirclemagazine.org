---
title: "Full Circle Magazine 73"
date: 2013-06-02
draft: false
tags:
  - "blender"
  - "cmus"
  - "inkscape"
  - "intro"
  - "introduction"
  - "libre"
  - "libreoffice"
  - "news"
  - "office"
  - "python"
  - "results"
  - "sqlite"
  - "survey"
  - "tv browser"
cover: covers/magazines/issue-73.webp
pdf: https://dl.fullcirclemagazine.org/issue73_en.pdf
epub: https://dl.fullcirclemagazine.org/issue73_en.epub
---

**This month**

- Ubuntu News.
- How-To : Programming in Python, LibreOffice, and An Intro To SQLite.
- Graphics : Blender, and Inkscape.
- Review: TV Browser & CMUS.
- AND: New Ubuntu Games writers and the Reader Survey 2013 results!

Plus: Q&A, Command & Conquer, Linux Labs, Ask The New Guy, My Story, and soooo much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAow8rChBw/full_circle_magazine_73_lite](http://www.google.com/producer/editions/CAow8rChBw/full_circle_magazine_73_lite)
Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue73_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue73_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue73_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue73_it.pdf)
