---
title: "Full Circle Magazine 188"
date: 2022-12-30
draft: false
tags:
  - "python"
  - "latex"
  - "inkscape"
  - "micro"
  - "kubuntu"
  - "22.10"
  - "cinnamon"
  - "ubuntu"
  - "touch"
  - "ubports"
  - "dwarf fortress"
  - "steam"
  - "waddle"
cover: "covers/magazines/issue-188.webp"
pdf: "https://dl.fullcirclemagazine.org/issue188_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue188_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Blender and Latex
- Graphics : Inkscape
- Everyday Ubuntu
- Micro This Micro That
- Review : Kubuntu 22.10
- Review : Ubuntu Cinnamon 22.04
- Ubports Touch : OTA-24
- Tabletop Ubuntu
- Ubuntu Games : Dwarf Fortress (Steam Edition)

  plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue188_fr.pdf)
