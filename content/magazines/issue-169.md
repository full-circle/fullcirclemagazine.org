---
title: "Full Circle Magazine 169"
date: 2021-05-21
draft: false
tags:
  - "21.04"
  - "alpha"
  - "bibletime"
  - "book"
  - "esp"
  - "inkscape"
  - "latex"
  - "lmms"
  - "micro"
  - "mutropolis"
  - "ota-17"
  - "ota17"
  - "pi"
  - "pico"
  - "projects"
  - "python"
  - "software"
  - "touch"
  - "ubports"
  - "ubuntu"
  - "usb2"
  - "usb3"
  - "waddle"
cover: covers/magazines/issue-169.webp
pdf: https://dl.fullcirclemagazine.org/issue169_en.pdf
epub: https://dl.fullcirclemagazine.org/issue169_en.epub
---

**This month**

- Command & Conquer : LMMS
- How-To : Python, Latex and Using USB3 on USB2
- Graphics : Inkscape
- My Opinion – Use Case For Alpha Software
- Everyday Ubuntu : BibleTime Pt2
- Micro This Micro That
- Ubports Devices – OTA-17
- Review : Ubuntu 21.04
- Book Review: Big Book Of Small Python Projects
- Ubuntu Games : Mutropolis

Plus: News, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue169_fr.pdf)
