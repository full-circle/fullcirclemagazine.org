---
title: "Full Circle Magazine 157"
date: 2020-05-29
draft: false
tags:
  - "20.04"
  - "budgie"
  - "eagle"
  - "island"
  - "game"
  - "inkscape"
  - "krita"
  - "livepatch"
  - "lubuntu"
  - "ota-12"
  - "ota12"
  - "photography"
  - "photos"
  - "python"
  - "rawtherapee"
  - "review"
  - "rust"
  - "touch"
  - "turbogfx"
  - "ubports"
  - "ubuntu"
cover: covers/magazines/issue-157.webp
pdf: https://dl.fullcirclemagazine.org/issue157_en.pdf
epub: https://dl.fullcirclemagazine.org/issue157_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LivePatch, and Rawtherapee
- Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback
- Everyday Ubuntu : Turbogfx 16
- Ubports Touch : OTA-12
- Review : Ubuntu, Lubuntu and Budgie 20.04
- Ubuntu Games : Eagle Island

Plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue157_fr.pdf)
