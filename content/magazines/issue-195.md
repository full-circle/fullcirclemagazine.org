---
title: "Full Circle Magazine 195"
date: 2023-07-28
draft: false
tags:
  - "stable"
  - "diffusion"
  - "latex"
  - "inkscape"
  - "freecad"
  - "micro"
  - "lubuntu"
  - "freetuxtv"
  - "hypnotix"
  - "freetv"
  - "voxel tycoon"
cover: "covers/magazines/issue-195.webp"
pdf: "https://dl.fullcirclemagazine.org/issue195_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue195_en.epub"
---

This month:

- --Command & Conquer--
- How-To : --Python--, Stable Diffusion and Latex
- Graphics : Inkscape
- Graphics : FreeCAD
- Micro This Micro That
- Review : Lubuntu 23.04
- Review : FreeTuxTV (and Hypnotix)
- Ubuntu Games : Voxel Tycoon

plus: News, My Story, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue195\_fr.pdf)
