---
title: "Full Circle Magazine 114"
date: 2016-10-29
draft: false
tags:
  - "book"
  - "cpu"
  - "eletronics"
  - "FREE"
  - "freepascal"
  - "inkscape"
  - "interview"
  - "kids"
  - "kodi"
  - "pascal"
  - "python"
  - "ship"
  - "system76"
cover: covers/magazines/issue-114.webp
pdf: https://dl.fullcirclemagazine.org/issue114_en.pdf
epub: https://dl.fullcirclemagazine.org/issue114_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Stop Your CPU Overheating, and Program With FreePascal
- Graphics : Inkscape
- Linux Labs: CD Ripping
- [**NEW!**] KODI Room: Tips & Tricks
- Book Review: Electronics for Kids
- Interview: Ryan Sipes at System76
- Ubuntu Games: The Ship Remastered

Plus: News, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue114_fr.pdf)
