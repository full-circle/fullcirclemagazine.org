---
title: "Full Circle Magazine 122"
date: 2017-06-30
draft: false
tags:
  - "desktop"
  - "etcher"
  - "freecad"
  - "inkscape"
  - "kdenlive"
  - "python"
  - "siltbreaker"
  - "touch"
  - "ubports"
cover: covers/magazines/issue-122.webp
pdf: https://dl.fullcirclemagazine.org/issue122_en.pdf
epub: https://dl.fullcirclemagazine.org/issue122_en.epub
---

**This month**

- Command & Conquer
- How-To : Python (Arduino), Intro To FreeCAD, and Installing UBports
- Graphics : Inkscape & Kdenlive
- [**NEW!**] Researching With Linux
- Linux Labs
- Review: Etcher
- Ubuntu Games: Siltbreaker Act 1

Plus: News, Q&A, My Desktop, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue122_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue122_hu.pdf)
