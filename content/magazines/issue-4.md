---
title: "Full Circle Magazine 4"
date: 2007-08-30
draft: false
tags:
  - "code"
  - "colin watson"
  - "developer"
  - "edubuntu"
  - "hosting"
  - "interview"
  - "kids"
  - "launch"
  - "launchpad"
  - "miro"
  - "pad"
  - "review"
  - "scribus"
cover: covers/magazines/issue-4.webp
pdf: https://dl.fullcirclemagazine.org/issue4_en.pdf
epub: https://dl.fullcirclemagazine.org/issue4_en.epub
---

**This issue contains**

- Edubuntu - What's in it for the kids?
- How-To : Hosting Code on LaunchPad, Learning Scribus part 4 and Keep your kids safe in Ubuntu.
- Preview of Miro 0.9.8.
- Interview with Ubuntu Developer Colin Watson.
- Letters, Q&A, MyDesktop, MyPC, Top5 and more!

**Other languages**

- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue4_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue4_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue4_it.pdf)
- [Spanish PDF](https://dl.fullcirclemagazine.org/issue4_es.pdf)
