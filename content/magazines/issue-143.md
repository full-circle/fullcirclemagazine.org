---
title: "Full Circle Magazine 143"
date: 2019-03-29
draft: false
tags:
  - "bsd"
  - "darktable"
  - "freebsd"
  - "freeplane"
  - "gdpr"
  - "ghostbsd"
  - "inkscape"
  - "lubuntu"
  - "ota-8"
  - "ota8"
  - "python"
  - "touch"
  - "ubports"
  - "Ubuntu"
cover: covers/magazines/issue-143.webp
pdf: https://dl.fullcirclemagazine.org/issue143_en.pdf
epub: https://dl.fullcirclemagazine.org/issue143_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Freeplane, and Darktable
- Graphics : Inkscape
- Ubuntu Devices: OTA-8
- My Opinion: GDPR Pt3
- Linux Loopback: BSD
- Book Review: Practical Binary Analysis
- Interview: Simon Quigley (Lubuntu)
- Ubuntu Games: This Is The Police 2 plus: News, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue143_fr.pdf)
