---
title: "Full Circle Magazine 202"
date: 2024-02-23
draft: false
tags:
  - "python"
  - "inkscape"
  - "stable"
  - "diffusion"
  - "latex"
  - "xubuntu"
  - "uwuntu"
  - "mint"
  - "osx"
  - "ginsha"
  - "game"
  - "touch"
  - "ubports"
cover: "covers/magazines/issue-202.webp"
pdf: "https://dl.fullcirclemagazine.org/issue202_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue202_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion and Latex
- Graphics : Inkscape
- Review : Xubuntu 23.10 and Uwuntu
- UbPorts Touch : OTA-4
- My Story : OSX To Linux Mint
- Ubuntu Games - Ginsha

plus: News, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue202\_fr.pdf)
