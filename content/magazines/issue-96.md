---
title: "Full Circle Magazine 96"
date: 2015-04-24
draft: false
tags:
  - "arduino"
  - "BQ"
  - "chrome"
  - "cities"
  - "e4.5"
  - "inkscape"
  - "javascript"
  - "laptop"
  - "latex"
  - "libre"
  - "libreoffice"
  - "m3800"
  - "news"
  - "office"
  - "owncloud"
  - "phone"
  - "precision"
  - "python"
  - "skylines"
cover: covers/magazines/issue-96.webp
pdf: https://dl.fullcirclemagazine.org/issue96_en.pdf
epub: https://dl.fullcirclemagazine.org/issue96_en.epub
---

**This month**

- Command & Conquer
- How-To : Program in Python, LibreOffice, Using LaTeX, and [**NEW!**] Programming JavaScript
- Graphics : Inkscape.
- [**NEW!**] Chrome Cult
- Linux Labs: OwnCloud
- [**NEW!**] Ubuntu Phones - Interview with Cristian Parrino
- Review: Precision m3800 DE laptop
- Ubuntu Games: Cities: Skylines

Plus: News, Arduino, Q&A, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.
**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue96_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue96_hu.pdf)
