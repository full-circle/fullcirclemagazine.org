---
title: "Full Circle Magazine 101"
date: 2015-09-25
draft: false
tags:
  - "arduino"
  - "box"
  - "chrome"
  - "drupal"
  - "electronics"
  - "emulators"
  - "games"
  - "inkscape"
  - "monthly"
  - "ppa"
  - "principle"
  - "retro"
  - "software"
  - "talos"
  - "tron-club"
  - "tronclub"
  - "update"
  - "upgrade"
  - "versions"
  - "website"
cover: covers/magazines/issue-101.webp
pdf: https://dl.fullcirclemagazine.org/issue101_en.pdf
epub: https://dl.fullcirclemagazine.org/issue101_en.epub
---

**This month**

- Command & Conquer
- How-To : Install Newer Software Versions, LibreOffice, and Website With Infrastructure
- Graphics : Inkscape.
- Chrome Cult
- Linux Labs: Drupal 7
- Ubuntu Phones
- Ubuntu Games: Retro Games Emulators, and The Talos Principle

Plus: News, Arduino, Q&A, Security, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.
**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue101_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue101_hu.pdf)
- [Spanish PDF](https://dl.fullcirclemagazine.org/issue101_es.pdf)
