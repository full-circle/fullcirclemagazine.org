---
title: "Full Circle Magazine 138"
date: 2018-10-26
draft: false
tags:
  - "basic"
  - "chromebook"
  - "freeplane"
  - "godot"
  - "inkscape"
  - "tekken"
  - "ubuntu"
  - "waddle"
cover: covers/magazines/issue-138.webp
pdf: https://dl.fullcirclemagazine.org/issue138_en.pdf
epub: https://dl.fullcirclemagazine.org/issue138_en.epub
---

**This month**

- Command & Conquer
- How-To : Linux Apps on Chromebook, Freeplane, and Great Cow BASIC
- Graphics : Inkscape
- My Story
- Book Review: Godot Engine Game Development
- Ubuntu Games: Tekken 7

Plus: News, The Daily Waddle [**NEW!**], My Story, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue138_fr.pdf)
