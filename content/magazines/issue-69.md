---
title: "Full Circle Magazine 69"
date: 2013-01-25
draft: false
tags:
  - "android"
  - "blender"
  - "google"
  - "google tv"
  - "googletv"
  - "gtv"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "news"
  - "office"
  - "python"
  - "sony"
  - "tv"
cover: covers/magazines/issue-69.webp
pdf: https://dl.fullcirclemagazine.org/issue69_en.pdf
epub: https://dl.fullcirclemagazine.org/issue69_en.epub
---

**This month**

- Ubuntu News.
- How-To : Programming in Python, LibreOffice, and Ubuntu Gnome 2 Style.
- Graphics : Blender, and Inkscape.
- Review: Sony Google TV.

Plus: Q&A, Command & Conquer, Linux Labs, Ask The New Guy, My Story, and so much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAowkqr8Bg/full_circle_magazine_69_lite](http://www.google.com/producer/editions/CAowkqr8Bg/full_circle_magazine_69_lite)
Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)
Or, try the **MP3 audio edition**: [http://ubuntuone.com/23lhsOrnviLFxbGh2VIekr](https://web.archive.org/web/20130530224643/http://ubuntuone.com/23lhsOrnviLFxbGh2VIekr)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue69_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue69_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue69_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue69_it.pdf)
