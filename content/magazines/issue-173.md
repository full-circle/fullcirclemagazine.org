---
title: "Full Circle Magazine 173"
date: 2021-09-24
draft: false
tags:
  - "clone"
  - "games"
  - "inkscape"
  - "latex"
  - "linux"
  - "lite"
  - "lmms"
  - "micro"
  - "photofilmstrip"
  - "python"
  - "uefi"
  - "waddle"
cover: covers/magazines/issue-173.webp
pdf: https://dl.fullcirclemagazine.org/issue173_en.pdf
epub: https://dl.fullcirclemagazine.org/issue173_en.epub
---

**This month**

- Command & Conquer : LMMS
- How-To : Python, Latex and Portable UEFI Clone
- Graphics : Inkscape
- ~~Everyday Ubuntu~~
- Micro This Micro That
- Review : Linux Lite
- Review : Photofilmstrip
- Ubuntu Games : The Games People Play

Plus: News, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue173_fr.pdf)
