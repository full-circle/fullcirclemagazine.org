---
title: "Full Circle Magazine 66"
date: 2012-10-25
draft: false
tags:
  - "games"
  - "kdenlive"
  - "latex"
  - "libreoffice"
  - "nexus"
  - "torchlight"
  - "uefi"
  - "ubuntu"
cover: covers/magazines/issue-66.webp
pdf: https://dl.fullcirclemagazine.org/issue66_en.pdf
epub: https://dl.fullcirclemagazine.org/issue66_en.epub
---

**This month**

- How-To : Use LaTeX, LibreOffice Part 18, and Do Quick Reinstalls.
- Graphics : Kdenlive Part 3, and Inkscape Part 6.
- Web Dev : CRUD Part 2
- Reviews - Nexus 7 tablet & Peppermint Three OS.
- Closing Windows - File Associations (Open With).
- [**NEW!**] What Is - UEFI
- Ubuntu Games - Torchlight

Plus: Command & Conquer, Linux Labs, Ask The New Guy, Ubuntu Women, My Desktop, My Story, and much much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAow_crpAw/full_circle_magazine_66_lite](http://www.google.com/producer/editions/CAow_crpAw/full_circle_magazine_66_lite)
**EPUB Edition** coming soon...
Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue66_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue66_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue66_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue66_it.pdf)
