---
title: "Full Circle Magazine 191"
date: 2023-03-31
draft: false
tags:
  - "ubuntu"
  - "python"
  - "latex"
  - "stable"
  - "diffusion"
  - "inkscape"
  - "freecad"
  - "budgie"
  - "pardus"
  - "splendor"
  - "ubports"
  - "focal"
  
cover: "covers/magazines/issue-191.webp"
pdf: "https://dl.fullcirclemagazine.org/issue191_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue191_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion [NEW!] and Latex
- Graphics : Inkscape
- Graphics : FreeCAD
- Review : Ubuntu Budgie 22.10
- Review : Pardus
- Tabletop Ubuntu : Splendor
- Ubports Touch : Focal OTA-1

plus: News, My Opinion, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue191\_fr.pdf)
