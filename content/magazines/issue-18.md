---
title: "Full Circle Magazine 18"
date: 2008-11-02
draft: false
tags:
  - "8.10"
  - "c"
  - "drive"
  - "gimp"
  - "interview"
  - "management"
  - "mapping"
  - "mind"
  - "motu"
  - "network"
  - "package"
  - "secure"
  - "stephan hermann"
  - "urban terror"
cover: covers/magazines/issue-18.webp
pdf: https://dl.fullcirclemagazine.org/issue18_en.pdf
epub: https://dl.fullcirclemagazine.org/issue18_en.epub
---

**This month**

- Command and Conquer - Package Management.
- How-To : Program in C - Part 2, Secure Network Drive, Using GIMP - Part 7 and Ubuntu 8.10 Intrepid Ibex.
- My Story - Ubuntu And Me
- Review - Urban Terror
- My Opinion - How Many Distros Is Too Many?
- MOTU Interview - Stephan Hermann
- Top 5 - Mind Mapping Applications

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue18_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue18_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue18_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue18_it.pdf)
