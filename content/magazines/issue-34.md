---
title: "Full Circle Magazine 34"
date: 2010-02-27
draft: false
tags:
  - "acer"
  - "gimp"
  - "interview"
  - "laptop"
  - "motu"
  - "perfect"
  - "photo"
  - "pwerpets"
  - "python"
  - "reference"
  - "retouch"
  - "retouching"
  - "review"
  - "roderick greening"
  - "server"
  - "tools"
  - "ul30-a"
cover: covers/magazines/issue-34.webp
pdf: https://dl.fullcirclemagazine.org/issue34_en.pdf
epub: https://dl.fullcirclemagazine.org/issue34_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 8, Digitally Retouching a Photo in GIMP, and The Perfect Server - Part 4.
- My Story - a Linux User, and Powerpets.
- Review - Acer UL30-A laptop.
- MOTU Interview - Roderick Greening.
- Top 5 - Reference Tools.

Plus : Ubuntu Women, Ubuntu Games, My Opinion, and all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue34_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue34_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue34_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue34_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue34_ru.pdf)
