---
title: "Full Circle Magazine 167"
date: 2021-03-26
draft: false
tags:
  - "ares"
  - "audio"
  - "book"
  - "coco"
  - "electronics"
  - "entroware"
  - "fritzing"
  - "game"
  - "games"
  - "inkscape"
  - "latex"
  - "learn"
  - "lmms"
  - "micro"
  - "microcontroller"
  - "micropython"
  - "pi"
  - "pico"
  - "python"
  - "raspberry"
  - "ubuntu"
cover: covers/magazines/issue-167.webp
pdf: https://dl.fullcirclemagazine.org/issue167_en.pdf
epub: https://dl.fullcirclemagazine.org/issue167_en.epub
---

**This month**

- Command & Conquer : LMMS
- How-To : Python, Latex [**NEW!**] and Fritzing
- Graphics : Inkscape
- Linux Loopback : My Story
- Everyday Ubuntu : RetroComputing CoCo Nuts Pt2
- Micro This Micro That [**NEW!**]
- Review : Entroware Ares
- Book Review: Learn Linux Quickly
- Ubuntu Games : Nebuchadnezar

Plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue167_fr.pdf)
