---
title: "Full Circle Magazine 103"
date: 2015-11-27
draft: false
tags:
  - "3d"
  - "advent"
  - "bibliography"
  - "inkscape"
  - "latex"
  - "libre"
  - "libreoffice"
  - "office"
  - "ota-8"
  - "ota8"
  - "pi"
  - "printer"
  - "printing"
  - "programming"
  - "python"
  - "raspberry"
  - "touch"
cover: covers/magazines/issue-103.webp
pdf: https://dl.fullcirclemagazine.org/issue103_en.pdf
epub: https://dl.fullcirclemagazine.org/issue103_en.epub
---

**This month**

- Command & Conquer
- How-To : Python in the Real World, LibreOffice, LaTeX and Practice Programming
- Graphics : Inkscape.
- Chrome Cult
- Linux Labs: 3D Printer Building
- Ubuntu Phones: OTA-8

Plus: Ubuntu Games, News, Arduino, Book Review, Q&A, Security, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.
**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue103_fr.pdf)
