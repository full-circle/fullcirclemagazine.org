---
title: "Full Circle Magazine 29"
date: 2009-09-26
draft: false
tags:
  - "games"
  - "interview"
  - "Iulian Udrea"
  - "kompozer"
  - "lamp"
  - "motu"
  - "network"
  - "networking"
  - "physics"
  - "private"
  - "python"
  - "server"
  - "virtual"
  - "vpn"
cover: covers/magazines/issue-29.webp
pdf: https://dl.fullcirclemagazine.org/issue29_en.pdf
epub: https://dl.fullcirclemagazine.org/issue29_en.epub
---

**This month**

- Command and Conquer
- How-To: Program in Python - Part 3, LAMP Server - Part 2, Virtual Private Networking.
- My Story - One Man's Journey, and Walk With Ubuntu.
- Review - Kompozer.
- MOTU Interview - Iulian Udrea.
- Top 5 - Physics Games.

Plus : Ubuntu Games, as well as all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue29_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue29_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue29_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue29_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue29_ru.pdf)
