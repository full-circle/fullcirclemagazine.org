---
title: "Full Circle Magazine 95"
date: 2015-03-27
draft: false
tags:
  - "able2extract"
  - "aquaris"
  - "BQ"
  - "e4.5"
  - "golf"
  - "inkscape"
  - "latex"
  - "libre"
  - "molecules"
  - "necrologue"
  - "ocr"
  - "office"
  - "perfect"
  - "phone"
  - "python"
  - "syncthing"
  - "Ubuntu"
cover: covers/magazines/issue-95.webp
pdf: https://dl.fullcirclemagazine.org/issue95_en.pdf
epub: https://dl.fullcirclemagazine.org/issue95_en.epub
---

**This month**

- Command & Conquer
- How-To : Program in Python, LibreOffice, and Using LaTeX
- Graphics : Inkscape.
- Linux Labs: Syncthing
- Review: BQ Aquaris E4.5 Ubuntu Phone & Able2Extract Pro 9
- Competition: WIN a copy of Able2Extract Pro 9
- Ubuntu Games: Penumbra Necrologue & Perfect Golf
- My Story special on handling molecules in Linux

Plus: News, Arduino, Q&A, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.
**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue95_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue95_hu.pdf)
