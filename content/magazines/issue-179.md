---
title: "Full Circle Magazine 179"
date: 2022-03-25
draft: false
tags:
  - "ubuntu"
  - "blender"
  - "budgie"
  - "comics"
  - "hibernation"
  - "inkscape"
  - "micro"
  - "python"
  - "terminal"
  - "virtuaverse"
cover: "covers/magazines/issue-179.webp"
pdf: "https://dl.fullcirclemagazine.org/issue179_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue179_en.epub"
---

**This month:**

- Command & Conquer : Terminal
- How-To : Python, Blender and Hibernating Your Laptop
- Graphics : Inkscape
- Everyday Ubuntu : Comics
- Micro This Micro That
- Review : Ubuntu Budgie 21.10
- ~~My Opinion : The Origins Of The GUI~~
- ~~Ubports Touch~~
- Ubuntu Games : Virtuaverse

plus: News, Letters, My Story, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue179_fr.pdf)
