---
title: "Full Circle Magazine 19"
date: 2008-11-30
draft: false
tags:
  - "access"
  - "c"
  - "emilio monfort"
  - "found"
  - "gimp"
  - "interview"
  - "lost"
  - "mobile"
  - "motu"
  - "multimedia"
  - "open arena"
  - "point"
  - "quake"
  - "virtualisation"
  - "virtualization"
  - "wifi"
cover: covers/magazines/issue-19.webp
pdf: https://dl.fullcirclemagazine.org/issue19_en.pdf
epub: https://dl.fullcirclemagazine.org/issue19_en.epub
---

**This month**

- Command and Conquer - Lost and Found.
- How-To : Program in C - Part 3, Make a WiFi Access Point, Using GIMP - Part 8 and Create Mobile Multimedia.
- My Story - Ubuntu John
- Book Review - Beginning Ubuntu Linux 3rd Ed.
- MOTU Interview - Emilio Monfort
- Top 5 - Virtualization Tools
- [**NEW!**] Ubuntu Games - Open Arena

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue19_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue19_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue19_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue19_it.pdf)
