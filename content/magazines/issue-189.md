---
title: "Full Circle Magazine 189"
date: 2023-01-27
draft: false
tags:
  - "lubuntu"
  - "python"
  - "latex"
  - "inkscape"
  - "freecad"
  - "zorin"
  - "book"
  - "kids"
  - "ixion"
  - "waddle"
cover: "covers/magazines/issue-189.webp"
pdf: "https://dl.fullcirclemagazine.org/issue189_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue189_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Setup Multiple Hard Drives and Latex
- Graphics : Inkscape
- Graphics : FreeCAD [NEW!]
- Review : Lubuntu 22.10
- Review : Zorin OS Pro 16.1
- Book Review : Python for Kids
- Ubuntu Games : Ixion

  plus: News, My Opinion, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue189_fr.pdf)
