---
title: "Full Circle Magazine 24"
date: 2009-04-25
draft: false
tags:
  - "book"
  - "c"
  - "chinese"
  - "grandma"
  - "granny"
  - "inkscape"
  - "interview"
  - "james westby"
  - "knightwise"
  - "mame"
  - "motu"
  - "review"
  - "translations"
  - "unleashed"
cover: covers/magazines/issue-24.webp
pdf: https://dl.fullcirclemagazine.org/issue24_en.pdf
epub: https://dl.fullcirclemagazine.org/issue24_en.epub
---

**This month : it's our birthday issue! And, a redesign is in store!**
**Coming your way**

- Command and Conquer - Cron.
- How-To: Program in C - Part 8, Create a MAME Machine, and Spreading Ubuntu - Part 3 and Inkscape - Part 1.
- My Story - Great-grandma Goes Shopping and Chinese Translations
- Book Review - Ubuntu Unleashed, three copies up for grabs!
- MOTU Interview - James Westby
- Top 10 - Best of Top5, 2007-'09

Plus : all the usual goodness, doubled!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue24_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue24_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue24_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue24_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue24_ru.pdf)
