---
title: "Full Circle Magazine 159"
date: 2020-07-31
draft: false
tags:
  - "game"
  - "inkscape"
  - "krita"
  - "mable"
  - "mintcast"
  - "photography"
  - "podcast"
  - "python"
  - "rawtherapee"
  - "ubuntu"
  - "unity"
  - "wood"
  - "zfs"
cover: covers/magazines/issue-159.webp
pdf: https://dl.fullcirclemagazine.org/issue159_en.pdf
epub: https://dl.fullcirclemagazine.org/issue159_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Podcast Production, and Rawtherapee
- Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback
- Everyday Ubuntu
- Ubports Touch
- Review : Ubuntu Unity 20.04
- Ubuntu Games : Mable And The Wood

Plus: News, My Opinion, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue159_fr.pdf)
