---
title: "Full Circle Magazine 183"
date: 2022-07-29
draft: false
tags:
  - "python"
  - "latex"
  - "inkscape"
  - "lubuntu"
  - "slitaz"
  - "recursive"
  - "pico"
  - "waddle"
  - "22.04"
  - "waddle"
cover: "covers/magazines/issue-183.webp"
pdf: "https://dl.fullcirclemagazine.org/issue183_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue183_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, --Blender-- and Latex
- Graphics : Inkscape
- Review : Lubuntu 22.04
- Review : Slitaz 5
- Book Review : The Recursive Book of Recursion
- Ubuntu Games : Pico-8

plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue183_fr.pdf)
