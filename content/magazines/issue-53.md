---
title: "Full Circle Magazine 53"
date: 2011-10-01
draft: false
tags:
  - "backup"
  - "cctv"
  - "Circle"
  - "email"
  - "fcm"
  - "Full"
  - "fullcircle"
  - "gramps"
  - "libre"
  - "libreoffice"
  - "Linux"
  - "magazine"
  - "mint"
  - "news"
  - "office"
  - "python"
  - "Ubuntu"
  - "webcam"
  - "zoneminder"
cover: covers/magazines/issue-53.webp
pdf: https://dl.fullcirclemagazine.org/issue53_en.pdf
epub: https://dl.fullcirclemagazine.org/issue53_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 27, LibreOffice - Part 8, Backup Strategy - Part 1, GRAMPS - Part 2, and Ubuntu For Business & Education - Part 2.
- Linux Lab - ZoneMinder CCTV - Part 2.
- Review - Linux Mint 11.
- I Think - With the rise of internet email, do we still need an email client installed by default?

Plus: Ubuntu Games, My Story, and much much more!

PLEASE NOTE: Apologies for the blank columns in this issue. Richard is MIA, and I have the flu :(

- **Ronnie**

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue53_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue53_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue53_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue53_it.pdf)
