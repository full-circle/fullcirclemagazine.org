---
title: "Full Circle Magazine 32"
date: 2010-01-01
draft: false
tags:
  - "browser"
  - "center"
  - "centre"
  - "chromium"
  - "classroom"
  - "daemon"
  - "experience"
  - "install"
  - "interview"
  - "medai"
  - "motu"
  - "music"
  - "offline"
  - "package"
  - "perfect"
  - "player"
  - "python"
  - "roderick greening"
  - "server"
cover: covers/magazines/issue-32.webp
pdf: https://dl.fullcirclemagazine.org/issue32_en.pdf
epub: https://dl.fullcirclemagazine.org/issue32_en.epub
---

**This month**

- Command and Conquer.
- How-To: Program in Python - Part 6, The Perfect Server - Part 2, Installing Chromium (browser) and Offline Package Install.
- My Story - Classroom Experiences, and How I Became An Ubuntu Woman.
- My Opinion - Time To Review The Release Schedule? and Will Linux Ever Get It Right?
- Review - Music Player Daemon.
- MOTU Interview - Roderick Greening.
- Top 5 - Media Centers.

Plus : Ubuntu Women, Ubuntu Games and all the usual goodness!

**CORRECTION TO FCM#32** - The article on MPD should link to Lucas's example file, the contents of his example file is listed at: [http://ubuntuforums.org/showpost.php?p=8591879&postcount=2](http://ubuntuforums.org/showpost.php?p=8591879&postcount=2)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue32_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue32_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue32_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue32_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue32_ru.pdf)
