---
title: "Full Circle Magazine 88"
date: 2014-08-29
draft: false
tags:
  - "arduino"
  - "blender"
  - "boot"
  - "compile"
  - "dvd"
  - "games"
  - "grub"
  - "handbrake"
  - "inkscape"
  - "install"
  - "kernel"
  - "libre"
  - "libreoffice"
  - "loader"
  - "magazine"
  - "minimal"
  - "office"
  - "rip"
  - "ripper"
  - "security"
  - "Ubuntu"
cover: covers/magazines/issue-88.webp
pdf: https://dl.fullcirclemagazine.org/issue88_en.pdf
epub: https://dl.fullcirclemagazine.org/issue88_en.epub
---

**This month**

- Command & Conquer
- How-To : Minimal Ubuntu Install, LibreOffice, and GRUB2.
- Graphics : Blender and Inkscape
- Linux Labs: Ripping DVDs with Handdrake, and Compiling a Kernel
- Arduino

Plus: Q&A, Security, Ubuntu Games, and _soooo_ much more.

**ALSO**: Don't forget to [search for 'full circle magazine' on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker "Full Circle Magazine on Google Play/Books").

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue88_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue88_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue88_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue88_it.pdf)
