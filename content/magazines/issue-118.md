---
title: "Full Circle Magazine 118"
date: 2017-02-24
draft: false
tags:
  - "cryptup"
  - "inkscape"
  - "kdenlive"
  - "python"
  - "rclone"
  - "touch"
  - "Ubuntu"
cover: covers/magazines/issue-118.webp
pdf: https://dl.fullcirclemagazine.org/issue118_en.pdf
epub: https://dl.fullcirclemagazine.org/issue118_en.epub
---

**This month**

- Command & Conquer
- How-To : Python (Arduino), Keep Your Linux Trim, and Program For Ubuntu Touch
- Graphics : Inkscape & Kdenlive
- ChromeCult: CryptUP
- Linux Labs: Digital Video & Rclone
- Book Review: Wicked Cool Shell Scripts
- Ubuntu Games: Mad Max

Plus: News, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue118_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue118_hu.pdf)
