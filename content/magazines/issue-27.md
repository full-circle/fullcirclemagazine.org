---
title: "Full Circle Magazine 27"
date: 2009-08-02
draft: false
tags:
  - "amarok"
  - "apps"
  - "associate"
  - "djvu"
  - "file"
  - "home"
  - "inkscape"
  - "interview"
  - "motu"
  - "nathan handler"
  - "pdf"
  - "python"
  - "scan"
  - "screenshot"
  - "server"
  - "tools"
  - "type"
cover: covers/magazines/issue-27.webp
pdf: https://dl.fullcirclemagazine.org/issue27_en.pdf
epub: https://dl.fullcirclemagazine.org/issue27_en.epub
---

**This month**

- Command and Conquer
- How To: Program in Python - Part 1, Scan To PDF/DJVU, Associate A File Type
- Inkscape - Part 4.
- My Story - My Ubuntu Home Server.
- Review - Amarok 1.4.
- MOTU Interview - Nathan Handler.
- Top 5 - Screenshot Tools.

Plus : Ubuntu Women, Ubuntu Games plus all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue27_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue27_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue27_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue27_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue27_ru.pdf)
