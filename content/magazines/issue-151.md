---
title: "Full Circle Magazine 151"
date: 2019-11-29
draft: false
tags:
  - "automation"
  - "darktable"
  - "editing"
  - "editor"
  - "games"
  - "inkscape"
  - "krita"
  - "olive"
  - "photography"
  - "python"
  - "squiffy"
  - "video"
  - "xubuntu"
cover: covers/magazines/issue-151.webp
pdf: https://dl.fullcirclemagazine.org/issue151_en.pdf
epub: https://dl.fullcirclemagazine.org/issue151_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Automation, and Darktable
- Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback
- Everyday Ubuntu
- Review : Olive Video Editor
- Review : Xubuntu 19.10
- Linux Certified
- Ubuntu Games : Squiffy

Plus: News, My Story, The Daily Waddle, Q&A, Crossword and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue151_fr.pdf)
