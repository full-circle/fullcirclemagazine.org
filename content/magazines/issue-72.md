---
title: "Full Circle Magazine 72"
date: 2013-04-26
draft: false
tags:
  - "blender"
  - "book"
  - "gimp"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "network"
  - "news"
  - "office"
  - "python"
  - "review"
  - "settings"
  - "solydxk"
cover: covers/magazines/issue-72.webp
pdf: https://dl.fullcirclemagazine.org/issue72_en.pdf
epub: https://dl.fullcirclemagazine.org/issue72_en.epub
---

**This month**

- Ubuntu News.
- How-To : Programming in Python, LibreOffice, and Network Settings.
- Graphics : Blender, and Inkscape.
- Book Review: The Book of GIMP.
- Review: SolydXK

Plus: Q&A, Command & Conquer, Linux Labs, Ask The New Guy, My Story, and soooo much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAow07SYBw/full_circle_magazine_72_lite](http://www.google.com/producer/editions/CAow07SYBw/full_circle_magazine_72_lite)
Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue72_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue72_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue72_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue72_it.pdf)
