---
title: "Full Circle Magazine 100"
date: 2015-08-28
draft: false
tags:
  - "chrome"
  - "cloud"
  - "cobol"
  - "games"
  - "history"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "office"
  - "python"
  - "viruses"
  - "warthog"
  - "warty"
  - "website"
cover: covers/magazines/issue-100.webp
pdf: https://dl.fullcirclemagazine.org/issue100_en.pdf
epub: https://dl.fullcirclemagazine.org/issue100_en.epub
---

**This month**

- Our Great Ancestor: Warty Warthog
- Command & Conquer
- How-To : Python, LibreOffice, Website with Infrastructure, and Programming COBOL
- Graphics : Inkscape.
- Survey Results
- Chrome Cult
- Linux Labs: How I Learned To Love Ubuntu
- Site Review
- A Quick Look At: Linux in Industry, and the French Translation Team
- Ubuntu Phones
- [**NEW!**] Linux Loopback
- Ubuntu Games: The Current State of Linux Gaming

Plus: News, Q&A, and soooo much more.

We now have [several issues available for download on Google Play/Books](https://play.google.com/store/books/author?id=Ronnie+Tucker). If you like Full Circle, please leave a review.

**AND**: We have [a Pushbullet channel](https://www.pushbullet.com/channel?tag=fcm) which we hope will make it easier to automatically receive FCM on launch day.

**Feel free to upvote this issue on Reddit** [https://www.reddit.com/r/Ubuntu/comments/3iqy13/full_circle_magazine_releases_issue_100/](https://www.reddit.com/r/Ubuntu/comments/3iqy13/full_circle_magazine_releases_issue_100/)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue100_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue100_hu.pdf)
- [Spanish PDF](https://dl.fullcirclemagazine.org/issue100_es.pdf)
