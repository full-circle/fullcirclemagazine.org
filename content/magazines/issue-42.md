---
title: "Full Circle Magazine 42"
date: 2010-10-29
draft: false
tags:
  - "arch"
  - "kubuntu"
  - "moonos"
  - "organise"
  - "organize"
  - "photos"
  - "python"
  - "virtualise"
  - "virtualize"
cover: covers/magazines/issue-42.webp
pdf: https://dl.fullcirclemagazine.org/issue42_en.pdf
epub: https://dl.fullcirclemagazine.org/issue42_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 16, Virtualize Part 5 - Arch Linux, and Organise Your Photos.
- Review - MoonOS and Kubuntu 10.10.
- Top 5 - Alternatives To Gnome.
- Readers Survey 2010 Results!
- [**NEW!**] Linux Lab

Plus: Interviews, Ubuntu Games, My Opinion, My Story, and much much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue42_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue42_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue42_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue42_ru.pdf)
