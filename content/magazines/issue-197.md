---
title: "Full Circle Magazine 197"
date: 2023-09-29
draft: false
tags:
  - "python"
  - "stable"
  - "diffusion"
  - "inkscape"
  - "micro"
  - "unity"
  - "pikaos"
  - "virtualbox"
  - "littlewood"
  - "steam"
cover: "covers/magazines/issue-197.webp"
pdf: "https://dl.fullcirclemagazine.org/issue197_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue197_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion and Latex
- Graphics : Inkscape
- Micro This Micro That
- Review : Ubuntu  Unity 23.04 and PikaOS
- Ubuntu Games : Littlewood

plus: News, My Opinion, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue197\_fr.pdf)
