---
title: "Full Circle Magazine 37"
date: 2010-05-28
draft: false
tags:
  - "interview"
  - "lubuntu"
  - "managers"
  - "media"
  - "motu"
  - "python"
  - "screenlets"
  - "stefan lesicnik"
  - "streaming"
  - "top 5"
  - "window"
cover: covers/magazines/issue-37.webp
pdf: https://dl.fullcirclemagazine.org/issue37_en.pdf
epub: https://dl.fullcirclemagazine.org/issue37_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 11, Adding Screenlets, and Streaming Media.
- Review - Lubuntu.
- MOTU Interview - Stefan Lesicnik.
- Top 5 - Tiling Window Managers.

Plus: Ubuntu Women, Ubuntu Games, My Opinion, My Story, and all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue37_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue37_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue37_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue37_ru.pdf)
