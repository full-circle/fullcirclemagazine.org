---
title: "Full Circle Magazine 65"
date: 2012-09-26
draft: false
tags:
  - "crud"
  - "drive"
  - "inkscape"
  - "kdenlive"
  - "libre"
  - "libreoffice"
  - "mapping"
  - "network"
  - "news"
  - "office"
  - "python"
cover: covers/magazines/issue-65.webp
pdf: https://dl.fullcirclemagazine.org/issue65_en.pdf
epub: https://dl.fullcirclemagazine.org/issue65_en.epub
---

**This month**

- Command and Conquer.
- How-To : Python - Part 37, LibreOffice Part 18, and Use Encryption.
- Graphics : Kdenlive Part 2, and Inkscape Part 5.
- Web Dev : CRUD
- Linux Lab
- Book Review - Ubuntu Made Easy.
- Closing Windows - Mapping A Network Drive.

Plus: Ask The New Guy, Ubuntu Games, My Desktop, My Opinion, My Story, and much much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAow3qrhAw/full_circle_magazine_65_lite](http://www.google.com/producer/editions/CAow3qrhAw/full_circle_magazine_65_lite)
**EPUB Edition** coming soon...
**Also available via Issuu** [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue65_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue65_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue65_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue65_it.pdf)
