---
title: "Full Circle Magazine 70"
date: 2013-02-22
draft: false
tags:
  - "blender"
  - "cron"
  - "htc one s"
  - "inkscape"
  - "job"
  - "jobs"
  - "libre"
  - "libreoffice"
  - "news"
  - "office"
  - "python"
cover: covers/magazines/issue-70.webp
pdf: https://dl.fullcirclemagazine.org/issue70_en.pdf
epub: https://dl.fullcirclemagazine.org/issue70_en.epub
---

**This month**

- Ubuntu News.
- How-To : Programming in Python, LibreOffice, and Cron.
- Graphics : Blender, and Inkscape.
- Review: HTC One S.

Plus: Q&A, Command & Conquer, Linux Labs, Ask The New Guy, My Story, and so much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAowjPKEBw/full_circle_magazine_70_lite](http://www.google.com/producer/editions/CAowjPKEBw/full_circle_magazine_70_lite)
Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue70_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue70_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue70_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue70_it.pdf)
