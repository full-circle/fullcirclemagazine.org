---
title: "Full Circle Magazine 136"
date: 2018-08-31
draft: false
tags:
  - "basic"
  - "cow"
  - "cudatext"
  - "freeplane"
  - "gcb"
  - "great"
  - "inkscape"
  - "python"
cover: covers/magazines/issue-136.webp
pdf: https://dl.fullcirclemagazine.org/issue136_en.pdf
epub: https://dl.fullcirclemagazine.org/issue136_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Freeplane, and Great Cow Basic
- Graphics : Inkscape
- Everyday Ubuntu
- My Story
- Review: Cudatext

Plus: News, Q&A, and much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue136_fr.pdf)
