---
title: "Full Circle Magazine 170"
date: 2021-06-25
draft: false
tags:
  - "bibletime"
  - "boot"
  - "booting"
  - "defold"
  - "esp32"
  - "game"
  - "grub"
  - "inkscape"
  - "latex"
  - "Linux"
  - "lmms"
  - "lubuntu"
  - "menu"
  - "micro"
  - "news"
  - "python"
  - "ubuntu"
  - "waddle"
  - "xubuntu"
cover: covers/magazines/issue-170.webp
pdf: https://dl.fullcirclemagazine.org/issue170_en.pdf
epub: https://dl.fullcirclemagazine.org/issue170_en.epub
---

**This month**

- Command & Conquer : LMMS
- How-To : Python, Latex and Taming Your GRUB Menu
- Graphics : Inkscape
- My Opinion – Booting Ubuntu
- Everyday Ubuntu : BibleTime Pt3
- Micro This Micro That
- ~~Ubports Devices~~
- Review : Lubuntu 21.04
- Review : Xubuntu 21.04
- Ubuntu Games : Defold

Plus: News, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue170_fr.pdf)
