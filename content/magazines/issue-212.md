---
title: "Full Circle Magazine 212"
date: 2024-12-27
draft: false
tags:
  - "lxd"
  - "latex"
  - "inkscape"
  - "lubuntu"
  - "xubuntu"
  - "ubports"
  - "wagotabi"
cover: "covers/magazines/issue-212.webp"
pdf: "https://dl.fullcirclemagazine.org/issue212_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue212_en.epub"
---

This month:

- How-To : LXD and Latex
- Graphics : Inkscape
- Review : Lubuntu & Xubuntu 24.10
- UbPorts Touch
- Ubuntu Games - Wagotabi

plus: News, My Opinion, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue212_fr.pdf)
