---
title: "Full Circle Magazine 124"
date: 2017-08-25
draft: false
tags:
  - "arduino"
  - "desktop"
  - "fedora"
  - "freecad"
  - "inkscape"
  - "kdenlive"
  - "kodi"
  - "Linux"
  - "python"
  - "zfs"
cover: covers/magazines/issue-124.webp
pdf: https://dl.fullcirclemagazine.org/issue124_en.pdf
epub: https://dl.fullcirclemagazine.org/issue124_en.epub
---

**This month**

- Command & Conquer
- How-To : Python (Arduino), Intro To FreeCAD, and Backup With ZFS Snapshots
- Graphics : Inkscape & Kdenlive
- Researching With Linux
- KODI Room: Our KODI Setup Pt.1

Plus: News, Q&A, My Desktop, and much more.

**NOTE**
We're running **VERY** low on articles.
Please consider writing a HowTo, a game/software/hardware/book review, or anything else you think suitable.
Send it to: [ronnie@fullcirclemagazine.org](mailto:ronnie@fullcirclemagazine.org)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue124_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue124_hu.pdf)
