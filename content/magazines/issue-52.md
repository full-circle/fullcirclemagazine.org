---
title: "Full Circle Magazine 52"
date: 2011-08-26
draft: false
tags:
  - "audacity"
  - "business"
  - "cctv"
  - "chrome"
  - "chromebook"
  - "dev"
  - "development"
  - "education"
  - "gramps"
  - "libre"
  - "libreoffice"
  - "news"
  - "office"
  - "python"
  - "zoneminder"
cover: covers/magazines/issue-52.webp
pdf: https://dl.fullcirclemagazine.org/issue52_en.pdf
epub: https://dl.fullcirclemagazine.org/issue52_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 26, LibreOffice - Part 7, Ubuntu Development - Part 4, GRAMPS - Part 1, and Ubuntu For Business & Education.
- Linux Lab - ZoneMinder CCTV - Part 1.
- Review - Chrombook.
- I Think - Would you like to see an audio editing series with Audacity?

Plus: Ubuntu Games, My Story, and much much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue52_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue52_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue52_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue52_it.pdf)
