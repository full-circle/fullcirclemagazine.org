---
title: "Full Circle Magazine 155"
date: 2020-03-27
draft: false
tags:
  - "asciiker"
  - "bsd"
  - "inkscape"
  - "krita"
  - "nas"
  - "nomadBSD"
  - "photography"
  - "pim"
  - "python"
  - "qnap"
  - "rawtherapee"
  - "security"
  - "Ubuntu"
cover: covers/magazines/issue-155.webp
pdf: https://dl.fullcirclemagazine.org/issue155_en.pdf
epub: https://dl.fullcirclemagazine.org/issue155_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Ubuntu & Security, and Rawtherapee**[1]** [**NEW!**]
- Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback: nomadBSD
- Everyday Ubuntu
- Review : QNAP NAS
- Ubuntu Games : Asciiker
  Plus: News, My Opinion, The Daily Waddle, Q&A, and more.

**[1]** New link for downloading the Rawtherapee example image (now hosted here): [https://bit.ly/2WNVfPf](https://bit.ly/2WNVfPf)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue155_fr.pdf)
