---
title: "Full Circle Magazine 129"
date: 2018-01-26
draft: false
tags:
  - "basic"
  - "cow"
  - "freecad"
  - "gcb"
  - "great"
  - "inkscape"
  - "news"
  - "online"
  - "pymol"
cover: covers/magazines/issue-129.webp
pdf: https://dl.fullcirclemagazine.org/issue129_en.pdf
epub: https://dl.fullcirclemagazine.org/issue129_en.epub
---

**This month**

- Command & Conquer
- How-To : PyMOL, Intro To FreeCAD, and Great Cow Basic [**NEW!**]
- Graphics : Inkscape
- Everyday Linux [**NEW!**]
- Researching With Linux
- My Opinion & My Story
- Review: Online Apps & Services

Plus: News, Q&A, and much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue129_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue129_hu.pdf)
