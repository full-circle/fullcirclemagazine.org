---
title: "Full Circle Magazine 59"
date: 2012-03-30
draft: false
tags:
  - "bodhi"
  - "cards"
  - "data"
  - "distro"
  - "emag"
  - "fcm59"
  - "foremost"
  - "greeting"
  - "libre"
  - "libreoffice"
  - "Linux"
  - "mag"
  - "magazine"
  - "manager"
  - "office"
  - "portable"
  - "python"
  - "recovery"
  - "task"
  - "trine 2"
  - "vbox"
  - "virtualbox"
  - "xp"
cover: covers/magazines/issue-59.webp
pdf: https://dl.fullcirclemagazine.org/issue59_en.pdf
epub: https://dl.fullcirclemagazine.org/issue59_en.epub
---

**This month**

- Command and Conquer.
- How-To : Beginning Python - Part 31, LibreOffice - Part 13, Portable Linue, Resizing Your VirtualBox Drive, and Create Your Own Greeting Card.
- Linux Lab - Foremost for data recovery.
- Review - Bodhi Linux
- I Think - What turns to/from a distro?
- Closing Windows - Task Manager

Plus: more Ubuntu Games, My Desktop, My Opinion, My Story, and much much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAownNt7/full_circle_magazine_59_lite](http://www.google.com/producer/editions/CAownNt7/full_circle_magazine_59_lite)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue59_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue59_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue59_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue59_it.pdf)
