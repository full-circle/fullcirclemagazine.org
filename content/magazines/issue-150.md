---
title: "Full Circle Magazine 150"
date: 2019-10-25
draft: false
tags:
  - "automation"
  - "bodhi"
  - "book"
  - "crossword"
  - "darktable"
  - "featherpad"
  - "game"
  - "inkscape"
  - "interview"
  - "Linux"
  - "pathway"
  - "photography"
  - "pi"
  - "python"
  - "raspberry"
  - "story"
  - "Ubuntu"
cover: covers/magazines/issue-150.webp
pdf: https://dl.fullcirclemagazine.org/issue150_en.pdf
epub: https://dl.fullcirclemagazine.org/issue150_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Automation, and Darktable
- Graphics : Inkscape
- Linux Loopback
- Everyday Ubuntu
- Book Review : Developing Games on the Raspberry Pi
- Review : Bodhi Linux
- Interview : Tsu Jan - FeatherPad Dev
- Linux Certified
- Ubuntu Games : Pathway

Plus: News, My Story, The Daily Waddle, Q&A, Crossword and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue150_fr.pdf)
