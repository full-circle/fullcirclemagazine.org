---
title: "Full Circle Magazine 115"
date: 2016-11-25
draft: false
tags:
  - "command"
  - "freepascal"
  - "inkscape"
  - "kodi"
  - "line"
  - "pascal"
  - "python"
  - "recycling"
  - "rocket league"
  - "smtp"
cover: covers/magazines/issue-115.webp
pdf: https://dl.fullcirclemagazine.org/issue115_en.pdf
epub: https://dl.fullcirclemagazine.org/issue115_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Command Line SMTP, and Program With FreePascal
- Graphics : Inkscape
- Linux Labs: Computer Recycling at The Working Centre
- [**NEW!**] KODI Room: Tips
- Book Review: Object Orientated Programming with ANSI-C
- Ubuntu Games: Rocket League

Plus: News, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue115_fr.pdf)
