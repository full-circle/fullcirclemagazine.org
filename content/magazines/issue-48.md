---
title: "Full Circle Magazine 48"
date: 2011-04-22
draft: false
tags:
  - "arduino"
  - "ebooks"
  - "libre"
  - "libreoffice"
  - "management"
  - "office"
  - "python"
  - "remastersys.project"
  - "swapiness"
  - "tools"
cover: covers/magazines/issue-48.webp
pdf: https://dl.fullcirclemagazine.org/issue48_en.pdf
epub: https://dl.fullcirclemagazine.org/issue48_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 22, LibreOffice - Part 3, Finding eBooks and Using an Arduino in Ubuntu.
- Linux Lab - Swappiness Part One.
- Review - Remastersys.
- Top 5 - Project Management Tools.

Plus: Ubuntu Women, Ubuntu Games, My Story, and much much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue48_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue48_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue48_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue48_it.pdf)
