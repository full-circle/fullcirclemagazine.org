---
title: "Full Circle Magazine 80"
date: 2013-12-27
draft: false
tags:
  - "book"
  - "Circle"
  - "e-book"
  - "ebook"
  - "fcm"
  - "FREE"
  - "Full"
  - "full circle"
  - "kubuntu"
  - "Linux"
  - "lubuntu"
  - "mag"
  - "magazine"
  - "Ubuntu"
  - "xubuntu"
cover: covers/magazines/issue-80.webp
pdf: https://dl.fullcirclemagazine.org/issue80_en.pdf
epub: https://dl.fullcirclemagazine.org/issue80_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, and Use VLM.
- Graphics : JPG>PDF, and Inkscape.
- Review: USB Microscope

Plus: Q&A, Linux Labs, Software Showdown, Ask The New Guy, My Story, and soooo much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue80_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue80_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue80_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue80_it.pdf)
