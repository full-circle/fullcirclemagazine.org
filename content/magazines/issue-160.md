---
title: "Full Circle Magazine 160"
date: 2020-08-28
draft: false
tags:
  - "audacity"
  - "chome"
  - "chromeos"
  - "gallium"
  - "galliumos"
  - "game"
  - "inkscape"
  - "kde"
  - "krita"
  - "mumble"
  - "photography"
  - "podcast"
  - "podcasting"
  - "production"
  - "python"
  - "rawtherapee"
cover: covers/magazines/issue-160.webp
pdf: https://dl.fullcirclemagazine.org/issue160_en.pdf
epub: https://dl.fullcirclemagazine.org/issue160_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Podcast Production, and Rawtherapee
- Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback
- Everyday Ubuntu
- Review : GalliumOS 3.1
- Ubuntu Games : Tonight We Riot

Plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue160_fr.pdf)
