---
title: "Full Circle Magazine 152"
date: 2019-12-27
draft: false
tags:
  - "book"
  - "cad"
  - "darktable"
  - "finalcrypt"
  - "google"
  - "inkscape"
  - "interview"
  - "krita"
  - "oxygen"
  - "photography"
  - "python"
  - "retro"
  - "review"
  - "stadia"
cover: covers/magazines/issue-152.webp
pdf: https://dl.fullcirclemagazine.org/issue152_en.pdf
epub: https://dl.fullcirclemagazine.org/issue152_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, CAD Sizes, and Darktable
- Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback
- Everyday Ubuntu
- Book Review : Linux Inside
- Review : Stadia
- Interview: Ron de Jong (FinalCrypt Dev)
- Linux Certified
- Ubuntu Games : Oxygen Not Included

Plus: News, My Story, The Daily Waddle, Q&A, Crossword and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue152_fr.pdf)
