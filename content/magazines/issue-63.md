---
title: "Full Circle Magazine 63"
date: 2012-07-27
draft: false
tags:
  - "boot"
  - "cc"
  - "dev"
  - "development"
  - "faster"
  - "files"
  - "folders"
  - "gimp"
  - "gpodder"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "music"
  - "news"
  - "office"
  - "photo"
  - "python"
  - "retro"
  - "share"
  - "tracks"
  - "web"
cover: covers/magazines/issue-63.webp
pdf: https://dl.fullcirclemagazine.org/issue63_en.pdf
epub: https://dl.fullcirclemagazine.org/issue63_en.epub
---

**This month**

- Command and Conquer.
- How-To : Beginning Python - Part 35, LibreOffice Part 16, and Linux Astronomy Part 2.
- Graphics : GIMP Retro Photo, and Inkscape Part 3.
- [**NEW!**] Web Dev series
- Linux Lab - Making
- Ubuntu 12.04 Boot Faster.
- Review - gPodder.
- Closing Windows - Share Files/Folders.
- Audio Flux - some CC music tracks.
- [**NEW!**] Ask The New Guy.

Plus: Ubuntu Games, Ubuntu Women, My Desktop, My Opinion, My Story, and much much more!

**NOTE**: PDF is updated to now include the previously missing Sudoku solution.
**Google Currents Edition** [http://www.google.com/producer/editions/CAow4cXNAg/full_circle_magazine_63_lite](http://www.google.com/producer/editions/CAow4cXNAg/full_circle_magazine_63_lite)
**EPUB Edition** coming soon...

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue63_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue63_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue63_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue63_it.pdf)
