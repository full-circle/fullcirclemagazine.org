---
title: "Full Circle Magazine 166"
date: 2021-02-26
draft: false
tags:
  - "audacity"
  - "audio"
  - "budget"
  - "cyber"
  - "shadow"
  - "games"
  - "gaming"
  - "inkscape"
  - "Linux"
  - "lmms"
  - "news"
  - "office"
  - "podcast"
  - "podcasting"
  - "production"
  - "python"
  - "retrocomputing"
  - "ubuntu"
  - "ubuntu"
  - "web"
  - "unetbootin"
cover: covers/magazines/issue-166.webp
pdf: https://dl.fullcirclemagazine.org/issue166_en.pdf
epub: https://dl.fullcirclemagazine.org/issue166_en.epub
---

**This month**

- Command & Conquer : LMMS
- How-To : Python, Podcast Production, and Make a Budget
- Graphics : Inkscape
- Linux Loopback
- Everyday Ubuntu : RetroComputing TRS-80
- Review : Ubuntu Web 20.04.1 & Unetbootin
- Ubuntu Games : Cyber Shadow

Plus: News, My Opinion, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue166_fr.pdf)
