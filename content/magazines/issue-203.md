---
title: "Full Circle Magazine 203"
date: 2024-03-29
draft: false
tags:
  - "ubuntu"
  - "python"
  - "stable"
  - "diffusion"
  - "latex"
  - "unity"
  - "cubuntu"
  - "touchdown"
  - "ubports"
  - "openoffice"
  - "office"
  - "bzzt"
  - "micro"
cover: "covers/magazines/issue-203.webp"
pdf: "https://dl.fullcirclemagazine.org/issue203_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue203_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion and Latex
- Graphics : Inkscape
- Review : Ubuntu Unity 23.10 and Cubuntu
- UbPorts Touch : TouchDown
- My Story : OPENOFFICE
- Ubuntu Games - Bzzt!

plus: News, Micro This, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue203\_fr.pdf)
