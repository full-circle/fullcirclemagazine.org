---
title: "Full Circle Magazine 55"
date: 2011-11-25
draft: false
tags:
  - "audacity"
  - "backbox"
  - "backtrack"
  - "control"
  - "device"
  - "home"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "manager"
  - "news"
  - "office"
  - "panel"
  - "python"
  - "quick"
  - "server"
  - "voip"
  - "windows"
cover: covers/magazines/issue-55.webp
pdf: https://dl.fullcirclemagazine.org/issue55_en.pdf
epub: https://dl.fullcirclemagazine.org/issue55_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 29, LibreOffice - Part 9, Backup Strategy - Part 3, Audacity Basics, and Quick Home Server.
- Linux Lab - VOIP at Home.
- Review - BackTrack vs BackBox.
- I Think - Did you update your current install or do a fresh install?
- Closing Windows - Control Panel and Device Manager

Plus: Ubuntu Women, Ubuntu Games, My Desktop, My Story, and much much more!

**Google Currents Edition**: [http://www.google.com/producer/editions/CAow7dpE/full_circle_magazine_55_lite](http://www.google.com/producer/editions/CAow7dpE/full_circle_magazine_55_lite)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue55_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue55_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue55_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue55_it.pdf)
