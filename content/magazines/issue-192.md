---
title: "Full Circle Magazine 192"
date: 2023-04-28
draft: false
tags:
  - "ubuntu"
  - "python"
  - "stable"
  - "diffusion"
  - "latex"
  - "inkscape"
  - "freecad"
  - "unity"
  - "spirallinux"
  - "apple"
  - "monster prom"
  
cover: "covers/magazines/issue-192.webp"
pdf: "https://dl.fullcirclemagazine.org/issue192_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue192_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion [NEW!] and Latex
- Graphics : Inkscape
- Graphics : FreeCAD
- Review : Ubuntu Unity 22.10
- Review : SpiralLinux
- My Opinion : Is Apple Still Relevant For Linux Users?
- Ubuntu Games : Monster Prom 3 Roadtrip

plus: News, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue192\_fr.pdf)

