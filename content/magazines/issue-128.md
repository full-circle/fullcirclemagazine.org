---
title: "Full Circle Magazine 128"
date: 2017-12-29
draft: false
tags:
  - "basic"
  - "cow"
  - "diluvion"
  - "emmabuntus"
  - "freecad"
  - "gcb"
  - "great"
  - "greatcowbasic"
  - "inkscape"
  - "tmux"
cover: covers/magazines/issue-128.webp
pdf: https://dl.fullcirclemagazine.org/issue128_en.pdf
epub: https://dl.fullcirclemagazine.org/issue128_en.epub
---

**Everyone at Full Circle would like to wish all our readers a very happy new year, and all the best for 2018.**

**This month**

- Command & Conquer
- How-To : tmux, Intro To FreeCAD, and Great Cow Basic [**NEW!**]
- Graphics : Inkscape
- Researching With Linux
- My Opinion & My Story
- Review: Emmabuntus 2
- Ubuntu Games: Diluvion

Plus: News, Q&A, and much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue128_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue128_hu.pdf)
