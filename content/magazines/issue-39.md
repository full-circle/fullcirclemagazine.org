---
title: "Full Circle Magazine 39"
date: 2010-07-30
draft: false
tags:
  - "documentation"
  - "fedora"
  - "ipad"
  - "irobot"
  - "memory"
  - "motu"
  - "python"
  - "virtual"
  - "virtualise"
  - "virtualize"
cover: covers/magazines/issue-39.webp
pdf: https://dl.fullcirclemagazine.org/issue39_en.pdf
epub: https://dl.fullcirclemagazine.org/issue39_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 13, Virtualize - Fedora 13, and Understand Virtual Memory.
- Review - iRobot iPad.
- Top 5 - Documentation Sites.

Plus: MOTU Interview, Ubuntu Games, My Opinion, My Story, and now with all new LoCo and Translation Team interviews!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue39_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue39_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue39_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue39_ru.pdf)
