---
title: "Full Circle Magazine 77"
date: 2013-09-27
draft: false
tags:
  - "blender"
  - "digikam"
  - "elementary"
  - "inkscape"
  - "kubuntu"
  - "lazarus"
  - "libre"
  - "lubuntu"
  - "mint"
  - "office"
  - "os"
  - "pascal"
  - "python"
  - "Ubuntu"
  - "xbmc"
  - "xubuntu"
cover: covers/magazines/issue-77.webp
pdf: https://dl.fullcirclemagazine.org/issue77_en.pdf
epub: https://dl.fullcirclemagazine.org/issue77_en.epub
---

**This month**

- Ubuntu News
- Command & Conquer
- How-To : Python, LibreOffice, and Lazarus.
- Graphics : Blender, and Inkscape.
- Review: Elementary OS
- Book Review: Instant XBMC

Plus: Q&A, Linux Labs, Ask The New Guy, My Story, and soooo much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAowgMHIBw/full_circle_magazine_77_lite](http://www.google.com/producer/editions/CAowgMHIBw/full_circle_magazine_77_lite)
Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue77_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue77_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue77_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue77_it.pdf)
