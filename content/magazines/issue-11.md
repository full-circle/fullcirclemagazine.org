---
title: "Full Circle Magazine 11"
date: 2008-03-28
draft: false
tags:
  - "3000"
  - "amarok"
  - "c200"
  - "classic"
  - "ipod"
  - "laptop"
  - "latex"
  - "lenovo"
  - "mint"
  - "review"
  - "truecrypt"
cover: covers/magazines/issue-11.webp
pdf: https://dl.fullcirclemagazine.org/issue11_en.pdf
epub: https://dl.fullcirclemagazine.org/issue11_en.epub
---

**This issue contains...**

- Linux Mint vs. Ubuntu
- TrueCrypt on Ubuntu, iPod Classic + Amarok, Introduction to LaTeX, and more!
- Lenovo 3000 C200 and Ubuntu
- Top 5, My Desktop, and more!

**Correction: p.12 - ligaguitils1-dev should be libsgutils1-dev**

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue11_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue11_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue11_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue11_it.pdf)
