---
title: "Full Circle Magazine 44"
date: 2010-12-31
draft: false
tags:
  - "bittorrent"
  - "phurl"
  - "python"
  - "quake"
  - "unetbootin"
  - "wubi"
cover: covers/magazines/issue-44.webp
pdf: https://dl.fullcirclemagazine.org/issue44_en.pdf
epub: https://dl.fullcirclemagazine.org/issue44_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 18, Backup with Wubi & Link Shortening with Phurl.
- Review - Unetbootin.
- Top 5 - BitTorrent Clients.

Plus: Interviews, Ubuntu Games, My Opinion, My Story, and **much** much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue44_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue44_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue44_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue44_it.pdf)
