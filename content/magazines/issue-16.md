---
title: "Full Circle Magazine 16"
date: 2008-08-30
draft: false
tags:
  - "create"
  - "creating"
  - "files"
  - "gimp"
  - "gnome"
  - "interview"
  - "jamie strandboge"
  - "kde"
  - "motu"
  - "moving"
  - "server"
  - "theme"
  - "themes"
  - "twitter"
cover: covers/magazines/issue-16.webp
pdf: https://dl.fullcirclemagazine.org/issue16_en.pdf
epub: https://dl.fullcirclemagazine.org/issue16_en.epub
---

**This month**

- Command and Conquer - Creating And Moving Files.
- How-To: Create Your Own Ubuntu, Create Your Own Server Part 8, Using GIMP Part 5 and GNOME-Look Guide.
- My Story - Out With The New, In With The Old
- My Opinion - GNOME And KDE Themes
- MOTU Interview - Jamie Strandboge
- Top 5 - Twitter Clients

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue16_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue16_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue16_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue16_it.pdf)
