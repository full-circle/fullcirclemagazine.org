---
title: "Full Circle Magazine 106"
date: 2016-02-26
draft: false
tags:
  - "3d"
  - "apricity"
  - "arduino"
  - "dying light"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "m10"
  - "migrating"
  - "network"
  - "office"
  - "printer"
  - "tablet"
  - "usb"
  - "vax"
cover: covers/magazines/issue-106.webp
pdf: https://dl.fullcirclemagazine.org/issue106_en.pdf
epub: https://dl.fullcirclemagazine.org/issue106_en.epub
---

**This month**

- Command & Conquer
- How-To : Run from USB, LibreOffice, and Migrating From VAX
- Graphics : Inkscape
- Chrome Cult: Apricity OS
- Linux Labs: Building A 3D Printer and My Home Network
- Ubuntu Devices: BQ M10 Tablet
- Book Review: How Software Works
- Ubuntu Games: Dying Light

Plus: News, Arduino, Q&A, and soooo much more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue106_fr.pdf)
