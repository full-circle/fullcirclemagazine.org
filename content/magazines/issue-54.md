---
title: "Full Circle Magazine 54"
date: 2011-10-28
draft: false
tags:
  - "apt-cache"
  - "apt-get"
  - "backup"
  - "business"
  - "cctv"
  - "computer"
  - "drive"
  - "education"
  - "freemind"
  - "python"
  - "synergy"
  - "webcam"
  - "zoneminder"
cover: covers/magazines/issue-54.webp
pdf: https://dl.fullcirclemagazine.org/issue54_en.pdf
epub: https://dl.fullcirclemagazine.org/issue54_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 28, FreeMind, Backup Strategy - Part 2, Apt-Cache NG, and Ubuntu For Business & Education - Part 3.
- Linux Lab - ZoneMinder CCTV - Part 3.
- Review - Synergy.
- I Think - What age do you think the average Linux user is?
- [**NEW!**]  **Closing Windows** (Linux equivalents to Windows features) - My Computer and C:\\ drive

Plus: My Desktop, My Story, and much much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue54_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue54_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue54_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue54_it.pdf)
