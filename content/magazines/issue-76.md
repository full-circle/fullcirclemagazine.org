---
title: "Full Circle Magazine 76"
date: 2013-08-30
draft: false
tags:
  - "blender"
  - "book"
  - "docs"
  - "drive"
  - "google"
  - "inkscape"
  - "integration"
  - "libre"
  - "libreoffice"
  - "news"
  - "office"
  - "python"
  - "review"
  - "spring"
cover: covers/magazines/issue-76.webp
pdf: https://dl.fullcirclemagazine.org/issue76_en.pdf
epub: https://dl.fullcirclemagazine.org/issue76_en.epub
---

**This month**

- Ubuntu News
- Command & Conquer
- How-To : Python, LibreOffice, and Spring Integration.
- Graphics : Blender, and Inkscape.
- Review: Google Docs/Drive
- Book Review: Blender Master Class

Plus: Q&A, Linux Labs, Ask The New Guy, My Story, and soooo much more!

**Google Currents Edition** [http://www.google.com/producer/editions/CAow2NnBBw/full_circle_magazine_76_lite](http://www.google.com/producer/editions/CAow2NnBBw/full_circle_magazine_76_lite)
Also available via **Issuu**: [http://issuu.com/fullcirclemagazine](http://issuu.com/fullcirclemagazine)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue76_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue76_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue76_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue76_it.pdf)
