---
title: "Full Circle Magazine 51"
date: 2011-07-29
draft: false
tags:
  - "clients"
  - "creating"
  - "dev"
  - "development"
  - "distro"
  - "distros"
  - "gramps"
  - "kde"
  - "libre"
  - "libreoffice"
  - "office"
  - "own"
  - "python"
  - "repo"
  - "repository"
  - "running"
  - "software"
  - "voip"
cover: covers/magazines/issue-51.webp
pdf: https://dl.fullcirclemagazine.org/issue51_en.pdf
epub: https://dl.fullcirclemagazine.org/issue51_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 25, LibreOffice - Part 6, Ubuntu Development - Part 3, and Using KDE (4.6) Part 2.
- Linux Lab - Creating Your Own Repository.
- Review - GRAMPS.
- Top 5 - VOIP Clients.
- I Think - What Distro(s) Do You Use?

Plus: Ubuntu Games, My Story, and much much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue51_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue51_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue51_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue51_it.pdf)
