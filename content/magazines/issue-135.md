---
title: "Full Circle Magazine 135"
date: 2018-07-29
draft: false
tags:
  - "cryptomator"
  - "encrypt"
  - "freeplane"
  - "inkscape"
  - "minecraft"
  - "minetest"
  - "nnn"
  - "python"
  - "touch"
  - "ubports"
  - "warcraft"
cover: covers/magazines/issue-135.webp
pdf: https://dl.fullcirclemagazine.org/issue135_en.pdf
epub: https://dl.fullcirclemagazine.org/issue135_en.epub
---

This month: \* Command & Conquer \* How-To : Python, Freeplane, and Encrypt With Cryptomator \* Graphics : Inkscape \* Everyday Linux \* Researching With Linux \* My Story \* Review: nnn \* Ubuntu Games: Warcraft III pt.2 and Make Minetest Like Minecraft \* Ubuntu Devices: UBports Touch Update plus: News, Q&A, and much more.

---

**HELP SUPPORT FULL CIRCLE MAGAZINE**
[![](images/patreon-logo-e1511553774539.png)](https://www.patreon.com/fullcirclemagazine) [![](images/PayPal-Donate-Button-PNG-HD-e1511553955245.png)](https://paypal.me/ronnietucker)
[![](images/donorbox-double-the-donation-partner.webp)](https://donorbox.org/recurring-monthly-donation)

---

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue135_fr.pdf)
