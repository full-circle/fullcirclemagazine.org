---
title: "Full Circle Magazine 158"
date: 2020-06-26
draft: false
tags:
  - "20.04"
  - "bsd"
  - "game"
  - "inkscape"
  - "into"
  - "the"
  - "breach"
  - "krita"
  - "kubuntu"
  - "photography"
  - "python"
  - "rawtherapee"
  - "tablet"
  - "ubuntu"
  - "xubuntu"
cover: covers/magazines/issue-158.webp
pdf: https://dl.fullcirclemagazine.org/issue158_en.pdf
epub: https://dl.fullcirclemagazine.org/issue158_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, Ubuntu On a 2-in-1 Tablet, and Rawtherapee
  - Graphics : Inkscape
- Graphics : Krita for Old Photos
- Linux Loopback
- Everyday Ubuntu : Starting Again
- Ubports Touch
- Review : Kubuntu, and Xubuntu 20.04
- Ubuntu Games : Into The Breach

Plus: News, My Opinion, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue158_fr.pdf)
