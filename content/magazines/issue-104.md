---
title: "Full Circle Magazine 104"
date: 2015-12-25
draft: false
tags:
  - "arduino"
  - "book"
  - "chrome"
  - "command"
  - "controller"
  - "install"
  - "libre"
  - "libreoffice"
  - "link"
  - "math with python"
  - "mint"
  - "nikscape"
  - "office"
  - "ota"
  - "python"
  - "raid"
  - "raid0"
  - "review"
  - "spam nation"
  - "steam"
  - "top"
  - "touch"
cover: covers/magazines/issue-104.webp
pdf: https://dl.fullcirclemagazine.org/issue104_en.pdf
epub: https://dl.fullcirclemagazine.org/issue104_en.epub
---

**This holiday month**

- Command & Conquer
- How-To : Python in the Real World, LibreOffice, Install Mint on Raid 0 and Using The TOP Command
- Graphics : Inkscape.
- Chrome Cult: Security
- Linux Labs: Building A 3D Printer Pt.1
- Ubuntu Phones: OTA-8.5
- Book Review: Spam Nation and Doing Math With Python
- Ubuntu Games: Steam Link and Controller

Plus: News, Arduino, Q&A, Security, and soooo much more.

**Happy holidays to everyone out there in Ubuntu land from all of us at Full Circle.**

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue104_fr.pdf)
