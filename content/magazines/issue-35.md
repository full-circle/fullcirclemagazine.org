---
title: "Full Circle Magazine 35"
date: 2010-03-27
draft: false
tags:
  - "android"
  - "applications"
  - "digital"
  - "droid"
  - "gimp"
  - "google"
  - "interview"
  - "milestone"
  - "motorola"
  - "motu"
  - "pedro fragoso"
  - "photo"
  - "python"
  - "retouch"
  - "retouching"
  - "sketchup"
  - "wine"
cover: covers/magazines/issue-35.webp
pdf: https://dl.fullcirclemagazine.org/issue35_en.pdf
epub: https://dl.fullcirclemagazine.org/issue35_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 9, Digitally Retouching a Photo in GIMP - Part 2, and Installing Google SketchUp using Wine.
- Review - Motorola Milestone/Droid.
- MOTU Interview - Pedro Fragoso.
- Top 5 - Android Applications.

Plus : Ubuntu Women, Ubuntu Games, My Opinion, My Story, and all the usual goodness!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue35_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue35_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue35_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue35_ru.pdf)
