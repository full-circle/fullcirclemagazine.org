---
title: "Full Circle Magazine 57"
date: 2012-01-27
draft: false
tags:
  - "enlightenment"
  - "libre"
  - "libreoffice"
  - "mana"
  - "network"
  - "networking"
  - "news"
  - "office"
  - "openartist"
  - "server"
  - "spideroak"
  - "wireless"
  - "world"
cover: covers/magazines/issue-57.webp
pdf: https://dl.fullcirclemagazine.org/issue57_en.pdf
epub: https://dl.fullcirclemagazine.org/issue57_en.epub
---

**This month**

- Command and Conquer.
- How-To : Try Enlightenment, LibreOffice - Part 11, Backup Strategy - Part 5, Encrypted USB Stick, and Varnish Web Cache.
- Linux Lab - Mana World Server.
- Review - OpenArtist: 5th Incarnation
- I Think - SpiderOak Questionnaire.
- Closing Windows - Wireless Networking

Plus: more Ubuntu Games, My Desktop (and another extra!), My Opinion, My Story, and much much more!

**Google Currents** edition: [http://www.google.com/producer/editions/CAowvZtX/full_circle_magazine_57_lite](http://www.google.com/producer/editions/CAowvZtX/full_circle_magazine_57_lite)

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue57_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue57_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue57_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue57_it.pdf)
