---
title: "Full Circle Magazine 185"
date: 2022-09-30
draft: false
tags:
  - "python"
  - "blender"
  - "32-bit"
  - "inkscape"
  - "dia"
  - "ubuntu unity"
  - "ubuntu"
  - "unity"
  - "22.04"
  - "kaos"
  - "stoneshard"
  - "waddle"
cover: "covers/magazines/issue-185.webp"
pdf: "https://dl.fullcirclemagazine.org/issue185_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue185_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Blender and New Life to 32-bit PCs
- Graphics : Inkscape
- Everyday Ubuntu : Dia Pt.2
- Review : Ubuntu Unity 22.04
- Review : KaOS
- Ubuntu Games : Stoneshard
  plus: News, My Opinion, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue185_fr.pdf)
