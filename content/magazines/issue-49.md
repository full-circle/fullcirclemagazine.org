---
title: "Full Circle Magazine 49"
date: 2011-05-27
draft: false
tags:
  - "dev"
  - "development"
  - "google"
  - "libre"
  - "libreoffice"
  - "machine"
  - "management"
  - "office"
  - "python"
  - "swapiness"
  - "thunderbird"
  - "tools"
  - "virtual"
  - "vm"
  - "web"
cover: covers/magazines/issue-49.webp
pdf: https://dl.fullcirclemagazine.org/issue49_en.pdf
epub: https://dl.fullcirclemagazine.org/issue49_en.epub
---

**This month**

- Command and Conquer.
- How-To : Program in Python - Part 23, LibreOffice - Part 4, Ubuntu Development - Part 1, and Use Google In Thunderbird.
- Linux Lab - Swappiness Part Two.
- Review - Virtual Machines.
- Top 5 - Web Management Tools.
- [**NEW!**] **I Think** - we pose a question and you, the readers, give your thoughts. This month: _what do you think of Unity?_

Plus: Ubuntu Games, My Story, and much much more!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue49_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue49_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue49_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue49_it.pdf)
