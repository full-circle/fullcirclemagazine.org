---
title: "Full Circle Magazine 165"
date: 2021-01-29
draft: false
tags:
  - "baking"
  - "cookery"
  - "cooking"
  - "games"
  - "inkscape"
  - "lmms"
  - "music"
  - "podcast"
  - "python"
  - "redhat"
  - "unity"
  - "ventoy"
  - "xojo"
cover: covers/magazines/issue-165.webp
pdf: https://dl.fullcirclemagazine.org/issue165_en.pdf
epub: https://dl.fullcirclemagazine.org/issue165_en.epub
---

**This month**

- Command & Conquer : LMMS
- How-To : Python, Podcast Production, and Ventoy
- Graphics : Inkscape
- Linux Loopback
- Everyday Ubuntu : Cooking Pt.2
- Review : Ubuntu Unity 20.10 and Xojo
- Ubuntu Games : Waiting For The Raven

Plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue165_fr.pdf)
