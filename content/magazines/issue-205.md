---
title: "Full Circle Magazine 205"
date: 2024-05-31
draft: false
tags:
  - "ubuntu"
  - "python"
  - "stable"
  - "diffusion"
  - "latex"
  - "inkscape"
  - "24.10"
  - "dsl"
  - "damn small linux"
  - "cinnamon"
  - "mate"
  - "meshtastic"
  - "barony"
  - "ubports"
cover: "covers/magazines/issue-205.webp"
pdf: "https://dl.fullcirclemagazine.org/issue205_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue205_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion and Latex
- Graphics : Inkscape
- Review : Ubuntu 24.04, Damn Small Linux, Ubuntu Cinnamon 23.10 and Ubuntu MATE 23.10
- UbPorts Touch
- My Opinion : Meshtastic
- Ubuntu Games - Barony

plus: News, Micro This, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue205\_fr.pdf)

