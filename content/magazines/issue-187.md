---
title: "Full Circle Magazine 187"
date: 2022-11-25
draft: false
tags:
  - "python"
  - "latex"
  - "inkscape"
  - "ubuntu"
  - "22.10"
  - "vanillaos"
  - "vanilla"
  - "ventoy"
  - "pixel wheels"
  - "waddle"
cover: "covers/magazines/issue-187.webp"
pdf: "https://dl.fullcirclemagazine.org/issue187_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue187_en.epub"
---

This month:

- How-To : Python, --Blender-- and Latex
- Graphics : Inkscape
- Review : Ubuntu 22.10
- Review : VanillaOS
- Review : Ventoy
- Ubuntu Games : Pixel Wheels
  plus: News, My Story, The Daily Waddle, Q&A, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue187_fr.pdf)
