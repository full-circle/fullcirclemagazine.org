---
title: "Full Circle Magazine 0"
date: 2007-04-30
draft: false
cover: covers/magazines/issue-0.webp
pdf: https://dl.fullcirclemagazine.org/issue_en.pdf
epub: https://dl.fullcirclemagazine.org/issue_en.epub
---

**Download Issue 0 here**. This is a **preview** edition. Released April 15, 2007.
**Contents**

- History of Ubuntu
- Feisty Fawn preview and features

**Other languages**

- [Dutch PDF](https://dl.fullcirclemagazine.org/issue_nl.pdf)
- [French PDF](https://dl.fullcirclemagazine.org/issue_fr.pdf)
- [Galician PDF](https://dl.fullcirclemagazine.org/issue_gl.pdf)
- [German PDF](https://dl.fullcirclemagazine.org/issue_de.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue_hu.pdf)
- [Indonesian PDF](https://dl.fullcirclemagazine.org/issue_id.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue_it.pdf)
- [Romanian PDF](https://dl.fullcirclemagazine.org/issue_ro.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue_ru.pdf)
- [Spanish PDF](https://dl.fullcirclemagazine.org/issue_es.pdf)
