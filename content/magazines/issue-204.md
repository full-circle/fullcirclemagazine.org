---
title: "Full Circle Magazine 204"
date: 2024-04-26
draft: false
tags:
  - "python"
  - "stable"
  - "diffusion"
  - "latex"
  - "inkscape"
  - "budgie"
  - "wubuntu"
  - "ubports"
  - "touch"
  - "balena"
  - "etcher"
  - "shattered pixel dungeon"
  - "pixel"
  - "dungeon"
  - "game"
cover: "covers/magazines/issue-204.webp"
pdf: "https://dl.fullcirclemagazine.org/issue204_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue204_en.epub"
---

This month:

- Command & Conquer
- How-To : Python, Stable Diffusion and Latex
- Graphics : Inkscape
- Review : Ubuntu Budgie 23.10 and Wubuntu
- UbPorts Touch
- My Story : balena Etcher
- Ubuntu Games - Shattered Pixel Dungeon

plus: News, Micro This, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue204\_fr.pdf)
