---
title: "Full Circle Magazine 83"
date: 2014-03-28
draft: false
tags:
  - "backup"
  - "bitcoin"
  - "crypto"
  - "design"
  - "dogecoin"
  - "games"
  - "gimp"
  - "gmic"
  - "help"
  - "homebank"
  - "inkscape"
  - "kde"
  - "libre"
  - "litecoin"
  - "multisystem"
  - "office"
  - "open"
  - "source"
  - "Ubuntu"
cover: covers/magazines/issue-83.webp
pdf: https://dl.fullcirclemagazine.org/issue83_en.pdf
epub: https://dl.fullcirclemagazine.org/issue83_en.epub
---

**This month**

- Command & Conquer
- How-To : Backup In Ubuntu, LibreOffice, and MultiSystem.
- Graphics : GIMP G'MIC and Inkscape.
- Review: HomeBank
- Security Q&A
- What Is: CryptoCurrency
- [**NEW!**] Open Source Design

Plus: Q&A, Linux Labs, Ask The New Guy, Ubuntu Games, and a competition!

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue83_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue83_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue83_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue83_it.pdf)
