---
title: "Full Circle Magazine 175"
date: 2021-11-26
draft: false
tags:
  - "python"
  - "latex"
  - "webdav"
  - "inkscape"
  - "kubuntu"
  - "bluemail"
  - "ubuntu touch"
  - "nowhere prophet"

cover: "covers/magazines/issue-175.webp"
pdf: "https://dl.fullcirclemagazine.org/issue175_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue175_en.epub"
---

**This month:**

- Command & Conquer : Terminal
- How-To : Python, Latex and WebDAV Server
- Graphics : Inkscape
- Everyday Ubuntu
- Micro This Micro That
- Review : Kubuntu 21.10
- Review : Bluemail
- Ubports Touch : OTA-20
- Ubuntu Games : Nowhere Prophet

plus: News, The Daily Waddle, Q&A, and more.

**Other languages :**

- [French PDF](https://dl.fullcirclemagazine.org/issue175_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue175_hu.pdf)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue175_it.pdf)
- [Russian PDF](https://dl.fullcirclemagazine.org/issue175_ru.pdf)
