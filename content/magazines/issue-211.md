---
title: "Full Circle Magazine 211"
date: 2024-11-29
draft: false
tags:
  - "kubuntu"
  - "lxd"
  - "latex"
  - "inkscape"
  - "beaver"
  - "ubports"
  - "broken"
  - "alliance"
cover: "covers/magazines/issue-211.webp"
pdf: "https://dl.fullcirclemagazine.org/issue211_en.pdf"
epub: "https://dl.fullcirclemagazine.org/issue211_en.epub"
---

This month:

- Command & Conquer
- How-To : LXD and Latex
- Graphics : Inkscape
- Review : Kubuntu 24.10 and Beaver Notes
- UbPorts Touch
- Ubuntu Games - Broken Alliance

plus: News, My Opinion, Q&A, The Daily Waddle, and more.

**Other Languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue211_fr.pdf)
