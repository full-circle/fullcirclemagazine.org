---
title: "Full Circle Magazine 79"
date: 2013-11-29
draft: false
tags:
  - "blender"
  - "boinc"
  - "freemind"
  - "inkscape"
  - "libre"
  - "libreoffice"
  - "mapping"
  - "mind"
  - "office"
  - "python"
cover: covers/magazines/issue-79.webp
pdf: https://dl.fullcirclemagazine.org/issue79_en.pdf
epub: https://dl.fullcirclemagazine.org/issue79_en.epub
---

**This month**

- Command & Conquer
- How-To : Python, LibreOffice, and Use BOINC.
- Graphics : Blender, and Inkscape.
- Book Review: Mind Mapping With Freemind

Plus: Q&A, Linux Labs, Ask The New Guy, My Story, and soooo much more!

I have to apologise for the state of this month's issue. **Please, don't email me about blank pages/columns or spelling mistakes I can't fix any of it.**
If you want to read the full story of what happened, read on. Otherwise, scroll down to grab the PDF/EPUB editions.

I'm writing this to explain why the upcoming issue of Full Circle (FCM#79) will be a bit less polished than the 78 before it, and also to be somewhat theraputic.
It all began on Saturday afternoon when I was browsing through the list of recently released distros on Distrowatch.
I spotted one called FreeNAS and had the brainwave that I could install that on my old ASUS Revo setting it up as a NAS drive for backups.
Genius! Of course, while setting it up I'd document the whole thing for an FCM article.
Trying to get FreeNAS to boot was a struggle. After much reading it turns out that the ISO file is only for burning to CD/DVD.
Grabbing the IMG file I try and copy that to my USB stick. Nothing.
Turns out you need to use the dd command to put the IMG file onto the USB as it doesn't really install to a drive it boots from the USB to make the device it's in a NAS drive.
OK so, I run the dd command and it still hasn't showed up on my USB stick. Weird! Wait, so where did it install FreeNAS?
It installed it on my 1TB drive wiping everything. All the partitions. The lot. Gone. My Euro Truck Simulator 2 profile. Gone! NOOOOOOooooooo!!!!!
Initially I thought it had wiped my OS which wouldn't have been a problem as I have (or had!) all my stuff on my /home partition. No. It took over the whole drive erasing all my partitions and files.
After I let that sink in and stopped sobbing, it was time to get up and running again. Did I have a
Ubuntu DVD sitting ready to load? Nope. I rifled through some magazines and found a DVD with Ubuntu 13.04 on it and booted from there.
I downloaded Kubuntu 13.10 (64bit) and used the Ubuntu startup creator tool to put Kubuntu on my USB. Reboot. Nothing. Ubuntu (for reasons unknown) hadn't copied Kubuntu to the USB properly for it to boot.
And of course, since I'm out of the live Ubuntu the Kubuntu ISO is gone.
I'm now thinking I might have to install Ubuntu 13.04 and upgrade to 13.10, but I really didn't want Unity. I wanted my beloved KDE back.
Now I try Mint. Mint 15 was on an old magazine DVD too. Booted it and it looked OK.
So, after 30 minutes or so I'm booting into Mint 15. Finally. Progress! But, no. Mint 15 isn't recognising my dual monitors, nor is it letting me change resolutions.
When I click the System Settings (in the Mint menu) it seems to be bringing up an invisible window that I can't see, but can resize.
More sobbing.
I use this time of self pity to download Mint 16 RC and burn it to a DVD. I boot from that and Mint 16 seems fine. I can change resolutions and can rearrange my monitor setup. Finally!
Here I sit in Mint 16 RC with absolutely no files what so ever. I've now installed SpiderOak and can at least download my photos that I'd backed up to there. That's something I suppose.
So. in short: **KEEP A REGULAR BACKUP!** Don't rely on your /home or some mystical force to save your files.
Needless to say, I never did write the FreeNAS article.
Anyway... that's MY My Story for this month. :/

**Other languages**

- [French PDF](https://dl.fullcirclemagazine.org/issue79_fr.pdf)
- [Hungarian PDF](https://dl.fullcirclemagazine.org/issue79_hu.pdf)
- [Italian EPUB](https://dl.fullcirclemagazine.org/issue79_it.epub)
- [Italian PDF](https://dl.fullcirclemagazine.org/issue79_it.pdf)
