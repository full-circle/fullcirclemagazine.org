---
title: "New Site"
date: 2023-01-21
draft: false
tags:
  - "website"
cover: "covers/news/fifteen.webp"
---

It's only taken fifteen years, but we've got a new site! <b>BIG HUUUGE</b> thanks to J_Arun_Mani for making the site and fixing it every time I've broke it (ie: lots!).
